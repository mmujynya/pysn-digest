# PySN-lib ˈpaɪ-sən/ (Python for SN)

A library of tools to enhance Supernote workflows. 
The script runs on a computer (PC, Mac, Linux) and comunicates to the tablets via: 
- USB
- WI-FI
- or the mirrored folders of a cloud provider chosen for synchronization.

## Features
Original features listed on first 2 minutes of this video: [Python for Supernote - installation & features overview](https://youtu.be/fKnpdr5G1qU).
A fast step-by-step installation process is described on the following 5 minutes of the same video.

For more recent features, please look at [PySN's Supernote playlist on YouTube](https://www.youtube.com/playlist?list=PLO0R0numVXGXooMYGTEuUKE2GsAKEc127).

:warning: **IMPORTANT: Does not work with Python 3.13 or later!!!**
Please use [Python version 3.12.3](https://www.python.org/downloads/release/python-3123/)

# Updating from a previous installation: 
**If you want to keep the same folders, I recommend the following steps**

1. Backup of your entire SN_vault folder by renaming it to something like "SN_vault_old". It not only has your custom settings, but possibly your backups.
2. Open a terminal in your pysn_digest foldert and type "git pull"
3. Type "python setup_venv.py"
4. I included a demo folder "demo 1.32" that you may want to push to your SN device
5. Edit user_settings.json to select specific folders of the demo or restore your previous paths (from the user_settings.json backed up on point 1)
6. Open a ticket issue if you have difficulties: https://gitlab.com/mmujynya/pysn-digest/-/issues

# Demo / Test files
**TEST files are included in folder demo1.32 (available in SN_vault). You may want to copy this folder on the root of your "Notes" folder, on your Supernote and change accordingly the ine "input" parameter in user_settings.json.**

Please see Demo related information in the first 3 chapters of the last [YouTube video](https://youtu.be/wJ2Sb112kG8)

# Change log
PySN-lib is an open-source Python script library designed to improve and or customize workflows involving Supernote devices.
- **NEW (v 1.33): Bug fixes and compatibility with Chauvet 3.22.31**
- **NEW (v 1.32): Manta compatibility, importing to Supernote (except OCR)**
- **NEW (v 1.27): Basic Word docx to notes (via markdown), multiple font sets, font tracer templates**
- **NEW (v 1.23 Beta branch only): Basic markdown-to-notes**
- **NEW (v 1.20): Styling page number for text from pdf converted to pen strokes. Contains an updated myfonts.note**
- Fixed (hopefully) a bug preventing A5x users to use USB transfer because internal prefix wasn't detected
- **NEW (v 1.17): Text-to-pen strokes (or reverse text recognition tool) allowing text from pdf or docx to be converted to pen strokes**
- ** (v 1.08): Altering pen strokes through keyword settings (see included strokes-example1.json)**
- ** (v 1.06): Markdown / Text file of highlighted text on Pdf files with OCR**
- ** (v 1.04): Integration with Supernote Partner App**

- Full backup with earlier versions saved on archive
- Bulk export to pdf maintaining external links between pdf converted notebooks
- Dictionaries to correct misidentified words by the recognition engine
- Inclusion of text regonized in pdf allowing visual handwritten notes searches in pdf
- Pdf Table of Contents based on headers (Text bookmarks)
- Pdf Table of Contents based on headers (Graphic links)
- Pdf Table of Contents for Keywords
- Pdf Table of Contents for Stars
- Markdown file from recognized text
- Html file from recognized text
- Keyword settings for customized settings at a notebook level
   - Custom dictionary loading
   - Extra copies to (remote) folder allowing customized sharing
   - Coloring styles
   - Embedded styles allowing conversion html to docx
- Enhanced digest for pdf annotations, including for Pdf without OCR
- Optional handwritings text recognition for annotations with Microsoft Computer Vision
- Supernote original path, Author, Keywords and links included in pdf Metadata, allowing computer indexing


## NOTICE & Credit: 

This code uses and alters [Github supernote-tool](https://github.com/jya-dev/supernote-tool/tree/master)
See changes at: [Supernotelib commits](https://gitlab.com/mmujynya/pysn-digest/-/commit/c8b9ca72c71293a666176405e1bc1fc21e90e0ba)

## Downloading the community contributors bug-fixes version

Occasionally, selected PySN contributors will merges changes addressing issues that they encountered. From time to time, PySN maintainers will merge their changes to the main branch. Please regfer to open issues at: [PySN Issues](https://gitlab.com/mmujynya/pysn-digest/-/issues) and see if a fix has been submitted by a contributor.
You can switch to the "bug-fixes" branch by selecting the it in the branch dropdown menu or by following steps 1-2 below.

Please note that there is no guarantee that a fix won't create other issue(s). A fix may also have an impact on the code's stability. Use it at your own risk.

1. Open your terminal.
2. Clone the repository:
   ```bash
   git clone https://gitlab.com/mmujynya/pysn-digest.git
   cd pysn-digest
   git checkout bug-fixes
   ```


## Downloading the beta (developer) version

The beta (developer) version of this project contains new functionalities and bug fixes and is considered to be in test.
As such, it is more volatile to changes, without notice. If you want to try it out, follow these steps:

1. Open your terminal.
2. Clone the repository:
   ```bash
   git clone https://gitlab.com/mmujynya/pysn-digest.git
   cd pysn-digest
   git checkout beta
   ```
See **In development** at the end of this ReadMe for the list of current developments.

## WINDOWS

### Prerequisites

- Python 3.x (3.12 recommended)
- `pip` (Python package installer)
- If using **git clone**, you need to have git installed. Head to [git download](https://git-scm.com/downloads)
- If using USB: Please ensure that SN is connected to USB port AND that Sideloading is allowed"
   - Supernote settings: Security & Privacy -> Sideloading -> ON


#### Checking if Python and pip are Already Installed

1. **Open Command Prompt (Windows) or Terminal (Mac/Linux)**
   
   Press `Win + R`, type `cmd`, and press `Enter`.

2. **Check Python Version**

   - Type `python --version` (Windows) and press `Enter`.
   - If you see a version number that starts with 3 (e.g., `Python 3.12.3`), then Python 3.x is already installed.

3. **Check pip Version**
   - Type `pip --version` (Windows) or `pip3 --version` (macOS & Linux) and press `Enter`.
   - If you see a version number (e.g., `pip 24`), then pip is already installed.
   - **Updating pip**: Even if pip is already installed, it’s a good idea to make sure it’s up-to-date. Run the following command:

        ```sh
        python -m pip install --upgrade pip
        ```
If Python 3.x or pip is not installed, follow the steps below to install them.


#### Installing Python 3.x and pip

1. **Download Python Installer**
   - Go to the official [Python website](https://www.python.org/downloads/).
   - Click the "Download Python 3.x.x" button (the version number might be different).
   - Important: check the "Add python.exe to PATH" checkbox

2. **Run the Installer**
   - Open the downloaded file to start the installation.
   - Check the box that says "Add Python 3.x to PATH".
   - Click "Install Now" and follow the on-screen instructions.

3. **Verify Installation**
   - Open Command Prompt and type `python --version` and `pip --version` to verify that both Python and pip are installed correctly.


### Setting Up the Virtual Environment

1. Open command prompt
2. Navigate to the project directory.
3. Run the setup script:
   - First: 
    ```sh
    pip install pywin32
    ```
   - Then:
    ```sh
    python setup_venv.py
    ```

4. Follow the instructions provided by the script to activate the virtual environment. The script will output the command you need to run to activate the environment.

    - Normally:

        ```sh
        pysnvenv\Scripts\activate
        ```
    - If you have bash for Windows:   

        ```sh
        source pysnvenv/Scripts/activate
        ```        

### Usage

**<span style="color: red">IMPORTANT</span>**: Prior to using digest.py for the first time, you should either define your **input folders**, the **output folder**, and the **export folder** or  ensure that the SN has the default settings for these directories. 

#### First time use (or changing settings in the future)

- ***<span style="color: red">Activating the virtual environment is required prior to all command line uses</span>*** `pysnvenv\Scripts\activate` (see above if using bash for Windows)

- ***Set the input folders*** (one or several folders containing the annotated pdfs, separated by space) by typing (*)

    ```sh
        python digest.py --input Document/pysn-input/ SDcard/pysn-input
    ```

- ***Set the output folder*** (the unique folder containing the processed annotated files pdfs) by typing (*)

    ```sh
        python digest.py --output Document/pysn-output
    ```

- ***Set the export folder*** (the unique folder containing apdf of a merged original pdf + .mark) by typing (*)

    ```sh
        python digest.py --export EXPORT/pysn-export
    ```

(*) Replace 'Document/pysn-input/', 'SDcard/pysn-input/', 'Document/pysn-output' and 'EXPORT/pysn-export' by existing folders on your Supernote. Type `python digest.py --help` for CLI help.

Alternatively, you could also use the CLI help (first activate the virtual environment, then type  `python3 digest.py --help`), read the current values listed in brackets [] at the end of --input, --output and --export and create these folders, iff applicable, on your SN.

#### Routine use

- Using a shortcut. It should automatically activate the virtual environment and lauch the script

- Using a task scheduler (MS Windows task scheduler). When set properly, it should run within the virtual environment (*come back later for step by step instructions*)

- Using the CLI (Command Line Interface), ***after*** activating the virtual environment
    ```sh
        python digest.py
    ```



### Settings

The most used global variables can be updated in the CLI (Command Line Interface).

- To display the available settings through the CLI and their meaning, type

    ```sh
        python digest.py --help
    ```

There are many other global variable (in UPPERCASE) in the script, available for more customization.
All global variables are listed at the beginning of the script. See information and warning in the associated comments.



## MACOS & LINUX


### Prerequisites

- Python 3.x (3.12 recommended)
- `pip3` (Python package installer)

### Checking if Python and pip are Already Installed

1. **Open Terminal**
   - **Mac**: Press `Command + Space`, type `Terminal`, and press `Enter`.
   - **Linux**: Open your Terminal application from the application menu.

2. **Check Python Version**
   - Type `python3 --version` and press `Enter`.
   - If you see a version number that starts with 3 (e.g., `Python 3.12.3`), then Python 3.x is already installed.
   *Old macOS have Python 2.x have installed; that version is not compatible with PySN and you need to install Python 3.x*

3. **Check pip Version**
   - Type `pip3 --version`and press `Enter`.
   - If you see a version number (e.g., `pip 24`), then pip is already installed.

If Python 3.x or pip is not installed, follow the steps below to install them.

### Installing Python 3.x and pip 3.x

1. **Download Python Installer**
   - Go to the official [Python website](https://www.python.org/downloads/).
   - Click the "Download Python 3.x.x" button (the version number might be different).

2. **Run the Installer**
   - Open the downloaded file to start the installation.
   - Check the box that says "Add Python 3.x to PATH".
   - Click "Install Now" and follow the on-screen instructions.

3. **Verify Installation**
   - Open Command Prompt and type `python --version` and `pip --version` to verify that both Python and pip are installed correctly.

#### For Mac Users

- **Using Homebrew (Recommended)**
   - If Python 3.x is not installed, install Homebrew first (if not already installed):
     ```sh
     /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
     ```
   - After installing Homebrew, run:
     ```sh
     brew install python
     ```
   - This will install the latest version of Python 3 and pip.

- **Verify Installation**
   - Open Terminal and type `python3 --version` and `pip3 --version` to verify the installation.

#### For Linux Users

- **Using Package Manager**
   - Open Terminal and update your package list:
     ```sh
     sudo apt update
     ```
   - Install Python 3 and pip:
     ```sh
     sudo apt install python3 python3-pip
     ```

- **Verify Installation**
   - Open Terminal and type `python3 --version` and `pip3 --version` to verify the installation.

### Additional Tips

- **Updating pip**: Even if pip is already installed, it’s a good idea to make sure it’s up-to-date. Run the following command:
  ```sh
  python -m pip install --upgrade pip
  ```


### Setting Up the Virtual Environment

1. Open a terminal
2. Navigate to the project directory.
3. Run the setup script:

    ```sh
    python3 setup_venv.py
    ```    

4. Follow the instructions provided by the script to activate the virtual environment. The script will output the command you need to run to activate the environment.

    ```sh
    source pysnvenv/bin/activate
    ```
  

### Usage

**<span style="color: red">IMPORTANT</span>**: Prior to using digest.py for the first time, you should either define your **input folders**, the **output folder**, and the **export folder** or  ensure that the SN has the default settings for these directories. 

#### First time use (or changing settings in the future)

- ***<span style="color: red">Activating the virtual environment is required prior to all command line uses</span>*** (`source pysnvenv/bin/activate`)

- ***Set the input folders*** (one or several folders containing the annotated pdfs, separated by space) by typing (*)

    ```sh
        python3 digest.py --input Document/pysn-input/ SDcard/pysn-input
    ```

- ***Set the output folder*** (the unique folder containing the processed annotated files pdfs) by typing (*)

    ```sh
        python3 digest.py --output Document/pysn-output
    ```

- ***Set the export folder*** (the unique folder containing apdf of a merged original pdf + .mark) by typing (*)

    ```sh
        python3 digest.py --export EXPORT/pysn-export
    ```

(*) Replace 'Document/pysn-input/', 'SDcard/pysn-input/', 'Document/pysn-output' and 'EXPORT/pysn-export' by existing folders on your Supernote. Type `python3 digest.py --help` for CLI help.

Alternatively, you could also use the CLI help (first activate the virtual environment, then type  `python3 digest.py --help`), read the current values listed in brackets [] at the end of --input, --output and --export and create these folders, if applicable, on your SN.

#### Routine use

- Using a shortcut (it should automatically activate the virtual environment and launch the script). The installation script creates: 

    - for Windows: ***'PySN.lnk'***
    - for Linux:  ***'Run Python Script'***
    - for macOS: ***'PySN.command'***

- Using a task scheduler (MS Windows task scheduler, Mac Launchd or Linux cron). When set properly, it should run within the virtual environment

- Using the CLI (Command Line Interface), ***after*** activating the virtual environment

    ```sh
        python3 digest.py
    ```

### Settings

The most used global variables can be updated in the CLI (Command Line Interface).

- To display the available settings through the CLI and their meaning, type

    ```sh
        python3 digest.py --help
    ```

There are many other global variable (in UPPERCASE) in the script, available for more customization.
All global variables are listed at the beginning of the script. See information and warning in the associated comments.



## Test files and more information

YouTube video links and details steps on test files will be posted, come back later!

## YouTube videos

See dedicated playlist on YouTube: [PySN playlist](https://www.youtube.com/playlist?list=PLO0R0numVXGXooMYGTEuUKE2GsAKEc127)



## In development

- GUI settings for Windows, macOS and Linux

