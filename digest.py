import sys
import base64
import os
import io
import shutil
import zipfile
import fitz
import json
import hashlib
from datetime import datetime, timezone, timedelta
from PIL import Image, ImageColor,  ImageDraw, ImageFilter
import numpy as np
from io import BytesIO
import supernotelib as sn
from supernotelib.converter import ImageConverter, PdfConverter
from supernotelib.converter import VisibilityOverlay
import functools
import uuid
from unidecode import unidecode
import docx
from collections import defaultdict
from typing import List, Dict, Tuple  # Set , Optional
import struct

import xml.etree.ElementTree as ET
import cv2

from skimage.measure import label
from scipy.spatial.distance import cdist
import random

import copy
import math
from shapely.geometry import LineString, MultiLineString
import potrace

import ctypes


import scipy.ndimage as ndimage

from bs4 import BeautifulSoup
import cssutils
from urllib.parse import urljoin, unquote
import aiohttp
import asyncio
import aiofiles
import re
import platform
from ppadb.client import Client as AdbClient
import subprocess
import psutil
import mimetypes
import time
import requests
import argparse
from colour import Color
from dataclasses import dataclass


# --------------------------------------------------------------------------------------
# Max Mujynya, May 27 2024
# ---------------------------------------------------------------------------------------

# Some preps to hide the terminal poping up, for tasks
# launched by the Windows task scheduler

PYSN_VERSION = 'PySN 1.33'
# -------------------------------------------------------------
#  (*) Means "Don't change, unless you know what you are doing"
#  (!) Means you SHOULD set this value for your machine/environment
# -------------------------------------------------------------

SUPER_HACK_MODE = True             # (!) Alters the source pdf and 'binds' it with the .mark file TODO: Probably not useful anymore, since this is the default
AUTO_EXPORT = True                 # (!) Creates a ready to export pdf (without marking)
AUTO_CONVERT_NOTES = True          # (!) Convert to PDF and export any .note file in SN_IO_WATCHED_FOLDERS
USE_MS_VISION = False               # Optional handwritten text recognition us MS vision. Requires external account setup.

PRIORITY_NOTES_THRESHOLD = 4
PRIORITY_DIGEST_THRESHOLD = 2
NOTEBOOK_SERIES = {}
NOTEBOOK_DEVICE = 'N5'
ENABLE_LOW_PRIORITY = platform.system() == 'Windows'

# --------------------------------------------------------------------------------------------------
# IMAGE PROCESSING SETTINGS
#
HIGHLIGHTS_COLOR = {
    'light gray': (201, 201, 201),
    'dark gray': (157, 157, 157),
    'white': (254, 254, 254),
    'black': (0, 0, 0)}               # (*) Available color choices for 'Digest' done from highlights
NEEDLE_POINT_SIZES = {
    "0.4": 500,
    "0.1": 200,
    "0.2": 300,
    "0.3": 400,
    "0.5": 700,
    "0.6": 800,
    "0.7": 1000,
    "0.8": 1100,
    "0.9": 1200,
    "1": 1300,
    "2": 2400
}

DIGEST_COLOR = 'light gray'         # (*) Color used to 'select' items
HIGHLIGHT_COLOR = 'dark gray'       # (*) Color used to 'highlight' items. Used on scans where highlighting not possible by SN
ERASE_COLOR = 'white'               # (*) Color used to 'erase' items. Useful when editing a notebook that has already been exported to pdf
NOTES_COLOR = 'black'               # (*) Color used for handwritten notes
TRANSPARENCY_LVL = 5
TRANSPARENCY_OTHER = 98
PROXIMITY_THRESHOLD = 35
PROXIMITY_NOTES = 35                # (*) Pixels proximity distance between handwritten notes. Used to merge adjacent words when selecting notes within digested are
RESOLUTION_FACTOR = 3               # Increasing this increases the quality of the crops, but also the size of the digested pdf

EXPAND_SHRINK_PXL = 6               # (*) This is the pixel amount used when expanding/contracting rect zones. Should be just a bit higher than space between letters but lower than space between words
DELTA_PXL_SEARCH = 8                # (*) Distance in pixels used when trying to define a rect snapping on pdf text
MAX_HORIZONTAL_PIXELS_N6 = 1404        # Default pixels size on A6x2. You may have to change that for other devices
MAX_VERTICAL_PIXELS_N6 = 1872          # Default pixels size on A6x2. You may have to change that for other devices
MAX_HORIZONTAL_PIXELS_N5 = 1920
MAX_VERTICAL_PIXELS_N5 = 2560
VERTICAL_GAP = 0                    # The gap we introduce when looking to the original text behind a rect
PROXIMITY_RECT_MERGE = 5            # The gap considered to merge two rects, in pixels

COLORCODE_BACKGROUND = 0x62         # (*)
COLORCODE_WHITE = 0x65              # (*)
SPECIAL_LENGTH = 0x4000             # (*)
SPECIAL_LENGTH_FOR_BLANK = 0x400    # (*)
SPECIAL_LENGTH_MARKER = 0xff        # (*)
LOAD_NON_KS_DICTIONARIES = True     # (*) Loads existing dictionaries for existing keywords that have a corresponding dictionary, even if not starting with ks-dictionary
COLOR_PALETTE = ["#000000", "#9d9d9d", "#c9c9c9", "#fefefe"]
COLOR_PALETTE_NAMES = ['black', 'dark gray', 'light gray', 'white']
DIMMED_MENU = 0.65

# --------------------------------------------------------------------------------------------------
# EXPORT SIZE SETTINGS
#
PDF_LETTER_WIDTH = 612              # US Letter format
PDF_LETTER_HEIGHT = 792             # US Letter format
SUMMARY_REPORT_MARGIN = 50
NOTES_SHRINK_RATIO = 1.0            # Srinking ratio applied to handwritten notes
VERTICAL_REPORT_SPACING = 25
EMBED_SUMMARY = True                # (*) Untested for False, that should create 2 files: a 'digested' pdf and a separate 'digest summary'
SHOW_REC_NOTES = False              # TODO: Test if True. Should be True and at least on another layer when EMBED_SUMMARY is False
AUTHOR = "SN User"                  # (!) Replace with your name or initials. Used for comments.
DIGEST_LINE_HEIGHT = 30             # The guided lines for the digest summary section (set to <=0 if you don't want any)

TITLE_STYLES = ["1000254", "1157254", "1201000", "1000000"]     # Supernote titles styles, sorted. DO NOT CHANGE
#  --------------------------------------------------------------------------------------------------
# FILES AND FOLDERS LOCATION - INCLUDING CACHE CONTAINERS
#
PYSN_DIRECTORY = os.path.dirname(
    os.path.abspath(__file__))                          # (*) Folder where this script is running
if os.name == 'nt':
    SHORTCUT_NAME = 'PySN.lnk'
elif sys.platform == 'darwin':
    SHORTCUT_NAME = 'PySN.app'
elif sys.platform.startswith('linux'):
    SHORTCUT_NAME = 'PySN.desktop'
SHORTCUT_PATH = os.path.join(
    os.path.dirname(PYSN_DIRECTORY), SHORTCUT_NAME)         # (!) Path to shortcut, assumed to be by default in script parent's directory
SN_VAULT = os.path.join(PYSN_DIRECTORY, 'SN_vault')     # (*) The vault is the working directory folder

PDF_CACHED = os.path.join(SN_VAULT, 'cache')             # Cached folder of original PDF files. Can improve execution time for large files on slow com
if not os.path.exists(PDF_CACHED):
    os.makedirs(PDF_CACHED)

SN_REGISTRY_FN = 'registry.json'                        # (*) Name of the double dictionary, used to sore file hash and detect changes
USER_SETTINGS_FN = 'user_settings.json'                 # (*) Name of last user's settings dictionary. Will be reused unless deleted
PIX_DICT_FN = os.path.join(SN_VAULT, 'pix_dict.json')   # (*) Name of the dictionary used to save API calls for text recognition in previously submitted files

DEBUG_MODE = False                                       # Leaving to True will not delete temporary files.

# ---------------------------------------------------------------------------------------------------
# IMPORTING SCANNED OR PDF HANDWRITING TO NOTEBOOK
#
OFFSET_T_X = 16    # 16 vs 1920 31.76
OFFSET_T_Y = 32   # 32 vs 2560 43
OFFSET_V_Y = 0   # 1000
LINE_HEIGHT_THRESHOLD = 20
OCR_REJECTION_THRESHOLD = 0.10
RECOGNIZE_TEXT = USE_MS_VISION
DENSE_MIN_AREA = 30000
DENSE_SENSITIVITY = 0.2
PRE_DENOISE = None
DENSE_THRESHOLD = 200
MINIMUM_PATH_LENGTH = 4
PRESERVE_ASPECT_RATIO = True
SCREEN_CENTER = True
ADAPTATIVE_THRESHOLDING = True
CONTOUR_DILATION = 5
CONTOUR_RESOLUTION = 2
DEFAULT_PEN = 10  # 10 is Needle-Point
DEFAULT_PEN_COLOR = "black"
DEFAULT_PEN_SIZE = "0.3"
DEFAULT_PRESSURE = 2000
ZOOM_X = 3
ZOOM_Y = 3
MIN_DIMENSION = 2300
DETAIL_LEVEL = 1.5
NOISE_REDUCTION = 0.5
SHADOW_BLUR = 1.0
GAUSSIAN_BLUR = 21
INTENSITY = 1.5
THINNING = 0
UNSTITCH = False
MAX_ZOOM = 12
REMOVE_TEMPLATES = True
LINE_DETECT_MIN_REP = 20
LINE_DETECT_MAX_REP = 200
TEMPLATES_LIST = [
    '5mm engineering.pdf', '9mm ruled lines.pdf', '10mm grid.pdf', '5mm dots.pdf',
    'Remarkable college ruled 1.pdf', 'Remarkable college ruled 2.pdf']  # 'Remarkable grid 1.pdf',
TO_NOTES_FILTER = 'import2sn'
FILE_RECOGN_LANGUAGE = 'en_US'
# ---------------------------------------------------------------------------------------------------
# CREATION OF BINARIES AND TEXT-TO-PEN STROKES
#
SHAPES_COLOR = 'dark gray'                              # (*) Color filter for shapes detection
CONVERTED_SHAPES_COLOR = 'black'                          # (*) Color of replaced shapes
MIN_SHAPE_AREA = 5000                                   # (*) Minimum size threshold
BLANK_TEMPLATE_PICTURE = os.path.join(
    SN_VAULT, 'blanknomad.png')                              # (*) Template blank filename (ckear layer)
REFRESH_PICTURE = os.path.join(
    SN_VAULT, 'refresh.png')                            # (*) For built binaries, message to refresh
BLANK_TEMPLATE_PICTURE2 = os.path.join(
    SN_VAULT, 'blankmanta.png')                              # (*) Template blank filename (ckear layer)
REFRESH_PICTURE2 = os.path.join(
    SN_VAULT, 'refresh2.png')                            # (*) For built binaries, message to refresh
DEFAULT_FONTS = os.path.join(
    SN_VAULT, 'myfonts.note')                           # (!) Custom fonts notebook
DEFAULT_FONTS2 = os.path.join(
    SN_VAULT, 'myfonts2.note')
TO_PEN_STROKES_FILTER = 'text2notes'                    # (*) If non-null, doc files for conversion to notebooks MUST be in a folder containing that keyword
SCALE_RATIO_TEXT2NOTE = 1                             # (!) You may want to adjust your font-size using this parameter
FONT_NAME = "calibri"                                     # (!) The name of the fonts set used for text-to-notes. keyword must exist in fonts.note
MARGIN_LEFT_SIZE = 0.8                                    # If non-zero, it will leave a margin to add more personal strokes
ASCII_X0 = 111
ASCII_X02 = 152
ASCII_SIDE = 118
ASCII_SIDE2 = 161
TEXT2NOTES_STYLING = {
        "$page": {
            "next_word_count": 1,
            "ord": 170,
            "weight": 0.4,
            "toc_bg_color": 202
        },
        "^h1": {
            "ord": 170,
            "weight": 0.6,
            "toc_bg_color": 254,
            "indent": 4,
            "creturn": 4,
            "size": 0.8,
            "case": "upper"
        },
        "^h2": {
            "ord": 171,
            "weight": 0.4,
            "toc_bg_color": 254,
            "indent": 3,
            "creturn": 3
        },
        "^h3": {
            "ord": 169,
            "weight": 0.2,
            "toc_bg_color": 254,
            "indent": 2,
            "creturn": 2
        },
        "^h4": {
            "ord": 169,
            "weight": 0.2,
            "toc_bg_color": 254,
            "indent": 1,
            "creturn": 1
        },
        "^l1": {
            "prefix": "-",
            "indent": -2,
            "creturn": 1
        },
        "^l2": {
            "prefix": "+",
            "indent": -4,
            "creturn": 1
        },
        "^b1": {
            "weight": 0.4
        }
    }

CHAR_V_OFFSET = {
    "g": 15,
    "p": 15,
    "q": 15,
    "y": 15,
    "f": 15,
    "'": -25,
    "-": -15,
    ",": 15,
    ";": 15,
    '“': -30,
    "”": -30,
    "’": -30,
    "–": -30,
    '"': -30,
    '^': -30,
    chr(170): 40,
    chr(176): 10}                                        # (!) Vertical offset for some fonts
MAX_PEN_TO_STROKE_LOOP = 500                             # (*) Safety while loop exit
CUSTOM_MAP = {
    "Ç": 128,
    "ü": 129,
    "é": 130,
    "â": 131,
    "ä": 132,
    "à": 133,
    "å": 134,
    "ç": 135,
    "ê": 136,
    "ë": 137,
    "è": 138,
    "ï": 139,
    "î": 140,
    "ì": 141,
    "Ä": 142,
    "Å": 143,
    "É": 144,
    "æ": 145,
    "Æ": 146,
    "ô": 147,
    "ö": 148,
    "ò": 149,
    "û": 150,
    "ù": 151,
    "ÿ": 152,
    "Ö": 153,
    "Ü": 154,
    "¢": 155,
    "£": 156,
    "¥": 157,
    "₧": 158,
    "ƒ": 159,
    "á": 160,
    "í": 161,
    "ó": 162,
    "ú": 163,
    "ñ": 164,
    "Ñ": 165,
    "ª": 166,
    "º": 167,
    "¿": 168,
    "•": 172,
    "○": 173,
    "■": 174,
    "□": 175}
#  --------------------------------------------------------------------------------------------------
# COMMON SETTINGS FOR USB, LAN and FOLDERS. *** IMPORTANT: KEEP THE LINUX  folder notation with forward slash (whether or no you are in Windows) *****
#
SN_IO_FOLDER = '_sn_local'                              # (*) This is the short name of a vault subfolder. The script will populate this with with files.

SN_IO_WATCHED_FOLDERS = ['Document/digest_in/',
                         'SDcard/digest_in/']          # (!) Relative path to the list of folders you want the script to monitor. Subfolders ignored.
DIGESTIBLE_FILTER = 'digest_in'                         # (*) If non-null, candidates files for PySN digest MUST be in a folder containing that keyword
SN_IO_UPLOAD_FOLDER = 'Document/pysn-output'            # (!) Relative path to folder destination for the modified pdf and mark files. Subfolders ignored

NEVER_USE_CACHE_FILES = []                              # (!) List of base filenames you don't want to be taken from the cache (like for different files with same name)
EXPORT_FOLDER = 'EXPORT/pysn-export'
LOCAL_PDF_NOTES = os.path.join(
    SN_VAULT, 'exported_notes')                          # (!) Default root folder for local copy of pdf conversions
if not os.path.exists(LOCAL_PDF_NOTES):
    os.makedirs(LOCAL_PDF_NOTES)
LOCAL_BKP_NOTES = os.path.join(
    SN_VAULT, 'backup_notes')                            # (!) Default root folder for local copy of pdf conversions
ARCHIVE = os.path.join(
    SN_VAULT, 'archive')                                 # (!) Default archive root folder
if not os.path.exists(LOCAL_BKP_NOTES):
    os.makedirs(LOCAL_BKP_NOTES)
UPLOAD_CONVERTED_NOTES = False                           # (*) If True, every notebook conversion will land tto the export folder, and that can be expensive
SUPERNOTE_FOLDERS = [
    'Document', 'EXPORT', 'MyStyle',
    'Note', 'SCREENSHOT', 'INBOX', 'SDcard']                       # (*) Default Supernote folders. Do not change!
SPECIAL_TOC = [
    'SETTINGS',
    'Last annotations',
    'KEYWORDS',
    'STARS', 'Digested', 'color', 'copy']                # (*) Special bookmark categories we want to keep the style active

#  --------------------------------------------------------------------------------------------------
# ADB SETTINGS (requires SN connected and prior download of ADB and folder 'platform-tools' installed in the same folder as this script
# https://developer.android.com/studio/releases/platform-tools
#
DEVICE_SN = "SNXXXXXXXXXXXX"                            # (!) Set Supernote serial number


ADB_FOLDER = os.path.join(
    PYSN_DIRECTORY, 'platform-tools')                   # (*) Full path to ADB folder. Change only if you have ADB already installed in another folder

if platform.system() == 'Windows':
    ADB_PATH = os.path.join(ADB_FOLDER, 'adb.exe')      # (*) Full path to ADB executable for Windows. Do not change
else:
    ADB_PATH = os.path.join(ADB_FOLDER, 'adb')          # (*) Full path to ADB executable for Mac OS & Linux. Do not change


ADB_HOST = '127.0.0.1'
ADB_LOCAL_PORT = 5037                                   # (*) Default port
PDF_CACHED = os.path.join(SN_VAULT, 'cache')             # (*) Full path to cached folder of pdf files
OVERWRITE_OUTPUT = True                                 # (!) When True, will attempt to overwrite existing output file.

# ---------------------------------------------------------------------------------------------------
# LOCAL WIFI TRANSFER SETTINGS (requires SN switch "Browse & Access-On" to be active)
#
USE_DIRECT_LAN_TRANSFER = True                          # (*) True to enable this communication channel. *** AVOID ON PUBLIC NETWORKS ***
SN_IO_IP_ADDRESS = '192.168.4.232'                      # (!) Local IP address displayed when "Browse & Access-On" is turned on
SN_IO_PORT = "8089"                                     # (*) Port displayed when "Browse & Access-On" is turned on

# ---------------------------------------------------------------------------------------------------
# CLOUD SETTINGS. HERE ONEDRIVE. REMOTE FOLDERS ARE ACTUALLY LOCAL FOLDERS SYNCHRONIZED WITH THE CLOUD
#

REMOTE_BASE_PATH = 'C:\\Users\\max\\OneDrive - mcicllc.com\\Supernote\\'

AVS_WAIT = 3                                            # Time to wait between calls to Azure Vision to avoid hitting rate limits

# ---------------------------------------------------------------------------------------------------
# NOTES EXPORT TO PDF SETTINGS
#
VECTORIZE_HANDWRITINGS = False                          # (!) If set to true, this will take time and CPU resource, see https://github.com/jya-dev/supernote-tool
MYSCRIPT_RATIO = 12                                     # (*) The approximate and estimated ratio between screen size and coordinates stored for text recognition
REC_TEXT_FONT_SIZE = 20                                 # (!) The font-size of the recognized text being written on the 'search' layer
BACK_LINK_FONT_SIZE = 25                                # (!) The font-size of the back link
EXTRACT_HIGHLIGHTS = True                              # (!) If True, PySN will export the highlighted text
HIGHLIGHT_OFFSET = 4
HIGHLIGHT_SEPARATOR = 80
EXPORT_MD = False
EXPORT_HTML = False
# ---------------------------------------------------------------------------------------------------
# ADB SIMULATION
#
# ADB_SCREEN_MAX_X = 11800
# ADB_SCREEN_MIN_X = 30
# ADB_SCREEN_MAX_Y = 15800
# ADB_SCREEN_MIN_Y = 64
# ADB_SCREEN_OFFSET = 5


ADB_SCREEN_MAX_X_N6 = 11864    # Inferred from binaries;I previously estimated this to be 11800
ADB_SCREEN_MAX_X_N5 = 16224   # For N5 series like Manta
ADB_SCREEN_MIN_X = 0
ADB_SCREEN_MAX_Y_N6 = 15819    # I previously estimated this to be 15800
ADB_SCREEN_MAX_Y_N5 = 21632  # For N5 series like Manta
ADB_SCREEN_MIN_Y = 0
ADB_SCREEN_OFFSET = 5

# List of allowed global variable names,  from the CLI and storable in the user's settings
allowed_globals = {
    'AUTO_EXPORT', 'AUTO_CONVERT_NOTES', 'USE_MS_VISION', 'ENABLE_LOW_PRIORITY', 'NOTEBOOK_SERIES', 'EXPORT_MD', 'EXPORT_HTML', 'SHORTCUT_PATH', 'DIGEST_COLOR', 'ERASE_COLOR',
    'PROXIMITY_THRESHOLD', 'PROXIMITY_NOTES', 'RESOLUTION_FACTOR', 'AUTHOR',
    'SN_IO_WATCHED_FOLDERS', 'SN_IO_UPLOAD_FOLDER', 'EXPORT_FOLDER', 'DEVICE_SN', 'FILE_RECOGN_LANGUAGE', 'ADB_FOLDER',
    'ADB_HOST', 'ADB_LOCAL_PORT', 'SN_IO_IP_ADDRESS', 'SN_IO_PORT', 'REMOTE_BASE_PATH', 'DEBUG_MODE',
    'VECTORIZE_HANDWRITINGS', 'USE_DIRECT_LAN_TRANSFER', 'DIGESTIBLE_FILTER', 'LOCAL_PDF_NOTES', 'LOCAL_BKP_NOTES',
    'ARCHIVE', 'BACK_LINK_FONT_SIZE', 'EXTRACT_HIGHLIGHTS', 'TO_PEN_STROKES_FILTER', 'SCALE_RATIO_TEXT2NOTE',
    'FONT_NAME', 'MARGIN_LEFT_SIZE', 'TEXT2NOTES_STYLING', 'TO_NOTES_FILTER', 'NOTEBOOK_DEVICE'}
# Dictionary of global variables exposed to the CLI: option long-name, short-name, descrition and type
settings_desc_dict = {
    'debug': {
        'short': 'debug',
        'description': 'DEBUG_MODE      Enable (prefix -no- to disable) debug. When enabled, more info print on screen and temp folder is not deleted',
        'type': bool},
    'input': {
        'short': 'input',
        'description': 'One or several SN folder paths, separated by a space, indicating the location of the files to be processed. SD card paths must start with "SDcard". Subfolders are ignored.',
        'type': str},
    'output': {
        'short': 'output',
        'description': 'SN destination folder for the modified pdf and mark files. SD card path must start with "SDcard".',
        'type': str
    },
    'converted': {
        'short': 'converted',
        'description': 'Local folder where the converted notebooks are exported in PDF, Markdown and HTML formats.',
        'type': str
    },
    'backup': {
        'short': 'backup',
        'description': 'Local folder where notebooks are backed up',
        'type': str
    },
    'archive': {
        'short': 'archive',
        'description': 'Local archive folder for files deleted or with a newer version',
        'type': str
    },
    'digest': {
        'short': 'digest',
        'description': 'Keyword that must be present in the path of files to digest.',
        'type': str
    },
    'export': {
        'short': 'xp',
        'description': 'Relative path of SN destination folder where merged pdf and annotation will be uploaded',
        'type': str},
    'autoxp': {
        'short': 'ae',
        'description': 'AUTO_EXPORT Enable (prefix -no- to disable) automatic export of a merged pdf (pdf+annotation merged)',
        'type': bool},
    'autonote': {
        'short': 'an',
        'description': 'AUTO_CONVERT_NOTES Enable (prefix -no- to disable) automatic conversion and export of .note files',
        'type': bool},
    'vector': {
        'short': 'vc',
        'description': 'VECTORIZE_HANDWRITINGS Enable (prefix -no- to disable) .note conversion verctorizes handwritings',
        'type': bool},
    'msvision': {
        'short': 'mv',
        'description': 'USE_MS_VISION       Enable (prefix -no- to disable) text recognition using MS vision. This functionality requires external account setup with Microsoft.',
        'type': bool},
    'low_priority_on': {
        'short': 'low_priority_on',
        'description': 'ENABLE_LOW_PRIORITY       Enable (prefix -no- to disable) script to switch to idle.',
        'type': bool},
    'series': {
        'short': 'series',
        'description': 'NOTEBOOK_SERIES     Notebook Series dictionary',
        'type': dict},
    'device': {
        'short': 'device',
        'description': 'NOTEBOOK_DEVICE     Notebook Series (A5 or A6)',
        'type': str},
    'export_md': {
        'short': 'export_md',
        'description': 'EXPORT_MD       Enable (prefix -no- to disable) export of notebooks to markdown.',
        'type': bool},
    'export_html': {
        'short': 'export_html',
        'description': 'EXPORT_HTML       Enable (prefix -no- to disable) export of notebooks to html.',
        'type': bool},
    'lan': {
        'short': 'web',
        'description': 'USE_DIRECT_LAN_TRANSFER        Enable (prefix -no- to disable) transfer through Local Area Network (webserver on SN)',
        'type': bool},
    'shortcut': {
        'short': 'sc',
        'description': 'Windows only: path to shortcut', 'type': str},
    'selcolor': {
        'short': 'dc',
        'description': 'Color used to select pdf items',
        'type': str},
    'delcolor': {
        'short': 'ec',
        'description': 'Color used to erase pdf items',
        'type': str},
    'selpix': {
        'short': 'sp',
        'description': 'Selections within that pixel distance will be included in the digest',
        'type': int},
    'notepix': {
        'short': 'np',
        'description': 'Handwriting notes within that pixel distance will be included in the digest',
        'type': int},
    'res': {
        'short': 'r',
        'description': 'Increasing this increases the quality of the crops, but also the size of the digested pdf',
        'type': int},
    'auth': {
        'short': 'auth',
        'description': 'Author name showing in pdf comments',
        'type': str},

    'serial': {
        'short': 'sn',
        'description': 'Serial number of the SN, read from the USB port. For future programming to avoid sync with unregistered SNs',
        'type': str},
    'language': {
        'short': 'language',
        'description': 'FILE_RECOGN_LANGUAGE  Language to be used by MyScript',
        'type': str},
    'adbf': {
        'short': 'adb_path',
        'description': 'Path to ADB binary. Set this if ADB is already on your machine',
        'type': str},
    'adbh': {
        'short': 'adb_host',
        'description': 'ADB local IP address',
        'type': str},
    'adbp': {
        'short': 'adb_port',
        'description': 'ADB communication port',
        'type': str},
    'webip': {
        'short': 'web_host',
        'description': 'SN local web IP address displayed when "Browse & Access-On" is turned on',
        'type': str},
    'webport': {
        'short': 'web_port',
        'description': 'SN local port number displayed when "Browse & Access-On" is turned on',
        'type': str},
    'syncf': {
        'short': 'mirror',
        'description': 'Local path to the mirrored root of the synch cloud folder',
        'type': str},
    'link_fz': {
        'short': 'link_fz',
        'description': 'Back link font size',
        'type': int},
    'highlights': {
        'short': 'highlights',
        'description': 'Export pdf higlighted text',
        'type': bool},
    'text2notes': {
        'short': 'text2notes',
        'description': 'Keyword that must be present in the path of text files to convert to note.',
        'type': str},
    'import': {
        'short': 'import',
        'description': 'Keyword that must be present in the path of pdf files to convert to note.',
        'type': str},
    'fontr': {
        'short': 'fontr',
        'description': 'The ratio to apply to the real font-size. for example: 0.7 means 70 percent of the real size',
        'type': float},
    'font_name': {
        'short': 'font_name',
        'description': 'The name of the fonts set used for text-to-notes',
        'type': str},
    'lmargin': {
        'short': 'lmargin',
        'description': 'The left margin to apply when converting a text to note',
        'type': float},
    'styling': {
        'short': 'styling',
        'description': 'Styling applied to headers in text2notes',
        'type': dict}
}
# Dictionary of global variables exposed to the CLI: option long-name and global variable
param_to_global_var = {
    'autoxp': 'AUTO_EXPORT',
    'autonote': 'AUTO_CONVERT_NOTES',
    'vector': 'VECTORIZE_HANDWRITINGS',
    'msvision': 'USE_MS_VISION',
    'low_priority_on': 'ENABLE_LOW_PRIORITY',
    'series': 'NOTEBOOK_SERIES',
    'device': 'NOTEBOOK_DEVICE',
    'export_md': 'EXPORT_MD',
    'export_html': 'EXPORT_HTML',
    'shortcut': 'SHORTCUT_PATH',
    'selcolor': 'DIGEST_COLOR',
    'delcolor': 'ERASE_COLOR',
    'selpix': 'PROXIMITY_THRESHOLD',
    'notepix': 'PROXIMITY_NOTES',
    'res': 'RESOLUTION_FACTOR',
    'auth': 'AUTHOR',
    'input': 'SN_IO_WATCHED_FOLDERS',
    'output': 'SN_IO_UPLOAD_FOLDER',
    'converted': 'LOCAL_PDF_NOTES',
    'export': 'EXPORT_FOLDER',
    'serial': 'DEVICE_SN',
    'language': 'FILE_RECOGN_LANGUAGE',
    'adbf': 'ADB_FOLDER',
    'adbh': 'ADB_HOST',
    'adbp': 'ADB_LOCAL_PORT',
    'lan': 'USE_DIRECT_LAN_TRANSFER',
    'webip': 'SN_IO_IP_ADDRESS',
    'webport': 'SN_IO_PORT',
    'syncf': 'REMOTE_BASE_PATH',
    'debug': 'DEBUG_MODE',
    'digest': 'DIGESTIBLE_FILTER',
    'text2notes': 'TO_PEN_STROKES_FILTER',
    'lmargin': 'MARGIN_LEFT_SIZE',
    'fontr': 'SCALE_RATIO_TEXT2NOTE',
    'font_name': 'FONT_NAME',
    'backup': 'LOCAL_BKP_NOTES',
    'archive': 'ARCHIVE',
    'link_fz': 'BACK_LINK_FONT_SIZE',
    'highlights': 'EXTRACT_HIGHLIGHTS',
    'styling': 'TEXT2NOTES_STYLING',
    'import': 'TO_NOTES_FILTER'
}

# Visibility parameters for the png extraction (from Supernotelib library)
bg_visibility = VisibilityOverlay.DEFAULT
bg_invisibility = VisibilityOverlay.INVISIBLE
vo = sn.converter.build_visibility_overlay(background=bg_visibility)
vi = sn.converter.build_visibility_overlay(background=bg_invisibility)


def generate_link_json(sn_page_nb, pdf_path, pdf_page_nb, link_rect, binary):
    """ Appends link json to a binary and return updated binary and location """
    try:
        # Insertion position
        link_bitmap = len(binary)

        # Inserts a default link bitmap of reasonable size
        # binary += bytes.fromhex('0800000062FF62FF6289625F')
        binary += bytes.fromhex('0800000062FF62FF62D06226')
        
        link_json_address = len(binary)

        # Supernote path to pdf
        sn_file_path = str(base64.b64encode(f'/storage/emulated/0{pdf_path}'.encode('utf-8')))[2:-1]

        # Create the name of the link
        x0, y0, dx, dy = link_rect
        link_name = f'LINKO_{str(sn_page_nb).zfill(4)}{str(y0).zfill(4)}{str(x0).zfill(4)}{str(dy).zfill(4)}{str(dx).zfill(4)}'

        # Create the JSON
        link_json = f'<LINKTYPE:2><LINKINOUT:0><LINKBITMAP:{link_bitmap}><LINKSTYLE:20000000>'
        link_json += '<LINKTIMESTAMP:' + str(datetime.now().strftime("%Y%m%d%H%M%S%f")) + '>'
        link_json += f'<LINKRECT:{x0},{y0},{dx},{dy}><LINKRECTORI:{x0},{y0},{dx},{dy}>'
        link_json += f'<LINKPROTOCAL:RATTA_RLE><LINKFILE:{sn_file_path}><LINKFILEID:none><PAGEID:none><OBJPAGE:{pdf_page_nb}>'
        link_json += '<FONTSIZE:0.000000><FONTPATH:none><FULLTEXT:none><SHOWTEXT:none>'
        encoded_link_json = link_json.encode('utf-8')
        encoded_link_json_size = int_to_little_endian_bytes(len(encoded_link_json))
        link_json_block = encoded_link_json_size + encoded_link_json
        binary += link_json_block
    except Exception as e:
        print(f'*** generate_link_json: {e}')
        return None, None, binary
    return link_name, link_json_address, binary


def blank_encoded_image(device):
    if device in ['N5']:
        background_image_block = bytes.fromhex((
            '5802000062FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'))
    else:
        background_image_block = bytes.fromhex((
            '4401000062FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF'
            '62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62FF62B4623F'))
    return background_image_block


def series_bounds(series):
    """ Returns screen bounds based on device series """
    # print(f'       series:{series}')
    if series in ['N5']:
        return [
            MAX_HORIZONTAL_PIXELS_N5, MAX_VERTICAL_PIXELS_N5,
            ADB_SCREEN_MAX_X_N5, ADB_SCREEN_MAX_Y_N5]
    else:
        return [
            MAX_HORIZONTAL_PIXELS_N6, MAX_VERTICAL_PIXELS_N6,
            ADB_SCREEN_MAX_X_N6, ADB_SCREEN_MAX_Y_N6]


def create_natural_cell_info(
    rect: Tuple[float, float, float, float],
    pen_strokes: List[List[Tuple[float, float]]],
    rows: int = 3,
    cols: int = 3
) -> Dict[Tuple[int, int], Dict[str, Tuple[float, float]]]:
    """
    Given:
      - rect: (x, y, w, h) bounding box of the character
      - pen_strokes: list of strokes; each stroke is a list of (x, y) points
      - rows, cols: number of rows and columns for the grid (default 3x3)

    Returns a dictionary keyed by (row, col), each containing:
      {
        'start_point': (sx, sy),
        'direction':   (dx, dy),
      }
    where 'start_point' is the first point that enters that cell,
    and 'direction' is a vector from start to the last point in that cell.
    """
    # Path initialization
    path_list = []

    # Unpack rectangle
    x_min, y_min, w, h = rect

    # Dimensions of each cell
    cell_w = w / cols
    cell_h = h / rows

    # Helper to find which cell a point belongs to
    def get_cell_idx(px, py):
        # clamp the row/col to ensure it doesn't exceed grid boundaries
        col_idx = min(cols - 1, max(0, int((px - x_min) // cell_w)))
        row_idx = min(rows - 1, max(0, int((py - y_min) // cell_h)))
        # print(f'----(px, py): {(px, py)} --- col_idxL {col_idx}  row_idx: {row_idx}')
        return (row_idx, col_idx)

    # Initialize a dictionary to store the cell information
    # We'll keep track of a list of points that land in each cell,
    # so we can choose how to compute direction, etc.
    cell_points = {
        (r, c): [] for r in range(rows) for c in range(cols)}

    # Go through each stroke, then each point in the stroke
    previous_rc = None
    counter_idx = 0
    for stroke in pen_strokes:
        for (py, px) in stroke:
            # find the cell where (px, py) belongs

            cell_rc = get_cell_idx(px, py)
            current_rc = (cell_rc[0]+1)*10 + cell_rc[1] + 1
            cell_points[cell_rc].append((px, py))
            if previous_rc != current_rc:
                # path_list.append({'cell': current_rc, 'entry': (py,px)})
                previous_rc = current_rc
                counter_idx = 0
                previous_py = py
                previous_px = px

            elif counter_idx == 0 and ((py != previous_py) or (px != previous_px)):
                counter_idx += 1
                path_list.append({'cell': current_rc, 'entry': (previous_py, previous_px), 'direction': (py-previous_py, px-previous_px)})

    # Now build up the final structure:
    # For each cell: find the first point that appeared, then compute direction
    # as (last_point - first_point).

    cell_info = {}
    for rc, points in cell_points.items():
        if not points:
            # If no points in this cell, skip or store empty
            continue

        # We define 'start_point' as simply the first point in that cell
        start_pt = points[0]
        # We define 'direction' from the start point to the last point in that cell
        last_pt = points[-1]
        direction = (last_pt[0] - start_pt[0], last_pt[1] - start_pt[1])

        cell_info[rc] = {
            'start_point': start_pt,
            'direction': direction,
        }

    return cell_info, path_list, cell_points


@dataclass
class Point:
    x: float
    y: float


def map_to_recsn(a_map_list, aggregate=False):
    """ Converts a list of recognized text in a map list to the SN's
        MyScript format dictionary """
    try:
        MYSCRIPT_RATIO_X = 11.9275558467356
        MYSCRIPT_RATIO_Y = 11.8973247420597
        # MYSCRIPT_RATIO_X = MYSCRIPT_RATIO_Y = MYSCRIPT_RATIO

        result_list = []
        result_dict = {}
        result = None

        for an_item in a_map_list:
            rec_text = an_item[0]
            x0, y0, x1, y1 = an_item[1]
            result_list.append(
                {
                    "label": rec_text,
                    "type": "Text",
                    "words": [
                        {
                            "bounding-box": {
                                "height": round((y1 - y0) / MYSCRIPT_RATIO_Y, 7),
                                "width": round((x1 - x0) / MYSCRIPT_RATIO_X, 7),
                                "x": round(x0 / MYSCRIPT_RATIO_X, 7),
                                "y":  round(y0 / MYSCRIPT_RATIO_Y, 7)},
                            "label": rec_text
                        }
                    ]
                }
            )
        result_dict["elements"] = result_list
        result_dict["type"] = "Raw Content"
        result = json.dumps(result_dict)
    except Exception as e:
        print(f'*** map_to_recsn: {e}')
    return result


def binary_md5(binary_file, data_start=0, data_length=None, max_hash_length=None):
    """ Computes md5 hash of a block of bytes and returns it, postfixed
        with the length of block considered for the hash computation.

        Supernote uses this logic to compute a hash of the RLE encoded bytes of
        templates. We do not know how max_hash_length is picked so we choose
        randomly in an empirical range.

        Input Parameters:
            - binary_file: binary data of a file containing a block to hash
            - data_start: start location of the block of data to hash
            - data_length: length of the block of data to hash
            - max_hash_length: maximum length to consider for hashing purpose

        Output:
            - A 32 characters long string representing the hash, postfixed by
              underscore and the max_hash_length"""

    try:
        if data_length is None:
            data_length = len(binary_file)
        # Get the block to encode
        data_end = data_start + data_length
        data_full = binary_file[data_start: data_end]

        # Picking a random max_length
        if max_hash_length is None:
            # max_hash_length = random.randint(101800, 105000)
            max_hash_length = 103639

        max_hash_length = min(len(data_full), max_hash_length)
        data_to_hash = data_full[:max_hash_length]

        # Initialize hash
        md5 = hashlib.md5()
        md5.update(data_to_hash)
        return f'{md5.hexdigest()}_{max_hash_length}'
    except Exception as e:
        print(f'*** binary_md5: {e}')
        return None


def decimal_to_little_endian_4bytes(decimal_number):
    hex_string = struct.pack('<I', decimal_number).hex().upper()
    return ' '.join(hex_string[i:i+2] for i in range(0, len(hex_string), 2))


def read_zero_sequence(binary_data, start_position):
    """
    Read a continuous sequence of zero bytes from binary data starting at a specific position.

    Args:
        binary_data (bytes): Binary data to analyze
        start_position (int): Starting position to read from

    Returns:
        int: Length of continuous zero bytes sequence
    """
    sequence_length = 0
    current_position = start_position

    # Check if start position is valid
    if start_position >= len(binary_data):
        return 0

    # Count consecutive zero bytes
    while current_position < len(binary_data):
        if binary_data[current_position] == 0:
            sequence_length += 1
            current_position += 1
        else:
            break

    return min(sequence_length, 16)


def is_polygon_in_contours(path, dense_contours):
    """ Checks if a path is included in a list of dense_contours """
    try:
        for i, cnt in enumerate(dense_contours):
            # Check each corner of the rectangle
            all_points_inside = all(cv2.pointPolygonTest(cnt, (float(point[0]), float(point[1])), False) >= 0 for point in path)
            if all_points_inside:
                return i, cnt
    except Exception as e:
        print(f'*** is_polygon_in_contours: {e}')
    return -1, None


def detect_dense_objects(image, density_min_area=DENSE_MIN_AREA, density_sensitivity=DENSE_SENSITIVITY, density_threshold=DENSE_THRESHOLD):
    """Detects large objects with high pixel density."""
    if isinstance(image, str):
        image = cv2.imread(image)
        if image is None:
            raise ValueError("Could not read image file")

    # Basic preprocessing
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) if len(image.shape) == 3 else image.copy()
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    _, binary = cv2.threshold(blurred, density_threshold, 255, cv2.THRESH_BINARY_INV)

    # Morphological operations
    kernel = np.ones((15, 15), np.uint8)
    binary = cv2.morphologyEx(binary, cv2.MORPH_CLOSE, kernel)

    # Find contours
    contours, _ = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Filter by area and density
    dense_contours = []
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > density_min_area:
            # Create mask for this contour
            mask = np.zeros_like(gray)
            cv2.drawContours(mask, [cnt], -1, 255, -1)

            # Calculate density (ratio of non-zero pixels to area)
            roi = cv2.bitwise_and(binary, mask)
            pixel_count = np.count_nonzero(roi)
            density = pixel_count / area

            if density > density_sensitivity:
                dense_contours.append(cnt)

    return dense_contours


def apply_selective_sketch(
    image, contours, ksize=(GAUSSIAN_BLUR, GAUSSIAN_BLUR), intensity=INTENSITY, shadow_amount=0.2,
        detail_level=DETAIL_LEVEL, noise_reduction=NOISE_REDUCTION, shadow_blur_factor=SHADOW_BLUR):
    """
    Applies an enhanced pencil sketch effect only within detected contours with configurable parameters.
    If contours list is empty, returns the original image unchanged.

    Parameters:
        image: Input image or path to image file
        contours: List of contours where the effect should be applied
        ksize: Tuple of kernel size for Gaussian blur (default: (21, 21))
        intensity: Controls the overall intensity of the sketch effect (0.1 to 2.0)
        shadow_amount: Controls the darkness of shadows (0.0 to 1.0)
        detail_level: Controls the amount of detail preserved (0.5 to 2.0)
        noise_reduction: Amount of noise reduction to apply (0 to 5)
        shadow_blur_factor: Factor to determine shadow blur kernel size relative to main ksize (default: 1.3)
                          Higher values create softer shadows

    Returns:
        Processed image with pencil sketch effect only within contours
    """
    if isinstance(image, str):
        image = cv2.imread(image)
        if image is None:
            raise ValueError("Could not read image file")

    # Convert to grayscale
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) if len(image.shape) == 3 else image.copy()

    # Early return if no contours provided
    if not contours:
        return gray_image

    # Create mask for contours
    mask = np.zeros_like(gray_image, dtype=np.uint8)
    cv2.drawContours(mask, contours, -1, 255, -1)

    # Create a copy of the original image for processing
    processed_region = gray_image.copy()

    # Apply noise reduction only to the region we'll process
    if noise_reduction > 0:
        processed_region = cv2.medianBlur(processed_region, 2 * int(noise_reduction) + 1)

    # Enhance contrast based on detail level
    if detail_level != 1.0:
        processed_region = cv2.convertScaleAbs(processed_region, alpha=detail_level, beta=0)

    # Apply pencil sketch effect with enhanced control
    invert_image = 255 - processed_region

    # Adjust blur kernel size based on detail level
    adjusted_ksize = tuple(int(k * (2 - detail_level)) if k > 1 else 1 for k in ksize)
    adjusted_ksize = tuple(k if k % 2 == 1 else k + 1 for k in adjusted_ksize)  # Ensure odd kernel size

    blurred = cv2.GaussianBlur(invert_image, adjusted_ksize, 0)
    inverted_blurred = 255 - blurred

    # Apply intensity adjustment
    inverted_blurred = cv2.convertScaleAbs(inverted_blurred, alpha=intensity, beta=0)

    # Add shadow effect only to the processed region
    if shadow_amount > 0:
        # Calculate shadow blur kernel size based on main kernel size
        shadow_ksize = tuple(int(k * shadow_blur_factor) if k > 1 else 1 for k in adjusted_ksize)
        shadow_ksize = tuple(k if k % 2 == 1 else k + 1 for k in shadow_ksize)  # Ensure odd kernel size

        shadow = cv2.GaussianBlur(processed_region, shadow_ksize, 0)
        shadow = cv2.convertScaleAbs(shadow, alpha=shadow_amount, beta=0)
        processed_region = cv2.addWeighted(processed_region, 1 - shadow_amount, shadow, shadow_amount, 0)

    # Create pencil sketch effect
    pencil_sketch = cv2.divide(processed_region, inverted_blurred, scale=256.0)

    # Enhance contrast of the sketch
    pencil_sketch = cv2.convertScaleAbs(pencil_sketch, alpha=1.2, beta=0)

    # Create the final result by combining original and processed regions
    result = gray_image.copy()
    result[mask == 255] = pencil_sketch[mask == 255]

    return result


def visualize_contours(image, contours, output_path=None):
    """Same visualization function as before"""
    if isinstance(image, str):
        image = cv2.imread(image)
    result = image.copy()
    cv2.drawContours(result, contours, -1, (0, 255, 0), 2)
    if output_path:
        cv2.imwrite(output_path, result)
    return result


def get_image_paths(image_path, scale_ratio=1, threshold=128, alphamax=0.3, turdsize=2, opticurve=1, opttolerance=0.1):
    """ Load the image and convert to grayscale """
    try:
        # Load the image and increase its resolution
        image = Image.open(image_path).convert("L")
        image = image.filter(ImageFilter.GaussianBlur(radius=1))
        if scale_ratio != 1:
            image = image.resize((image.width * scale_ratio, image.height * scale_ratio), Image.Resampling.LANCZOS)

        # bitmap = potrace.Bitmap(amask)
        bitmap = potrace.Bitmap(np.array(image) > threshold)

        # path = bitmap.trace(turnpolicy=potrace.TURNPOLICY_MINORITY, turdsize=2, alphamax=0.7, opticurve=True)
        # path = bitmap.trace(turdsize=turdsize, turnpolicy='minority', alphamax=alphamax, opticurve=opticurve, opttolerance=opttolerance)
        path = bitmap.trace()

        paths = []
        for curve in path:
            segment = []
            current_point = curve.start_point
            segment.append(current_point)
            for curve_segment in curve:
                current_point = curve_segment.end_point
                segment.append(current_point)
            paths.append(segment)

        return paths
    except Exception as e:
        print(f'*** get_image_paths: {e}')
        return None


def sort_coordinates(coords):
    """
    Sort coordinates by nearest neighbor, optimized version.

    Args:
        coords: numpy array of shape (N, 2) containing coordinates

    Returns:
        numpy array of sorted coordinates
    """
    try:
        if len(coords) <= 1:
            return coords

        # Convert to numpy array if not already
        coords = np.asarray(coords)
        n_points = len(coords)

        # Start with the first point
        current_idx = 0
        sorted_indices = [current_idx]
        unvisited = set(range(1, n_points))

        # Pre-compute all pairwise distances once
        distances = cdist(coords, coords)

        # Find nearest neighbors iteratively
        while unvisited:
            # Get distances from current point to all unvisited points
            current_distances = distances[current_idx]
            # Set visited points to infinity so they won't be selected
            current_distances[sorted_indices] = np.inf

            # Find the nearest unvisited point
            current_idx = np.argmin(current_distances)
            sorted_indices.append(current_idx)
            unvisited.remove(current_idx)

        return coords[sorted_indices]
    except Exception as e:
        print(f'*** sort_coordinates: {e}')
        return coords


def order_stroke_points(points):
    if not points:
        return []
    try:
        # Convert points to numpy array for faster operations
        points = np.array(points)
        print('\r', end='', flush=True)
        print(f'  > Rendering {len(points)} points ...', end='', flush=True)
        # Create a fast lookup dictionary using point tuples as keys
        point_dict = {tuple(point): i for i, point in enumerate(points)}

        # Pre-compute neighbor offsets
        neighbor_offsets = np.array([
            (-1, -1), (-1, 0), (-1, 1),
            (0, -1),           (0, 1),
            (1, -1),  (1, 0),  (1, 1)
        ])

        # Build adjacency list more efficiently
        adjacency = defaultdict(list)
        points_broadcast = points[:, np.newaxis, :]  # Shape: (n_points, 1, 2)
        neighbor_candidates = points_broadcast + neighbor_offsets  # Shape: (n_points, 8, 2)

        # Vectorized neighbor finding
        for i, point in enumerate(points):
            point_tuple = tuple(point)
            candidates = neighbor_candidates[i]

            # Vectorized containment check
            for candidate in candidates:
                candidate_tuple = tuple(candidate)
                if candidate_tuple in point_dict:
                    adjacency[point_tuple].append(candidate_tuple)

        # Optimized DFS with minimal path adjustments
        visited = set()
        path = []

        def fast_dfs(start):
            stack = [start]
            last_added = None

            while stack:
                current = stack[-1]

                if current not in visited:
                    visited.add(current)
                    path.append(current)
                    last_added = current

                # Find next unvisited neighbor
                next_point = None
                for neighbor in adjacency[current]:
                    if neighbor not in visited:
                        next_point = neighbor
                        break

                if next_point:
                    stack.append(next_point)
                else:
                    stack.pop()
                    if stack and last_added != stack[-1]:
                        path.append(stack[-1])
                        last_added = stack[-1]

        # Process all components
        for point_tuple in point_dict:
            if point_tuple not in visited:
                fast_dfs(point_tuple)

        # Convert tuples back to lists for consistency
    finally:
        print('', end='\r', flush=True)
    return [list(point) for point in path]


def pdf_to_cv2_grayscale(
    pdf_path, unstitch=UNSTITCH, base_zoom_x=ZOOM_X, base_zoom_y=ZOOM_Y,
        min_dimension=MIN_DIMENSION, template=False, resizing=None, recognize=RECOGNIZE_TEXT,
        series=NOTEBOOK_DEVICE):
    """
    Convert a PDF file to a list of PIL Images with dynamic zoom based on image resolution.
    Optionally unstitch images by rotating to landscape and splitting into two.

    Args:
        pdf_path (str): Path to the PDF file
        unstitch (bool): If True, rotate to landscape and split into two images
        base_zoom_x (float): Base horizontal zoom factor for rendering (default: 2.0)
        base_zoom_y (float): Base vertical zoom factor for rendering (default: 2.0)
        min_dimension (int): Minimum desired pixel dimension (width or height) (default: 1500)

    Returns:
        list: List of numpy arrays (grayscale images), one or two for each page depending on unstitch
    """
    try:
        pdf_document = fitz.open(pdf_path)
        images = []
        doc_len = pdf_document.page_count
        resizing_list = []
        encoded_rec_text = []
        hw_ocr_detail_list = []
        index_image = 0
        new_zoom_x = base_zoom_x
        new_zoom_y = base_zoom_y
        max_horizontal_pixels, max_vertical_pixels, _, _ = series_bounds(series)

        print('')

        for page_num in range(doc_len):

            if template:
                basename = os.path.basename(pdf_path)
                print(f'  > Assessing {basename}', end='', flush=True)
            else:
                print(f'  > Extracting image {page_num+1}/{doc_len}', end='', flush=True)

            page = pdf_document[page_num]

            # First try with base zoom to check dimensions
            mat = fitz.Matrix(base_zoom_x, base_zoom_y)
            pix = page.get_pixmap(matrix=mat, alpha=False, colorspace="gray")

            # Check current dimensions
            current_width = pix.width
            current_height = pix.height

            if resizing is not None:

                new_shape = resizing[index_image]
                ratio_x = new_shape[0]/pix.width
                ratio_y = new_shape[1]/pix.height

                # Create new matrix with adjusted zoom
                mat = fitz.Matrix(base_zoom_x*ratio_x, base_zoom_y*ratio_y)
                pix = page.get_pixmap(matrix=mat, alpha=False, colorspace="gray")
                pix_w = pix.width
                pix_h = pix.height
                print('\r', end='', flush=True)
                print()
                print(f'    (Zoom: {base_zoom_x*ratio_x}x -> {pix_w} x {pix_h})', end='', flush=True)
                print()

            else:

                # If either dimension is too small, increase zoom factors
                if current_width < min_dimension or current_height < min_dimension:
                    width_scale = min_dimension / current_width if current_width < min_dimension else 1
                    height_scale = min_dimension / current_height if current_height < min_dimension else 1
                    scale_factor = max(width_scale, height_scale)

                    new_zoom_x = base_zoom_x * scale_factor
                    new_zoom_y = base_zoom_y * scale_factor

                    # Limit maximum zoom to prevent excessive memory usage
                    max_zoom = MAX_ZOOM
                    new_zoom_x = min(new_zoom_x, max_zoom)
                    new_zoom_y = min(new_zoom_y, max_zoom)

                    # Create new matrix with adjusted zoom
                    mat = fitz.Matrix(new_zoom_x, new_zoom_y)
                    pix = page.get_pixmap(matrix=mat, alpha=False, colorspace="gray")
                    pix_w = pix.width
                    pix_h = pix.height

                    print('\r', end='', flush=True)
                    print()

                    print(f'    (Zoom: {new_zoom_x}x -> {pix_w} x {pix_h})', end='', flush=True)
                    print()

            # Convert pixmap to numpy array

            img_array = np.frombuffer(pix.samples, dtype=np.uint8).reshape(pix.height, pix.width)

            if unstitch:
                # Rotate 90 degrees counterclockwise if the image is in portrait mode
                if img_array.shape[0] > img_array.shape[1]:
                    img_array = np.rot90(img_array)

                # Calculate the midpoint for splitting
                mid_point = img_array.shape[1] // 2

                # Split the image into two halves
                left_half = img_array[:, :mid_point]
                right_half = img_array[:, mid_point:]

                # Add both halves to the images list
                images.append(left_half)
                images.append(right_half)
                resizing_list.append([left_half.shape[1], left_half.shape[0]])
                resizing_list.append([right_half.shape[1], right_half.shape[0]])

            else:
                images.append(img_array)
                resizing_list.append([pix.width, pix.height])

            if recognize:
                matrx = page.rect.torect(pix.irect)
                pr_matrix = page.rotation_matrix * matrx
                inv_pr_matrix = ~pr_matrix
                # print(f'-----inv_pr_matrix:{inv_pr_matrix}') TODO: Keep only the relevant matrix
                a_pic_fn = f'{pdf_path}_{page_num}.png'
                # Convert NumPy array to PIL Image
                try:
                    a_pic = np.stack([img_array] * 3, axis=-1)
                except Exception as e:
                    print(f'**- : {e}')
                pil_image = Image.fromarray(a_pic)
                pil_image.save(a_pic_fn)
                x_size, y_size = pil_image.size

                inv_pr_matrix = fitz.Matrix(max_horizontal_pixels/x_size, max_vertical_pixels/y_size)

                page_hw_text, page_hw_map, hw_ocr_detail_list_ = get_text_from_image(
                    a_pic_fn, inv_pr_matrix, recognize=recognize, device=series)
                page_hw_map_sn = map_to_recsn(page_hw_map)
                encoded_rec_text.append(base64.b64encode(page_hw_map_sn.encode('utf-8')))
                hw_ocr_detail_list.append(hw_ocr_detail_list_)

            index_image += 1
            print(end='\r', flush=True)

        pdf_document.close()
        return images, resizing_list, encoded_rec_text, hw_ocr_detail_list

    except Exception as e:
        print(f"*** pdf_to_images: {str(e)}")
        return [], None, [], []


def extract_handwriting(image_full, image_pattern):
    """
    Extract handwriting from the image by removing the background pattern without introducing holes.

    Parameters:
    - image_full: cv2 image object, the original scanned image with handwriting and background pattern.
    - image_pattern: cv2 image object, the background pattern image.

    Returns:
    - handwriting_only: cv2 image object, the image with handwriting extracted as a binary image.
    """

    # Step 1: Convert images to grayscale if they are not already
    if len(image_full.shape) == 3:
        image_full_gray = cv2.cvtColor(image_full, cv2.COLOR_BGR2GRAY)
    else:
        image_full_gray = image_full.copy()

    if len(image_pattern.shape) == 3:
        image_pattern_gray = cv2.cvtColor(image_pattern, cv2.COLOR_BGR2GRAY)
    else:
        image_pattern_gray = image_pattern.copy()

    image_full_gray = cv2.medianBlur(image_full_gray, 3)

    # Step 2: Threshold and invert the images to get binary images
    # Inverted so that handwriting and lines are white (255), background is black (0)
    _, binary_full = cv2.threshold(image_full_gray, 127, 255, cv2.THRESH_BINARY_INV)
    _, binary_pattern = cv2.threshold(image_pattern_gray, 127, 255, cv2.THRESH_BINARY_INV)

    # Step 3: Invert the background pattern image
    binary_pattern_inv = cv2.bitwise_not(binary_pattern)

    # Step 4: Use bitwise AND to isolate the handwriting
    handwriting_only = cv2.bitwise_and(binary_full, binary_pattern_inv)

    # Step 5: Morphological closing to fill small holes (optional)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
    handwriting_only = cv2.morphologyEx(handwriting_only, cv2.MORPH_CLOSE, kernel)

    # Step 6: Invert the image back to original format if needed
    handwriting_only = cv2.bitwise_not(handwriting_only)

    return handwriting_only


def analyze_lines(img: np.ndarray, direction: str = 'horizontal') -> Tuple[int, List[int]]:
    height, width = img.shape

    if direction == 'horizontal':
        # Sample columns at quarter positions
        positions = [width // 6, width // 3, width // 2, (2 * width) // 3, (5 * width) // 6]
        profiles = [img[:, x] for x in positions]
        combined_length = height
    elif direction == 'vertical':
        # Sample rows at quarter positions
        positions = [height // 6, height // 3, height // 2, (2 * height) // 3, (5 * height) // 6]
        profiles = [img[y, :] for y in positions]
        combined_length = width
    else:
        raise ValueError("Direction must be 'horizontal' or 'vertical'.")

    # Combine profiles to get more robust detection
    combined_profile = np.zeros(combined_length, dtype=np.int32)

    for profile in profiles:
        combined_profile += (profile > 0).astype(np.int32)

    # Find line positions (where at least 2 profiles detected a line)
    line_positions = np.where(combined_profile >= 3)[0]

    if len(line_positions) < 2:
        return 0, []

    # Calculate periods between consecutive positions
    periods = np.diff(line_positions)

    # Find periods in the expected range
    valid_periods = periods[(periods >= LINE_DETECT_MIN_REP) & (periods <= LINE_DETECT_MAX_REP)]

    if len(valid_periods) == 0:
        return 0, []

    # Use median of valid periods
    most_common_period = int(np.median(valid_periods))

    print(f'  - Found {direction} periodicity: {most_common_period}')

    return most_common_period, line_positions.tolist()


def remove_lines(image, handwriting_fn, gpt=False, debug=False, pre_denoise=None):
    """ Detect and remove repetitive horizontal and vertical lines from a blended
        handwriting + template input.

        pre_denoise makes significant distinction in denoising for insulating handwriting
        (this could be necessary for paper scan where any dust can make a difference)

        """
    try:
        filtered_result = None
        pattern_image = None
        template_removed = False

        for direction in ['horizontal', 'vertical']:

            # Threshold the image to separate handwriting from background
            # blur = cv2.medianBlur(image, 3)
            blur = cv2.GaussianBlur(image, (3, 3), 0)

            if pre_denoise:
                blur_handwriting = image
                for i in range(pre_denoise):
                    blur_handwriting = cv2.medianBlur(blur_handwriting, 3)
                _, blur_handwriting = cv2.threshold(blur_handwriting, 127, 255, cv2.THRESH_BINARY)
            else:
                blur_handwriting = blur

            # Use a more aggressive threshold to catch handwriting
            handwriting_thresh = cv2.adaptiveThreshold(
                blur_handwriting,
                255,
                cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                cv2.THRESH_BINARY_INV,
                35,
                10
            )

            # Use a different threshold for line detection
            line_thresh = cv2.adaptiveThreshold(
                blur,
                255,
                cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                cv2.THRESH_BINARY_INV,
                15,
                10
            )

            periodicity, positions = analyze_lines(line_thresh, direction=direction)
            # Create mask of lines
            height, width = image.shape
            line_mask = np.zeros_like(image)

            if periodicity > 0:
                # Mark detected line positions

                if direction == 'horizontal':
                    kernel = np.ones((2, 1), np.uint8)
                    for y in positions:
                        if 0 <= y < height:
                            line_mask[y, :] = 255
                elif direction == 'vertical':
                    kernel = np.ones((1, 2), np.uint8)
                    for x in positions:
                        if 0 <= x < width:
                            line_mask[:, x] = 255

                if pattern_image is None:
                    pattern_image = line_mask
                else:
                    pattern_image = cv2.bitwise_or(
                        pattern_image, line_mask)

                line = cv2.bitwise_not(line_mask)

                if gpt:
                    return extract_handwriting(image, line), True, None

                # Compute the absolute difference between the image and the template
                diff = cv2.absdiff(handwriting_thresh, line_mask)

                eroded = cv2.erode(diff, kernel, iterations=1)
                dilated = cv2.dilate(eroded, kernel, iterations=1)
                _, diff = cv2.threshold(dilated, 200, 255, cv2.THRESH_BINARY)

                common = cv2.bitwise_and(handwriting_thresh, line_mask)
                diff2 = cv2.absdiff(line_mask, common)
                eroded_diff2 = cv2.erode(diff2, kernel, iterations=1)
                dilated_diff2 = cv2.dilate(eroded_diff2, kernel, iterations=1)

                common = cv2.bitwise_not(common)

                _, dilated_diff2 = cv2.threshold(dilated_diff2, 200, 255, cv2.THRESH_BINARY)
                diff = cv2.absdiff(diff, dilated_diff2)   # MMB 1117

                # Convert the difference image to grayscale
                _, diff_mask = cv2.threshold(diff, 57, 255, cv2.THRESH_BINARY)

                # Morphological closing to fill small holes (optional)
                kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
                diff_mask = cv2.morphologyEx(diff_mask, cv2.MORPH_CLOSE, kernel)

                filtered_result = cv2.bitwise_not(diff_mask)
                if debug:
                    cv2.imwrite(handwriting_fn+f'_{direction}_dl_line_mask.png', pattern_image)
                    cv2.imwrite(handwriting_fn+f'_{direction}_dl_filtered_result.png', filtered_result)

                template_removed = cv2.countNonZero(pattern_image) > 100

                image = filtered_result
            else:
                return image, False, None
    except Exception as e:
        print(f'*** remove_lines: {e}')
    return filtered_result, template_removed, cv2.bitwise_not(pattern_image)


def detect_remove_template(
    handwriting_img, handwriting_fn, templates_list=TEMPLATES_LIST, threshold=5,
        similarity_threshold=0.4, resizing_list=None, series=NOTEBOOK_DEVICE):
    """
    Detect if the template is present in the handwriting image.

    Parameters:
    handwriting_img: numpy.ndarray - Grayscale image with possible template lines
    template_img: numpy.ndarray - Grayscale image of the template
    threshold: int - Threshold for line detection
    similarity_threshold: float - Minimum similarity score to consider template present (0-1)

    Returns:
    bool - True if template is detected, False otherwise
    float - Similarity score
    """

    try:
        # Initialize filtered result
        filtered_result = handwriting_img
        removed_template = False
        template_image_found = None

        hw_directory = os.path.dirname(handwriting_fn)
        hw_fn = os.path.basename(handwriting_fn)

        for template_fn_ in templates_list:

            template_fn = os.path.join(SN_VAULT, template_fn_)
            template_debug_fn = os.path.join(hw_directory, hw_fn)

            if os.path.exists(template_fn):
                # Retrieve the template image
                template_img_, _, _, _ = pdf_to_cv2_grayscale(
                    template_fn, template=True, resizing=resizing_list,
                    series=series)
                template_img = template_img_[0]

                # Ensure template image is grayscale and same size
                if len(template_img.shape) > 2:
                    template_img = cv2.cvtColor(template_img, cv2.COLOR_BGR2GRAY)

                template_img = cv2.resize(template_img, (handwriting_img.shape[1], handwriting_img.shape[0]))

                # Create binary masks
                template_mask = cv2.adaptiveThreshold(
                    template_img,
                    maxValue=255,
                    adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                    thresholdType=cv2.THRESH_BINARY,
                    blockSize=15,  # Must be odd and greater than 1
                    C=10           # Constant subtracted from the mean
                )
                handwriting_mask = cv2.adaptiveThreshold(
                    handwriting_img,
                    maxValue=255,
                    adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                    thresholdType=cv2.THRESH_BINARY,
                    blockSize=15,  # Must be odd and greater than 1
                    C=10           # Constant subtracted from the mean
                )

                # Count matching pixels
                handwriting_mask_inv1 = cv2.bitwise_not(handwriting_mask)
                template_mask_inv1 = cv2.bitwise_not(template_mask)
                matching_pixels_inv1 = cv2.bitwise_and(handwriting_mask_inv1, template_mask_inv1)
                template_pixels = cv2.countNonZero(template_mask_inv1)
                matching_count = cv2.countNonZero(matching_pixels_inv1)

                if template_pixels != 0:
                    similarity_score = matching_count / template_pixels
                    print(f'    score: {similarity_score:.2%}')

                    # cv2.imwrite(template_fn+'_hw_mask_inv.png', handwriting_mask_inv1)
                    # cv2.imwrite(template_fn+'_temp_mask_inv.png', template_mask_inv1)
                    # cv2.imwrite(template_fn+'_matching.png', matching_pixels_inv1)
                    # exit(1)

                    if similarity_score >= similarity_threshold:
                        print(f'  - {similarity_score:.2%} template match: {template_fn_}')

                        # Compute the absolute difference between the image and the template
                        diff = cv2.absdiff(handwriting_img, template_img)

                        # Convert the difference image to grayscale
                        _, diff_mask = cv2.threshold(diff, 120, 255, cv2.THRESH_BINARY)

                        # Inpaint the image using the mask
                        filtered_result = cv2.bitwise_not(diff_mask)

                        removed_template = True

                        template_image_found = template_img

                        if DEBUG_MODE:
                            cv2.imwrite(template_debug_fn+'_diff.png', diff)
                            cv2.imwrite(template_debug_fn+'diff_mask.png', diff_mask)
                            cv2.imwrite(template_debug_fn+'_filtered_result.png', filtered_result)

                        return filtered_result, removed_template, template_image_found

        print('  - No standard template detected                                                           ')
    except Exception as e:
        print(f'*** detect_remove_template: {e}.')
        print(f'Template: {template_fn}')
    return filtered_result, removed_template, template_image_found


def image2vectors(
    image_fn, image=None, line_height_threshold=LINE_HEIGHT_THRESHOLD,
        adaptative_thresholding=ADAPTATIVE_THRESHOLDING,
        gaussian_blur=GAUSSIAN_BLUR,
        intensity=INTENSITY,
        detail_level=DETAIL_LEVEL,
        noise_reduction=NOISE_REDUCTION,
        shadow_blur_factor=SHADOW_BLUR,
        thinning=THINNING,
        density_min_area=DENSE_MIN_AREA,
        density_threshold=DENSE_THRESHOLD,
        density_sensitivity=DENSE_SENSITIVITY,
        resizing_list=None,
        pre_denoise=PRE_DENOISE,
        remove_templates=REMOVE_TEMPLATES,
        series=NOTEBOOK_DEVICE):
    """ Reads an image and outputs a list of paths and a list of contours """
    try:
        paths_dict = {}
        paths_list = []
        dense_contours = None
        path_index = 0
        template_removed = False
        template_image = None

        screen_width, screen_height, _, _ = series_bounds(series)

        if image is None:
            # Step 1: Load and preprocess the image
            image = cv2.imread(image_fn, cv2.IMREAD_GRAYSCALE)

        if DEBUG_MODE:
            cv2.imwrite(image_fn+'_1_source.png', image)

        if remove_templates:
            try:
                image, template_removed, template_image = detect_remove_template(
                    image, image_fn, resizing_list=resizing_list, series=series)
            finally:
                pass

            try:
                if template_removed:
                    pass
                else:
                    image, template_removed, template_image = remove_lines(image, image_fn, debug=DEBUG_MODE, pre_denoise=pre_denoise)

            except Exception as e:
                print(f'**- Autodetection of pattern: {e}')
            finally:
                pass
        else:
            template_image = None

        if template_image is not None:
            gray_level = 157
            template_image[template_image < 255] = gray_level

        if DEBUG_MODE:
            if image is not None:
                cv2.imwrite(image_fn+'_filtered.png', image)
            if template_image is not None:
                cv2.imwrite(image_fn+'_template.png', template_image)

        # Find dense objects (=complex images) in the image and contour them
        dense_contours = detect_dense_objects(
            image, density_min_area=density_min_area,
            density_sensitivity=density_sensitivity,
            density_threshold=density_threshold)

        dense_contours_len = len(dense_contours)

        print(f'  - Dense areas: {dense_contours_len}')

        if dense_contours_len > 0:
            # "Pencilize" the dense contours found
            image = apply_selective_sketch(
                image, dense_contours, ksize=(gaussian_blur, gaussian_blur),
                intensity=intensity, shadow_amount=0.2,
                detail_level=detail_level,
                shadow_blur_factor=shadow_blur_factor,
                noise_reduction=noise_reduction)

        if DEBUG_MODE:
            visualize_contours(image, dense_contours, image_fn+'_sketch.png')

        image_height, image_width = image.shape  # Get image dimensions

        # Calculate scaling factors

        scale_x = screen_width / image_width
        scale_y = screen_height / image_height

        # To preserve the aspect ratio, scale uniformly based on the minimum of
        scale = min(scale_x, scale_y)
        if PRESERVE_ASPECT_RATIO:
            scale_x = scale
            scale_y = scale

        if SCREEN_CENTER:
            offset_x = (screen_width - image_width * scale_x) / 2
            offset_y = (screen_height - image_height * scale_y) / 2
        else:
            offset_x = offset_y = 0

        offset_x = offset_y = 0
        if DEBUG_MODE:
            cv2.imwrite(image_fn+'_2_b4_threshold.png', image)

        _, binary = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

        kernel = np.ones((3, 3), np.uint8)

        # Intializing thinning
        thinning_step = 0
        prev = binary.copy()

        while thinning_step < thinning:

            eroded = cv2.erode(prev, kernel, iterations=1)

            image = cv2.bitwise_not(eroded)

            _, prev = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

            if DEBUG_MODE:
                cv2.imwrite(image_fn + f'_3_thin_step{thinning_step}.png', image)

            thinning_step += 1

        # Ensure the image is binary (black and white)
        _, binary = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)

        # Create a kernel for morphological operations
        kernel = np.ones((2, 2), np.uint8)

        # Apply morphological operations to remove noise
        # 1. Dilate to connect nearby components
        dilated = cv2.dilate(binary, kernel, iterations=1)

        # 2. Remove small noise using area filtering
        # Find all connected components
        num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(dilated, connectivity=8)

        # Create a new image with only large components
        cleaned = np.zeros_like(dilated)

        # Skip the background component (index 0)

        for i in range(1, num_labels):
            if stats[i, cv2.CC_STAT_AREA] > 30:  # Adjust this threshold as needed
                cleaned[labels == i] = 255

        # 3. Close gaps in letters
        kernel_close = np.ones((3, 3), np.uint8)
        closed = cv2.morphologyEx(cleaned, cv2.MORPH_CLOSE, kernel_close)

        # 4. Smooth the edges
        kernel_smooth = np.ones((2, 2), np.uint8)
        smoothed = cv2.morphologyEx(closed, cv2.MORPH_OPEN, kernel_smooth)

        # Invert back to black text on white background
        thinned = cv2.bitwise_not(smoothed)

        if DEBUG_MODE:
            cv2.imwrite(image_fn+'_4_soothed.png', thinned)

        thresh = cv2.adaptiveThreshold(
            thinned,
            255,
            cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
            cv2.THRESH_BINARY_INV,
            11,
            2
        )

        # Perform morphological operations to remove small noise
        kernel = np.ones((2, 2), np.uint8)
        denoised = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

        # Dilate slightly to make text fuller
        kernel = np.ones((2, 2), np.uint8)
        dilated = cv2.dilate(denoised, kernel, iterations=1)

        # Invert back to black text on white background
        result = cv2.bitwise_not(dilated)

        # Apply one more slight blur to smoothen edges
        image = cv2.GaussianBlur(result, (3, 3), 0)

        if adaptative_thresholding:
            binary_image = cv2.adaptiveThreshold(
                image,
                maxValue=255,
                adaptiveMethod=cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                thresholdType=cv2.THRESH_BINARY_INV,
                blockSize=15,  # Must be odd and greater than 1
                C=10           # Constant subtracted from the mean
            )
        else:
            _, binary_image = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY_INV)

        print()
        print('  - Standard thinning ...')

        thinned_image = cv2.ximgproc.thinning(binary_image)

        if DEBUG_MODE:
            cv2.imwrite(image_fn+'_5_final_thinned.png', image)
            cv2.imwrite(image_fn+'_6_before_standard_thinning.png', binary_image)
            cv2.imwrite(image_fn+'_7_after_standard_thinning.png', thinned_image)

        # Step 2: Label connected components
        labeled_image = label(thinned_image, connectivity=2)

        # Step 3: Extract and sort coordinates for each path
        paths = {}
        path_bboxes = {}  # Dictionary to store bounding boxes
        num_labels = labeled_image.max()

        # print(f'   > Found {num_labels} labels')

        for label_num in range(1, num_labels + 1):

            print(f'  - Processing {label_num}/{num_labels}', end='', flush=True)

            coords = np.column_stack(np.where(labeled_image == label_num))

            if len(coords) > 1:

                # coords = sort_coordinates(coords) TODO: MMB Check impact removing this line

                # Check if the first and last points are the same or very close
                if np.allclose(coords[0], coords[-1], atol=1e-5):
                    coords = coords[:-1]  # Exclude the last point

            # Apply scaling to coordinates
            scaled_coords = np.copy(coords).astype(float)

            scaled_coords[:, 1] = scaled_coords[:, 1] * scale_x + offset_x  # x-coordinate
            scaled_coords[:, 0] = scaled_coords[:, 0] * scale_y + offset_y  # y-coordinate
            paths[label_num] = scaled_coords

            # Compute bounding box and mean positions
            y_coords = scaled_coords[:, 0]
            x_coords = scaled_coords[:, 1]
            min_y, max_y = y_coords.min(), y_coords.max()
            min_x, max_x = x_coords.min(), x_coords.max()
            path_bboxes[label_num] = {
                'min_y': min_y,
                'max_y': max_y,
                'min_x': min_x,
                'max_x': max_x,
                'mean_y': y_coords.mean(),
                'mean_x': x_coords.mean(), }
            print('', end='\r', flush=True)
        print()

        # Step 4: Group paths into lines based on vertical positions
        line_height_threshold = 30  # Adjust based on your image

        # Sort paths by mean_y (vertical position)
        sorted_paths = sorted(path_bboxes.items(), key=lambda item: item[1]['mean_y'])

        lines = []
        current_line = []
        current_line_mean_y = None

        for label_num, bbox in sorted_paths:
            mean_y = bbox['mean_y']

            if current_line and abs(mean_y - current_line_mean_y) > line_height_threshold:
                # Start a new line
                lines.append(current_line)
                current_line = [label_num]
                current_line_mean_y = mean_y
            else:
                # Add to current line
                current_line.append(label_num)
                if current_line_mean_y is not None:
                    current_line_mean_y = (current_line_mean_y + mean_y) / 2
                else:
                    current_line_mean_y = mean_y

        # Add the last line
        if current_line:
            lines.append(current_line)

        # Step 5: Sort paths within each line from left to right
        len_lines = len(lines)
        print(' ', end='\r', flush=True)
        print(f'  - Found {len_lines} paths')
        sorted_lines = []
        line_nm = 1
        for line in lines:
            # Extract the bounding boxes for paths in this line
            print(f'    vertical {line_nm}/{len_lines}', end='', flush=True)
            line_bboxes = [(label_num, path_bboxes[label_num]) for label_num in line]
            # Sort paths based on mean_x (horizontal position)
            sorted_line = sorted(line_bboxes, key=lambda item: item[1]['mean_x'])
            # Extract label numbers in sorted order
            sorted_line_labels = [item[0] for item in sorted_line]
            sorted_lines.append(sorted_line_labels)
            line_nm += 1
            print('                                           ', end='\r', flush=True)

        # Step 6: Save the paths to a text file in the sorted order
        for line_num, line in enumerate(sorted_lines, start=1):
            print(f'    horizontal {line_num}/{len_lines}', end='', flush=True)
            for label_num in line:
                coords = paths[label_num]
                path_points = []
                for y, x in coords:
                    current_point = [round(x), round(y)]
                    path_points.append(current_point)

                if len(path_points) >= MINIMUM_PATH_LENGTH:
                    ordered_points = order_stroke_points(path_points)
                    print('', end='', flush=True)
                    paths_list.append(ordered_points)
            print('                                           ', end='\r', flush=True)

        print()

        # Step 7: Compute and store contours
        for a_path in paths_list:
            dc_index = -1
            try:
                # Create a LineString from the path points
                line = LineString(a_path[:-1])

                # Generate a buffer (offset contour) around the line

                buffer_polygon = line.buffer(0.1, resolution=CONTOUR_RESOLUTION, cap_style='flat', join_style='round')

                # TODO: Check if this is more appropriate: offset_polygon = line.offset_curve(0.1)

                try:

                    # Extract the exterior coordinates of the buffer polygon
                    # contour_points_ = list(buffer_polygon.exterior.coords)
                    # contour_points = [[round(x[0]), round(x[1])] for x in contour_points_]

                    # Using the "normal" contours
                    boundary = buffer_polygon.boundary
                    list_contours = []

                    # If it's a MultiLineString
                    if type(boundary) is MultiLineString:

                        contour_points = []
                        for geom in boundary.geoms:
                            points = list(geom.coords)
                            contour_points.extend([[round(x[0], 4), round(x[1], 4)] for x in points])
                            list_contours.append([[round(x[0], 4), round(x[1], 4)] for x in points])

                    else:
                        # If it's a single LineString

                        boundary_points = list(boundary.coords)

                        contour_points = [[round(x[0], 4), round(x[1], 4)] for x in boundary_points]
                        list_contours.append([[round(x[0], 4), round(x[1], 4)] for x in boundary_points])

                    # # Approximating the contour with the path itself
                    # # (this is not ideal but somehow works)
                    # contour_points = [[round(x[0]), round(x[1])] for x in a_path]

                    # Check if a_path is in dense_contours
                    if dense_contours:
                        dc_index, _ = is_polygon_in_contours(a_path, dense_contours)
                    else:
                        dc_index = -1

                    paths_dict[str(path_index)] = {
                        "path": a_path, "contours": contour_points,
                        "dc_index": dc_index, "list_contours": list_contours}
                    # print(f'====len contour_points: {len(contour_points)}  -- len list_contours: {len(list_contours[0])}')
                    # print(list_contours)
                except Exception as e:
                    print(f'**- buffer_polygon: {e}')

                path_index += 1
            except Exception as e:
                print(f'**- buffer_polygon: {e}')

        if DEBUG_MODE:
            with open(image_fn+'_dict.txt', 'w', encoding='utf-8') as file:
                for apage, avalue in paths_dict.items():
                    file.write(f'path {apage}\n')
                    for apoint in avalue["path"]:
                        file.write(f'{round(apoint[0])},{round(apoint[1])}\n')

                    contour_index = 1

                    file.write(f'contour {contour_index}\n')
                    for apoint in avalue["contours"]:
                        file.write(f'{round(apoint[0])},{round(apoint[1])}\n')

    except Exception as e:
        print(f'*** image2vectors: {e}')
        return {}, template_image
    return paths_dict, template_image


def vectors2psd(
    paths_dict, unstitch=UNSTITCH, default_pen_size=DEFAULT_PEN_SIZE,
        default_pen_color=DEFAULT_PEN_COLOR, series=NOTEBOOK_DEVICE):
    """ Build a basic pen_strokes_dict object from vector points and contour vectors"""

    try:
        pen_strokes_dict = {}
        used_rand = set()
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)

        offset_x = OFFSET_T_X*max_horizontal_pixels/adb_screen_max_x
        offset_y = OFFSET_T_Y*max_vertical_pixels/adb_screen_max_y

        strokes = []
        strokes_nb = len(paths_dict.keys())
        # print(f'-----len paths_dict.items(): {len(paths_dict.items())}')
        # print(f'-----paths_dict.keys(): {paths_dict.keys()}')
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)
        for a_path_nb, a_path_bundle in paths_dict.items():

            # Intitialize and populate an individual stroke dict
            a_stroke_dict = {}

            # TODO: Keep this below
            list_contours = a_path_bundle['list_contours']

            contours_vector = a_path_bundle['contours']

            if UNSTITCH:
                # dc_index = a_path_bundle['dc_index']
                # a_stroke_dict['contours'] = {"1": contours_vector}
                # I don't understand why the below doesn't work

                a_stroke_dict['contours'] = {str(index + 1): value for index, value in enumerate(list_contours) if len(list_contours) > 0}

                x_coords, y_coords = zip(*contours_vector)

                # Calculate min, max, and averages
                min_x = round(min(x_coords) - offset_x)
                max_x = round(max(x_coords) - offset_x + 1)
                min_y = round(min(y_coords) - offset_y)
                max_y = round(max(y_coords) - offset_y + 1)
                avg_x = round(sum(x_coords) / len(x_coords) - 1)
                avg_y = round(sum(y_coords) / len(y_coords) - 1)
                a_stroke_dict['min_c_x'] = max(min(max_vertical_pixels - min_y, max_vertical_pixels), 0)
                a_stroke_dict['min_c_y'] = max(min(min_x, max_horizontal_pixels), 0)
                a_stroke_dict['max_c_x'] = max(min(max_vertical_pixels - max_y, max_vertical_pixels), 0)
                a_stroke_dict['max_c_y'] = max(min(max_x, max_horizontal_pixels), 0)
                a_stroke_dict['avg_c_x'] = max(min(max_vertical_pixels - avg_y, max_vertical_pixels), 0)
                a_stroke_dict['avg_c_y'] = max(min(avg_x, max_horizontal_pixels), 0)
            else:
                # dc_index = a_path_bundle['dc_index']
                a_stroke_dict['contours'] = {"1": contours_vector}
                # I don't understand why the below doesn't work
                # a_stroke_dict['contours'] = {str(index + 1): value for index, value in enumerate(list_contours)}
                if len(contours_vector) > 0:
                    x_coords, y_coords = zip(*contours_vector)

                    # Calculate min, max, and averages
                    min_x = round(min(x_coords) - offset_x)
                    max_x = round(max(x_coords) - offset_x + 1)
                    min_y = round(min(y_coords) - offset_y)
                    max_y = round(max(y_coords) - offset_y + 1)
                    avg_x = round(sum(x_coords) / len(x_coords) - 1)
                    avg_y = round(sum(y_coords) / len(y_coords) - 1)
                else:
                    min_x = min_x = min_x = min_x = min_x = 0

                a_stroke_dict['min_c_x'] = max(min(min_x, max_horizontal_pixels), 0)
                a_stroke_dict['min_c_y'] = max(min(min_y, max_vertical_pixels), 0)
                a_stroke_dict['max_c_x'] = max(min(max_x, max_horizontal_pixels), 0)
                a_stroke_dict['max_c_y'] = max(min(max_y, max_vertical_pixels), 0)
                a_stroke_dict['avg_c_x'] = max(min(avg_x, max_horizontal_pixels), 0)
                a_stroke_dict['avg_c_y'] = max(min(avg_y, max_vertical_pixels), 0)

            screen_vector_points = a_path_bundle['path']

            # denormalized_points = [topleft_to_topright(x, series=series, unstitch_mode=unstitch, offset_h=-OFFSET_T_X, offset_v=OFFSET_T_Y) for x in screen_vector_points if len(x) > 0]
            denormalized_points = [
                topleft_to_topright(
                    x, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x,
                    adb_screen_max_y, unstitch_mode=unstitch, offset_h=0, offset_v=0) for x in screen_vector_points if len(x) > 0]

            a_stroke_dict['vector_points'] = denormalized_points
            vector_size = len(screen_vector_points)
            a_stroke_dict['vector_size'] = vector_size
            a_stroke_dict['type'] = DEFAULT_PEN
            a_stroke_dict['color'] = HIGHLIGHTS_COLOR[default_pen_color][0]
            a_stroke_dict['weight'] = NEEDLE_POINT_SIZES[default_pen_size]
            a_stroke_dict['vector_pressure'] = [DEFAULT_PRESSURE] * vector_size
            a_stroke_dict['vector_one'] = [1] * vector_size

            draw_random = True

            # # # # Keep this code below Part A and Part B. I thought we could group same dense contour strokes
            # # # # by sharing random number but it doesn't work. logic may help later
            # # # if dc_index != -1:
            # # #     dc_index_str = str(dc_index)
            # # #     if dc_index_str in dc_index_dict:
            # # #         rand_unsigned_int = dc_index_dict[dc_index_str]
            # # #         draw_random = False
            # # #     else:
            # # #         # TODO: we should check that the number hasn't already been used
            # # #         draw_random = True

            while draw_random:
                rand_unsigned_int = random.randint(0, (2**32) - 1)
                draw_random = rand_unsigned_int in used_rand
                used_rand.add(rand_unsigned_int)

            # # # # Part B
            # # # if dc_index != -1:
            # # #     if dc_index_str not in dc_index_dict:
            # # #         dc_index_dict[dc_index_str] = rand_unsigned_int

            # rand_unsigned_int = random.randint(0, (2**32) - 1)
            a_stroke_dict['vector_unique'] = [rand_unsigned_int] * vector_size
            r_bytes = 'FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000F03F'
            if series != 'N5':
                # r_bytes += '0000000000000000007C050000500700000000000000040000006E6F6E65040000006E6F6E650000000001000000'
                r_bytes += '0000000000000000007C050000500700000000000000040000006E6F6E65040000006E6F6E6500000000030000000200000000000000000000000000000000000000'
            else:
                # r_bytes += '00000000000000000080070000000A00000000000000040000006E6F6E65040000006E6F6E650000000001000000'
                r_bytes += '00000000000000000080070000000A00000000000000040000006E6F6E65040000006E6F6E6500000000030000000200000000000000000000000000000000000000'

            a_stroke_dict['r_bytes'] = r_bytes

            strokes.append(a_stroke_dict)

        pen_strokes_dict['strokes'] = strokes
        pen_strokes_dict['strokes_nb'] = strokes_nb

    except Exception as e:
        print(f'*** vectors2psd: {e}')
        return {}

    return pen_strokes_dict


def upload_gen_file(file_name, source_fn, hist_dict, internal_prefix, external_prefix, transfer_mode, device=None, root_url=None):
    """ Upload a file to the SN
        In orig_fn, put the value of hist_dict[changed_file]"""
    try:
        if source_fn in hist_dict:
            source_sn_folder_list = hist_dict[source_fn].split('/')
        else:
            source_sn_folder_list = remove_storage_paths(source_fn).split('/')

        basename, ext_ = os.path.splitext(file_name)
        file_basename = os.path.basename(file_name)

        if transfer_mode == 'usb':  # For transfers via USB
            if source_sn_folder_list[0].lower() == 'sdcard':
                remote_folder_fn = external_prefix + '/'.join(source_sn_folder_list[1:-1])
            else:
                remote_folder_fn = internal_prefix + '/'.join(source_sn_folder_list[:-1])
            remote_destination_fn = remote_folder_fn + '/' + file_basename
            if os.path.exists(file_name):
                device.push(file_name, remote_destination_fn)

        elif transfer_mode == 'webserver':  # For transfers via WIFI
            try:
                source_sn_folder_path = ('/').join(source_sn_folder_list[:-1])
                url_export = f'{root_url}/{source_sn_folder_path}'
                rood_folder = source_sn_folder_list[0]
                if rood_folder not in SUPERNOTE_FOLDERS:
                    url_export = f'{root_url}{external_prefix}'+('/').join(source_sn_folder_list[1:-1])
                if os.path.exists(file_name):
                    asyncio.run(async_upload(file_name, url_export, file_basename))
            except Exception as e:
                print(e)
                print(f"**- Could not upload {f'{basename}.note'}. Is the SN access - Browse mode on?")

        elif transfer_mode == 'folder':  # For transfers via cloud sync
            try:
                local_file = source_sn_folder_list[0]
                relative_path = os.path.join(os.path.dirname(local_file), file_basename)
                dest_note_fn = os.path.join(REMOTE_BASE_PATH, relative_path)
                if os.path.exists(file_name):
                    shutil.copy(file_name, dest_note_fn)
            except Exception as e:
                print(e)
                print(f"**- Could not save file {file_name}. If you have it open, please close and try again.")

    except Exception as e:
        print(f'*** upload_file: {e}')


def skip_nth(elements, n):
    """
    Return a new list by skipping every nth element in `elements`.
    Elements are counted starting from 1.

    For example, if elements = [10, 20, 30, 40, 50, 60] and n = 2:
    skip_nth(elements, 2) -> [10, 30, 50]
    """
    return [
        element
        for i, element in enumerate(elements)
        # (i+1) because enumerate starts i at 0, but we want to skip
        # the "nth" item as if counting from 1
        if (i + 1) % n != 0
    ]


def fit_ephemeral_images(
    note_fn, pen_strokes_dict,
        image_crop=None, background_color=(254, 254, 254), line_width=3, pfix='', series=NOTEBOOK_DEVICE):
    """
    From a pen_strokes_dict, modifies a binary_data and returns it.
    if the current ephemeral image is larger than the generated image from the pen strokes
    """
    try:
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)

        # Read the binary file
        with open(note_fn, 'rb') as a_note_file:
            binary_data = a_note_file.read()

        # Parse the dictionary of totalpath objects
        for apage, avalue in pen_strokes_dict.items():

            # # # # Create a blank image with the specified background color
            # # # image = Image.new("RGBA", image_size, background_color)

            # Create a blank image with the specified background color
            if series in ['N5']:
                image = Image.open(BLANK_TEMPLATE_PICTURE2)
            else:
                image = Image.open(BLANK_TEMPLATE_PICTURE)

            draw = ImageDraw.Draw(image)

            # Read the location of the page image
            bitmap_location = avalue['bitmap']

            # Read the size of that image on binary
            _, bitmap_size = read_endian_int_at_position(binary_data, bitmap_location)

            # Retrieve the list of all pen stroke objects on page "apage"
            a_list_strokes = avalue['strokes']

            # Parse each stroke on that page and draw it.
            # IMPORTANT: We deliberatly skip the last stroke to increase the chances of
            # fitting within the existing space. After all, this is an ephemeral image

            a_list_strokes = skip_nth(a_list_strokes, 10)

            for a_stroke in a_list_strokes:

                a_stroke_color = a_stroke['color']

                line_color = (a_stroke_color, a_stroke_color, a_stroke_color)
                a_stroke_vector_points = a_stroke['vector_points']
                normalized_points = [
                    topright_to_topleft(
                        x, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x,
                        adb_screen_max_y) for x in a_stroke_vector_points]

                a_stroke_vector_size = a_stroke['vector_size']

                if a_stroke_vector_size > 1:  # Only draw if there are at least two points
                    draw.line(normalized_points, fill=line_color, width=line_width)

            # Crop if necessary
            if image_crop is not None:
                image = image.crop((image_crop[0], image_crop[1], image_crop[0]+image_crop[2], image_crop[1]+image_crop[3]))

            # Encode the image
            image = image.convert('L')
            rleimage, encoded = rle_encode_img(image)

            if encoded:
                # Now check if the size fits in the current bitmap space
                new_image_size = len(rleimage)

                # If size fits, create an entry in the dictionary
                if new_image_size <= bitmap_size:

                    # Encode the image size
                    image_size_bytes = int_to_little_endian_bytes(new_image_size)

                    # Append the size to create the full image block
                    full_image_block = image_size_bytes + rleimage

                    lenimb = len(full_image_block)

                    # Edit the binary data. Supernote should ignore extra bytes left
                    binary_data = binary_data[:bitmap_location] + full_image_block + binary_data[bitmap_location+lenimb:]

                    # image.show()
                else:
                    print()
                    print(f"     **- fit_ephemeral_images: NO PREVIEW for Page #{apage} ({new_image_size} > {bitmap_size} bytes)")

        # Write the binary file
        if pfix != '':
            basename, ext_ = os.path.splitext(note_fn)
            mod_fn = basename + pfix + ext_
        else:
            mod_fn = note_fn

        with open(mod_fn, 'wb') as file:
            file.write(binary_data)

    except Exception as e:
        print(f'*** fit_ephemeral_images: {e}')
        return None

    return mod_fn


def generate_ephemeral_images(
    pen_strokes_dict,
        image_crop=None, background_color=(254, 254, 254, 254), line_width=3, series=NOTEBOOK_DEVICE):
    """
    From a pen_strokes_dict, modifies a binary_data and returns it.
    if the current ephemeral image is larger than the generated image from the pen strokes
    """
    try:
        full_image_dict = {}
        a_stroke_color = None
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)

        # Parse the dictionary of totalpath objects
        for apage, avalue in pen_strokes_dict.items():

            full_image_block = None

            # Create a blank image with the specified background color
            if series in ['N5']:
                image = Image.open(BLANK_TEMPLATE_PICTURE2)
            else:
                image = Image.open(BLANK_TEMPLATE_PICTURE)

            draw = ImageDraw.Draw(image)

            # Retrieve the list of all pen stroke objects on page "apage"
            a_list_strokes = avalue['strokes']

            # Parse each stroke on that page and draw it
            for a_stroke in a_list_strokes:

                a_stroke_color = a_stroke['color']

                # line_color = (a_stroke_color, a_stroke_color, a_stroke_color)
                line_color = a_stroke_color
                a_stroke_vector_points = a_stroke['vector_points']
                normalized_points = [
                    topright_to_topleft(
                        x, max_horizontal_pixels, max_vertical_pixels,
                        adb_screen_max_x, adb_screen_max_y) for x in a_stroke_vector_points]
                a_stroke_vector_size = a_stroke['vector_size']

                if a_stroke_vector_size > 1:  # Only draw if there are at least two points
                    draw.line(normalized_points, fill=line_color, width=line_width)
                else:
                    print(a_stroke_vector_size)

            # Crop if necessary
            if image_crop is not None:
                image = image.crop((image_crop[0], image_crop[1], image_crop[0]+image_crop[2], image_crop[1]+image_crop[3]))

            # Encode the image
            image = image.convert('L')
            # image.show()

            rleimage, encoded = rle_encode_img(image)

            if encoded:
                # Now check if the size fits in the current bitmap space
                new_image_size = len(rleimage)

                # Encode the image size
                image_size_bytes = int_to_little_endian_bytes(new_image_size)

                # Append the size to create the full image block
                full_image_block = image_size_bytes + rleimage

                print(f'  - Generated {len(full_image_block)} bytes image')

            full_image_dict[apage] = full_image_block

    except Exception as e:
        print(f'*** generate_ephemeral_images: {e}')
        return None

    return full_image_dict


def replace_note_strokes(
    note_file_binary, pen_strokes_list, pen_strokes_size="bytes_nb",
        vector_points_key="replace", vector_size_key="points_nb", offset_location=220):
    """ Opens a notebook and replaces vector-points at certain positions
        Parameters:
            - note_file_binary: binary file variable
            - pen_strokes_list: a list of pen strokes as a dictionary
            - offset_location: the position of the vector points in the binary, relative to the address """
    try:
        completed = False
        stroke_color = signed_to_bytes(HIGHLIGHTS_COLOR[CONVERTED_SHAPES_COLOR][0], 1)
        # Parse the list of pen strokes
        for a_pen_stroke in pen_strokes_list:

            # Read the location of the pen stroke
            ps_address = a_pen_stroke['address']
            color_position = ps_address + 8
            a_pen_stroke_location = ps_address + offset_location

            # Read the vector of points
            vector_points = a_pen_stroke[vector_points_key]

            vector_points_bytes = None

            # convert each point of the vector to bytes
            for x in vector_points:

                x_0 = signed_to_bytes(round(float(np.float32(x[0]))), 4)
                x_1 = signed_to_bytes(round(float(np.float32(x[1]))), 4)

                if vector_points_bytes is None:
                    vector_points_bytes = x_0
                    vector_points_bytes += x_1
                else:
                    vector_points_bytes += x_0
                    vector_points_bytes += x_1

            a_pen_stroke_remainder_start = a_pen_stroke_location + len(vector_points_bytes)

            # Set the color of the pen stroke to the global variable, the 9th byte after the
            # stroke address in the file ( 4 bytes for size, the 1 by for pen type, then 3 zero bytes, then color byte)
            note_file_binary = note_file_binary[:color_position] + stroke_color + note_file_binary[color_position+1:]

            # Replace the vector points in the binary
            note_file_binary = note_file_binary[:a_pen_stroke_location] + vector_points_bytes + note_file_binary[a_pen_stroke_remainder_start:]

        completed = True

    except Exception as e:
        print(f'*** replace_note_strokes: {e}')
    return note_file_binary, completed


def generate_line_points(p1, p2, num_points):
    """
    Generates num_points evenly spaced points along a line from p1 to p2.
    """
    x_values = np.linspace(p1[0], p2[0], num=num_points)
    y_values = np.linspace(p1[1], p2[1], num=num_points)
    return list(zip(x_values, y_values))


def generate_triangle_perimeter_points(p1, p2, p3, num_points):
    """
    Generates num_points evenly spaced points along the perimeter of a triangle formed by p1, p2, and p3.
    """
    # Calculate the length of each side
    side1_length = np.linalg.norm(np.array(p2) - np.array(p1))
    side2_length = np.linalg.norm(np.array(p3) - np.array(p2))
    side3_length = np.linalg.norm(np.array(p1) - np.array(p3))
    perimeter = side1_length + side2_length + side3_length

    # Determine the number of points on each side proportional to the side length
    num_points_side1 = int(num_points * (side1_length / perimeter))
    num_points_side2 = int(num_points * (side2_length / perimeter))
    num_points_side3 = num_points - num_points_side1 - num_points_side2  # Remaining points

    # Ensure at least two points per side
    num_points_side1 = max(num_points_side1, 2)
    num_points_side2 = max(num_points_side2, 2)
    num_points_side3 = max(num_points_side3, 2)

    # Generate points for each side
    side1_points = generate_line_points(p1, p2, num_points_side1)  # [:-1]  # Exclude last point to prevent duplicates
    side2_points = generate_line_points(p2, p3, num_points_side2)
    side3_points = generate_line_points(p3, p1, num_points_side3)

    # Combine all points
    triangle_points = side1_points + side2_points + side3_points
    return triangle_points


def generate_circle_points(center, radius, num_points):
    angles = np.linspace(0, 2 * np.pi, num_points, endpoint=False)
    x_values = center[0] + radius * np.cos(angles)
    y_values = center[1] + radius * np.sin(angles)
    return list(zip(x_values, y_values))


def generate_rectangle_perimeter_points(p1, p2, p3, p4, num_points):
    """
    Generates num_points evenly spaced points along the perimeter of a rectangle formed by p1, p2, p3, and p4.
    Assumes that the points are ordered consecutively around the rectangle.
    """
    # Calculate the length of each side
    side1_length = np.linalg.norm(np.array(p2) - np.array(p1))
    side2_length = np.linalg.norm(np.array(p3) - np.array(p2))
    side3_length = np.linalg.norm(np.array(p4) - np.array(p3))
    side4_length = np.linalg.norm(np.array(p1) - np.array(p4))
    perimeter = side1_length + side2_length + side3_length + side4_length

    # Determine the number of points on each side proportional to the side length
    num_points_side1 = int(num_points * (side1_length / perimeter))
    num_points_side2 = int(num_points * (side2_length / perimeter))
    num_points_side3 = int(num_points * (side3_length / perimeter))
    num_points_side4 = num_points - num_points_side1 - num_points_side2 - num_points_side3  # Remaining points

    # Ensure at least two points per side
    num_points_side1 = max(num_points_side1, 2)
    num_points_side2 = max(num_points_side2, 2)
    num_points_side3 = max(num_points_side3, 2)
    num_points_side4 = max(num_points_side4, 2)

    # Adjust total points in case of over-allocation due to minimum points per side
    total_allocated_points = num_points_side1 + num_points_side2 + num_points_side3 + num_points_side4
    if total_allocated_points > num_points:
        # Reduce points from the side with the longest length
        side_lengths = [side1_length, side2_length, side3_length, side4_length]
        side_points = [num_points_side1, num_points_side2, num_points_side3, num_points_side4]
        max_side_index = side_lengths.index(max(side_lengths))
        side_points[max_side_index] -= total_allocated_points - num_points
        # Update the number of points per side
        num_points_side1, num_points_side2, num_points_side3, num_points_side4 = side_points

    # Generate points for each side
    side1_points = generate_line_points(p1, p2, num_points_side1)  # [:-1]  # Exclude last point to prevent duplicates
    side2_points = generate_line_points(p2, p3, num_points_side2)
    side3_points = generate_line_points(p3, p4, num_points_side3)
    side4_points = generate_line_points(p4, p1, num_points_side4)

    # Combine all points
    rectangle_points = side1_points + side2_points + side3_points + side4_points
    return rectangle_points


def process_shapes(shape_dict):
    """ Generates the ideal vectors to replace the handwritten shape
        We chose to replace by the exact same amount of points, to avoid rebuilding
        the binary from scratch
        TODO: Do the same for the 'envelope' of the pen stroke 'contours' for selection """
    for shape_type, pen_strokes in shape_dict.items():
        for pen_stroke in pen_strokes:
            points_nb = pen_stroke['points_nb']
            perfect_shape_points = pen_stroke['points']

            if shape_type == 'line':
                p1, p2 = perfect_shape_points
                generated_points = generate_line_points(p1, p2, points_nb)
            elif shape_type == 'triangle':
                p1, p2, p3 = perfect_shape_points
                generated_points = generate_triangle_perimeter_points(p1, p2, p3, points_nb)
            elif shape_type == 'rectangle':
                p1, p2, p3, p4 = perfect_shape_points
                generated_points = generate_rectangle_perimeter_points(p1, p2, p3, p4, points_nb)
            elif shape_type == 'circle':
                center, radius = perfect_shape_points  # Assuming points = [center_x, center_y], radius
                generated_points = generate_circle_points(center, radius, points_nb)
            else:
                generated_points = []
                print(f"Shape type '{shape_type}' is not supported.")

            pen_stroke['replace'] = generated_points


def compute_best_fit_line(points):
    x, y = zip(*points)
    x = np.array(x)
    y = np.array(y)
    A = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(A, y, rcond=None)[0]
    return m, c


def generate_perfect_line(points, original_points):
    m, c = compute_best_fit_line(points)
    x_vals = [point[0] for point in points]
    x_min, x_max = min(x_vals), max(x_vals)
    y_min = m * x_min + c
    y_max = m * x_max + c
    perfect_line_normalized = [(x_min, y_min), (x_max, y_max)]
    return perfect_line_normalized


def generate_perfect_triangle(vertices, original_points):
    centroid = np.mean(vertices, axis=0)
    # Compute average distance from centroid
    distances = [np.linalg.norm(vertex - centroid) for vertex in vertices]
    avg_distance = np.mean(distances)
    # Generate perfect triangle vertices
    angles = np.linspace(0, 2 * np.pi, 4)[:-1]  # 0, 120, 240 degrees
    perfect_vertices_normalized = [
        (
            centroid[0] + avg_distance * np.cos(angle),
            centroid[1] + avg_distance * np.sin(angle)
        )
        for angle in angles
    ]
    return perfect_vertices_normalized


def generate_perfect_rectangle(vertices, original_points):
    # Convert vertices to a NumPy array for arithmetic operations
    vertices = np.array(vertices)

    # Calculate the centroid of the rectangle
    centroid = np.mean(vertices, axis=0)

    # Compute the orientation of the rectangle
    dx = vertices[1][0] - vertices[0][0]
    dy = vertices[1][1] - vertices[0][1]
    angle = np.arctan2(dy, dx)

    # Compute average width and height
    widths = [np.linalg.norm(vertices[i] - vertices[(i + 1) % 4]) for i in range(0, 4, 2)]
    heights = [np.linalg.norm(vertices[i] - vertices[(i + 1) % 4]) for i in range(1, 4, 2)]
    width = np.mean(widths)
    height = np.mean(heights)

    # Generate perfect rectangle vertices in normalized coordinates
    perfect_vertices_normalized = create_rectangle_vertices(centroid, width, height, angle)

    return perfect_vertices_normalized


def generate_perfect_circle(fit_result, original_points):
    center_normalized, radius_normalized, _ = fit_result
    # Map back to original coordinates
    center_original = center_normalized
    radius_original = radius_normalized  # * average_range(original_points)
    return center_original, radius_original


def average_range(points):
    min_x, max_x, min_y, max_y = get_bounding_box(points)
    return (max_x - min_x + max_y - min_y) / 2 or 1


def create_rectangle_vertices(centroid, width, height, angle):
    half_width = width / 2
    half_height = height / 2
    cos_a = np.cos(angle)
    sin_a = np.sin(angle)
    offsets = [
        (-half_width, -half_height),
        (half_width, -half_height),
        (half_width, half_height),
        (-half_width, half_height)
    ]
    return [
        (
            centroid[0] + dx * cos_a - dy * sin_a,
            centroid[1] + dx * sin_a + dy * cos_a
        )
        for dx, dy in offsets
    ]


def get_bounding_box(points):
    if not points:
        return 0, 0, 0, 0
    x_coords, y_coords = zip(*points)
    min_x, max_x = min(x_coords), max(x_coords)
    min_y, max_y = min(y_coords), max(y_coords)
    return min_x, max_x, min_y, max_y


def normalize_points(
    points, max_horizontal_pixels, max_vertical_pixels,
        adb_screen_max_x, adb_screen_max_y, flip=True):
    if flip:
        normalized_points = [
            topright_to_topleft(
                x, max_horizontal_pixels, max_vertical_pixels,
                adb_screen_max_x, adb_screen_max_y) for x in points]
    else:
        normalized_points = points
    return normalized_points


def denormalize_points(
    points, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y,
        flip=True, series=NOTEBOOK_DEVICE):
    if flip:
        normalized_points = [
            topleft_to_topright(
                x, max_horizontal_pixels, max_vertical_pixels,
                adb_screen_max_x, adb_screen_max_y) for x in points]
    else:
        normalized_points = points
    return normalized_points


def merge_close_vertices(points, distance_tolerance):
    """
    Merges vertices that are closer than the specified distance tolerance.
    Handles closed shapes by checking the distance between the last and first point.
    """
    if not points:
        return []

    CLOSE_VICINITY = 50

    # Check if the shape is closed
    is_closed = np.linalg.norm(np.array(points[0]) - np.array(points[-1])) < CLOSE_VICINITY
    if is_closed and len(points) > 1:
        # points = points[:-1]  # Remove the duplicate last point
        points.append(points[0])

    merged_points = [points[0]]
    for point in points[1:]:
        last_point = merged_points[-1]
        distance = np.linalg.norm(np.array(point) - np.array(last_point))
        if distance < distance_tolerance:
            merged_points[-1] = last_point
        else:
            merged_points.append(point)

    if is_closed:
        # After merging, close the shape by appending the first point
        if np.linalg.norm(np.array(merged_points[0]) - np.array(merged_points[-1])) >= distance_tolerance:
            merged_points.append(merged_points[0])

    return merged_points


def simplify_path(points, tolerance):
    if len(points) < 2:
        # Not enough points to simplify; return the original points
        return points
    line = LineString(points)
    simplified_line = line.simplify(tolerance, preserve_topology=False)
    return list(simplified_line.coords)


def is_line(points, tolerance):
    if len(points) < 2:
        return False
    x, y = zip(*points)
    x = np.array(x)
    y = np.array(y)
    A = np.vstack([x, np.ones(len(x))]).T
    try:
        m, c = np.linalg.lstsq(A, y, rcond=None)[0]
    except np.linalg.LinAlgError:
        return False
    residuals = y - (m * x + c)
    return np.all(np.abs(residuals) < tolerance)


def calculate_angle(p1, p2, p3):
    a = np.array(p1) - np.array(p2)
    b = np.array(p3) - np.array(p2)
    norm_a = np.linalg.norm(a)
    norm_b = np.linalg.norm(b)
    if norm_a == 0 or norm_b == 0:
        return 0
    cosine_angle = np.dot(a, b) / (norm_a * norm_b)
    cosine_angle = np.clip(cosine_angle, -1.0, 1.0)  # Ensure within valid range
    angle = np.arccos(cosine_angle)
    return np.degrees(angle)


def is_rectangle(vertices, angle_tolerance):
    if len(vertices) != 4:
        return False
    angles = []
    for i in range(4):
        p1 = vertices[i - 1]
        p2 = vertices[i]
        p3 = vertices[(i + 1) % 4]
        angle = calculate_angle(p1, p2, p3)
        angles.append(angle)

    return all(abs(angle - 90) < angle_tolerance for angle in angles)


def is_triangle(vertices, angle_tolerance):
    if len(vertices) != 3:
        return False
    angles = []
    for i in range(3):
        p1 = vertices[i - 1]
        p2 = vertices[i]
        p3 = vertices[(i + 1) % 3]
        angle = calculate_angle(p1, p2, p3)
        angles.append(angle)
    total_angle = sum(angles)
    return abs(total_angle - 180) < angle_tolerance


def fit_circle(points):
    if len(points) < 3:
        return None
    x = np.array([p[0] for p in points])
    y = np.array([p[1] for p in points])
    x_m = np.mean(x)
    y_m = np.mean(y)
    u = x - x_m
    v = y - y_m
    Suu = np.sum(u**2)
    Suv = np.sum(u * v)
    Svv = np.sum(v**2)
    Suuu = np.sum(u**3)
    Suvv = np.sum(u * v**2)
    Svvv = np.sum(v**3)
    Svuu = np.sum(v * u**2)
    A = np.array([[Suu, Suv], [Suv, Svv]])
    B = np.array([0.5 * (Suuu + Suvv), 0.5 * (Svvv + Svuu)])
    try:
        uc, vc = np.linalg.solve(A, B)
    except np.linalg.LinAlgError:
        return None
    center = (uc + x_m, vc + y_m)
    radius = np.mean(np.sqrt((x - center[0])**2 + (y - center[1])**2))
    residuals = np.sqrt((x - center[0])**2 + (y - center[1])**2) - radius
    return center, radius, residuals


def is_circle(points, circle_tolerance):
    result = fit_circle(points)
    if result is not None:
        center, radius, residuals = result
        max_residual = np.max(np.abs(residuals))
        return max_residual < circle_tolerance
    return False


def calculate_bounding_box_area(points):
    """
    Calculates the area of the bounding box of the given points.
    """
    if not points:
        return 0
    min_x, max_x, min_y, max_y = get_bounding_box(points)
    width = abs(max_x - min_x)
    height = abs(max_y - min_y)
    area = width * height
    return area


def sort_points_clockwise(points):
    # Find center point
    center_x = sum(x for x, _ in points) / len(points)
    center_y = sum(y for _, y in points) / len(points)

    # Sort points based on angle from top-left point relative to center
    return sorted(points, key=lambda p: (
        -math.atan2(p[1] - center_y, p[0] - center_x) % (2 * math.pi)
    ))


def classify_shape(points, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y):
    if len(points) < 2:
        return None

    flip = True

    normalized_points = normalize_points(
        points, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x,
        adb_screen_max_y, flip=flip)

    # Calculate the bounding box area
    area = calculate_bounding_box_area(points)

    # Shape-specific tolerances
    tolerances = {
        'merge_distance': 20,    # Tolerance for merging close vertices (normalized units)
        'simplification': 20,    # General simplification tolerance
        'line': 0.05,             # Tolerance for line residuals
        'circle': 25,            # Tolerance for circle residuals
        'angle': 25                # Angle tolerance in degrees
    }

    # Merge close vertices
    merged_points = merge_close_vertices(normalized_points, tolerances['merge_distance'])

    # print(merged_points)

    # Simplify path
    simplified_points = simplify_path(merged_points, tolerance=tolerances['simplification'])

    simplified_points = sorted(simplified_points, key=lambda x: (x[0], x[1]))

    simplified_points_ = merge_close_vertices(simplified_points, tolerances['merge_distance'])

    simplified_points = sort_points_clockwise(simplified_points_)

    num_vertices = len(simplified_points)

    # Proceed with classification only if we have enough points
    if num_vertices < 2:
        return None

    # Classification logic
    if num_vertices == 2:
        if is_line(simplified_points, tolerance=tolerances['line']):
            if area < MIN_SHAPE_AREA*100:
                return None
            if math.sqrt(
                (simplified_points[0][0]-simplified_points[1][0])**2 + (
                    simplified_points[0][1]-simplified_points[1][1])**2) < 200:
                return None
            simplified_points = denormalize_points(
                simplified_points, max_horizontal_pixels, max_vertical_pixels,
                adb_screen_max_x, adb_screen_max_y, flip=flip)
            return {"shape": "line", "points": simplified_points}

    elif num_vertices == 3:
        if is_triangle(simplified_points, tolerances['angle']):
            if area < MIN_SHAPE_AREA*100:
                return None
            simplified_points = denormalize_points(
                simplified_points, max_horizontal_pixels, max_vertical_pixels,
                adb_screen_max_x, adb_screen_max_y, flip=flip)
            return {"shape": "triangle", "points": simplified_points}

    elif num_vertices == 4:
        if is_rectangle(simplified_points, angle_tolerance=tolerances['angle']):
            if area < MIN_SHAPE_AREA*50:
                return None
            simplified_points = denormalize_points(
                simplified_points, max_horizontal_pixels, max_vertical_pixels,
                adb_screen_max_x, adb_screen_max_y, flip=flip)
            return {"shape": "rectangle", "points": simplified_points}

    elif is_circle(simplified_points, tolerances['circle']):
        simplified_points = denormalize_points(
            simplified_points, max_horizontal_pixels, max_vertical_pixels,
            adb_screen_max_x, adb_screen_max_y, flip=flip)

        fit_result = fit_circle(simplified_points)
        perfect_shape = generate_perfect_circle(fit_result, points)
        return {"shape": "circle", "points": perfect_shape}
    else:
        if area < MIN_SHAPE_AREA*100:
            return None

    return None


def docx_to_markdown(docx_file):
    doc = docx.Document(docx_file)
    markdown_text = ""

    def get_list_level(paragraph):
        try:
            return int(paragraph._element.pPr.numPr.ilvl.val)
        except AttributeError:
            return 0

    for para in doc.paragraphs:

        if para.style.name.startswith('Heading'):
            header_level = int(para.style.name.split()[-1])
            if 1 <= header_level <= 4:
                markdown_text += f"{'#' * header_level} {para.text}\n\n"

        elif para.style.name == 'List Bullet' or para.style.name == 'List Number':
            level = get_list_level(para)
            if level == 0:
                indent = ""
            else:
                indent = "   " * level  # Three spaces per level after the first

            bullet = "-" if para.style.name == 'List Bullet' else "1."
            markdown_text += f"{indent}{bullet} {para.text}\n"

        elif any(run.bold for run in para.runs):
            bold_text = "".join([f"**{run.text.strip()}** " if run.bold else run.text for run in para.runs])
            markdown_text += bold_text.strip() + "\n\n"

        else:
            markdown_text += para.text + "\n\n"

    return markdown_text


def topleft_to_topright(
    a_point, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y,
        unstitch_mode=False, offset_h=0, offset_v=0):
    """ Screen coordinates conversion
        Warning: reverses x & y """
    if unstitch_mode:
        a_reversed_point = (
            max(min(
                round(adb_screen_max_y - (adb_screen_max_y-(a_point[0]*adb_screen_max_x/max_horizontal_pixels)+OFFSET_T_X) + offset_v), adb_screen_max_y), 0),
            max(min(
                round(a_point[1]*adb_screen_max_x/max_vertical_pixels)-OFFSET_T_Y + offset_h, adb_screen_max_x), 0))
    else:
        a_reversed_point = (
            max(min(
                round(a_point[1]*adb_screen_max_y/max_vertical_pixels-OFFSET_T_Y+a_point[1]*offset_v/max_vertical_pixels), adb_screen_max_y), 0),
            max(min(round(adb_screen_max_x-(a_point[0]*adb_screen_max_x/max_horizontal_pixels)+OFFSET_T_X+a_point[0]*offset_h/max_horizontal_pixels), adb_screen_max_x), 0))
    return a_reversed_point


def topright_to_topleft(a_reversed_point, max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y):
    """ Screen coordinates conversion
        Warning: reverts back to x & y """
    a_point = (
        max(min(
            round((adb_screen_max_x+OFFSET_T_X-a_reversed_point[1])*max_horizontal_pixels/adb_screen_max_x), max_horizontal_pixels), 0),
        max(min(round((a_reversed_point[0]+OFFSET_T_Y)*max_vertical_pixels/adb_screen_max_y), max_vertical_pixels), 0)
    )
    return a_point


def draw_bitmap_from_vectors(vector_dict, image_size=(500, 500), image_crop=None, background_color=(255, 255, 255), line_width=3):
    """
    Draws a bitmap from a list of vectors of points.
    Args:
    - vectors: List of vectors, where each vector is a list of (x, y) points.
    - image_size: Tuple specifying the size of the output image (width, height).
    - background_color: Tuple representing the RGB color of the background.
    - line_color: Tuple representing the RGB color of the drawn lines.
    - line_width: Width of the lines.
    Returns:
    - An Image object containing the drawn bitmap.
    """
    try:
        # Create a blank image with the specified background color
        image = Image.open(BLANK_TEMPLATE_PICTURE2)

        # image = Image.new("RGB", image_size, background_color)
        image = image.convert('L')
        draw = ImageDraw.Draw(image)

        for a_color_str, vectors in vector_dict.items():

            a_color = int(a_color_str)
            # line_color = (a_color, a_color, a_color)
            line_color = a_color

            for vector in vectors:
                # Iterate over each vector (path of points) and draw lines between the points

                if len(vector) > 1:  # Only draw if there are at least two points
                    draw.line(vector, fill=line_color, width=line_width)

        if image_crop is not None:
            image = image.crop((image_crop[0], image_crop[1], image_crop[0]+image_crop[2], image_crop[1]+image_crop[3]))

    except Exception as e:
        print(f'*** draw_bitmap_from_vectors: {e}')
        return None
    return image


def read_json(afilename):
    """ Reading a json from a filename """
    try:
        with open(afilename, 'r', encoding='utf-8') as file:
            a_json_text = file.read().strip().replace("\ufeff", "")
            return json.loads(a_json_text)
    except Exception as e:
        print()
        print(f'*** read_json: {e}')
        return {}


def save_json(afilename, ajson):
    """ Saving a json with identation """
    with open(afilename, 'w') as file:
        file.write(json.dumps(ajson, indent=4))


def extract_metadata(source_fn, json_fn):
    """ Extracts .note or .mark metadata, store it in a json_fn """
    metadata = None
    # Retrieving base filename and extension
    basename, extension = os.path.splitext(source_fn)
    if extension.lower() in ['.note', '.mark']:
        try:
            with open(source_fn, 'rb') as f:
                metadata = sn.parse_metadata(f)
            # Saving the metadata
            metadata_json = metadata.to_json(indent=4)
            if DEBUG_MODE:
                with open(json_fn, 'w') as file:
                    file.write(metadata_json)
            return metadata_json
        except Exception as e:
            print(f'Error in extract_metadata - type: {type(e).__name__}')
            print(f'Error message: {str(e)}')
            return {}


def intersection_area(rect1, rect2, threshold=0.95):
    """ Checks if 2 rects are intersecting above a threshold """
    # Unpack the coordinates of the rectangles

    x0, y0, x1, y1 = rect1
    a0, b0, a1, b1 = rect2

    rect1_area = (x1-x0)*(y1-y0)
    rect2_area = (a1-a0)*(b1-b0)
    rect_area = min(rect1_area, rect2_area)

    # Find the coordinates of the intersection rectangle
    x_left = max(x0, a0)
    y_bottom = max(y0, b0)
    x_right = min(x1, a1)
    y_top = min(y1, b1)

    # Check if there is no overlap
    if x_left > x_right or y_bottom > y_top:
        return False

    # Calculate the width and height of the intersection rectangle
    width = x_right - x_left
    height = y_top - y_bottom
    coverage = width * height / rect_area
    if coverage > threshold:
        return True
    return False


def decimal_to_custom_ieee754(value, offset=0, num_bytes=4):
    try:
        result = None
        # Adjust the value by the offset
        adjusted_value = value - offset

        # Handle special case for 0
        if adjusted_value == 0:
            return bytes.fromhex('00000000')

        # Determine the sign
        sign = 0 if adjusted_value >= 0 else 1
        adjusted_value = abs(adjusted_value)

        # Calculate the exponent and mantissa
        exponent = 0
        mantissa = adjusted_value

        while mantissa >= 2:
            mantissa /= 2
            exponent += 1

        while mantissa < 1:
            mantissa *= 2
            exponent -= 1

        # Adjust exponent for bias
        exponent += 127

        # Convert mantissa to binary string (23 bits)
        mantissa_str = ''
        mantissa -= 1  # Remove the leading 1
        for _ in range(23):
            mantissa *= 2
            bit = int(mantissa)
            mantissa_str += str(bit)
            mantissa -= bit

        # Construct the binary string
        binary_str = f"{sign:01b}{exponent:08b}{mantissa_str}"

        # Reorder the bytes as per the custom format
        byte4 = binary_str[0:8]
        byte3 = binary_str[8:16]
        byte1 = binary_str[16:24]
        byte2 = binary_str[24:32]

        # Convert to hexadecimal
        result = f"{int(byte1, 2):02X}{int(byte2, 2):02X}{int(byte3, 2):02X}{int(byte4, 2):02X}"
    except Exception as e:
        print(f'*** decimal_to_custom_ieee754: {e}')

    return bytes.fromhex(result)


def decimal_ieee754_from_binary(binary_data, position, offset=0, num_bytes=4):
    """ Extract 4 bytes starting from the specified position """
    try:
        if position + num_bytes > len(binary_data):
            return None
        bytes_data = binary_data[position:position + num_bytes]
        bytes_data = bytes_data.hex()

        byte1 = bytes_data[0:2]
        byte2 = bytes_data[2:4]
        byte3 = bytes_data[4:6]
        byte4 = bytes_data[6:8]

        # Convert each byte to binary string, keeping it 8 bits long
        byte1str = format(int(byte1, 16), '08b')
        byte2str = format(int(byte2, 16), '08b')
        byte3str = format(int(byte3, 16), '08b')
        byte4str = format(int(byte4, 16), '08b')

        # Concatenate binary representations in the custom order
        binary_str = byte4str + byte3str + byte1str + byte2str

        # Extract sign (1 bit), exponent (8 bits), and mantissa (23 bits)
        sign = int(binary_str[0], 2)
        exponent = int(binary_str[1:9], 2)
        mantissa_str = binary_str[9:]

        # Convert mantissa to decimal using the formula in Excel
        mantissa = 1 + sum(int(mantissa_str[i]) * 2**-(i + 1) for i in range(23))

        # Compute the final floating point value
        result = ((-1) ** sign) * (2 ** (exponent - 127)) * mantissa

        # Apply the scaling factor
        final_value = result + offset

        return final_value
    except Exception as e:
        print(f'*** decimal_ieee754_from_binary: {e} - len bin: {len(binary_data)} - pos: {position} - num_bytes: {num_bytes} - offset: {offset}')
        return None


def read_endian_int_at_position(data, position, num_bytes=4, endian='little'):
    """ Returns the endian integer equivalent of 'num_bytes' read
        from 'data' at 'position' """
    completed = False
    integer_value = 0

    try:
        # Ensures valid endian type
        if endian not in ['little', 'big']:
            raise ValueError("Endian must be 'little' or 'big'")

        # Ensure the position and number of bytes to read are within the data range
        if position < 0 or position + num_bytes > len(data):
            raise ValueError("Invalid position or number of bytes to read")

        # Read the specified number of bytes from the given position
        byte_sequence = data[position:position + num_bytes]

        # Convert the byte sequence from little-endian to an integer
        integer_value = int.from_bytes(byte_sequence, byteorder=endian)
        completed = True
    except Exception as e:
        if DEBUG_MODE:
            print()
            print(f'*** Error @ read_endian_int_at_position: {e}')
            print(f'>>> position: {position} - num_bytes: {num_bytes} - endian: {endian}')

    return completed, integer_value


def get_pen_strokes_address_dict(note_fn, search_keyword=None):
    """ Returns a dictionary of totalpaths addresses for a given notebook named note_fn"""
    try:
        a_metadata = None
        fonts_page_number = -1
        result_dict = {}
        file_type = None
        if os.path.exists(note_fn) and note_fn[-5:].lower() == '.note':
            json_fn = f'{note_fn}.json'
            a_metadata = json.loads(extract_metadata(note_fn, json_fn))
            if a_metadata is not None:
                # Retrieve the type of file
                if '__header__' in a_metadata:
                    a_header = a_metadata['__header__']
                    if 'APPLY_EQUIPMENT' in a_header:
                        file_type = a_header['APPLY_EQUIPMENT']
                if '__footer__' in a_metadata:
                    page_address_dict = a_metadata['__footer__']

                    if '__keywords__' in page_address_dict:
                        myfonts_keywords = page_address_dict['__keywords__']

                        if search_keyword:
                            for keyword in myfonts_keywords:
                                if keyword['KEYWORD'] == search_keyword:
                                    fonts_page_number = int(keyword['KEYWORDPAGE'])
                                    break

                    pdf_pages_list = [x[4:] for x in page_address_dict if x[:4] == 'PAGE']

                    if '__pages__' in a_metadata:
                        pages_list = a_metadata['__pages__']

                        pages_paths_list = [int(a_page['TOTALPATH']) for a_page in pages_list if 'TOTALPATH' in a_page.keys()]
                        pages_bitmap_list = [int(
                            a_page['__layers__'][0]['LAYERBITMAP']) for a_page in pages_list if len(a_page['__layers__']) > 0]

                        if fonts_page_number == -1:
                            for i in range(len(pdf_pages_list)):
                                result_dict[pdf_pages_list[i]] = {
                                    "totalpath": pages_paths_list[i],
                                    "bitmap": pages_bitmap_list[i]}
                        else:
                            result_dict[str(fonts_page_number)] = {
                                    "totalpath": pages_paths_list[fonts_page_number-1],
                                    "bitmap": pages_bitmap_list[fonts_page_number-1]}

        return result_dict, file_type
    except Exception as e:
        print()
        print(f'*** get_totalpath_address_dict: {e}')
        return None, None


def get_pen_strokes_dict(note_fn, search_keyword=None):
    """ Returns a dictionary of totalpaths for a given notebook named note_fn.
        the root keys are the page numbers"""
    try:
        if DEBUG_MODE:
            print(' # Entering get_pen_strokes_dict')

        offset_x = 0
        offset_y = 0
        pen_strokes_address_dict, file_type = get_pen_strokes_address_dict(note_fn, search_keyword=search_keyword)

        if pen_strokes_address_dict is not None:

            with open(note_fn, 'rb') as a_note_file:
                note_file_binaries = a_note_file.read()

            unique_used_list = []

            for a_page in pen_strokes_address_dict.keys():
                page_pen_strokes_dict = pen_strokes_address_dict[a_page]

                page_totalpath_address = page_pen_strokes_dict['totalpath']

                if page_totalpath_address != 0:
                    _, page_totalpath_size = read_endian_int_at_position(note_file_binaries, page_totalpath_address)
                    _, page_totalpath_strokes_nb = read_endian_int_at_position(note_file_binaries, page_totalpath_address + 4)
                    page_pen_strokes_dict['size'] = page_totalpath_size
                    page_pen_strokes_dict['strokes_nb'] = page_totalpath_strokes_nb

                    pen_strokes_list = []
                    a_position = page_totalpath_address + 8
                    for pen_stroke_index in range(page_totalpath_strokes_nb):
                        _, pen_stroke_size = read_endian_int_at_position(note_file_binaries, a_position)
                        _, pen_type = read_endian_int_at_position(note_file_binaries, a_position + 4, num_bytes=1)
                        _, pen_color = read_endian_int_at_position(note_file_binaries, a_position + 8, num_bytes=1)
                        _, pen_weight = read_endian_int_at_position(note_file_binaries, a_position + 12, num_bytes=2)
                        _, min_contours_x = read_endian_int_at_position(note_file_binaries, a_position + 104)
                        _, min_contours_y = read_endian_int_at_position(note_file_binaries, a_position + 108)
                        _, avg_contours_x = read_endian_int_at_position(note_file_binaries, a_position + 112)
                        _, avg_contours_y = read_endian_int_at_position(note_file_binaries, a_position + 116)
                        _, max_contours_x = read_endian_int_at_position(note_file_binaries, a_position + 120)
                        _, max_contours_y = read_endian_int_at_position(note_file_binaries, a_position + 124)
                        _, vector_size = read_endian_int_at_position(note_file_binaries, a_position + 216)
                        if DEBUG_MODE:
                            print(f' - Location: {a_position + 216} - vector_size:{vector_size} [{decimal_to_little_endian_4bytes(vector_size)}]')

                        vector_points = []
                        for index_point in range(vector_size):
                            _, point_y = read_endian_int_at_position(note_file_binaries, a_position + 220 + index_point * 8)
                            _, point_x = read_endian_int_at_position(note_file_binaries, a_position + 220 + index_point * 8 + 4)
                            vector_points.append([point_y, point_x])

                        pressure_points = []
                        for index_pressure in range(vector_size):
                            _, ppoint = read_endian_int_at_position(note_file_binaries, a_position + 224 + vector_size * 8 + index_pressure*2, num_bytes=2)
                            pressure_points.append(ppoint)

                        # Skip the headear of the unique vector to read the first unique number
                        _, unique_number = read_endian_int_at_position(note_file_binaries, a_position + 228 + vector_size * 8 + vector_size*2, num_bytes=4)

                        # Build the unique vector. TODO: Probably better to just read it
                        unique_used_list = [unique_number]*vector_size

                        # Then there is a vector of ones, vector_size*1 byte

                        # position is now:  a_position + 228 + vector_size * 8 + vector_size*2 + 4 + vector_size*4 + vector_size*1
                        #                   a_position + 232 + vector_size * 15
                        #
                        # Not sure if I had corrupted files, or if version changed, but some of my files have a sequence 0f 12 zero bytes
                        # following the "vector of 1"
                        # Let's read the sequence length
                        # sequence_length = read_zero_sequence(note_file_binaries, a_position + 232 + vector_size * 15)
                        sequence_length = 16  # Ignoring read_zero_sequence

                        #                 > We add sequence_length bytes of zeros
                        #                   a_position + 232 + sequence_length + vector_size * 15
                        #                 > We add 4 bytes for the pen stroke number
                        #                   a_position + 236 + sequence_length + vector_size * 15
                        #                 > We add 54 bytes
                        #                   a_position + 290 + sequence_length + vector_size * 15

                        _, contours_number = read_endian_int_at_position(note_file_binaries, a_position + 290 + sequence_length + vector_size * 15, num_bytes=4)
                        if DEBUG_MODE:
                            print(
                                f' - Location:{a_position + 290 + sequence_length + vector_size * 15 + 4} - '
                                f'contours #: {contours_number} [{decimal_to_little_endian_4bytes(contours_number)}] - sequence_length: {sequence_length}')

                        # position is now:  a_position + 310 + vector_size * 15
                        contour_processed_nb = 0
                        contours_dict = {}
                        contour_count = 0

                        # contour_problem = False
                        # print(f'----contours_number: {contours_number}')
                        if contours_number > 1000:
                            print(f'**- TOO HIGH contours_number: {contours_number} for page: {a_page} - Pen stroke #: {pen_stroke_index}')
                            return None

                        for contour_index in range(contours_number):
                            contour_count_fails = 0
                            _, contour_size = read_endian_int_at_position(note_file_binaries, a_position + 310 + vector_size * 15 + contour_processed_nb + contour_count, num_bytes=4)
                            contours_list = []

                            last_contour_point = None

                            for contour_element_idx in range(contour_size):
                                contour_x = decimal_ieee754_from_binary(note_file_binaries, a_position + 314 + vector_size * 15 + contour_element_idx*8 + contour_count, offset_x)
                                contour_y = decimal_ieee754_from_binary(note_file_binaries, a_position + 318 + vector_size * 15 + contour_element_idx*8 + contour_count, offset_y)
                                if contour_x and contour_y:
                                    last_contour_point = [contour_x, contour_y]
                                    contours_list.append(last_contour_point)
                                else:
                                    if last_contour_point:
                                        contours_list.append(last_contour_point)
                                    else:
                                        contour_count_fails += 1

                            contours_dict[str(contour_index)] = list(contours_list)
                            # contour_count += (contour_size-contour_count_fails)*8 + 4
                            contour_count += (contour_size)*8 + 4

                        remaining_bytes_start = a_position + 318 + vector_size * 15 + contour_count-4
                        remaining_bytes_end = a_position + pen_stroke_size + 4
                        remaining_bytes = note_file_binaries[remaining_bytes_start:remaining_bytes_end]

                        pen_strokes_list.append({
                            'address': a_position, 'size': pen_stroke_size,
                            'type': pen_type, 'color': pen_color, 'weight': pen_weight,
                            'min_c_x': min_contours_x, 'min_c_y': min_contours_y, 'avg_c_x': avg_contours_x, 'avg_c_y': avg_contours_y,
                            'max_c_x': max_contours_x, 'max_c_y': max_contours_y, 'vector_size': vector_size, 'vector_points': vector_points,
                            'vector_pressure': pressure_points, 'vector_unique': unique_used_list, 'vector_one': [1]*vector_size,
                            'contours': contours_dict, 'r_bytes': remaining_bytes.hex().upper()})

                        a_position += pen_stroke_size + 4
                    page_pen_strokes_dict['strokes'] = pen_strokes_list
                else:
                    page_pen_strokes_dict['strokes'] = []

        return pen_strokes_address_dict, file_type
    except Exception as e:
        print()
        print(f'*** get_pen_strokes_dict: {e}')
        return None, None


def strokes_at_point(strokes_list, x, y, row_height=90, scratio=1, get_xt_point=False, new_weight=None, series=NOTEBOOK_DEVICE):
    """ Returns a modified strokes list for coordinates x,y (ref MAX_HORIZONTAL_PIXELS/MAX_VERTICAL_PIXELS)
        The function first checks the relative delta_x and delta_y for all the
        strokes in the list. It then applies the x/y coordinates to that point
        and shifts the other strokes by their delta.
        We do not check if the shifting movs the strokes out of the screen boundaries

        strokes_list is a list of dictionaries"""
    try:

        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)

        min_x = min([x['min_c_x'] for x in strokes_list])
        max_x = max([x['max_c_x'] for x in strokes_list])
        min_y = min([x['min_c_y'] for x in strokes_list])
        max_y = max([x['max_c_y'] for x in strokes_list])

        stroke_width = max_x - min_x
        stroke_height = max_y - min_y

        y_adjustment = max(0, row_height - stroke_height)
        delta_x = x - min_x

        delta_y = y - min_y + y_adjustment
        delta_x_mu = -1 * round(delta_x * adb_screen_max_x / max_horizontal_pixels)

        delta_y_mu = round(delta_y * adb_screen_max_y / max_vertical_pixels)

        scratio_x = round(adb_screen_max_x*(1-scratio)*(1-MARGIN_LEFT_SIZE))

        scratio_pix = round(max_horizontal_pixels*(1-scratio)*(1-MARGIN_LEFT_SIZE))

        # Create a deep copy of the list
        mod_strokes_list = copy.deepcopy(strokes_list)

        # Parse the list

        for ps_dict in mod_strokes_list:

            new_min_x = round(scratio*(ps_dict['min_c_x'] + delta_x)) + scratio_pix
            new_max_x = round(scratio*(ps_dict['max_c_x'] + delta_x)) + scratio_pix
            new_min_y = round(scratio*(ps_dict['min_c_y'] + delta_y))
            new_max_y = round(scratio*(ps_dict['max_c_y'] + delta_y))

            if new_weight:
                ps_dict['weight'] = new_weight
            ps_dict['min_c_x'] = max(min(new_min_x, max_horizontal_pixels), 0)
            ps_dict['min_c_y'] = max(min(new_min_y, max_vertical_pixels), 0)
            ps_dict['max_c_x'] = max(min(new_max_x, max_horizontal_pixels), 0)
            ps_dict['max_c_y'] = max(min(new_max_y, max_vertical_pixels), 0)
            ps_dict['avg_c_x'] = max(min(round(scratio*(ps_dict['avg_c_x'] + delta_x)) + scratio_x, max_horizontal_pixels), 0)
            ps_dict['avg_c_y'] = max(min(round(scratio*(ps_dict['avg_c_y'] + delta_y)), max_vertical_pixels), 0)  # TODO: Based on 1 example. this was unchanged. WHY? confirm
            xt_vector = [
                [
                    max(min(round(scratio*(x[0] + delta_y_mu)), adb_screen_max_y), 0),
                    max(min(round(scratio*(x[1] + delta_x_mu) + scratio_x), adb_screen_max_x), 0)] for x in ps_dict['vector_points']]
            ps_dict['vector_points'] = xt_vector

            if get_xt_point:
                xt_x_list = [x[1] for x in xt_vector]
                xt_y_list = [x[0] for x in xt_vector]
                xt_rect = [min(xt_x_list), min(xt_y_list), max(xt_x_list), max(xt_y_list)]
            else:
                xt_rect = []

            mod_contours = {}
            for contour_nb, contour_value in ps_dict['contours'].items():
                new_list = [
                    [
                        max(min(round(scratio*(x[0] + delta_x) + scratio_pix), max_horizontal_pixels), 0),
                        max(min(round(scratio*(x[1] + delta_y)), max_vertical_pixels), 0)] for x in contour_value]
                mod_contours[contour_nb] = new_list
            ps_dict['contours'] = mod_contours

        return mod_strokes_list, stroke_width, stroke_height, xt_rect
    except Exception as e:
        print()
        print(f'*** strokes_at_point: {e}')
        return None, 0, 0, []


def strokes_at_point2(strokes_list, x=None, y=None, scratio=1, ratio_w=1, ratio_h=1, get_xt_point=False, new_weight=None, row_vertical_height=None, series=NOTEBOOK_DEVICE):
    """ Returns a modified strokes list for coordinates x,y (ref MAX_HORIZONTAL_PIXELS/MAX_VERTICAL_PIXELS)
        The function first checks the relative delta_x and delta_y for all the
        strokes in the list. It then applies the x/y coordinates to that point
        and shifts the other strokes by their delta.
        We do not check if the shifting moves the strokes out of the screen boundaries

        strokes_list is a list of dictionaries"""
    try:
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)

        # Get boundaries of all pen strokes in the list (usually for 1 character)
        min_x_l = [x['min_c_x'] for x in strokes_list]
        max_x_l = [x['max_c_x'] for x in strokes_list]
        min_y_l = [x['min_c_y'] for x in strokes_list]
        max_y_l = [x['max_c_y'] for x in strokes_list]
        xt_rect = []

        try:
            min_x = min(min_x_l)
            max_x = max(max_x_l)
            min_y = min(min_y_l)
            max_y = max(max_y_l)

            # Compute boundary width and height
            stroke_width = max_x - min_x
            stroke_height = max_y - min_y

            if row_vertical_height is not None:
                if y:
                    y += ratio_h*(row_vertical_height - stroke_height)

            # Compute offset for moving to start (top left coord)
            if x:
                delta_x = x - min_x
            else:
                delta_x = 0
            if y:
                delta_y = y - min_y
            else:
                delta_y = 0
        except Exception as e:
            print(f'**_ strokes_at_point2: {e}')
            delta_x = delta_y = min_x = min_y = stroke_width = stroke_height = 0

        # Compute offset for moving to start (top right coord)
        delta_x_mu = -1 * round(delta_x * adb_screen_max_x / max_horizontal_pixels)
        delta_y_mu = round(delta_y * adb_screen_max_y / max_vertical_pixels)

        # Create a deep copy of the list
        mod_strokes_list = copy.deepcopy(strokes_list)

        # Parse the list
        for ps_dict in mod_strokes_list:

            old_minx = ps_dict['min_c_x']
            old_miny = ps_dict['min_c_y']

            # Compute top right coordinate of old minx, miny
            old_min_y_mu, old_min_x_mu = topleft_to_topright(
                [old_minx, old_miny], max_horizontal_pixels,
                max_vertical_pixels, adb_screen_max_x, adb_screen_max_y)

            min_y_mu, min_x_mu = topleft_to_topright(
                [min_x, min_y], max_horizontal_pixels,
                max_vertical_pixels, adb_screen_max_x, adb_screen_max_y)

            new_min_x = round(old_minx + delta_x)
            new_min_y = round(old_miny + delta_y)

            new_max_x = round(old_minx+delta_x*(ratio_w))
            new_max_y = round(old_miny+delta_y*(ratio_h))

            if new_weight:
                ps_dict['weight'] = NEEDLE_POINT_SIZES[str(new_weight)]

            ps_dict['min_c_x'] = max(min(new_min_x, max_horizontal_pixels), 0)
            ps_dict['min_c_y'] = max(min(new_min_y, max_vertical_pixels), 0)
            ps_dict['max_c_x'] = max(min(new_max_x, max_horizontal_pixels), 0)
            ps_dict['max_c_y'] = max(min(new_max_y, max_vertical_pixels), 0)
            ps_dict['avg_c_x'] = max(min(round((new_min_x + new_max_x)/2), max_horizontal_pixels), 0)
            ps_dict['avg_c_y'] = max(min(round((new_min_y + new_max_y)/2), max_vertical_pixels), 0)

            xt_vector = [
                [
                    max(min(round(x[0]+delta_y_mu+(ratio_h-1)*(x[0]-min_y_mu)), adb_screen_max_y), 0),
                    max(min(round(x[1] + delta_x_mu+(ratio_w-1)*(x[1]-min_x_mu)), adb_screen_max_x), 0)] for x in ps_dict['vector_points']]

            ps_dict['vector_points'] = xt_vector

            if get_xt_point:
                xt_x_list = [x[1] for x in xt_vector]
                xt_y_list = [x[0] for x in xt_vector]
                xt_rect = [min(xt_x_list), min(xt_y_list), max(xt_x_list), max(xt_y_list)]
            else:
                xt_rect = []

            mod_contours = {}
            for contour_nb, contour_value in ps_dict['contours'].items():
                new_list = [
                    [
                        max(min(round(x[0]+delta_x+(ratio_w-1)*(x[0]-old_minx)), max_horizontal_pixels), 0),
                        max(min(round(x[1]+delta_y+(ratio_h-1)*(x[1]-old_miny)), max_vertical_pixels), 0)] for x in contour_value]
                mod_contours[contour_nb] = new_list
            ps_dict['contours'] = mod_contours

        return mod_strokes_list, stroke_width, stroke_height, xt_rect
    except Exception as e:
        print()
        print(f'*** strokes_at_point2: {e}')
        return None, 0, 0, []


def signed_to_bytes(a_number, num_bytes, byteorder='little'):
    """ Converts a positive number to bytes. If negative, returns a zero bytes"""
    a_signed = a_number < 0
    return a_number.to_bytes(num_bytes, byteorder=byteorder, signed=a_signed)


def pen_strokes_dict_to_bytes(pen_strokes_dict, num_bytes=4, byteorder='little', series=NOTEBOOK_DEVICE):
    """ Generates penstroke bytes from dictionary """
    try:
        bytes_dict = {}
        offset_x = 0
        offset_y = 0

        for page_str, page_pen_strokes_dict in pen_strokes_dict.items():

            page_in_bytes = int(page_str).to_bytes(2, byteorder=byteorder)
            page_tpath_strokes_nb = page_pen_strokes_dict['strokes_nb'].to_bytes(num_bytes, byteorder=byteorder)
            page_stroke_dicts = page_pen_strokes_dict['strokes']

            page_stroke_nb = 1

            total_stroke_bytes = page_tpath_strokes_nb

            for a_stroke_dict in page_stroke_dicts:

                if a_stroke_dict == {}:
                    print('*** Fatal error: empty dict in pen_strokes_dict_to_bytes, exiting')
                    exit(1)

                a_stroke_bytes = None

                stroke_pen_type = a_stroke_dict['type'].to_bytes(1, byteorder=byteorder)

                if a_stroke_bytes is None:
                    a_stroke_bytes = stroke_pen_type
                else:
                    a_stroke_bytes += stroke_pen_type

                a_stroke_bytes += bytes.fromhex('000000')

                stroke_color = signed_to_bytes(a_stroke_dict['color'], 1, byteorder=byteorder)
                a_stroke_bytes += stroke_color
                a_stroke_bytes += bytes.fromhex('000000')
                stroke_weight = signed_to_bytes(a_stroke_dict['weight'], 2, byteorder=byteorder)

                a_stroke_bytes += stroke_weight
                a_stroke_bytes += bytes.fromhex('00000A0000000000000020000000FFFFFFFF')
                a_stroke_bytes += page_in_bytes
                a_stroke_bytes += bytes.fromhex('0000000000000000000088130000000000006F746865727300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')

                a_stroke_min_c_x = signed_to_bytes(a_stroke_dict['min_c_x'], 4, byteorder=byteorder)
                a_stroke_bytes += a_stroke_min_c_x
                a_stroke_min_c_y = signed_to_bytes(a_stroke_dict['min_c_y'], 4, byteorder=byteorder)
                a_stroke_bytes += a_stroke_min_c_y
                a_stroke_avg_c_x = signed_to_bytes(a_stroke_dict['avg_c_x'], 4, byteorder=byteorder)
                a_stroke_bytes += a_stroke_avg_c_x
                a_stroke_avg_c_y = signed_to_bytes(a_stroke_dict['avg_c_y'], 4, byteorder=byteorder)

                a_stroke_bytes += a_stroke_avg_c_y
                a_stroke_max_c_x = signed_to_bytes(a_stroke_dict['max_c_x'], 4, byteorder=byteorder)
                a_stroke_bytes += a_stroke_max_c_x
                a_stroke_max_c_y = signed_to_bytes(a_stroke_dict['max_c_y'], 4, byteorder=byteorder)
                a_stroke_bytes += a_stroke_max_c_y

                if series in ['N5']:
                    somehexbytes = '0200000080540000603F000073757065724E6F74654E6F746500000000000000000000000000000000000000000000'
                else:
                    somehexbytes = '02000000CB3D0000582E000073757065724E6F74654E6F746500000000000000000000000000000000000000000000'

                somehexbytes += '0000000000000000000000000000000000010000000000000000000000000000000000000000000000'

                a_stroke_bytes += bytes.fromhex(somehexbytes)

                a_stroke_vector_points = a_stroke_dict['vector_points']

                a_stroke_vector_points_length = len(a_stroke_vector_points)
                a_stroke_bytes += a_stroke_vector_points_length.to_bytes(4, byteorder=byteorder)
                for x in a_stroke_vector_points:
                    a_stroke_bytes += signed_to_bytes(x[0], num_bytes, byteorder=byteorder)
                    a_stroke_bytes += signed_to_bytes(x[1], num_bytes, byteorder=byteorder)
                a_stroke_vector_pressure = a_stroke_dict['vector_pressure']

                a_stroke_bytes += a_stroke_vector_points_length.to_bytes(4, byteorder=byteorder)
                vector_pressure_bytes = [
                    signed_to_bytes(x, 2, byteorder=byteorder) for x in a_stroke_vector_pressure]
                a_stroke_bytes += b''.join(vector_pressure_bytes)
                a_stroke_vector_unique = a_stroke_dict['vector_unique']
                a_stroke_bytes += signed_to_bytes(a_stroke_vector_points_length, 4, byteorder=byteorder)
                vector_unique_bytes = [
                    signed_to_bytes(x, 4, byteorder=byteorder) for x in a_stroke_vector_unique]
                a_stroke_bytes += b''.join(vector_unique_bytes)
                a_stroke_vector_one = a_stroke_dict['vector_one']
                a_stroke_bytes += signed_to_bytes(a_stroke_vector_points_length, 4, byteorder=byteorder)
                vector_one_bytes = [
                    signed_to_bytes(x, 1, byteorder=byteorder) for x in a_stroke_vector_one]
                a_stroke_bytes += b''.join(vector_one_bytes)
                a_stroke_bytes += bytes.fromhex('00000000000000000000000000000000')
                a_stroke_bytes += page_stroke_nb.to_bytes(4, byteorder=byteorder)  # TODO: Validate 241029 correction The number of strokes is likely at least 4 bytes
                a_stroke_bytes += bytes.fromhex('000000000000000000000000000000000100000001000000000000000000000001000000010000000000000000000000000000000000')
                a_stroke_contours = a_stroke_dict['contours']

                a_stroke_contours_len = len(a_stroke_contours.keys())

                a_stroke_bytes += a_stroke_contours_len.to_bytes(4, byteorder=byteorder)

                for contour_nb, contour_vector in a_stroke_contours.items():

                    a_c_v = None
                    len_a_c_v = 0
                    for a_point in contour_vector:
                        d_x = decimal_to_custom_ieee754(a_point[0], offset=offset_x)
                        d_y = decimal_to_custom_ieee754(a_point[1], offset=offset_y)
                        if d_x and d_y:
                            if a_c_v is None:
                                a_c_v = d_x
                            else:
                                a_c_v += d_x
                            a_c_v += d_y
                            len_a_c_v += 1

                    if a_c_v:
                        a_stroke_bytes += len_a_c_v.to_bytes(4, byteorder=byteorder)
                        a_stroke_bytes += a_c_v

                a_stroke_bytes += a_stroke_contours_len.to_bytes(4, byteorder=byteorder)

                a_stroke_bytes += bytes.fromhex(a_stroke_dict['r_bytes'])

                page_stroke_nb += 1
                len_page_strokes_bytes = len(a_stroke_bytes).to_bytes(4, byteorder=byteorder)
                a_stroke_bytes = len_page_strokes_bytes + a_stroke_bytes
                if total_stroke_bytes is None:
                    total_stroke_bytes = a_stroke_bytes
                else:
                    total_stroke_bytes += a_stroke_bytes

            bytes_dict[page_str] = total_stroke_bytes
        return bytes_dict
    except Exception as e:
        print()
        print(f'*** pen_strokes_dict_to_bytes: {e}')


def char_from_ascii_tps(
    ascii_ps_list, searched_char='a', bypassed_unicode=None, device=NOTEBOOK_DEVICE,
        index_0=28, y0=0, elsize=10):
    """ From a list of ascii ps, returns pen strokes corresponding to a given ascii code
        The order the characters are placed on the table, and their loction within the table is very important.
        The characters don't have to be centered in a cell. They do need to be inside the cell, though
        An intersection threshold of 35% is set to detect all strokes in a given cell
        Parameters:
            - ascii_ps_list:    a list of pen stroke dictionaries
            - bypassed_unicode: Integer. If not None, just use the number to locate the cell in the ascii table
            - searched_char:    the character for which we want to retrieve pen strokes
            - index_0:          the ASCII index of the first character in the table
            - x0, y0:           pixel coordinates of the first element in the table
            - side:             size in pixels of one side of a square cell of the table
            - elsize:           number of cells in a row of the table
        """
    try:
        if device in ['N5']:
            x0 = ASCII_X02
            side = ASCII_SIDE2
        else:
            x0 = ASCII_X0
            side = ASCII_SIDE

        if bypassed_unicode is None:
            if searched_char in CUSTOM_MAP:
                ascii_index = CUSTOM_MAP[searched_char]
            else:
                ascii_index = ord(searched_char)
        else:
            ascii_index = bypassed_unicode
        index_diff = ascii_index - index_0
        char_col = index_diff % elsize + 1
        char_row = index_diff // elsize + 1
        char_x0 = x0 + (char_col - 1) * side
        char_y0 = y0 + (char_row - 1) * side
        char_rect = [char_x0, char_y0, char_x0 + side, char_y0 + side]

        select_strokes = [x for x in ascii_ps_list if intersection_area(
            char_rect, [x['min_c_x'], x['min_c_y'], x['max_c_x'], x['max_c_y']], threshold=0.35)]

        return select_strokes
    except Exception as e:
        print()
        print(f'*** char_from_ascii_tps: {e} - char : {searched_char}')
        return []


def text_to_pen_strokes(
    text_to_convert, ascii_ps_list, starting_point,
        xl_json=False, delta_x=3, delta_y=12, word_separator=30,
        padding_horizontal=40, padding_vertical=450, not_found_placeholder='#', scratio=1, style_bold_weight=300,
        series=NOTEBOOK_DEVICE):
    """ For a given text_to_convert, generates a dictionary of list of pen strokes, per page"""
    try:
        # TODO Pass this as a variable. Here the meaning is:
        # - extend to 1 next word
        # - use ord 170 for style
        # - with thickness 0.5
        # - background color to be used for table of contents = 217
        auto_title_dict = TEXT2NOTES_STYLING
        sum_letter_width = 0
        count_letter_width = 0
        titles_dict = {}
        images_dict = {}
        markdown_length_dict = {}
        markdown_length_list = []
        if series in ['N5']:
            a_v_offset_ratio = MAX_VERTICAL_PIXELS_N5 / MAX_VERTICAL_PIXELS_N5
        else:
            a_v_offset_ratio = 1

        try:
            fonts_vert_adj_json_fn = os.path.join(SN_VAULT, f'{FONT_NAME}.json')
            fonts_vertical_adjustment = read_json(fonts_vert_adj_json_fn)
            if fonts_vertical_adjustment == {}:
                fonts_vertical_adjustment = CHAR_V_OFFSET
        except Exception as e:
            print(f'*** Vertical adjustment: {e}')
            print(f' >  File: {fonts_vert_adj_json_fn}. Using default')
            fonts_vertical_adjustment = CHAR_V_OFFSET

        # Remove utf marking added by the Supernote
        text_to_convert = text_to_convert.replace("\ufeff", "")

        text_to_convert = text_to_convert.replace('###### ', '^h6')
        text_to_convert = text_to_convert.replace('##### ', '^h5')
        text_to_convert = text_to_convert.replace('#### ', '^h4')
        text_to_convert = text_to_convert.replace('### ', '^h3')
        text_to_convert = text_to_convert.replace('## ', '^h2')
        text_to_convert = text_to_convert.replace('# ', '^h1')
        text_to_convert = text_to_convert.replace('   - ', '^l2')
        text_to_convert = text_to_convert.replace('- ', '^l1')

        text_to_convert = text_to_convert.replace('**', '^b1')   # Marking bold
        text_to_convert = text_to_convert.replace('^b1^b1', '')   # Marking bold

        l_rows = text_to_convert.split('\n')

        markdown_length_list = [(x, len(x.split())-1) for x in l_rows if ((x != '') and (x[:2] == '^h' or x[:2] == '^l'))]

        # Build the dictionary of headings
        for x in markdown_length_list:
            key_word_l = x[0].split()

            key_word = key_word_l[0].lower()
            heading_next_nb = x[1]
            if key_word in markdown_length_dict:
                full_heading_list = markdown_length_dict[key_word]
                full_heading_list.append({"full": x[0][3:], "next": heading_next_nb})
            else:
                markdown_length_dict[key_word] = [{"full": x[0][3:], "next": heading_next_nb}]

        text_to_convert = text_to_convert.replace('\n', ' ^p ').replace('  ', ' ')

        text_to_convert_list = text_to_convert.split()

        nb_words = len(text_to_convert_list)

        dict_ps = {}
        full_dict_ps = {}
        list_ps = []
        list_xl_points = []
        list_titles_image = []

        starting_pos_x = starting_point[0]
        starting_pos_y = starting_point[1]

        max_iterations_loop = MAX_PEN_TO_STROKE_LOOP
        style_indent = 0
        pos_x = starting_pos_x
        pos_y = starting_pos_y

        index_word = 0
        index_letter = 0
        current_word_ps_count = 0
        max_row_height = 0
        nb_loops = 0
        page_number = 0
        title_ps = []
        title_ps_c = {}
        list_all_x = []
        list_all_y = []
        list_cw_reset = -1
        style_char = ''
        style_prefix = ''
        style_creturn = 0
        pdf_page_nb = 0
        style_weight = None
        style_post_word_nb = 1
        if '^b1' in auto_title_dict:
            style_bold_weight_settings = auto_title_dict['^b1']
            if 'weight' in style_bold_weight_settings:
                style_bold_weight = NEEDLE_POINT_SIZES[str(style_bold_weight_settings['weight'])]
                # style_weight = style_bold_weight
        style_type = ''
        style_bold_on = False

        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)

        max_horizontal_length_ps = max_horizontal_pixels/scratio - padding_horizontal - round(adb_screen_max_x*(1-scratio)*(1-MARGIN_LEFT_SIZE))
        style_char_ps = None  # MMB 241130
        while (index_word < nb_words) and (nb_loops < max_iterations_loop):

            current_word = text_to_convert_list[index_word]

            if '$Page' in current_word:
                pdf_page_nb = text_to_convert_list[index_word+1]

            current_word_ps_count = 0
            current_word_ps = []
            # style_char_ps = None   MMB 241130

            # The If block below breaks long words in pieces and restarts the loop
            if (len(current_word)*20 + pos_x > max_horizontal_length_ps):

                pos_x = starting_pos_x

                # Determining where to split
                split_position = round((max_horizontal_length_ps-pos_x)/20/2)

                current_word_1 = current_word[:split_position]
                current_word_2 = current_word[split_position:]

                # Updating the list of words by inserting a 2nd piece, updating the number of words
                if current_word_2 != '':
                    text_to_convert_list[index_word] = current_word_1
                    text_to_convert_list.insert(index_word+1, current_word_2)
                    nb_words += 1
                nb_loops += 1

                # Increment the vertical position
                pos_y += max_row_height + delta_y

                # Check if new page is needed TODO: set this as a function because code repeated elsewhere
                if pos_y > max_vertical_pixels/scratio - padding_vertical:

                    if len(list_ps) > 0:

                        if len(current_word_ps) > 0:
                            _temp_list = []
                            for sub_list in current_word_ps:
                                _temp_list.extend(sub_list)
                            list_ps.extend(_temp_list)

                        current_word_ps_count = len(current_word_ps)
                        if current_word_ps_count > 0:
                            list_ps = list_ps[:-1*current_word_ps_count]
                        # index_word -= 1
                        index_letter = 0
                        current_word_ps_count = 0
                        current_word_ps = []
                        current_letter_ps = []
                        current_letter_ps_ = []
                        style_char_ps_ = []
                        pdf_page_nb = 0

                        dict_ps[f'{page_number + 1}'] = list(list_ps)

                        list_ps = []

                        page_number += 1
                    pos_x = starting_pos_x
                    pos_y = starting_pos_y
                    list_ps = []
                    list_titles_image = []
                continue

            # Initialize current word ps list
            current_word_ps = []
            current_xl_points = []
            style_case = None

            index_letter = 0

            if current_word == '':
                pos_x += word_separator
                index_word += 1
                continue
            elif current_word == '^p':
                pos_x = starting_pos_x
                pos_y += max_row_height + delta_y
                if pos_y > max_vertical_pixels/scratio - padding_vertical:
                    if len(list_ps) > 0:

                        if len(current_word_ps) > 0:
                            _temp_list = []
                            for sub_list in current_word_ps:
                                _temp_list.extend(sub_list)
                            list_ps.extend(_temp_list)

                        current_word_ps_count = len(current_word_ps)
                        if current_word_ps_count > 0:
                            list_ps = list_ps[:-1*current_word_ps_count]
                        # index_word -= 1
                        index_letter = 0
                        current_word_ps_count = 0
                        current_word_ps = []
                        current_letter_ps = []
                        current_letter_ps_ = []
                        style_char_ps_ = []

                        dict_ps[f'{page_number + 1}'] = list(list_ps)

                        list_ps = []

                        page_number += 1
                    pos_x = starting_pos_x
                    pos_y = starting_pos_y
                    list_ps = []
                    list_titles_image = []
                index_word += 1
                continue

            cw_lower = current_word.lower()

            # Indicates whether or not the styling is ON
            get_xt_point = index_word <= list_cw_reset

            # This if condition below is entered when the styling is turned OFF
            if not get_xt_point:
                # style_bold_on = False
                list_all_x = []
                list_all_y = []
                list_cw_reset = -1
                title_ps = []
                title_ps_c = {}
                style_weight = None
                style_case_upper = style_case_lower = False

                pos_y += style_creturn*20
                if pos_y > max_vertical_pixels/scratio - padding_vertical:
                    if len(list_ps) > 0:

                        if len(current_word_ps) > 0:
                            _temp_list = []
                            for sub_list in current_word_ps:
                                _temp_list.extend(sub_list)
                            list_ps.extend(_temp_list)

                        current_word_ps_count = len(current_word_ps)
                        if current_word_ps_count > 0:
                            list_ps = list_ps[:-1*current_word_ps_count]
                        # index_word -= 1
                        index_letter = 0
                        current_word_ps_count = 0
                        current_word_ps = []
                        current_letter_ps = []
                        current_letter_ps = []
                        current_letter_ps_ = []
                        style_char_ps_ = []

                        dict_ps[f'{page_number + 1}'] = list(list_ps)

                        list_ps = []

                        page_number += 1
                    pos_x = starting_pos_x
                    pos_y = starting_pos_y
                    list_ps = []
                    list_titles_image = []
                style_creturn = 0

                style_indent = 0

                # If we find cw_lower in the list of styles, we load the parameters
                # print(f'------cw_lower: {cw_lower}')
                if cw_lower in auto_title_dict:
                    style_type = 'none'
                    style_settings = auto_title_dict[cw_lower]
                    # style_ord = style_settings["ord"]     # Read the value of the unicode in the dictionary
                    if 'ord' in style_settings:
                        style_ord = style_settings["ord"]   # Read the value of the unicode in the dictionary
                        style_char = chr(style_ord)         # The "char" for the style
                        style_char_ps = char_from_ascii_tps(
                            ascii_ps_list, bypassed_unicode=style_ord, device=series)
                    else:
                        style_ord = None
                        style_char = ''
                        style_char_ps = None

                    if 'toc_bg_color' in style_settings:
                        style_bg_color_ = style_settings["toc_bg_color"]
                        style_bg_color = (style_bg_color_, style_bg_color_, style_bg_color_)

                    if 'weight' in style_settings:
                        style_weight = NEEDLE_POINT_SIZES[str(style_settings['weight'])]  # See NEEDLE_POINT_SIZES
                    else:
                        style_weight = None
                    style_char_ps = char_from_ascii_tps(ascii_ps_list, bypassed_unicode=style_ord, device=series)

                    if 'next_word_count' in style_settings:
                        style_post_word_nb = auto_title_dict[cw_lower]['next_word_count']  # We extend the style to x following words
                    list_cw_reset = style_post_word_nb + index_word
                    get_xt_point = index_word <= list_cw_reset
                    if current_word[0] == '$':
                        current_word = current_word[1:]

                # ------- Test if markdown heading -----------------------------------------

                elif cw_lower in markdown_length_dict:
                    a_list = markdown_length_dict[cw_lower]
                    if a_list != []:
                        first_element = a_list.pop(0)
                        # first_element_full = first_element['full']
                        style_post_word_nb = first_element['next']

                        list_cw_reset = index_word + style_post_word_nb

                        get_xt_point = index_word <= list_cw_reset

                        mkd_mark = cw_lower[:3]
                        # print(f'-----mkd_mark: {mkd_mark}')

                        # If we find mkd_mark in the list of styles, we load the parameters
                        if mkd_mark in auto_title_dict:

                            if mkd_mark[1] == 'l':
                                style_type = 'bullet-point'
                            elif mkd_mark[1] == 'h':
                                style_type = 'heading'
                            elif mkd_mark[1:] == 'pa':
                                style_type = 'heading'
                            else:
                                style_type = 'none'

                            style_settings = auto_title_dict[mkd_mark]

                            if 'ord' in style_settings:
                                style_ord = style_settings["ord"]   # Read the value of the unicode in the dictionary
                                style_char = chr(style_ord)         # The "char" for the style
                                style_char_ps = char_from_ascii_tps(ascii_ps_list, bypassed_unicode=style_ord, device=series)
                            else:
                                style_ord = None
                                style_char = ''
                                style_char_ps = None

                            if 'toc_bg_color' in style_settings:
                                style_bg_color_ = style_settings["toc_bg_color"]
                                style_bg_color = (style_bg_color_, style_bg_color_, style_bg_color_)
                            else:
                                style_bg_color = (254, 254, 254)

                            if 'weight' in style_settings:
                                style_weight = NEEDLE_POINT_SIZES[str(style_settings['weight'])]  # See NEEDLE_POINT_SIZES
                            else:
                                style_weight = None

                            if 'prefix' in style_settings:
                                style_prefix = style_settings['prefix']
                            else:
                                style_prefix = ''

                            # Setting up case if relevant
                            if 'case' in style_settings:
                                style_case = style_settings['case']
                                style_case_upper = style_case.lower() in 'uppercase'
                                style_case_lower = style_case.lower() in 'lowercase'

                            # Setting up carriage return if relevant
                            if 'creturn' in style_settings:
                                style_creturn = style_settings['creturn']
                                pos_y += style_creturn*20
                                if pos_y > max_vertical_pixels/scratio - padding_vertical:
                                    if len(list_ps) > 0:

                                        if len(current_word_ps) > 0:
                                            _temp_list = []
                                            for sub_list in current_word_ps:
                                                _temp_list.extend(sub_list)
                                            list_ps.extend(_temp_list)

                                        current_word_ps_count = len(current_word_ps)
                                        if current_word_ps_count > 0:
                                            list_ps = list_ps[:-1*current_word_ps_count]
                                        # index_word -= 1
                                        index_letter = 0
                                        current_word_ps_count = 0
                                        current_word_ps = []
                                        current_letter_ps = []
                                        current_letter_ps_ = []
                                        style_char_ps_ = []

                                        dict_ps[f'{page_number + 1}'] = list(list_ps)

                                        list_ps = []

                                        page_number += 1
                                    pos_x = starting_pos_x
                                    pos_y = starting_pos_y
                                    list_ps = []
                                    list_titles_image = []
                            # Setting up indent if relevant
                            if 'indent' in style_settings:
                                style_indent = -1 * style_settings['indent'] * 50
                                pos_x += style_indent

            # Bullet points are in bold only if specifically marked so
            if current_word[:2] == '^l':
                if len(current_word) > 5:
                    style_bold_on = current_word[3:6] == '^b1'

            replaced_hl = False
            # print(f'------current_word:{current_word}')
            if current_word[:3] in ['^h1', '^h2', '^h3', '^h4', '^h5', 'h6', '^l1', '^l2', '^Pa']:
                style_bold_on = False  # MMB
                replaced_hl = True
                current_word = current_word[3:]

                # print(f'-----current_word:{current_word}')
            if current_word[:3] == '^b1':
                style_bold_on = not style_bold_on  # True
                current_word = current_word[3:]

            elif index_word > 0:
                previous_w = text_to_convert_list[index_word-1]
                if previous_w[-3:] == '^b1':
                    style_bold_on = False  # This is correct only if the bold style was set for the previous word

            if replaced_hl:
                current_word = style_prefix + current_word

            current_word = current_word.replace('^b1', '')
            if '$Page' in current_word:
                current_word = current_word.replace('$', '')
            index_letter = 0
            while index_letter < len(current_word):

                xt_rect = []
                current_letter = current_word[index_letter]
                # print(f'current_letter: |{current_letter}|')

                if style_case_upper:
                    current_letter = current_letter.upper()
                elif style_case_lower:
                    current_letter = current_letter.lower()

                current_letter_ps = char_from_ascii_tps(ascii_ps_list, searched_char=current_letter, device=series)

                # If the character is not found, replace by the placeholder_char
                if current_letter_ps == []:
                    ascii_equivalent = unidecode(current_letter)
                    current_letter_ps = char_from_ascii_tps(ascii_ps_list, searched_char=ascii_equivalent[0], device=series)
                    if current_letter_ps == []:
                        print(f'**- char_from_ascii_tps: "{current_letter}" not found. Replacing with "{not_found_placeholder}"')
                        current_letter = not_found_placeholder
                        current_letter_ps = char_from_ascii_tps(ascii_ps_list, searched_char=current_letter, device=series)

                current_word_ps_count += len(current_letter_ps)

                # Retrieve vertical offset adjustments, if needed
                if current_letter in fonts_vertical_adjustment:
                    a_v_offset = fonts_vertical_adjustment[current_letter]
                else:
                    a_v_offset = 0

                # Reposition the current_letter_ps based on its position
                if get_xt_point:
                    if style_type == 'heading':
                        new_weight = style_weight
                    else:
                        if style_bold_on:
                            new_weight = style_bold_weight
                        else:
                            new_weight = 300
                else:
                    if style_bold_on:
                        new_weight = style_bold_weight
                    else:
                        new_weight = 300

                current_letter_ps_, letter_width, letter_height, xt_rect = strokes_at_point(
                    current_letter_ps, pos_x, pos_y + a_v_offset*a_v_offset_ratio, scratio=scratio, get_xt_point=get_xt_point, new_weight=new_weight,
                    series=series)

                style_char_ps_ = []
                if get_xt_point:
                    list_all_x.append(xt_rect[0])
                    list_all_y.append(xt_rect[1])
                    list_all_x.append(xt_rect[2])
                    list_all_y.append(xt_rect[3])
                    # reposition the style char ps
                    # Retrieve vertical offset adjustments, if needed
                    if style_char in fonts_vertical_adjustment:
                        a_v_offset = fonts_vertical_adjustment[style_char]
                    else:
                        a_v_offset = 0
                    if style_char_ps:
                        style_char_ps_, _, _, _ = strokes_at_point(
                            style_char_ps, pos_x, pos_y + a_v_offset*a_v_offset_ratio, scratio=scratio, get_xt_point=False,
                            series=series)

                current_word_ps.append(style_char_ps_)
                current_word_ps.append(current_letter_ps_)

                # Compute the current max_row_height
                max_row_height = max(max_row_height, letter_height)

                # Compute the current horizontal position
                if style_weight:
                    delta_multiplier = round(3*(style_weight-400)/400)
                else:
                    delta_multiplier = 0
                pos_x += letter_width + delta_x*(1+delta_multiplier)
                sum_letter_width += letter_width
                count_letter_width += 1

                # Check if carriage return necessary
                carriage_return = False
                if index_letter == len(current_word) - 1:
                    if index_word+1 < nb_words:
                        next_word = text_to_convert_list[index_word+1]
                        avg_letter_width = sum_letter_width/count_letter_width
                        if next_word != '^p':
                            # print(f'---current_word:{current_word} next_word:{next_word}')
                            estimated_next_word_length = (len(next_word) + 1)*(avg_letter_width)
                            carriage_return = pos_x + estimated_next_word_length > max_horizontal_length_ps

                # If the horizontal position is getting offscreen, break the line
                if (pos_x > max_horizontal_length_ps) or carriage_return:
                    # TODO: the formula under "round()" is incorrect - MAX_HORIZONTAL_PIXELS*MARGIN_LEFT_SIZE*(1-scratio)
                    # Go to next vertical row
                    pos_y += max_row_height + delta_y

                    if pos_y > max_vertical_pixels/scratio - padding_vertical:

                        if len(list_ps) > 0:

                            if len(current_word_ps) > 0:
                                _temp_list = []
                                for sub_list in current_word_ps:
                                    _temp_list.extend(sub_list)
                                list_ps.extend(_temp_list)

                            current_word_ps_count = len(current_word_ps)

                            if current_word_ps_count > 0:
                                list_ps = list_ps[:-1*current_word_ps_count]
                            # index_word -= 1
                            index_letter = 0
                            current_word_ps_count = 0
                            current_word_ps = []
                            current_letter_ps = []
                            current_letter_ps_ = []
                            style_char_ps_ = []

                            dict_ps[f'{page_number+1}'] = list(list_ps)

                            list_ps = []

                            page_number += 1

                        else:
                            print(f'----list_ps was empty for word: {current_word} ')
                            print(f'------len(current_word_ps): {len(current_word_ps)}')
                            print(f'-------page_number: {page_number}')
                        pos_x = starting_pos_x
                        pos_y = starting_pos_y
                        list_ps = []
                        list_titles_image = []

                    # Reposition to starting horizontal position
                    pos_x = starting_pos_x

                    current_xl_points = []

                    if current_letter != style_char:
                        if len(current_letter_ps_) > 0:
                            title_color = str(current_letter_ps_[0]['color'])
                        l_title_vl = [x['vector_points'] for x in current_letter_ps_]

                        if title_color in title_ps_c:
                            a_color_list = title_ps_c[title_color]
                            a_color_list.extend(l_title_vl)
                        else:
                            title_ps_c[title_color] = l_title_vl
                        title_ps.extend(l_title_vl)
                    index_letter += 1
                    max_row_height = 0
                    continue

                # There is still room horizontally
                else:

                    if current_letter != style_char:
                        if len(current_letter_ps_) > 0:
                            title_color = str(current_letter_ps_[0]['color'])
                        l_title_vl = [x['vector_points'] for x in current_letter_ps_]

                        if title_color in title_ps_c:
                            a_color_list = title_ps_c[title_color]
                            a_color_list.extend(l_title_vl)
                        else:
                            title_ps_c[title_color] = l_title_vl
                        title_ps.extend(l_title_vl)

                    # Add all the individual points of the character strokes for XL
                    if xl_json:
                        for a_ps in current_letter_ps_:
                            for x in a_ps['vector_points']:
                                current_xl_points.append((x[0], x[1]))

                index_letter += 1

            if len(current_word_ps) > 0:
                _temp_list = []
                for sub_list in current_word_ps:
                    _temp_list.extend(sub_list)
                list_ps.extend(_temp_list)
                current_word_ps = []
                current_letter_ps = []
                current_letter_ps_ = []
                style_char_ps_ = []

                if xl_json:
                    list_xl_points.extend(current_xl_points)

                index_word += 1
                nb_loops = 0
                pos_x += delta_x + word_separator
            else:
                nb_loops += 1
                index_word += 1

            if list_all_x != []:
                if len(current_letter_ps_) > 0:
                    title_color = str(current_letter_ps_[0]['color'])
                    l_title_vl = [x['vector_points'] for x in current_letter_ps_]
                    title_ps.extend(l_title_vl)
                    if title_color in title_ps_c:
                        a_color_list = title_ps_c[title_color]
                        a_color_list.extend(l_title_vl)
                    else:
                        title_ps_c[title_color] = l_title_vl

                min_xt_x = min(list_all_x)
                min_xt_y = min(list_all_y)
                max_xt_x = max(list_all_x)
                max_xt_y = max(list_all_y)
                
                off_minx_v = 0
                off_miny_v = -10
                off_maxx_v = 10
                off_maxy_v = 10

                min_xt_x_v = round((adb_screen_max_x+16-max_xt_x)*(max_horizontal_pixels/adb_screen_max_x)+off_minx_v)
                min_xt_y_v = round((min_xt_y+32)*(max_vertical_pixels/adb_screen_max_y)+off_miny_v)

                hl_list = [
                    min_xt_x_v,
                    min_xt_y_v,
                    round((adb_screen_max_x+16-min_xt_x)*(max_horizontal_pixels/adb_screen_max_x)+off_maxx_v)-min_xt_x_v+25,
                    round((max_xt_y+32)*(max_vertical_pixels/adb_screen_max_y)+off_maxy_v)-min_xt_y_v]

                page_str = str(page_number+1)

                if index_word == list_cw_reset + 1:

                    # We only save images for toc for heading, not for bullet-points
                    if style_type == 'heading':
                        if page_str in titles_dict:

                            page_titles = titles_dict[page_str]

                            a_title_dict = {"rect": hl_list, "ps": title_ps_c, "pdf_page": pdf_page_nb}
                            page_titles.append(a_title_dict)
                            titles_dict[page_str] = page_titles
                        else:
                            titles_dict[page_str] = [
                                {
                                    "rect": hl_list,
                                    "ps": title_ps_c,
                                    "pdf_page": pdf_page_nb}
                                    ]
                    style_type = ''

        if nb_loops >= max_iterations_loop:
            print()
            print('**- text_to_pen_strokes: WARNING: A likely infinite while loop was detected and exited')
            print(text_to_convert_list)

        if list_ps != []:
            dict_ps[f'{page_number + 1}'] = list(list_ps)
            list_ps = []

        for a_page, a_list_ps in dict_ps.items():
            full_dict_ps[a_page] = {
                "strokes_nb": len(a_list_ps),
                "strokes": a_list_ps}

        list_xl_points = list(set(list_xl_points))
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)
        for a_page, a_pagetitle_ps_list in titles_dict.items():

            list_titles_image = []

            for a_pagetitle in a_pagetitle_ps_list:
                a_pagetitle_rect = a_pagetitle['rect']
                a_pagetitle_ps = a_pagetitle['ps']
                a_pagetitle_pdf = a_pagetitle['pdf_page']

                new_dict = {}
                for tpk_key, tps_value in a_pagetitle_ps.items():
                    converted_list = []

                    for an_el in tps_value:
                        an_el_converted = [
                            topright_to_topleft(
                                x, max_horizontal_pixels, max_vertical_pixels,
                                adb_screen_max_x, adb_screen_max_y) for x in an_el]
                        converted_list.append(an_el_converted)

                    new_dict[tpk_key] = list(converted_list)

                an_image = draw_bitmap_from_vectors(new_dict, (max_horizontal_pixels, max_vertical_pixels), image_crop=a_pagetitle_rect, background_color=style_bg_color)
                # an_image = draw_bitmap_from_vectors(new_dict, (MAX_HORIZONTAL_PIXELS_N6, MAX_VERTICAL_PIXELS_N6), image_crop=a_pagetitle_rect, background_color=style_bg_color)
                # an_image = an_image.convert('L')
                # png_buffer = io.BytesIO()
                # an_image.save(png_buffer, format="PNG")
                # an_image.show()
                rleimage, encoded = rle_encode_img(an_image)
                # print(f'------a_pagetitle_rect:{a_pagetitle_rect}')
                # apt0, apt1, apt2, apt3 = a_pagetitle_rect 
                # a_pagetitle_rect = [apt0-47, apt1, apt2, apt3]
                # print(f'------a_pagetitle_rect CHANGED:{a_pagetitle_rect}')
                if encoded:
                    rleimage_size = len(rleimage)
                    placeholder_image_size = int_to_little_endian_bytes(rleimage_size)
                    result = placeholder_image_size + rleimage

                    list_titles_image.append([a_pagetitle_rect, result.hex(), a_pagetitle_pdf])

            images_dict[a_page] = list(list_titles_image)

        if xl_json:
            new_list_xl_points = []
            for a_point in list_xl_points:
                new_list_xl_points.extend([a_point[0], a_point[1]])
            return full_dict_ps, new_list_xl_points, images_dict
        return full_dict_ps, None,  images_dict
    except Exception as e:
        print()
        print(f'*** text_to_pen_strokes: {e}')
        if xl_json:
            return None, None, None
        return None, None, None


def int_to_little_endian_bytes(value, num_bytes=4, byteorder='little'):
    # Ensures valid endian type
    if byteorder not in ['little', 'big']:
        raise ValueError("Endian must be 'little' or 'big'")
    # Ensure the value fits within the specified number of bytes
    if value < 0 or value >= (1 << (num_bytes * 8)):
        raise ValueError(f"Value {value} out of range for {num_bytes} bytes")

    # Convert the integer to a byte sequence in the specified endian format
    return value.to_bytes(num_bytes, byteorder=byteorder)


def rle_encode_img1(image):
    """ Ephemeral bitmap images are encoded in the .note and .mark files
        using a 'simple' RLE algorithm.
        Encodes image data in a run-length algorithm compatible with the
        supernote decode. This assumes that palette is none and blank is none
        TODO: comment all steps
    """
    encoded = False
    try:
        segments_list = []
        result = bytearray()
        # Convert the image to grayscale mode 'L'
        try:
            gray_image = image.convert('L')
        except Exception as e:
            print(f'*** Convert image: {e}')
            return image, True
        # Convert the grayscale image to a NumPy array
        img_array = np.array(gray_image)
        # Flatten the array to 1D
        pixels = img_array.flatten()
        # Detect where the value changes
        changes = np.diff(pixels) != 0
        change_indices = np.where(changes)[0] + 1
        # Include the start and end of the array
        segment_starts = np.concatenate(([0], change_indices))
        segment_ends = np.concatenate((change_indices, [len(pixels)]))

        large_threshold = int(str(SPECIAL_LENGTH))
        zip_data = zip(segment_starts, segment_ends)

        for start, end in zip_data:
            length_s = end - start
            color_s = pixels[start]

            segments_list.append((start, color_s, length_s))
            if color_s == 255:
                color_s = COLORCODE_BACKGROUND      # 62
            elif color_s == 0:
                color_s = 0x61
            elif color_s == 254:
                color_s = COLORCODE_WHITE           # 65
            elif color_s == 201:
                color_s = 0xCA
            elif color_s == 157:
                color_s = 0x9E       # 63
            else:
                color_s = 0xb7   #

            if length_s > 127:
                q_128, r_128 = divmod(length_s, 128)
                if r_128 == 0 and (128+q_128-1 < 255):
                    result.extend([color_s] + list(bytes.fromhex(format(128+q_128-1, '02x'))))
                else:
                    quotient, remainder = divmod(length_s, large_threshold)
                    result.extend([color_s, SPECIAL_LENGTH_MARKER]*quotient)
                    first_part = int((remainder-1)/128) + 127
                    second_part = remainder-1-((first_part % 127)*128)
                    b_1 = bytes.fromhex(format(first_part, '02x'))
                    b_2 = bytes.fromhex(format(second_part, '02x'))
                    if first_part == 127:
                        result.extend([color_s] + list(b_2))
                    else:
                        result.extend([color_s] + list(b_1) + [color_s] + list(b_2))
            else:
                try:
                    result.extend([color_s]+list(bytes.fromhex(format(length_s - 1, '02x'))))
                except Exception as e:
                    print(f'*** Error in RLE: {e}')
                    exit(1)
        encoded = True
    except Exception as e:
        print()
        print(f'*** Error in rle_encode_img: {e}')
        print(f'- 1:{first_part} 2:{second_part} r_128:{r_128} q:{128+q_128-1} length_s:{length_s - 1} - r: {128+q_128-1}')
        return None, encoded
    return result, encoded


def rle_encode_img(image):
    """ Ephemeral bitmap images are encoded in the .note and .mark files
        using a 'simple' RLE algorithm.
        Encodes image data in a run-length algorithm compatible with the
        supernote decode. This assumes that palette is none and blank is none
        TODO: comment all steps
    """
    encoded = False
    try:
        segments_list = []
        result = bytearray()
        # Convert the image to grayscale mode 'L'
        try:
            gray_image = image.convert('L')
        except Exception as e:
            print(f'*** Convert image: {e}')
            return image, True

        # Convert the grayscale image to a NumPy array
        img_array = np.array(gray_image)
        # Flatten the array to 1D
        pixels = img_array.flatten()
        # Detect where the value changes
        changes = np.diff(pixels) != 0
        change_indices = np.where(changes)[0] + 1
        # Include the start and end of the array
        segment_starts = np.concatenate(([0], change_indices))
        segment_ends = np.concatenate((change_indices, [len(pixels)]))

        large_threshold = int(str(SPECIAL_LENGTH))
        zip_data = zip(segment_starts, segment_ends)

        for start, end in zip_data:
            length_s = end - start
            color_s = pixels[start]

            segments_list.append((start, color_s, length_s))
            if color_s == 255:
                color_s = COLORCODE_BACKGROUND  # 62
            elif color_s == 0:
                color_s = 0x61
            elif color_s == 254:
                color_s = COLORCODE_WHITE  # 65
            elif color_s == 201:
                color_s = 0xCA
            elif color_s == 157:
                color_s = 0x9E  # 63
            else:
                color_s = 0xb7

            if length_s > 127:
                q_128, r_128 = divmod(length_s, 128)
                if r_128 == 0 and (128+q_128-1 < 255):
                    result.extend([color_s] + list(bytes.fromhex(format(128+q_128-1, '02x'))))
                else:
                    quotient, remainder = divmod(length_s, large_threshold)
                    result.extend([color_s, SPECIAL_LENGTH_MARKER]*quotient)
                    first_part = int((remainder-1)/128) + 127
                    second_part = remainder-1-((first_part % 127)*128)
                    try:
                        b_1 = bytes.fromhex(format(first_part, '02x'))
                        b_2 = bytes.fromhex(format(second_part, '02x'))
                        if first_part == 127:
                            result.extend([color_s] + list(b_2))
                        else:
                            result.extend([color_s] + list(b_1) + [color_s] + list(b_2))
                    except Exception as e:
                        # TODO: Find a flawless RLE encoder
                        # This is a problem in the encoder, (remainder == 0 creates negative "second_part)
                        # but I don't wan to display the error on the terminal and
                        # I don't want to change my flake rules so leaning "e"
                        e
            else:
                try:
                    result.extend([color_s]+list(bytes.fromhex(format(length_s - 1, '02x'))))
                except Exception as e:
                    print(f'*** Error in RLE: {e}')

        encoded = True
    except Exception as e:
        print()
        print(f'*** Error in rle_encode_img: {e}')
        print(f'- 1:{first_part} 2:{second_part} r_128:{r_128} q:{128+q_128-1} length_s:{length_s - 1} - r: {128+q_128-1}')
        return None, encoded

    return result, encoded


def datetime_identifier(file_id=None):
    """ Returns the current datetime as a 20 characters long string"""
    if file_id is None:
        original_uuid = uuid.uuid4()
        file_id_ = str(base64.encodebytes(original_uuid.bytes))
        file_id_ab = ''.join([x for x in file_id_ if x in 'AbcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'])
        file_id = file_id_ab[2:14]
    return str(datetime.now().strftime("%Y%m%d%H%M%S%f") + file_id)


def sn_bytes_header(version='20230015', type='note'):
    """ Returns the file signature for the binary header """
    signature = f'{type}SN_FILE_VER_{version}'
    return signature.encode('utf-8')


def sn_bytes_1_header_json(
    original_style='style_white', original_md5=0, templates_dict={}, file_type='NOTE',
        apply_equipment=NOTEBOOK_DEVICE, final_op_page=1, reco_lang=FILE_RECOGN_LANGUAGE,
        style_usage_type=0):
    """ Generates a file_id and returns it, alongside with the very first json, prefixed by its size"""
    # TODO: apply_equipment=NOTEBOOK_DEVICE N5 doesn't work on Manta. Perhaps need to check the adb max values, so hardcoding to N6 for now
    # print(f'-----apply_equipment:{apply_equipment} ADB_SCREEN_MAX_X:{ADB_SCREEN_MAX_X}  ADB_SCREEN_MAX_y:{ADB_SCREEN_MAX_Y}')

    # # # for a_page_t, a_value_t in templates_dict.items():
    # # #     an_md5 = a_value_t['md5']
    # # #     an_md5_ = an_md5.split('_')
    # # #     original_style = f'user_{an_md5_[0]}'
    # # #     original_md5 = an_md5
    # # #     break
    if reco_lang == 'none':
        text_reco = 0
    else:
        text_reco = 1

    file_id = f'F{datetime_identifier()}'
    part_1 = f'<MODULE_LABEL:SNFILE_FEATURE><FILE_TYPE:{file_type}><APPLY_EQUIPMENT:{apply_equipment}><FINALOPERATION_PAGE:{final_op_page}>'

    # part_2 = f'<FINALOPERATION_LAYER:1><ORIGINAL_STYLE:{original_style}><ORIGINAL_STYLEMD5:{original_md5}><DEVICE_DPI:0><SOFT_DPI:0><FILE_PARSE_TYPE:0><RATTA_ETMD:0><APP_VERSION:0>'

    part_2 = '<FINALOPERATION_LAYER:1><DEVICE_DPI:0><SOFT_DPI:0><FILE_PARSE_TYPE:0><RATTA_ETMD:0><APP_VERSION:0>'

    part_3 = f'<FILE_ID:{file_id}><FILE_RECOGN_TYPE:{text_reco}><FILE_RECOGN_LANGUAGE:{reco_lang}>'

    if len(templates_dict.keys()) > 0:
        used_styles_list = []
        part_4 = ''
        for a_page in templates_dict:
            template_page_dict = templates_dict[a_page]
            md5 = template_page_dict['md5']
            if md5 not in used_styles_list:
                used_styles_list.append(md5)
                # style_p1_name = f'user_pdf_xyz_{len(used_styles_list)}'
                style_p1_name = f'user_{md5}'
                pdfstylelist_name = f'{style_p1_name}_{md5}'
                pdfstylelist_name_encoded = base64.b64encode(pdfstylelist_name.encode('utf-8'))
                template_page_dict['pdfstylelist'] = {
                    'name': pdfstylelist_name,
                    'encoded': pdfstylelist_name_encoded}
                # part_4 += f'<PDFSTYLE:{style_p1_name}>'
                # part_4 += f'<PDFSTYLEMD5:{md5}>'
                part_4 += '<PDFSTYLE:none>'
                part_4 += '<PDFSTYLEMD5:0>'
        part_4 += '<STYLEUSAGETYPE:1>'
    else:
        part_4 = '<PDFSTYLE:none><PDFSTYLEMD5:0><STYLEUSAGETYPE:0>'
    part_5 = '<HIGHLIGHTINFO:0><HORIZONTAL_CHECK:0><IS_OLD_APPLY_EQUIPMENT:1><ANTIALIASING_CONVERT:2>'
    json_ = f'{part_1}{part_2}{part_3}{part_4}{part_5}'.encode('utf-8')
    # Computing firs json's size
    json_s = int_to_little_endian_bytes(len(json_))
    result = json_s + json_

    return file_id, result, templates_dict


def sn_bytes_2_basic_still_image(placeholder_bitmap_fn, testing_size=None):
    """ RLE Encode a bitmap image and returns its bytes, prefixed
        by 4 bytes indicating its size """
    try:
        result = None
        placeholder_image = Image.open(placeholder_bitmap_fn)

        placeholder_image = placeholder_image.convert('L')

        rleimage, encoded = rle_encode_img(placeholder_image)

        # Testing size is for debug only. TODO: Remove before publication
        if testing_size is not None:
            rleimage = rleimage[:testing_size]

        if encoded:
            rleimage_size = len(rleimage)
            placeholder_image_size = int_to_little_endian_bytes(rleimage_size)
            result = placeholder_image_size + rleimage
    except Exception as e:
        print()
        print(f'*** sn_bytes_2_basic_still_image: {e}')
    return result


def sn_bytes_layer_json(bitmap_address, file_type='NOTE', protocol='RATTA_RLE', name='MAINLAYER', layerpath=0, vectorgraph=0, recogn=0):
    """ Generates json for a layer and returns it, prefixed by its size"""
    a_json = f'<LAYERTYPE:{file_type}><LAYERPROTOCOL:{protocol}><LAYERNAME:{name}><LAYERPATH:{layerpath}>'
    a_json += f'<LAYERBITMAP:{bitmap_address}><LAYERVECTORGRAPH:{vectorgraph}><LAYERRECOGN:{recogn}>'
    a_json_ = a_json.encode('utf-8')

    # Computing json's size
    json_s = int_to_little_endian_bytes(len(a_json_))
    result = json_s + a_json_
    return result


def sn_bytes_page(
    main_layer_address, background_layer_address, totalpath_address,
        apply_equipment=NOTEBOOK_DEVICE, page_style_name=None,
        page_style_md5=None, rec_status=0, rec_address=0, recf_status=0, rec_page_type=0):
    """ Generates json for a page and returns it, prefixed by its size"""
    page_id = f'P{datetime_identifier()}'

    if page_style_name is None:
        if apply_equipment == 'N5':
            a_json = '<PAGESTYLE:style_white_a5x2>'
        else:
            a_json = '<PAGESTYLE:style_white>'
    else:
        a_json = f'<PAGESTYLE:{page_style_name}>'

    if page_style_md5 is None:
        a_json += '<PAGESTYLEMD5:0>'
    else:
        a_json += f'<PAGESTYLEMD5:{page_style_md5}>'

    a_json += '<LAYERINFO:[{"layerId"#3,"name"#"Layer 3","isBackgroundLayer"#false,"isAllowAdd"#false,'
    a_json += '"isCurrentLayer"#false,"isVisible"#true,"isDeleted"#true,"isAllowUp"#false,"isAllowDown"#false},{"layerId"#2,"name"#"Layer 2",'
    a_json += '"isBackgroundLayer"#false,"isAllowAdd"#false,"isCurrentLayer"#false,"isVisible"#true,"isDeleted"#true,"isAllowUp"#false,"isAllowDown"#false},'
    a_json += '{"layerId"#1,"name"#"Layer 1","isBackgroundLayer"#false,"isAllowAdd"#false,"isCurrentLayer"#false,"isVisible"#true,"isDeleted"#true,"isAllowUp"'
    a_json += '#false,"isAllowDown"#false},{"layerId"#0,"name"#"Main Layer","isBackgroundLayer"#false,"isAllowAdd"#false,"isCurrentLayer"#true,"isVisible"#true,'
    a_json += '"isDeleted"#false,"isAllowUp"#false,"isAllowDown"#false},{"layerId"#-1,"name"#"Background '
    a_json += 'Layer","isBackgroundLayer"#true,"isAllowAdd"#true,"isCurrentLayer"#false,"isVisible"#true,"isDeleted"#false,"isAllowUp"#false,"isAllowDown"#false}]>'
    a_json += f'<LAYERSEQ:MAINLAYER,BGLAYER><MAINLAYER:{main_layer_address}><LAYER1:0><LAYER2:0><LAYER3:0><BGLAYER:{background_layer_address}><TOTALPATH:{totalpath_address}>'
    a_json += f'<THUMBNAILTYPE:0><RECOGNSTATUS:{rec_status}><RECOGNTEXT:{rec_address}><RECOGNFILE:0><PAGEID:{page_id}><RECOGNTYPE:{rec_page_type}><RECOGNFILESTATUS:{recf_status}><RECOGNLANGUAGE:none>'
    a_json += '<EXTERNALLINKINFO:0><IDTABLE:0><ORIENTATION:1000>'
    a_json_ = a_json.encode('utf-8')
    json_s = int_to_little_endian_bytes(len(a_json_))
    result = json_s + a_json_
    return result


def sn_bytes_page_recap(
    page_recap_location, page_address_dict, placeholder_image_bloc_address,
        apply_equipment=NOTEBOOK_DEVICE, templates_dict={}, titles_recap='', page_keyword_dict={}):
    """ Generates json for the page recap dict, including prefix size, tail and validator"""
    try:
        a_json = ''
        for a_page_key, a_page_value in page_address_dict.items():
            a_json += f'<PAGE{a_page_key}:{a_page_value["pg_address"]}>'

        if titles_recap != '':
            a_json += titles_recap

        for a_page_key, a_page_value in page_keyword_dict.items():
            a_json += f'<TITLE_{a_page_key}:{a_page_value["pg_address"]}>'

        a_json += '<COVER_0:0><DIRTY:0><FILE_FEATURE:24>'
        added_styles_list = []
        for a_page_key, a_page_value in page_address_dict.items():

            if a_page_key in templates_dict:
                a_page_templates_dict = templates_dict[a_page_key]
                pdfstylelist_dict = a_page_templates_dict['pdfstylelist']

                style_name_ = pdfstylelist_dict['name'].split('_')

                style_name = f'{style_name_[0]}_{style_name_[1]}{style_name_[3]}_{style_name_[4]}'
                if style_name not in added_styles_list:
                    added_styles_list.append(style_name)
                    style_rle_address = a_page_templates_dict['address']
                    a_json += f'<STYLE_{style_name}:{style_rle_address}>'

            # Removing this because it seems that every note references the style_white
            # else:
            #     if 'style_white' not in added_styles_list:
            #         added_styles_list.append('style_white')
            #         a_json += f'<STYLE_style_white:{placeholder_image_bloc_address}>'

            if apply_equipment == 'N5':
                if 'style_white_a5x2' not in added_styles_list:
                    added_styles_list.append('style_white_a5x2')
                    a_json += f'<STYLE_style_white_a5x2:{placeholder_image_bloc_address}>'
            else:
                if 'style_white' not in added_styles_list:  # MMB 241120
                    added_styles_list.append('style_white')
                    a_json += f'<STYLE_style_white:{placeholder_image_bloc_address}>'

        a_json += 'tail'
        a_json_ = a_json.encode('utf-8')
        json_s = int_to_little_endian_bytes(len(a_json_)-4)
        result = json_s + a_json_ + int_to_little_endian_bytes(page_recap_location)
    except Exception as e:
        print(f'*** sn_bytes_page_recap: {e}')
    return result


def get_pen_stokes_list_from_table(asciiset_fn=DEFAULT_FONTS, font_name=FONT_NAME):
    """ Gets the list of pen strokes.
        Parameters:
        - asciiset_fn: A Notebook containing an fonts in an ASCII table format
                        See char_from_ascii_tps for default format of that table
        It is preferable to store this in one variable as Mac with 8GB reaches some limits"""
    try:
        print(f'   > Getting penstrokes for font: {font_name} using {asciiset_fn}')
        print()
        ascii_type = None
        # Load the dictionary of pen strokes contained in the fonts note
        ascii_pen_strokes_dict, ascii_type = get_pen_strokes_dict(asciiset_fn, search_keyword=font_name)

        # Extract its list of pen strokes (we assume the ascii table fits in page 1)
        for akey, avalue in ascii_pen_strokes_dict.items():
            return avalue['strokes'], ascii_type

        # ascii_ps_list = ascii_pen_strokes_dict['1']['strokes']
        # return ascii_ps_list
    except Exception as e:
        print(f'*** get_pen_stokes_list_from_table: {e}')
        return [], None


def build_titles_image_n_details(titles_dict, current_bytes):
    """ Generates successive blocks of title bitmap blocks,
        followed by title details block"""

    titles_recap = ''

    try:

        for a_page, a_page_titles in titles_dict.items():

            for a_page_title in a_page_titles:

                starting_position = len(current_bytes)
                title_bitmap_rect = a_page_title[0]
                title_bitmap_hex = a_page_title[1]
                title_bitmap_bytes = bytes.fromhex(title_bitmap_hex)
                title_bitmap_size = len(title_bitmap_bytes)
                title_bitmap_size_bytes = int_to_little_endian_bytes(title_bitmap_size)

                current_bytes += title_bitmap_bytes

                detail_location = len(current_bytes)
                x, y, dx, dy = title_bitmap_rect
                title_detail_str = f'<TITLESEQNO:0><TITLELEVEL:1><TITLERECT:{x},{y},{dx},{dy}><TITLERECTORI:{x},{y},{dx},{dy}>'
                title_detail_str += f'<TITLEBITMAP:{starting_position}><TITLEPROTOCOL:RATTA_RLE><TITLESTYLE:1201000>'
                title_detail_str_ = title_detail_str.encode('utf-8')
                title_detail_str_size = len(title_detail_str_)
                title_detail_str_size_bytes = int_to_little_endian_bytes(title_detail_str_size)

                current_bytes += title_detail_str_size_bytes + title_detail_str_

                titles_recap += f'<TITLE_{a_page.zfill(4)}{str(y).zfill(4)}{str(x).zfill(4)}:{detail_location}>'

    except Exception as e:
        print(f'*** build_titles_image_n_details: {e}')

    return current_bytes, titles_recap


def text_to_note(text_to_convert, output_fn, ascii_ps_list,  starting_point=None, scratio=1, series=NOTEBOOK_DEVICE, sn_pdf_fn=''):
    """ Create a json with pen_strokes corresponding to text

    Parameters:
        - Starting_point: Pixels coordinate of the first print on the notebook
        - text_to_convert: Text to convert
        - output_fn: Name of output file in a .Note format

    Returns: a dictionary with list of strokes for each page
        """
    try:

        if starting_point is None:
            if series in ['N5']:
                starting_point = [10, 250]
            else:
                starting_point = [7, 183]

        ps_dict, xl_list, titles_dict = text_to_pen_strokes(
            text_to_convert, ascii_ps_list, starting_point, scratio=scratio, series=series)

        if DEBUG_MODE:
            save_json(output_fn+'_titles_t2notes.json', titles_dict)

        # ---- Binary .Note file building ---------

        file_signature = sn_bytes_header()  # Generate the file signature
        file_id, first_json, _ = sn_bytes_1_header_json(apply_equipment=series)  # Generate fileid and 1st json

        # placeholder_image_bloc = sn_bytes_2_basic_still_image(placeholder_bitmap_fn)
        placeholder_image_bloc = blank_encoded_image(series)

        # Load the dictionary of binary representations of totalpath, per page
        totalpath_bytes = pen_strokes_dict_to_bytes(ps_dict, series=series)

        image_layer_dict = generate_ephemeral_images(ps_dict, series=series)

        first_bytes_before_image = file_signature + first_json

        current_bytes = first_bytes_before_image

        placeholder_image_bloc_address = len(current_bytes)
        current_bytes += placeholder_image_bloc

        main_dict = {}
        for ild_p, ild_v in image_layer_dict.items():
            ild_bitmap_address = len(current_bytes)
            current_bytes += ild_v
            ild_main = sn_bytes_layer_json(ild_bitmap_address, name='MAINLAYER')
            ild_main_address = len(current_bytes)
            current_bytes += ild_main
            main_dict[ild_p] = ild_main_address
        background_layer_address = len(current_bytes)

        background_layer = sn_bytes_layer_json(placeholder_image_bloc_address, name='BGLAYER')
        current_bytes += background_layer

        current_bytes, titles_recap = build_titles_image_n_details(titles_dict, current_bytes)

        # print(f'----titles_dict:{titles_dict}')

        # Build the page links, if applicable
        title_links_recap = ''
        if sn_pdf_fn != '':

            for sn_page_nb, list_title_links in titles_dict.items():
                for a_title_link in list_title_links:
                    link_rect, _, pdf_page_nb = a_title_link
                    link_name, link_json_address, current_bytes = generate_link_json(
                        sn_page_nb, sn_pdf_fn, pdf_page_nb, link_rect, current_bytes)
                    title_links_recap += f'<{link_name}:{link_json_address}>'

        titles_recap += title_links_recap

        # Parsing the pages and add totalpath bytes for each page
        totalpath_address_dict = {}
        for a_page_nb, page_totalpath_bytes in totalpath_bytes.items():
            totalpath_size = len(page_totalpath_bytes)
            totalpath_size_bytes = int_to_little_endian_bytes(totalpath_size)
            totalpath_address_dict[a_page_nb] = {"tp_address": len(current_bytes)}
            current_bytes += totalpath_size_bytes + page_totalpath_bytes

        # Parsing the pages and add page information for each page
        for a_page_nb in totalpath_address_dict:
            a_page_ref_dict = totalpath_address_dict[a_page_nb]
            a_page_total_path = a_page_ref_dict['tp_address']
            a_page_ref_dict['pg_address'] = len(current_bytes)

            main_layer_address = main_dict[a_page_nb]

            page_bytes = sn_bytes_page(main_layer_address, background_layer_address, a_page_total_path)
            current_bytes += page_bytes

        page_recap_location = len(current_bytes)

        recap_pages = sn_bytes_page_recap(
            page_recap_location, totalpath_address_dict, placeholder_image_bloc_address,
            apply_equipment=series, titles_recap=titles_recap)

        current_bytes += recap_pages

        with open(output_fn, 'wb') as file:
            file.write(current_bytes)

    except Exception as e:
        print()
        print(f'*** text_to_note: {e}')


def import_pics(mypdf, output_fn, starting_point=None, scratio=1, not_found_placeholder='#', series=NOTEBOOK_DEVICE):
    """ Import a list of images into a note format """
    try:
        source_folder = os.path.dirname(mypdf)
        templates_dict = {}
        background_layer_address = 0

        # Detecting the pattern for settings file in folder
        # (must start with "settings")
        pattern = re.compile(r'^settings.*\.txt$')
        matching_files = [f for f in os.listdir(source_folder) if pattern.match(f)]

        if len(matching_files) > 0:
            # Using settings from folder
            settings_path = os.path.join(source_folder, matching_files[0])

            import_settings = read_json(settings_path)
            print('')
            print(f'  > Using {matching_files[0]} ...')

            if 'thinning' in import_settings:
                thinning = import_settings['thinning']
                print(f'   - thinning: {thinning}')
            else:
                thinning = THINNING

            if 'default_pen_size' in import_settings:
                default_pen_size = str(import_settings['default_pen_size'])
                print(f'   - pen_size: {default_pen_size}')
            else:
                default_pen_size = DEFAULT_PEN_SIZE

            if 'default_pen_color' in import_settings:
                default_pen_color = str(import_settings['default_pen_color'])
                print(f'   - default_pen_color: {default_pen_color}')
            else:
                default_pen_color = DEFAULT_PEN_COLOR

            if 'adaptative' in import_settings:
                adaptative = bool(import_settings['adaptative'])
                print(f'   - adaptative: {adaptative}')
            else:
                adaptative = ADAPTATIVE_THRESHOLDING

            if 'recognize' in import_settings:
                recognize = bool(import_settings['recognize'])
                print(f'   - recognize: {recognize}')
            else:
                recognize = RECOGNIZE_TEXT

            if 'split' in import_settings:
                unstitch = bool(import_settings['split'])
                print(f'   - split: {unstitch}')
            else:
                unstitch = UNSTITCH

            if 'remove_templates' in import_settings:
                remove_templates = bool(import_settings['remove_templates'])
                print(f'   - remove_templates: {remove_templates}')
            else:
                remove_templates = REMOVE_TEMPLATES

            if 'intensity' in import_settings:
                intensity = import_settings['intensity']
                print(f'   - intensity: {intensity}')
            else:
                intensity = INTENSITY

            if 'noise_reduction' in import_settings:
                noise_reduction = import_settings['noise_reduction']
                print(f'   - noise_reduction: {noise_reduction}')
            else:
                noise_reduction = NOISE_REDUCTION

            if 'detail_level' in import_settings:
                detail_level = import_settings['detail_level']
                print(f'   - detail_level: {detail_level}')
            else:
                detail_level = DETAIL_LEVEL

            if 'shadow_blur' in import_settings:
                shadow_blur = import_settings['shadow_blur']
                print(f'   - shadow_blur: {shadow_blur}')
            else:
                shadow_blur = SHADOW_BLUR

            if 'gaussian_blur' in import_settings:
                gaussian_blur = import_settings['gaussian_blur']
                print(f'   - gaussian_blur: {gaussian_blur}')
            else:
                gaussian_blur = GAUSSIAN_BLUR

            if 'min_dimension' in import_settings:
                min_dimension = import_settings['min_dimension']
                print(f'   - min_dimension: {min_dimension}')
            else:
                min_dimension = MIN_DIMENSION

            if 'density_min_area' in import_settings:
                density_min_area = import_settings['density_min_area']
                print(f'   - density_min_area: {density_min_area}')
            else:
                density_min_area = DENSE_MIN_AREA

            if 'density_sensitivity' in import_settings:
                density_sensitivity = import_settings['density_sensitivity']
                print(f'   - density_sensitivity: {density_sensitivity}')
            else:
                density_sensitivity = DENSE_SENSITIVITY

            if 'density_threshold' in import_settings:
                density_threshold = import_settings['density_threshold']
                print(f'   - density_threshold: {density_threshold}')
            else:
                density_threshold = DENSE_THRESHOLD

            if 'pre_denoise' in import_settings:
                pre_denoise = import_settings['pre_denoise']
                print(f'   - pre_denoise: {pre_denoise}')
            else:
                pre_denoise = PRE_DENOISE

            print()
        else:
            # Using default settings
            thinning = THINNING
            default_pen_size = DEFAULT_PEN_SIZE
            default_pen_color = DEFAULT_PEN_COLOR
            adaptative = ADAPTATIVE_THRESHOLDING
            unstitch = UNSTITCH
            intensity = INTENSITY
            noise_reduction = NOISE_REDUCTION
            detail_level = DETAIL_LEVEL
            shadow_blur = SHADOW_BLUR
            gaussian_blur = GAUSSIAN_BLUR
            min_dimension = MIN_DIMENSION
            density_min_area = DENSE_MIN_AREA
            density_sensitivity = DENSE_SENSITIVITY
            density_threshold = DENSE_THRESHOLD
            min_dimension = MIN_DIMENSION
            pre_denoise = PRE_DENOISE
            recognize = RECOGNIZE_TEXT
            remove_templates = REMOVE_TEMPLATES

        max_horizontal_pixels, max_vertical_pixels, _, _ = series_bounds(series)

        # Create the list of images
        pics_list, resizing_list, encoded_rec_text, hw_ocr_detail_list = pdf_to_cv2_grayscale(
            mypdf, unstitch=unstitch, min_dimension=min_dimension, recognize=recognize, series=series)
        print()

        import_json_fn = mypdf[:-4] + '.json'

        if starting_point is None:
            starting_point = [-20, 250]

        # Initialize dict index
        a_pic_index = 1
        ps_dict = {}

        len_pix = len(pics_list)

        # Parsing the list of pictures
        for a_pic in pics_list:

            print(f'  > Processing image {a_pic_index}/{len_pix}', end='', flush=True)
            a_pic_fn = f'{mypdf[:-4]}_pic_{a_pic_index}.json'

            # Generate list of paths and contours for current image
            vect_points, template_image = image2vectors(
                a_pic_fn, a_pic,
                adaptative_thresholding=adaptative,
                gaussian_blur=gaussian_blur,
                intensity=intensity,
                detail_level=detail_level,
                noise_reduction=noise_reduction,
                shadow_blur_factor=shadow_blur,
                thinning=thinning,
                density_min_area=density_min_area,
                density_sensitivity=density_sensitivity,
                density_threshold=density_threshold,
                resizing_list=resizing_list,
                pre_denoise=pre_denoise,
                remove_templates=remove_templates,
                series=series)

            if template_image is not None:
                # pil_image = Image.fromarray(template_image)
                pil_image = Image.fromarray(template_image).resize((max_horizontal_pixels, max_vertical_pixels), Image.Resampling.LANCZOS)
                png_buffer = BytesIO()
                pil_image.save(png_buffer, format='PNG')
                png_binary = png_buffer.getvalue()
                pil_image = pil_image.convert('L')
                pil_image_encoded, encoded = rle_encode_img(pil_image)
                # pil_image_encoded, encoded = rle_encode_img(pil_image)
                if encoded:
                    an_md5 = binary_md5(pil_image_encoded)
                else:
                    pil_image_encoded = None
                    an_md5 = 0
                # Write the PNG binary content to a file
                with open(os.path.join(source_folder, f'user_{an_md5}.png'), 'wb') as f:
                    f.write(png_binary)

                pil_image_encoded = png_binary   # MMB testing
                templates_dict[str(a_pic_index)] = {
                    "image": template_image,
                    "rle_image": pil_image_encoded,
                    "md5": an_md5}

            # Build penstrokes from the list of paths
            # print(vect_points)
            # save_json(os.path.join(SN_VAULT, 'vp.json'), str(vect_points))
            ps_from_image = vectors2psd(
                vect_points, unstitch=unstitch,
                default_pen_size=default_pen_size,
                default_pen_color=default_pen_color,
                series=series)

            # Initialize a list of processed pen strokes
            processed_pen_strokes_list = []

            if 'strokes' in ps_from_image:
                image_strokes = ps_from_image['strokes']

            # Update dictionary of all penstrokes

            pen_strokes_dict = {}

            # If we have a list of OCR for importing image
            if hw_ocr_detail_list != []:

                if NOTEBOOK_DEVICE in ['N5']:
                    asciiset_fn = DEFAULT_FONTS2
                    a_v_offset_ratio = 1
                else:
                    asciiset_fn = DEFAULT_FONTS
                    a_v_offset_ratio = MAX_VERTICAL_PIXELS_N6 / MAX_VERTICAL_PIXELS_N5

                ascii_ps_list, ascii_type = get_pen_stokes_list_from_table(
                    asciiset_fn=asciiset_fn, font_name=FONT_NAME)
                if ascii_type != series:
                    print(f'   WARNING: Using a {series} device with a {ascii_type} ASCII table')

                # Style for weak OCR
                style_char_weak = char_from_ascii_tps(ascii_ps_list, bypassed_unicode=176, device=series)

                # Loading the fonts vertical offsets
                try:
                    fonts_vert_adj_json_fn = os.path.join(SN_VAULT, f'{FONT_NAME}.json')
                    fonts_vertical_adjustment = read_json(fonts_vert_adj_json_fn)
                    if fonts_vertical_adjustment == {}:
                        fonts_vertical_adjustment = CHAR_V_OFFSET
                except Exception as e:
                    print(f'*** Vertical adjustment: {e}')
                    print(f' >  File: {fonts_vert_adj_json_fn}. Using default')
                    fonts_vertical_adjustment = CHAR_V_OFFSET

                a_page_ocr_detail_list = hw_ocr_detail_list[a_pic_index-1]

                strokes = []
                strokes_nb = 0

                word_offset = 0
                previous_y = None
                row_vertical_height = 0
                cumulative_vertical_offset = 0
                offset_v_2 = 0
                for amapd_item in a_page_ocr_detail_list:
                    amapd_item_word = amapd_item[0]
                    x0, y0, x1, y1 = amapd_item[1]

                    amapd_item_confidence = amapd_item[2]

                    if previous_y is None:
                        previous_y = y0
                    else:
                        if y0 - previous_y > 80:
                            # print(f'---previous_y:{previous_y} -- Currenty: {y0}')
                            previous_y = y0
                            word_offset = 0
                            row_vertical_height = 0
                            cumulative_vertical_offset += offset_v_2
                            offset_v_2 = 0

                    if amapd_item_word != '':
                        index_letter = 0
                        position_letter = 0
                        a_word_width = 0
                        a_word_min_y = max_vertical_pixels
                        a_word_max_y = 0
                        a_word_height = 0
                        ascii_word_ps_list = []
                        ascii_word_ps_nb = 0

                        # First: extract all characters ps from table
                        word_ps_list = []
                        a_word_height_list = []

                        for a_char in amapd_item_word:

                            # Retrieve the character from ascii table
                            current_letter_ps = char_from_ascii_tps(
                                ascii_ps_list, searched_char=a_char, device=series)

                            if current_letter_ps == []:
                                print(f'**- char_from_ascii_tps: "{a_char}" not found. Replacing with "{not_found_placeholder}"')
                                current_letter = not_found_placeholder
                                current_letter_ps = char_from_ascii_tps(ascii_ps_list, searched_char=current_letter, device=series)

                            list_x = []
                            list_y = []

                            # Create boundary of the entire character
                            for a_p_s in current_letter_ps:
                                a_p_s_max_c_x = a_p_s['max_c_x']
                                a_p_s_min_c_x = a_p_s['min_c_x']
                                a_p_s_max_c_y = a_p_s['max_c_y']
                                a_p_s_min_c_y = a_p_s['min_c_y']
                                list_x.append(a_p_s_max_c_x)
                                list_x.append(a_p_s_min_c_x)
                                list_y.append(a_p_s_max_c_y)
                                list_y.append(a_p_s_min_c_y)

                            min_c_x = min(list_x)
                            min_c_y = min(list_y)
                            max_c_x = max(list_x)
                            max_c_y = max(list_y)

                            # Compute height and width of character
                            cl_h = max_c_y - min_c_y
                            cl_w = max_c_x - min_c_x
                            a_word_height_list.append(-cl_h + y1 - y0)

                            # Compute height of curent row
                            if row_vertical_height is not None:
                                row_vertical_height = max(row_vertical_height, cl_h)

                            # Increment the current word width
                            a_word_width += cl_w

                            # Update the current word height
                            a_word_height = max(a_word_height, cl_h)
                            a_word_min_y = min(a_word_min_y, min_c_y)
                            a_word_max_y = max(a_word_max_y, max_c_y)

                            word_ps_list.append([a_char, current_letter_ps, cl_w, cl_h])

                        # Compute ratio word actual/ascii composed
                        ratio_w = min((x1 - x0)/a_word_width, 1.0)
                        ratio_h = min((y1 - y0)/a_word_height, 0.70)
                        # print(f'----a_word_height_list:{a_word_height_list}')
                        # ratio_h = (y1 - y0)/a_word_height

                        # 2nd: Make adjustments in positioning
                        if offset_v_2 == 0:
                            offset_v_2 = max(a_word_height_list)*min(ratio_h, 0.7)

                        for a_word_ps_pack in word_ps_list:

                            # Read char, list of char ps, char width and height
                            a_char, current_letter_ps, cl_w, cl_h = a_word_ps_pack

                            # Retrieve vertical offset adjustments, if needed
                            if a_char in fonts_vertical_adjustment:
                                a_v_offset = fonts_vertical_adjustment[a_char]
                            else:
                                a_v_offset = 0

                            # print(
                            #     f'-a_char: {a_char} -  - confidence: {amapd_item_confidence:.1%}')

                            if amapd_item_confidence >= OCR_REJECTION_THRESHOLD:
                                new_current_letter_ps = strokes_at_point2(
                                    current_letter_ps, x0 + position_letter+0+word_offset, y0+a_v_offset*a_v_offset_ratio+offset_v_2,
                                    ratio_w=ratio_w, ratio_h=ratio_h, get_xt_point=False, new_weight=0.2,
                                    row_vertical_height=row_vertical_height, series=series)

                                position_letter += ratio_w*(cl_w+0)

                                strokes.extend(new_current_letter_ps[0])
                                ascii_word_ps_list.extend(new_current_letter_ps[0])
                                index_letter += 1
                                strokes_nb += len(current_letter_ps)
                                ascii_word_ps_nb += len(current_letter_ps)

                        word_offset += max(position_letter - (x1 - x0) + 0, 0)

                        # Let's retrieve the rect of the recognized text
                        word_rect = [x0, y0, x1, y1]

                        # Now let's extract the handwritten pen strokes under that rect

                        word_image_strokes = []
                        offset_x = None
                        offset_y = None
                        min__x = None
                        min__y = None
                        max__x = None
                        max__y = None

                        for x in image_strokes:
                            # If rects are intersecting
                            if intersection_area(
                                word_rect, [x['min_c_x'], x['min_c_y'], x['max_c_x'], x['max_c_y']],
                                    threshold=0.35):
                                # Append the ps
                                word_image_strokes.append(x)
                                # Compute offsets
                                offset_x_i = word_rect[0] - x['min_c_x']
                                offset_y_i = word_rect[1] - x['min_c_y']

                                try:
                                    if offset_x:
                                        offset_x = min(offset_x, offset_x_i)
                                    else:
                                        offset_x = offset_x_i
                                    if offset_y:
                                        offset_y = min(offset_y, offset_y_i)
                                    else:
                                        offset_y = offset_y_i

                                    if min__x:
                                        min__x = min(min__x, x['min_c_x'])
                                    else:
                                        min__x = x['min_c_x']
                                    if min__y:
                                        min__y = min(min__y, x['min_c_y'])
                                    else:
                                        min__y = x['min_c_y']

                                    if max__x:
                                        max__x = min(max__x, x['max_c_x'])
                                    else:
                                        max__x = x['max_c_x']
                                    if max__y:
                                        max__y = min(max__y, x['max_c_y'])
                                    else:
                                        max__y = x['max_c_y']
                                except Exception as e:
                                    print(f'**- offest: {e}')

                        # Now let's update the list of used pen_strokes
                        processed_pen_strokes_list.extend(word_image_strokes)

                        natural_n = [x['vector_points'] for x in ascii_word_ps_list]

                        try:
                            list_minx = [min([x[1] for x in y]) for y in natural_n]
                            list_miny = [min([x[0] for x in y]) for y in natural_n]
                            list_maxx = [max([x[1] for x in y]) for y in natural_n]
                            list_maxy = [max([x[0] for x in y]) for y in natural_n]
                            min_xx = min(list_minx)
                            max_xx = max(list_maxx)
                            min_yy = min(list_miny)
                            max_yy = max(list_maxy)
                            rect_c = [min_xx, min_yy, max_xx-min_xx, max_yy-min_yy]

                            cell_data, path_list, _ = create_natural_cell_info(rect_c, natural_n)
                        except Exception as e:
                            if DEBUG_MODE:
                                print(f'**-create_natural_cell_info: {e}')

                        handwritten_strokes = [list(set(x['vector_points'])) for x in word_image_strokes]

                        list_minx = [min([x[1] for x in y]) for y in handwritten_strokes]
                        list_miny = [min([x[0] for x in y]) for y in handwritten_strokes]
                        list_maxx = [max([x[1] for x in y]) for y in handwritten_strokes]
                        list_maxy = [max([x[0] for x in y]) for y in handwritten_strokes]
                        # print(f'---list_minx:{list_minx}  list_miny:{list_miny}  list_maxx:{list_maxx}  list_maxy:{list_maxy} ')
                        try:
                            min_xx = min(list_minx)
                            max_xx = max(list_maxx)
                            min_yy = min(list_miny)
                            max_yy = max(list_maxy)
                            rect_hs = [min_xx, min_yy, max_xx-min_xx, max_yy-min_yy]

                            cell_data_hw, path_list_hw, cell_points = create_natural_cell_info(rect_hs, handwritten_strokes)
                        except Exception as e:
                            if DEBUG_MODE:
                                print(f'**-create_natural_cell_info: {e}')

                        # If the OCR confidence is below the threshold, use the image
                        if amapd_item_confidence < OCR_REJECTION_THRESHOLD:

                            if DEBUG_MODE:
                                print(f'** {amapd_item_confidence:.1%} : {amapd_item_word}')

                            word_offset = 0
                            try:
                                height__ = max__y - min__y
                                if height__ > 0:
                                    ratio__y = min((y1 - y0) / height__, 1/0.7)
                                else:
                                    ratio__y = 1

                            except Exception as e:
                                print(f'**- Computing ratio__y: {e}')
                                ratio__y = 1

                            style_char_weak_, _, _, _ = strokes_at_point2(
                                style_char_weak, x0 + position_letter+0+word_offset, y0+a_v_offset*a_v_offset_ratio+offset_v_2,
                                ratio_w=ratio_w, ratio_h=ratio_h, get_xt_point=False, new_weight=0.6,
                                row_vertical_height=row_vertical_height, series=series)

                            new_word_image_strokes, _, _, _ = strokes_at_point2(
                               word_image_strokes, x=None, y=None,
                               ratio_w=1, ratio_h=ratio__y, get_xt_point=False, new_weight=0.2,
                               row_vertical_height=row_vertical_height, series=series)

                            strokes.extend(style_char_weak_)
                            strokes_nb += len(style_char_weak_)
                            strokes.extend(new_word_image_strokes)
                            strokes_nb += len(new_word_image_strokes)
                        else:
                            if DEBUG_MODE:
                                print(f'   {amapd_item_confidence:.1%} : {amapd_item_word}')

                # Add remaining handwritten ps
                # rpsize = [x['vector_size'] for x in image_strokes if x not in processed_pen_strokes_list]
                # print(rpsize)
                remaining_ps = [x for x in image_strokes if x not in processed_pen_strokes_list and x['vector_size'] > 1000]

                strokes.extend(remaining_ps)
                strokes_nb += len(remaining_ps)
                pen_strokes_dict['strokes'] = strokes
                pen_strokes_dict['strokes_nb'] = strokes_nb

            if pen_strokes_dict != {}:
                ps_dict[str(a_pic_index)] = pen_strokes_dict
            else:
                ps_dict[str(a_pic_index)] = ps_from_image

            a_pic_index += 1
            print('', end='\r', flush=True)
        print()

        if DEBUG_MODE:
            save_json(import_json_fn, ps_dict)

        # Build the dictionary of binary representations of totalpath (one per page)
        totalpath_bytes = pen_strokes_dict_to_bytes(ps_dict, series=series)

        # Build dictionary of all ephemeral images (one per page)
        image_layer_dict = generate_ephemeral_images(ps_dict, series=series)

        # ---- Binary .Note file building ---------

        # Generate the file signature
        file_signature = sn_bytes_header()

        # Generate fileid and 1st json
        final_op_page = len(pics_list)
        if len(templates_dict.keys()) > 0:
            style_usage_type = 2
        else:
            style_usage_type = 0

        if encoded_rec_text != []:
            # text_reco = 1
            reco_lang = 'en_US'
        else:
            # text_reco = 0
            reco_lang = 'none'

        file_id, first_json, templates_dict = sn_bytes_1_header_json(
            templates_dict=templates_dict, final_op_page=final_op_page, style_usage_type=style_usage_type,
            reco_lang=reco_lang, apply_equipment=series)

        # Create bytes for background template image
        # background_bitmap_fn = BLANK_TEMPLATE_PICTURE
        # background_image_block = sn_bytes_2_basic_still_image(background_bitmap_fn)

        background_image_block = blank_encoded_image(series)

        # Combine header and first json
        first_bytes_before_image = file_signature + first_json

        current_bytes = first_bytes_before_image

        for a_page in templates_dict:
            a_page_dict = templates_dict[a_page]
            an_item_dict = a_page_dict['pdfstylelist']
            an_item_b64 = an_item_dict['encoded']
            an_item_b64_len = len(an_item_b64)
            an_item_b64_len_bytes = int_to_little_endian_bytes(an_item_b64_len)
            an_item_dict['address'] = len(current_bytes)
            current_bytes += an_item_b64_len_bytes + an_item_b64

        # Store decimal location of background template image
        blank_background_image_address = len(current_bytes)

        # Update binary with background template image MMB 1115
        current_bytes += background_image_block

        # Create blank background layer

        if templates_dict != {}:
            blank_background_layer = sn_bytes_layer_json(blank_background_image_address, name='BGLAYER')
            current_bytes += blank_background_layer

        # Parsing the pages and add totalpath bytes for each page
        totalpath_address_dict = {}
        for a_page_nb in totalpath_bytes:

            # Get the image
            image_block = image_layer_dict[a_page_nb]
            # Store the address of the image
            image_address = len(current_bytes)
            # Update binary with image_block
            current_bytes += image_block
            # Add location of image_address to dictionary
            totalpath_address_dict[a_page_nb] = {
                "image_address": image_address}

        # Now doing the same for sn_bytes_layer_json
        for a_page_nb in totalpath_bytes:
            # Store location of the background layer
            main_layer_address = len(current_bytes)
            atad = totalpath_address_dict[a_page_nb]
            image_address = atad['image_address']
            # Generate the image layer bytes (including size prefix)
            main_layer = sn_bytes_layer_json(image_address, name='MAINLAYER')
            # Update binary with background_layer
            current_bytes += main_layer
            # Add location of Mainlayer and totalpath to dictionary
            atad['main_layer_address'] = main_layer_address

            # # # # Store location of the background layer   MMB 241120 commented
            # # # background_layer_address = len(current_bytes)
            # # # atad['background_layer_address'] = background_layer_address

            # # # Generate the background layer bytes (including size prefix) MMB 241120 commented
            # # # INCORRECT should be image of template, not blank template
            # # background_layer = sn_bytes_layer_json(background_image_address, name='BGLAYER')

            # # # Update binary with background_layer
            # # # MMB 1115 erasing this
            # # current_bytes += background_layer

        for a_page_nb in templates_dict:
            image_dict = templates_dict[a_page_nb]
            pil_image_encoded = image_dict["rle_image"]
            if pil_image_encoded is not None:

                pil_image_encoded_len = len(pil_image_encoded)
                pil_image_encoded_len_bytes = int_to_little_endian_bytes(pil_image_encoded_len)
                a_t_i_block = pil_image_encoded_len_bytes + pil_image_encoded
                a_t_i_block_address = len(current_bytes)
                current_bytes += a_t_i_block
                ati_layer = sn_bytes_layer_json(a_t_i_block_address, name='BGLAYER')
                current_bytes += ati_layer
                image_dict['address'] = a_t_i_block_address

                if a_page_nb in totalpath_bytes:
                    atad = totalpath_address_dict[a_page_nb]
                    # Store location of the background layer
                    background_layer_address = len(current_bytes)
                    atad['background_layer_address'] = background_layer_address
                    background_layer = sn_bytes_layer_json(a_t_i_block_address, name='BGLAYER')
                    current_bytes += background_layer

        # Parsing the pages and add totalpath bytes for each page

        for a_page_nb, page_totalpath_bytes in totalpath_bytes.items():
            atad = totalpath_address_dict[a_page_nb]
            totalpath_size = len(page_totalpath_bytes)
            totalpath_size_bytes = int_to_little_endian_bytes(totalpath_size)
            # Add location of Mainlayer and totalpath to dictionary
            atad['tp_address'] = len(current_bytes)
            # Update binary with totalpath
            current_bytes += totalpath_size_bytes + page_totalpath_bytes

        # Parsing the pages and add page information for each page
        used_styles_list1 = []
        for a_page_nb in totalpath_address_dict:
            a_page_ref_dict = totalpath_address_dict[a_page_nb]
            a_page_total_path = a_page_ref_dict['tp_address']
            a_page_main_layer_address = a_page_ref_dict['main_layer_address']
            if 'background_layer_address' in a_page_ref_dict:
                a_page_background_layer_address = a_page_ref_dict['background_layer_address']
            else:
                a_page_background_layer_address = 0

            if encoded_rec_text != []:
                rec_text = encoded_rec_text[int(a_page_nb)-1]
                rec_text_len = len(rec_text)
                rec_text_len_bytes = int_to_little_endian_bytes(rec_text_len)
                rec_address = len(current_bytes)
                rec_status = 1
                recf_status = 1
                rec_page_type = 1
                current_bytes += rec_text_len_bytes
                current_bytes += rec_text
            else:
                rec_address = 0
                rec_status = 0
                recf_status = 0
                rec_page_type = 0

            a_page_ref_dict['pg_address'] = len(current_bytes)

            # Generate page json
            if a_page_nb in templates_dict:
                apt_dict = templates_dict[a_page_nb]
                md5 = apt_dict['md5']
                if md5 not in used_styles_list1:
                    used_styles_list1.append(md5)
                # style_p1_name = f'user_pdf_xyz_{len(used_styles_list1)}'
                style_p1_name = f'user_{md5}'
                pdfstylelist_name_ = style_p1_name.split('_')
                pdfstylelist_name = '_'.join(pdfstylelist_name_[:2])

                page_bytes = sn_bytes_page(
                    a_page_main_layer_address, a_page_background_layer_address,
                    a_page_total_path, page_style_name=pdfstylelist_name, page_style_md5=md5,
                    rec_status=rec_status, rec_address=rec_address, recf_status=recf_status,
                    rec_page_type=rec_page_type)
            else:
                page_bytes = sn_bytes_page(
                    a_page_main_layer_address, 0, a_page_total_path,
                    rec_status=rec_status, rec_address=rec_address, recf_status=recf_status,
                    rec_page_type=rec_page_type)

            current_bytes += page_bytes

        page_recap_location = len(current_bytes)

        recap_pages = sn_bytes_page_recap(
            page_recap_location, totalpath_address_dict, blank_background_image_address,
            apply_equipment=series, templates_dict=templates_dict)

        current_bytes += recap_pages

        with open(output_fn, 'wb') as file:
            file.write(current_bytes)

    except Exception as e:
        print()
        print(f'*** import_pics: {e}')


def extract_default_styles(epub_path):
    # styles = {}
    styles_list = []
    with zipfile.ZipFile(epub_path, 'r') as zip_ref:
        # Find the OPF file
        opf_path = next((file for file in zip_ref.namelist() if file.endswith('.opf')), None)
        if not opf_path:
            raise ValueError("OPF file not found in EPUB")

        with zip_ref.open(opf_path) as opf:
            tree = ET.parse(opf)
            root = tree.getroot()
            ns = {'opf': 'http://www.idpf.org/2007/opf'}

            # Find all CSS files
            css_files = [item.get('href') for item in root.findall('.//opf:item[@media-type="text/css"]', ns)]

            for css_path in css_files:
                try:
                    # Resolve relative path
                    full_css_path = '/'.join(opf_path.split('/')[:-1] + [css_path])
                    with zip_ref.open(full_css_path) as css_file:
                        css_content = css_file.read().decode('utf-8')
                        styles_list.append(css_content)

                        # body_match = re.search(r'body\s*{([^}]*)}', css_content)
                        # if body_match:
                        #     body_styles = body_match.group(1)
                        #     for style in body_styles.split(';'):
                        #         if ':' in style:
                        #             key, value = style.split(':')
                        #             styles[key.strip()] = value.strip()
                except KeyError:
                    print(f"Warning: CSS file {css_path} not found in EPUB")
                    continue

    return '\n'.join(styles_list)


def snpoint_to_adbpoint(snpoint, series=NOTEBOOK_DEVICE):
    """ Converts the MAX_HORIZONTAL_PIXELS x MAX_VERTICAL_PIXELS coordinates into coordinates that adb
        can use sending a "sendevent" command """
    try:
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)
        x_ = adb_screen_max_x - round(snpoint[0] * adb_screen_max_x / max_horizontal_pixels) + ADB_SCREEN_MIN_X
        y_ = round(snpoint[1] * adb_screen_max_y / max_vertical_pixels) + ADB_SCREEN_MIN_Y
        return [x_, y_]
    except Exception as e:
        print(f'*** snpoint_to_adbpoint: {e}')
        return None


def load_non_clear_pixels(file_path):
    # Prepare a list to store the pixels
    pixels = []
    # Open the file and read line by line
    with open(file_path, "r") as file:
        for line in file:
            # Remove any leading/trailing whitespace (like newline characters)
            line = line.strip()
            # Convert the string representation of the tuple back to an actual tuple
            pixel_tuple = eval(line)
            # Add the tuple to the list
            pixels.append(pixel_tuple)
    return pixels


def adb_screen_point(snpoint, device):
    """ Draws a point on the screen """
    try:
        adb_screen_point_c = snpoint_to_adbpoint(snpoint)
        if adb_screen_point_c:
            a_command_list = [
                '# Pen approach',
                'sendevent /dev/input/event7 1 320 1',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1',
                '# Pen touch',
                'sendevent /dev/input/event7 1 330 1',
                'sendevent /dev/input/event7 0 0 0',
                '# Start point (middle)',
                'sendevent /dev/input/event7 3 24 2000',
                f'sendevent /dev/input/event7 3 0 {adb_screen_point_c[1]}',
                f'sendevent /dev/input/event7 3 1 {adb_screen_point_c[0]}',
                'sendevent /dev/input/event7 0 0 0',
                'sendevent /dev/input/event7 3 24 2000',
                f'sendevent /dev/input/event7 3 0 {adb_screen_point_c[1]+ADB_SCREEN_OFFSET}',
                f'sendevent /dev/input/event7 3 1 {adb_screen_point_c[0]+ADB_SCREEN_OFFSET}',
                'sendevent /dev/input/event7 0 0 0',
                '# Pen lift (remove pressure)',
                'sendevent /dev/input/event7 3 24 0',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1',
                '# Pen untouches',
                'sendevent /dev/input/event7 1 330 0',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1',
                '# Pen away',
                'sendevent /dev/input/event7 1 320 0',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1']
            a_command = '\n'.join(a_command_list)
            # print(f'su -c "{a_command}"')
            device.shell(f'su -c "{a_command}"')
    except Exception as e:
        print(f'*** adb_screen_point: {e}')


def adb_pen_down(snpoint, device):
    """ Draws a point on the screen and doesn't lift the pen"""
    try:
        adb_screen_point_c = snpoint_to_adbpoint(snpoint)
        if adb_screen_point_c:
            a_command_list = [
                '# Pen approach',
                'sendevent /dev/input/event7 1 320 1',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1',
                '# Pen touch',
                'sendevent /dev/input/event7 1 330 1',
                'sendevent /dev/input/event7 0 0 0',
                '# Start point (middle)',
                'sendevent /dev/input/event7 3 24 2000',
                f'sendevent /dev/input/event7 3 0 {adb_screen_point_c[1]}',
                f'sendevent /dev/input/event7 3 1 {adb_screen_point_c[0]}',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1']
            a_command = '\n'.join(a_command_list)
            # print(f'su -c "{a_command}"')
            device.shell(f'su -c "{a_command}"')
    except Exception as e:
        print(f'*** adb_screen_point: {e}')


def adb_pen_move_down(snpoint, device):
    """ Draws a point on the screen and doesn't lift the pen"""
    try:
        adb_screen_point_c = snpoint_to_adbpoint(snpoint)
        if adb_screen_point_c:
            a_command_list = [
                'sendevent /dev/input/event7 3 24 2000',
                f'sendevent /dev/input/event7 3 0 {adb_screen_point_c[1]}',
                f'sendevent /dev/input/event7 3 1 {adb_screen_point_c[0]}',
                'sendevent /dev/input/event7 0 0 0',
                'sleep 0.1']
            a_command = '\n'.join(a_command_list)
            # print(f'su -c "{a_command}"')
            device.shell(f'su -c "{a_command}"')
    except Exception as e:
        print(f'*** adb_screen_point: {e}')


def lift_pen(device):
    """ Lift pen function """
    try:
        lift_command = [
            'sendevent /dev/input/event7 3 24 0',
            'sendevent /dev/input/event7 0 0 0',
            'sleep 0.1',
            'sendevent /dev/input/event7 1 330 0',
            'sendevent /dev/input/event7 0 0 0',
            'sleep 0.1',
            'sendevent /dev/input/event7 1 320 0',
            'sendevent /dev/input/event7 0 0 0',
            'sleep 0.1']
        device.shell('su -c "' + "\n".join(lift_command) + '"')
    except Exception as e:
        print(f'*** lift_pen: {e}')


def simulate_path(device, segment, scale_ratio=1):
    """ Simulate drawing of a path on the screen """
    try:
        start_point = segment[0]
        adb_start_point = (round(start_point.x/scale_ratio), round(start_point.y/scale_ratio))

        # Start the pen stroke
        adb_pen_down(adb_start_point, device)
        segment_points_list = [(round(apoint.x/scale_ratio), round(apoint.y/scale_ratio)) for apoint in segment[1:]]
        for apoint in segment_points_list:
            adb_pen_move_down(apoint, device)
        lift_pen(device)
    except Exception as e:
        print(f'*** simulate_path: {e}')


def complex_to_point(z):
    return (z.real, z.imag)


def interpolate_bezier(bezier, num_points=50):
    t_values = np.linspace(0, 1, num_points)
    return [complex_to_point(bezier.point(t)) for t in t_values]


def convert_bezier_data_to_points(bezier_data):
    """ Helper function to convert CubicBezier data to a list of complex numbers"""
    points = []
    for bezier in bezier_data:
        points.extend([bezier.start, bezier.control1, bezier.control2, bezier.end])
    return points


def adb_command(device, command):
    try:
        result = device.shell(command)
        return result
    except AttributeError:
        print(f"Device object does not have a serial attribute. Device: {device}")
    except Exception as e:
        print(f"An error occurred while executing adb_command: {e}")


def simulate_touch(device, x, y, offset_x=100, offset_y=100):
    adb_command(device, f'input touchscreen tap {x+offset_x} {y+offset_y}')


def tap_pixels_of_color(device, image_path, target_color=None, tolerance=0, step=5, offset_x=100, offset_y=100):
    print(f"Opening image: {image_path}")
    print(f"Type of image_path: {type(image_path)}")

    # Open the image
    img = Image.open(image_path)
    img = img.convert('RGB')  # Ensure image is in RGB mode
    width, height = img.size

    # Convert target color to RGB tuple if it's not
    if target_color is not None and isinstance(target_color, str):
        target_color = ImageColor.getrgb(target_color)

    # Iterate through the pixels with a step to reduce the number of commands
    for x in range(0, width, step):
        for y in range(0, height, step):
            pixel_color = img.getpixel((x, y))
            if target_color is None or all(abs(pc - tc) <= tolerance for pc, tc in zip(pixel_color, target_color)):
                simulate_touch(device, x, y, offset_x, offset_y)
                time.sleep(0.001)  # Smaller delay for faster execution


def set_bundle_icon(icon_name, app_path):
    """ Set the icon for macOS """
    if not sys.platform == 'darwin':
        print('*** set_bundle_icon: Only for macOS')
        return
    from AppKit import NSWorkspace, NSImage
    icon_path = os.path.join(PYSN_DIRECTORY, icon_name)
    if not icon_path or not os.path.exists(icon_path) or not icon_path.endswith('.icns'):
        print("Invalid icon path or not an .icns file. Skipping icon setting.")
        return

    try:
        # Create NSImage from the icon file
        icon = NSImage.alloc().initWithContentsOfFile_(icon_path)

        # Set the icon
        workspace = NSWorkspace.sharedWorkspace()
        workspace.setIcon_forFile_options_(icon, app_path, 0)

        # Touch the app to refresh its icon
        subprocess.run(["touch", app_path], check=True)

        print(f"Icon set successfully for {app_path}")
    except Exception as e:
        print(f"Failed to set icon: {e}")


def extract_relative_path(full_path):
    """ Extracts relative path to one of the Supernote default folders.
        This function is needed when user sets different backup directory """
    last_occurrence_index = -1
    selected_folder = None

    for folder in SUPERNOTE_FOLDERS:
        index = full_path.rfind(folder)
        if index != -1 and index > last_occurrence_index:
            last_occurrence_index = index
            selected_folder = folder

    if selected_folder:
        return full_path[last_occurrence_index:]
    return None  # Return None if no folder from SUPERNOTE_FOLDERS is found in the path


def archive_with_structure(original_file_path, archive_folder=ARCHIVE, full_path=False, remove=False):
    """
    Archives a file by recreating its directory structure under a specified archive folder,
    then creates a zip file containing the original file with its full path preserved.

    Args:
        original_file_path (str): The full path to the original file.
        archive_folder (str): The path to the archive folder.

    Returns:
        str: The full path to the created zip file.
    """
    try:
        associated_pdf = False
        if os.path.exists(original_file_path):
            current_datetime = get_last_modified_date(original_file_path, datetime_format='%Y%m%d_%H%M%S')

            # Get the relative path and basename
            relative_path = extract_relative_path(original_file_path)
            if not relative_path:
                raise ValueError("Archived path need to be a valid Supernote path")
            file_basename = os.path.basename(original_file_path)

            # Create the target directory structure under the archive folder
            target_folder = os.path.join(archive_folder, os.path.dirname(relative_path), file_basename)
            os.makedirs(target_folder, exist_ok=True)

            # Create the zip file path
            zip_file_path = os.path.join(target_folder, f"{current_datetime}.zip")

            # For .mark file, it's less confusion to backup the associated pdf, with the same timestamp
            _, f_ext = os.path.splitext(file_basename)
            if f_ext.lower() == '.mark':
                original_file_path_pdf = original_file_path[:-5]
                if os.path.exists(original_file_path_pdf):
                    associated_pdf = True
                    relative_path_pdf = extract_relative_path(original_file_path_pdf)
                    file_basename_pdf = os.path.basename(original_file_path_pdf)

            # Determine the arcname based on the full_path flag
            if full_path:
                arcname = os.path.join("pysn-digest", relative_path)
                if associated_pdf:
                    arcname_pdf = os.path.join("pysn-digest", relative_path_pdf)
            else:
                arcname = file_basename
                if associated_pdf:
                    arcname_pdf = file_basename_pdf

            # Create the zip file and add the original file with the determined arcname
            with zipfile.ZipFile(zip_file_path, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as zipf:
                zipf.write(original_file_path, arcname=arcname)
                if associated_pdf:
                    zipf.write(original_file_path_pdf, arcname=arcname_pdf)
            if remove:
                os.remove(original_file_path)
            return zip_file_path
    except Exception as e:
        print()
        print(f'*** archive_with_structure: {e}')


def remove_file_with_retry(filename, retries=5, delay=1):
    """
    Attempt to remove a file with retries and delay.

    :param filename: The file to remove.
    :param retries: Number of times to retry before giving up.
    :param delay: Delay (in seconds) between retries.
    :return: None
    """
    for attempt in range(retries):
        try:
            os.remove(filename)
            print(f"File {filename} removed successfully.")
            return
        except PermissionError as e:
            print(f"Attempt {attempt + 1}: {e}")
            time.sleep(delay)
    print(f"Failed to remove file {filename} after {retries} attempts.")


def copy_file_with_directories(source_filename, dest_filename):
    """
    Copy a file from source_filename to dest_filename, creating any necessary directories.

    Parameters:
    source_filename (str): The path to the source file.
    dest_filename (str): The path to the destination file, including any non-existent directories.
    """

    try:
        # Extract the directory path from the destination filename
        dest_directory = os.path.dirname(dest_filename)

        # Check if the destination directory exists
        if not os.path.exists(dest_directory):
            # If the destination directory does not exist, create it (including any intermediate directories)
            os.makedirs(dest_directory)
            # print(f'---created dir: {dest_directory}')

        # Copy the source file to the destination
        shutil.copy2(source_filename, dest_filename)
    except Exception as e:
        print()
        print(f'*** copy_file_with_directories: {e} - src:{source_filename} - dest: {dest_filename}')


def decimal_to_rgba_hex(decimal_color, alpha=255):
    """
    Convert a decimal color to RGBA hex.

    :param decimal_color: The decimal color value.
    :param alpha: The alpha value (default is 255 for full opacity).
    :return: The RGBA hex string.
    """
    # Convert decimal to hex
    hex_color = hex(decimal_color)[2:].zfill(6)  # Remove '0x' and pad with zeros if necessary

    # Convert alpha to hex
    hex_alpha = hex(alpha)[2:].zfill(2)  # Remove '0x' and pad with zeros if necessary

    # Combine the RGB hex color with the alpha hex
    rgba_hex = f'#{hex_color.upper()}{hex_alpha.upper()}'

    return rgba_hex


def replace_color(image: Image, original_hex: str, modified_hex: str) -> Image:
    """
    Replace a specific color in an image with another color.

    Args:
        image (Image): The input image in which the color replacement will occur.
        original_hex (str): The hex code of the original color to be replaced.
        modified_hex (str): The hex code of the new color to replace the original color.

    Returns:
        Image: The image with the specified color replaced.
    """
    try:
        # Convert hex colors to RGB tuples (ignoring alpha for original color)
        original_color = tuple(int(original_hex[i:i+2], 16) for i in (1, 3, 5))
        modified_color = tuple(int(modified_hex[i:i+2], 16) for i in (1, 3, 5))

        # Convert image to numpy array
        image_array = np.array(image)

        # Extract the RGB channels (ignoring alpha)
        rgb_array = image_array[:, :, :3]

        # Create a mask for the original color pixels (ignoring alpha)
        mask = np.all(rgb_array == original_color, axis=-1)

        # Replace the original color pixels with the modified color but keep alpha
        image_array[mask, :3] = modified_color

        # Convert numpy array back to image
        new_image = Image.fromarray(image_array)
    except Exception as e:
        print(f'*** replace_color: {e}')
        return image

    return new_image


def parse_color(color_string):
    """
    Source: supernotelib/cmds/supernote_tool.py
    Parses a comma-separated string of color codes and converts them to hexadecimal integers.

    Args:
        color_string (str): A comma-separated string containing four color codes.

    Returns:
        tuple: A tuple containing four integers representing the hexadecimal values of the colors.

    Raises:
        ValueError: If the input string does not contain exactly four color codes.
    """
    # Split the input string by commas to get individual color codes
    colorcodes = color_string.split(',')

    # Check if the number of color codes is exactly four
    if len(colorcodes) != 4:
        raise ValueError(f'Few color codes, 4 colors are required: {color_string}')

    # Convert each color code to a hexadecimal integer
    black = int(Color(colorcodes[0]).hex_l[1:7], 16)
    darkgray = int(Color(colorcodes[1]).hex_l[1:7], 16)
    gray = int(Color(colorcodes[2]).hex_l[1:7], 16)
    white = int(Color(colorcodes[3]).hex_l[1:7], 16)

    # Return the color codes as a tuple of integers
    return (black, darkgray, gray, white)


def generate_relative_css_link(css_path, html_path):
    """
    Generate a relative CSS link for an HTML file.

    Args:
        css_path (str): The path to the CSS file.
        html_path (str): The path to the HTML file.

    Returns:
        str: A string containing the HTML link tag with the relative path to the CSS file.
    """
    # Split the HTML path using a predefined folder constant
    rel_path_l = html_path.split(SN_IO_FOLDER)
    rel_path = rel_path_l[-1]

    # Convert to absolute paths
    css_dir_r = os.path.dirname(css_path)
    css_path = os.path.abspath(css_path)
    css_dir = os.path.dirname(css_path)
    html_path = os.path.normpath(css_dir_r + rel_path)
    html_path = os.path.abspath(html_path)

    # Get the directory paths
    html_dir = os.path.dirname(html_path)

    # Calculate the relative path
    relative_path = os.path.relpath(css_dir, html_dir)

    # Append the CSS filename to the relative path
    relative_css_path = os.path.join(relative_path, os.path.basename(css_path))

    # Normalize the path to use forward slashes
    relative_css_path = relative_css_path.replace(os.sep, '/')

    # Return the link tag
    return f'<link rel="stylesheet" type="text/css" href="{relative_css_path}">'


def apply_inline_styles(html_content, css_file_path):
    """
    Apply CSS rules from a CSS file as inline styles to the provided HTML content.

    Args:
        html_content (str): The HTML content to which the inline styles will be applied.
        css_file_path (str): The path to the CSS file containing the styles.

    Returns:
        str: The modified HTML content with inline styles applied.
    """
    # Read the CSS file
    with open(css_file_path, 'r', encoding='utf-8') as css_file:
        css_content = css_file.read()

    # Parse the HTML content with BeautifulSoup
    soup = BeautifulSoup(html_content, 'html.parser')

    # Parse the CSS content with cssutils
    css_parser = cssutils.CSSParser(validate=False)
    stylesheet = css_parser.parseString(css_content, href='https://fonts.googleapis.com')

    # Apply CSS rules as inline styles
    for rule in stylesheet:
        if rule.type == rule.STYLE_RULE:
            for selector in rule.selectorList:
                elements = soup.select(selector.selectorText)
                for element in elements:
                    for property in rule.style:
                        # Only add valid CSS properties

                        if property.name not in ['font-display']:
                            element_style = element.get('style', '')
                            new_style = f'{property.name}: {property.value};'
                            if new_style not in element_style:
                                element_style += f' {new_style}'
                            element['style'] = element_style.strip()

    # Return the modified HTML content
    return str(soup)


def copy_sn_file(a_file_fname, a_dest):
    """ This function was added to make immediately available the downloaded
        files to the backup folder """
    try:
        bname, an_ext = os.path.splitext(a_file_fname)

        if os.path.exists(a_file_fname):
            split_path = os.path.split(a_dest)

            new_dir_note = os.path.normpath(os.path.join(LOCAL_BKP_NOTES, os.sep.join(split_path[:-1])))
            if not os.path.exists(new_dir_note):
                os.makedirs(new_dir_note)
                # print(f' **** new_dir_note: {new_dir_note}')

            dest_fn = os.path.normpath(os.path.join(new_dir_note, os.path.basename(a_file_fname)))
            # If the file already exists, we want to create a version of the previou file
            if os.path.exists(dest_fn):
                print(f'  > Archived: {archive_with_structure(dest_fn)}')
            shutil.copy(a_file_fname, dest_fn)

    except Exception as e:
        print()
        print(f'*** Error: {e}')
        print('     copy_sn_file of SN file structure aborted')


def build_dict_backlinks(a_list_pointers):
    """
    Build a dictionary of backlinks from a list of pointers.

    Args:
        a_list_pointers (list): A list of tuples where each tuple contains
                                (source_page, dest_file, dest_page).

    Returns:
        dict: A dictionary where the keys are source pages and the values are lists of tuples
              containing (dest_file, dest_page).
    """
    temp_dict = {}
    for an_item in a_list_pointers:
        source_page = an_item[0]
        dest_file = an_item[1]
        dest_page = an_item[2]

        # Check if the source_page already exists in the dictionary
        if source_page in temp_dict:
            a_list_page = temp_dict[source_page]
            a_list_page.append((dest_file, dest_page))
        else:
            # Initialize the list for the source_page if it doesn't exist
            temp_dict[source_page] = [(dest_file, dest_page)]

    return temp_dict


def copy_files_without_subfolders(src_folder, dest_folder):
    """
    Copy files from the source folder to the destination folder without including subfolders.

    Args:
        src_folder (str): The path to the source folder.
        dest_folder (str): The path to the destination folder.
    """

    try:
        # Check if the destination folder exists, if not, create it
        if not os.path.exists(dest_folder):
            os.makedirs(dest_folder)

        # Iterate over all items in the source folder
        for item in os.listdir(src_folder):
            src_path = os.path.join(src_folder, item)
            dest_path = os.path.join(dest_folder, item)

            # If the item is a file, copy it to the destination folder
            if os.path.isfile(src_path):
                shutil.copy2(src_path, dest_path)
    except Exception as e:
        print()
        print(f'*** copy_files_without_subfolders: {e} - src: {src_folder} - dest: {dest_folder}')


def sort_tuples_natural_order(tuples):
    """
    Sort a list of tuples in natural reading order based on the rectangle positions.
    Each tuple is in the format (rect, text).

    :param tuples: List of tuples where each tuple contains a rectangle and a text.
                   The rectangle is in [x0, y0, x1, y1] format.
    :return: List of tuples sorted in natural reading order.
    """
    def custom_compare(t1, t2):
        r1 = t1[0]
        r2 = t2[0]

        # Compare top edges first
        if r1[1] < r2[1] - 10:
            return -1
        elif r1[1] > r2[1] + 10:
            return 1
        else:
            # If top edges are close, compare left edges
            if r1[0] < r2[0]:
                return -1
            elif r1[0] > r2[0]:
                return 1
            else:
                return 0

    # Sort the tuples using the custom comparator
    sorted_tuples = sorted(tuples, key=functools.cmp_to_key(custom_compare))
    return sorted_tuples


def natural_reading_order(tuples_list, threshold=60):
    """ rearrange a list of tuples based in natural reading order.
    Each tuple on the list must have a rect as the first element."""

    # Initialize a dict
    rows_dict = {}
    # Populate the dict
    for a_tuple in tuples_list:
        a_rect = a_tuple[0]                                             # Take its rect
        a_middle = [(a_rect[0]+a_rect[2])/2, (a_rect[1]+a_rect[3])/2]   # Compute the middle
        a_key = round(a_middle[1])                                      # Round its vertical component
        a_tuple[0].append(a_middle)                                     # Add the middle as a 5th element of the rect
        if rows_dict.keys == []:                                        # If this is the first dict entry, add it
            rows_dict[a_key] = [a_tuple]
        else:
            inserted_tuple = False                                      # Otherwise find an existing "row" where you can fit it
            existing_keys = list(rows_dict.keys())
            for existing_key in existing_keys:
                if abs(existing_key - a_key) < threshold:
                    rows_dict[existing_key].append(a_tuple)
                    inserted_tuple = True
                    break
            if not inserted_tuple:
                rows_dict[a_key] = [a_tuple]                            # No existing row, creating one

    # Sort each row using the horizontal coordinates
    for a_row, a_row_list in rows_dict.items():
        sorted_row = sorted(a_row_list, key=lambda x: x[0][4][0])
        rows_dict[a_row] = sorted_row

    # Sort the rows using the vertical position (the keys)
    rows_list = rows_dict.keys()
    sorted_rows_list = sorted(rows_list, key=lambda x: int(x))

    result_list = []
    for a_row_key in sorted_rows_list:
        an_augmented_row = rows_dict[a_row_key]
        an_original_row = [(x[0][:-1], x[1]) for x in an_augmented_row]

        result_list.extend(an_original_row)
    return result_list


def load_dictionaries(lookup_folder, list_keywords, merged_dict=None):
    """
    Merge multiple JSON dictionaries based on given keywords.

    Args:
        lookup_folder (str): Path to the folder containing JSON files.
        list_keywords (list): List of keywords to construct dictionary names.

    Returns:
        dict: Merged dictionary containing unique key-value pairs from all JSON files.

    Raises:
        FileNotFoundError: If any JSON file specified by keyword does not exist.
        JSONDecodeError: If there is an error decoding JSON from any file.

    """
    try:
        if merged_dict is None:
            merged_dict = {}
        for keyword in list_keywords:
            json_filename = f"dictionary-{keyword.lower()}.json"
            json_path = os.path.join(lookup_folder, json_filename)
            if os.path.exists(json_path):
                with open(json_path, 'r', encoding='utf-8') as json_file:
                    current_dict = json.load(json_file)
                    for key, value in current_dict.items():
                        if key not in merged_dict and key != 'pysn-usage':
                            merged_dict[key] = value
        sorted_keys = sorted(merged_dict.keys(), key=lambda k: (-len(k), k.lower()))
        sorted_merged_dict = {key: merged_dict[key] for key in sorted_keys}
    except Exception as e:
        print()
        print(f'*** load_dictionaries: {e}')
        return None
    if merged_dict == {}:
        sorted_merged_dict = None
    return sorted_merged_dict


def generate_md(data):
    """
    Generate markdown text from a dictionary of data.

    Args:
        data (dict): A dictionary where keys are strings and values are strings containing lines of text.

    Returns:
        str: A string containing the formatted markdown text.
    """
    md_rows = []
    for k, v in data.items():
        v_list = v.split('\n')  # Split the value string into a list of lines
        for item in v_list:
            if '%' in item:
                item = item.replace('%', '')  # Remove '%' from the item
                item_list_ = item.split('-')  # Split the item by '-'
                item_list__ = [item_list_[0]]  # Initialize list with the first part
                if len(item_list_) > 1:
                    # Format the remaining parts with '- ' prefix if they are not empty
                    item_list__1 = [f'- {x}' for x in item_list_[1:] if x.strip() != '']
                else:
                    item_list__1 = []
                item_list__.extend(item_list__1)
                # Replace '- $' with '  -' in the list
                item_list = [x.replace('- $', '  -') for x in item_list__]
            else:
                item_list = [item]
            md_rows.extend(item_list)  # Add the processed items to the markdown rows
    md_text = '\n'.join(md_rows)  # Join all rows into a single markdown text
    return md_text


def markdown_to_html(markdown):
    """
    Converts a markdown string to HTML.

    Args:
        markdown (str): The markdown text to convert.

    Returns:
        str: The converted HTML text.
    """
    html = []
    list_stack = []

    def close_lists(depth):
        """Closes open list tags until the specified depth."""
        while list_stack and list_stack[-1] >= depth:
            html.append('  ' * (len(list_stack) - 1) + '</ul>')
            list_stack.pop()

    for line in markdown.split('\n'):
        try:
            stripped = line.strip()
            indent = len(line) - len(line.lstrip())

            if stripped.startswith('#'):
                # Handle headers
                close_lists(0)
                level = len(stripped.split()[0])
                text = stripped.split(' ', 1)[1]
                html.append(f'<h{level}>{text}</h{level}>')
            elif stripped.startswith(('- ', '* ', '+ ')):
                # Handle list items
                depth = indent // 2
                close_lists(depth)

                if not list_stack or depth > list_stack[-1]:
                    html.append('  ' * len(list_stack) + '<ul>')
                    list_stack.append(depth)

                content = stripped[2:]
                html.append('  ' * len(list_stack) + f'<li>{content}')
            elif stripped:
                # Handle paragraphs
                close_lists(0)
                html.append(f'<p>{line}</p>')
        except Exception as e:
            pass
            if DEBUG_MODE:
                print(f'    * markdown_to_html: {e}')

    close_lists(0)
    return '\n'.join(html)


def generate_html(markdown_text, css_path, html_path, inline=False):
    """
    Converts markdown text to HTML and applies CSS styles.

    Args:
        markdown_text (str): The markdown text to be converted.
        css_path (str): The path to the CSS file.
        html_path (str): The path where the HTML file will be saved.
        inline (bool, optional): If True, CSS styles will be applied inline. Defaults to False.

    Returns:
        str: The generated HTML content.
    """
    # Convert markdown text to HTML
    html_ = markdown_to_html(markdown_text)

    if inline:
        # Apply CSS styles inline
        html_output = apply_inline_styles(html_, css_path)
    else:
        # Generate a relative link to the CSS file
        link_tag = generate_relative_css_link(css_path, html_path)
        # Construct the final HTML output
        html_output = f'<html><head>{link_tag}</head><body>{html_}</body></html>'

    return html_output


def is_low_priority():
    """
    Check if the current process is running with low priority.

    Returns:
        bool: True if the process is running with low priority, False otherwise.
    """
    p = psutil.Process(os.getpid())
    system = platform.system()

    if system == 'Windows':
        # On Windows, IDLE_PRIORITY_CLASS indicates low priority
        return p.nice() == psutil.IDLE_PRIORITY_CLASS
    elif system in ['Linux', 'Darwin']:  # Darwin is the system name for macOS
        # On Linux and macOS, a nice value of 19 indicates low priority
        return p.nice() == 19
    return False


def set_low_priority():
    """
    Set the current process to low priority based on the operating system.

    On Windows, it sets the process priority to IDLE_PRIORITY_CLASS.
    On Unix-like systems (Linux, macOS), it sets the process nice value to 19.
    """

    if ENABLE_LOW_PRIORITY:
        p = psutil.Process(os.getpid())
        system = platform.system()

        if system == 'Windows':
            p.nice(psutil.IDLE_PRIORITY_CLASS)  # Set low priority on Windows
        elif system in ['Linux', 'Darwin']:  # Darwin is the system name for macOS
            p.nice(19)  # Set lowest priority on Unix-like systems

        print()
        print('>>> PySN priority switched to idle')
        print()


def set_normal_priority():
    try:
        p = psutil.Process(os.getpid())
        system = platform.system()

        if system == 'Windows':
            p.nice(psutil.NORMAL_PRIORITY_CLASS)  # Windows normal priority
        elif system in ['Linux', 'Darwin']:  # Darwin is the system name for macOS
            os.setpriority(os.PRIO_PROCESS, 0, 0)  # Normal priority for Unix-like systems
        print()
        print('>>> PySN priority switched to normal')
        print()
    except Exception as e:
        print()
        print(f'*** set_normal_priority: {e}')
        print('If on a macOS or Linux: try next time to start as sudo')


def convert_android_path(android_path_):
    """
    Convert an Android file path to a format compatible with the current operating system.

    Args:
        android_path_ (str): The original Android file path.

    Returns:
        str: The converted file path compatible with the current operating system.
    """
    # Determine the prefix based on the presence of '/emulated/0/' in the path
    if '/emulated/0/' in android_path_:
        prefix_ = '/'
    else:
        prefix_ = 'SDCard/'

    # Remove the storage prefix from the Android path
    android_path = prefix_ + re.sub(r'(/storage/[^/]+[0/]*)', '', android_path_)

    # Split the Android path into parts
    relevant_parts = android_path.split('/')

    # Join the relevant parts using the OS-specific separator
    converted_path = os.path.join(*relevant_parts)

    return converted_path


def remove_storage_paths(text):
    """
    Cleans the input text by removing storage paths and adjusting the path based on the transfer method.

    Args:
        text (str): The input text containing storage paths.

    Returns:
        str: The cleaned text with storage paths removed or adjusted.
    """
    # Remove storage paths using regex
    cleaned_text = re.sub(r'(/storage/[^/]+[0/]*)|(http://[^/]*/(REMOVABLE0/)*)', '', text)

    if 'http' in text:  # WIFI Transfer
        return cleaned_text
    elif '/storage/' in text:  # USB Transfer
        if 'emulated/0' not in text:  # If this is an external drive
            return f'SDcard/{cleaned_text}'
        else:
            return cleaned_text
    else:
        # Adjust path relative to SN_VAULT and SN_IO_FOLDER
        return os.path.relpath(cleaned_text, os.path.join(SN_VAULT, SN_IO_FOLDER))


def find_pdf_or_else(root_directory, extension=['.pdf']):
    """
    Walks through a directory tree and returns all paths to PDF files or files of a given extension.

    Args:
        root_directory (str): The root directory to start the search.
        extension (list, optional): List of file extensions to search for. Defaults to ['.pdf'].

    Returns:
        list: A list of full paths to the files with the specified extensions.
    """
    pdf_paths = []
    # os.walk traverses the directory tree
    for dirpath, _, filenames in os.walk(root_directory):
        # Loop through each file in the current directory
        for filename in filenames:
            # Check if the file has the desired extension (case-insensitive)
            _, file_extension = os.path.splitext(filename)
            if file_extension.lower() in extension:
                # Append the full path of the file to the list
                pdf_paths.append(os.path.join(dirpath, filename))
    return pdf_paths


def find_folders_with_extension_cloud(sn_folders, extensions=['.pdf', '.mark', '.note', '.png']):
    """
    Walks through a tree file structure and returns all folder paths containing files of a given extension.

    Args:
        sn_folders (list): List of root directories to traverse.
        extensions (list, optional): List of file extensions to look for. Defaults to ['.pdf', '.mark', '.note', '.png'].

    Returns:
        list: List of folder paths containing files with the specified extensions.
    """
    folders_with_extension = set()
    for root_directory in sn_folders:
        # os.walk traverses the directory tree
        for dirpath, _, filenames in os.walk(root_directory):
            # Check if the directory has already been processed
            if dirpath in folders_with_extension:
                continue
            # Loop through each file in the current directory
            for filename in filenames:
                # Check if the file has the given extension (case-insensitive)
                if any(filename.lower().endswith(ext) for ext in extensions):
                    # Add the directory path to the set
                    folders_with_extension.add(dirpath)
                    break  # Exit the loop early since we only need the folder path
    return list(folders_with_extension)


def get_folders_containing_files_usb(device, sn_folders, internal_prefix, external_prefix, extensions=['.pdf', '.mark', '.note', '.png', '.doc', '.docx', '.txt']):
    """For USB connection through ADB, retrieves the tree structure of folders
    containing files of the specified type.

    Args:
        device: The device object to communicate with via ADB.
        sn_folders (list): List of Supernote folders to search within.
        internal_prefix (str): Prefix for internal storage paths.
        external_prefix (str): Prefix for external storage paths.
        extensions (list, optional): List of file extensions to look for. Defaults to ['.pdf', '.mark', '.note'].

    Returns:
        list: List of directories containing files with the specified extensions.
    """
    try:
        folders_with_extension = set()
        for root_directory in sn_folders:
            if 'sdcard' in root_directory.lower():
                prefix_ = external_prefix
                root_directory = re.sub(re.escape('sdcard/'), '', root_directory, flags=re.IGNORECASE)
            else:
                prefix_ = internal_prefix

            # Construct the command to find files
            command_sending = f'find "{prefix_}{root_directory}" -type f'
            filelist_result = device.shell(command_sending)

            # Split the result into lines (each line is a file path)
            file_paths = filelist_result.splitlines()

            # Loop through each file path
            for file_path in file_paths:
                # Extract the directory path from the file path
                dir_path = os.path.dirname(file_path) + '/'

                # Check if the directory has already been processed
                if dir_path in folders_with_extension:
                    continue

                # Check if the file has the given extension (case-insensitive)
                if any(file_path.lower().endswith(ext) for ext in extensions):
                    # Add the directory path to the set
                    folders_with_extension.add(dir_path)

        return list(folders_with_extension)
    except Exception as e:
        print()
        print(f'*** Error in find_folders_for_file_type: {e}')
        print(f'  -- Root: {root_directory} -- Extensions: {extensions}')
        return []


def count_files(device, subfolders, internal_prefix, external_prefix, file_types=['.note', '.mark']):
    """
    Counts the number of files in a list of folders.

    Args:
        device: The device object to execute shell commands.
        subfolders (list): List of subfolders to search for files.
        internal_prefix (str): Prefix path for internal storage.
        external_prefix (str): Prefix path for external storage (e.g., SD card).
        file_types (list, optional): List of file extensions to count. Defaults to ['.note', '.mark'].

    Returns:
        dict: A dictionary with file types as keys and their respective counts as values.
    """
    counts = {file_type: 0 for file_type in file_types}

    for subfolder in subfolders:
        try:
            # Determine the prefix based on whether the subfolder is on the SD card
            if 'sdcard' in subfolder.lower():
                prefix_ = external_prefix
                subfolder = re.sub(re.escape('sdcard/'), '', subfolder, flags=re.IGNORECASE)
            else:
                prefix_ = internal_prefix

            for file_type in file_types:
                # Construct the shell command to count files of a specific type
                command_sending = f'find "{prefix_}{subfolder}" -type f -name "*{file_type}" | wc -l'
                result = device.shell(command_sending)
                counts[file_type] += int(result.replace('\n', '').strip())

        except Exception as e:
            print()
            print(f'*** count_files: {e}')
            print(f'*** internal_prefix: {internal_prefix}')
            print(f'*** subfolders: {subfolders}')
            print(f'*** external_prefix: {external_prefix}')

    return counts


async def fetch(session, url):
    """
    Fetches the content from the given URL using the provided session.

    Args:
        session (aiohttp.ClientSession): The aiohttp session to use for the request.
        url (str): The URL to fetch content from.

    Returns:
        str: The content of the response as a string if successful.
        None: If there was a connection error.
    """
    try:
        async with session.get(url) as response:
            return await response.text()
    except aiohttp.client_exceptions.ClientConnectorError as e:
        # Handle connection errors
        # print(f"*** Failed to connect: {e} ***")
        print(e)
        # print('>>> In the Supernote settings, you may want to check that the "Browse & Access-On" is turned on.')
        # print()
        return None


async def explore_directory(base_url, extensions, folders_with_extension, session):
    """
    Helper function to explore directories recursively, when transfer_mode is LAN(WIFI).

    Args:
        base_url (str): The base URL of the directory to explore.
        extensions (list): List of file extensions to look for.
        folders_with_extension (set): Set to store directories containing files with the specified extensions.
        session (aiohttp.ClientSession): The session object for making HTTP requests.
    """
    print(f'-exploring:{base_url}')

    # Fetch the HTML content of the base URL
    html = await fetch(session, base_url)
    if not html:
        return

    # Parse the HTML content using BeautifulSoup
    soup = BeautifulSoup(html, 'html.parser')
    script_text = ''
    found = False

    # Search for the script containing JSON data
    for script in soup.find_all("script"):
        if 'const json =' in script.string or 'const json=' in script.string:  # Check if the script contains the JSON variable
            script_text = script.string
            found = True
            break

    if not found:
        print("No script with JSON data found.")
        return

    # Extract JSON data from the script
    match = re.search(r"const json = '({[^']*})'", script_text, re.DOTALL)
    if match:
        json_text = match.group(1)
        data = json.loads(json_text)
    else:
        print("Failed to extract JSON data.")
        return

    # Iterate through the file list in the JSON data
    for item in data['fileList']:
        if item['isDirectory']:
            # Recursively explore subdirectories
            await explore_directory(urljoin(base_url, item['uri']), extensions, folders_with_extension, session)
        elif any(item['name'].lower().endswith(ext) for ext in extensions):
            # Add the directory to the set if a file with the specified extension is found
            folders_with_extension.add(base_url)
            break  # Stop searching this directory if one file matches


async def find_folders_with_extensions_lan(base_url, extensions, session):
    """Recursively find folders containing files with specified extensions via tablet webserver."""
    folders_with_extension = set()
    await explore_directory(base_url, extensions, folders_with_extension, session)
    return list(folders_with_extension)


def sort_by_reading_order(rect_list, y_tolerance=15):
    def sort_key(rect):
        # Calculate the y0 coordinate with tolerance
        y0_tolerant = round(rect[0][1] / y_tolerance)
        return (y0_tolerant, rect[0][0])

    # Sort the list by adjusted y-coordinate, then by x-coordinate
    sorted_list = sorted(rect_list, key=sort_key)
    return sorted_list


def bytes_to_hex_string(byte_data):
    # Convert each byte to a two-character hexadecimal string
    hex_string = ' '.join(f'{byte:02x}' for byte in byte_data)
    return hex_string


def load_image_with_frombytes(png_file_path):
    # Open the image using Pillow
    with Image.open(png_file_path) as img:
        # Convert image to RGBA (if it's not already) to ensure consistent handling
        img = img.convert('RGBA')

        # Use io.BytesIO to handle the image data in memory
        img_byte_arr = io.BytesIO()
        img.save(img_byte_arr, format='PNG')  # Save image to the byte array in PNG format
        img_byte_arr.seek(0)  # Move to the start of the byte array

        # Read the image data back from the byte stream
        byte_data = img_byte_arr.read()

        # Decode the PNG data back into an image (for educational/demonstration purposes)
        # Note: Normally, you would not re-encode and then decode like this.
        new_img = Image.open(io.BytesIO(byte_data))
        new_img = new_img.convert('RGBA')  # Ensure format consistency

        return new_img


def erase_pixels(img, rect=None, color_to_erase=HIGHLIGHTS_COLOR[DIGEST_COLOR]):
    """ If a rect is given, erase the rectangle, otherwise erase pixel colors (by default digestion selection) """
    # Load the image
    img_array = np.array(img)

    # Determine the 'blank' pixel depending on image mode (RGBA or not)
    if img.mode == 'RGBA':
        color_to_erase = np.array(list(color_to_erase) + [255])  # Assuming full opacity for matching
        blank_pixel = [255, 255, 255, 0]  # Fully transparent for RGBA
    else:
        blank_pixel = 255  # White for grayscale or RGB

    # If a rect is given, erase the rectangle, otherwise erase pixel colors (by default digestion selection)
    if rect:
        # Define the rectangle boundaries
        x1, y1, x2, y2 = rect
        # Erase pixels in the rectangle by setting them to the 'blank' pixel
        img_array[y1:y2, x1:x2] = blank_pixel

    else:
        # Find all pixels matching the color to erase
        matches = np.all(img_array[:, :, :3] == color_to_erase[:3], axis=-1)

        # Apply the blank pixel to all matching pixels
        if img.mode == 'RGBA':
            img_array[matches] = blank_pixel
        else:
            # For non-RGBA images, ensure the blank pixel doesn't try to assign multiple values
            img_array[matches] = blank_pixel if img.mode != 'RGB' else [blank_pixel] * 3

    # Create a new image from the modified array
    new_img = Image.fromarray(img_array, img.mode)

    return new_img


def replace_hex_bytes_in_file_block(binary_content, replacement_bytes, start_pos):
    """ Replaces block of bytes at a given position in the file.
        It is assumed that the 4 bytes at position hold the size of the block
        Proceed only if the current size of the block is larger than the replacement_hex size
        TODO: Change the address and move replacement bytes at the end of the file for larger blocks"""
    replaced = False
    # Read Current block size at position
    completed, current_block_size = read_endian_int_at_position(binary_content, start_pos)
    if completed:
        new_block_size = len(replacement_bytes)
        if new_block_size <= current_block_size:
            # Prepare the new block with the size header
            header_new_block = int_to_little_endian_bytes(new_block_size)
            new_block = header_new_block + replacement_bytes

            # Overwrite the block in the binary content
            new_binary_content = (
                binary_content[:start_pos] + new_block + binary_content[start_pos + 4 + new_block_size:])
            replaced = True
        else:
            new_binary_content = binary_content
    return replaced, new_binary_content


def mute_stroke_color(binary_data, position_list, colors=[HIGHLIGHTS_COLOR[DIGEST_COLOR][0], 202, 254]):
    """ Replace color of strokes in trailcontainer to 'FF'
        This is not an ephemeral action and we do not really erase the strokes, we just make them invisible """
    completed_function = True
    muted_color = int('FF', 16)
    try:
        # Convert the bytes object to a bytearray
        data = bytearray(binary_data)
        # Positionning of the byte for color in a given trail
        color_byte_pos = 5
        # Parse the list of 'Totalpath' positions
        for a_position in position_list:
            # Read the size of a trailcontainer (4 bytes)
            completed, tc_size = read_endian_int_at_position(data, a_position, num_bytes=4)

            if completed:
                # Read the number of trails in the container
                completed, tc_nb = read_endian_int_at_position(data, a_position + 4, num_bytes=4)

                if completed:
                    tc_offset = 8

                    # Parse the number of trails in the container
                    for tc_index in range(tc_nb):
                        # read size of trail
                        completed, t_size = read_endian_int_at_position(data, a_position + tc_offset, num_bytes=4)

                        if completed:
                            # read cor_byte_value
                            byte_position = a_position+tc_offset+4+color_byte_pos-1

                            color_byte_value = data[byte_position]
                            if color_byte_value in colors:
                                data[byte_position] = muted_color

                            tc_offset += t_size + 4
                        else:
                            completed_function = False
                            break
                else:
                    completed_function = False
                    break
            else:
                completed_function = False
                break
    except Exception as e:
        print()
        print(f'*** Error in mute_stroke_color: {e}')
        return False, binary_data

    return completed_function, bytes(data)


def mute_page(binary_data, page_address):
    """ 'Deletes' marking of a page in the binary file.
        To meet our goal of minimalistic intervention in editing binaries, we don't actually delete
        the information, but we edit the size header of the page binaries to indicate that the file
         is empty (zero size). Since the pdf file and the mark files are 2 dinstinct files, when deleting
        a pdf page, the corresponding pen strokes in the mark file need to be muted or the SN will 'spill'
         these in the next pdf page. Muting the page addresses the issue. """
    try:
        size_zero = b'\x00\x00\x00\x00'
        # Convert the bytes object to a bytearray
        data = bytearray(binary_data)
        # Set block size to zero
        data[page_address:page_address+4] = size_zero
    except Exception as e:
        print()
        print(f'*** Error in mute_page: {e}')
    return bytes(data)


def xref_layer_by_name(doc, layer_name):
    """ Returns the 'layer_name' layer xref """
    try:
        ocgs_dict = doc.get_ocgs()
        for ocg, ocg_dict in ocgs_dict.items():
            if ocg_dict['name'] == layer_name:
                return ocg
        return None
    except Exception as e:
        print(f'*** xref_layer_by_name: {e}')
        return None


def compare_lists(list1, list2):
    """ returns intersections and items specifics to each list"""
    # Convert lists of lists to sets of tuples to perform set operations
    set1 = set(tuple(item) for item in list1)
    set2 = set(tuple(item) for item in list2)

    # Calculate intersection
    common_items = [list(item) for item in set1 & set2]

    # Calculate items unique to list1
    specific_to_list1 = [list(item) for item in set1 - set2]

    # Calculate items unique to list2
    specific_to_list2 = [list(item) for item in set2 - set1]

    # Calculate union of the 2 lists
    union_list = list(set(tuple(x) for x in list1) | set(tuple(x) for x in list2))

    return common_items, specific_to_list1, specific_to_list2, union_list


def save_to_extended_metadata(pdfdoc, xref_meta, key, value):
    """ Save private information  the extended metadata """
    try:
        if isinstance(value, dict):
            pdfdoc.xref_set_key(xref_meta, key, fitz.get_pdf_str(json.dumps(value)))
        else:
            pdfdoc.xref_set_key(xref_meta, key, fitz.get_pdf_str(value))
    except Exception as e:
        print()
        print(f'*** Error: {e}')
        print(f'Could not save {key} to extended metadata')


def extended_metadata(doc):
    """ Returns the reference to the 'info' data and the extended metadata, including private information"""
    metadata = {}  # make my own metadata dict
    try:
        what, value = doc.xref_get_key(-1, "Info")  # /Info key in the trailer
        if what != "xref":
            pass  # PDF has no metadata
        else:
            xref = int(value.replace("0 R", ""))  # extract the metadata xref
            for key in doc.xref_get_keys(xref):
                metadata[key] = doc.xref_get_key(xref, key)[1]
        return xref, metadata
    except Exception as e:
        print()
        print(f'*** extended_metadata: {e}')
        return None, metadata


def concatenate_text_and_get_rect(data):
    """ Concatenates text and return rect of container of a list of text, rect
        Used to aggregate CVision output """
    concatenated_text = ''
    min_x1, min_y1, max_x2, max_y2 = float('inf'), float('inf'), float('-inf'), float('-inf')

    for text, rect in data:
        concatenated_text += text + ' '
        x1, y1, x2, y2 = rect
        min_x1 = min(min_x1, x1)
        min_y1 = min(min_y1, y1)
        max_x2 = max(max_x2, x2)
        max_y2 = max(max_y2, y2)

    concatenated_text = concatenated_text.strip()
    overall_rect = [min_x1, min_y1, max_x2, max_y2]

    return concatenated_text, overall_rect


def pdf_datetime_to_timestamp(pdf_datetime):
    # Extract the main datetime components and the timezone offset
    date_str = pdf_datetime[2:16]  # Strip off the initial 'D:'
    sign = pdf_datetime[16]
    offset_hours = int(pdf_datetime[17:19])
    offset_minutes = int(pdf_datetime[20:22])

    # Create a datetime object from the string
    dt = datetime.strptime(date_str, '%Y%m%d%H%M%S')

    # Calculate the total offset in minutes
    total_offset = (offset_hours * 60 + offset_minutes) * (1 if sign == '+' else -1)

    # Apply the timezone offset
    tz = timezone(timedelta(minutes=total_offset))
    dt = dt.replace(tzinfo=tz)

    # Convert datetime to UTC
    dt_utc = dt.astimezone(timezone.utc)

    # Convert to Unix timestamp in milliseconds
    timestamp = int(dt_utc.timestamp() * 1000)

    return timestamp


def sorted_annots_toc(doc, last_annot_toc, type=None):
    """ Creates a sorted list of annotation by date """
    list_annots = []

    try:

        if not doc.has_annots():
            return
        else:
            doc_toc = doc.get_toc(simple=False)
            existing_annots_toc = [(x[1], x[2]) for x in doc_toc]
            for page in doc.pages():
                for annot in page.annots():
                    if type is not None:
                        if annot.type != type:
                            continue
                    annot_text = annot.get_text()
                    annot_info = annot.info
                    annot_date = ''
                    if annot_info is not None:
                        if 'modDate' in annot_info:
                            annot_date = annot_info['modDate']    #
                            if annot_date != '':
                                annot_date_ts = pdf_datetime_to_timestamp(annot_date)
                                annot_page = page.number+1
                                if annot_text == '':
                                    annot_text = annot_info['content']
                                    if annot_text == '':
                                        annot_text = annot_info['subject']
                                    date_str = annot_date[2:16]
                                    dt = datetime.strptime(date_str, '%Y%m%d%H%M%S')
                                    standard_datetime = dt.strftime('%Y-%m-%d %H:%M:%S')
                                    annot_text += '\n'+standard_datetime
                                a_bkmark = (annot_text, annot_page, annot_date_ts)
                                if (annot_text, annot_page) not in existing_annots_toc:
                                    list_annots.append(a_bkmark)

            list_annots.sort(key=lambda x: -x[2])
            toc_annote_date = [(2, x[0], x[1]) for x in list_annots]

            previous_last = list(last_annot_toc)

            if len(toc_annote_date) > 0:
                if previous_last == []:
                    toc_annote_date.insert(0, (1, 'Last annotations', toc_annote_date[0][2], {"color": (1, 0, 0)}))
                    toc_annote_date.sort(key=lambda x: x[2])
                    doc_toc.extend(toc_annote_date)
                    doc_toc = simplify_toc(doc_toc)
                    doc.set_toc(doc_toc, collapse=1)
                else:
                    for el_toc in toc_annote_date:
                        if el_toc not in previous_last:
                            previous_last.append(el_toc)
                    previous_last_sorted = sorted(previous_last, key=lambda x: x[1][-19:])[::-1]

                    doc_toc.extend(previous_last_sorted)
                    doc_toc = simplify_toc(doc_toc)
                    doc.set_toc(doc_toc, collapse=1)

            else:
                if previous_last != []:
                    doc_toc.extend(previous_last)
                    doc_toc = simplify_toc(doc_toc)
                    doc.set_toc(doc_toc, collapse=1)

        return
    except Exception as e:
        print()
        print(f'*** sorted_annots_toc: {e} - doc_toc: {doc_toc}')


def delete_annotation_at_rect(page, target_rect, target_color, tolerance=0.2):
    """
    Delete annotations at a specified rectangle and of a specified color.

    Args:
    target_rect (fitz.Rect): The rectangle area in which annotations should be checked.
    target_color (tuple): The RGB color of the annotations to delete (values 0-1).
    tolerance (float): The tolerance for color comparison.
    """
    annotations = page.annots()
    if annotations:
        for annot in annotations:
            if annot.rect.intersects(target_rect):  # Check if the rects intersect
                color = annot.colors.get("fill")  # Get the annotation's color
                if color and all(abs(color[i] - target_color[i]) <= tolerance for i in range(3)):
                    page.delete_annot(annot)


def rect_exists(page_settings, rect, threshold=0.80, rec_position=2, flag_position=5):
    """ Lookup in the page_settings if an entry is
        intersecting with the rect above the threshold. This function is useful to avoid
        reprocessing links to a file that has them already built-in """
    try:
        result = False
        intersecting_rect_position = -1

        if 's_marker' in page_settings:
            selected_rects = page_settings['s_marker']
            index = 0
            for a_rect_set in selected_rects:
                selected_rect = a_rect_set[rec_position]
                selected_rect_has_link = a_rect_set[flag_position] == 1
                if intersection_area(selected_rect, rect, threshold=threshold):
                    result = selected_rect_has_link
                    intersecting_rect_position = index
                    break
                index += 1
    except Exception as e:
        print()
        print(f'*** rect_exists: {e} - {rect}')
    return result, intersecting_rect_position


def submit_png_to_cvision(file_path):
    """ Submits a png image to Microsoft Azure Computer Vision
        EndPoint. Details about the endpoint, as well as the API key,
        are stored in the environment variables.

        The endpoint contains reference to the resource that
        has been provisioned on Microsoft Azure
        """
    cvision_response = False
    # Open the image file
    try:
        image = Image.open(file_path)
        # Convert the image to a NumPy array
        image_array = np.array(image)
        # Check if there is any non-zero pixel in the image
        try:
            if not np.any(np.all(image_array != [255, 255, 255], axis=-1)):
                return [], cvision_response
        except Exception as e:
            print(e)
    except Exception as e:
        print()
        print(f'*** Error in submit_png_to_cvision: {e}')
        return [], cvision_response

    # Loads environment variables
    global USE_MS_VISION
    DCHECKS_AVS_END_POINT = os.environ.get('DCHECKS_AVS_END_POINT')
    DCHECKS_AVS_KEY = os.environ.get('DCHECKS_AVS_KEY')

    url = f'{DCHECKS_AVS_END_POINT}/computervision/imageanalysis:analyze?api-version=2023-10-01&features=read&model-version=latest&language=en'

    # Get the image content-type
    content_type, _ = mimetypes.guess_type(file_path)
    if content_type is None:
        content_type = 'application/octet-stream'

    # Format the header
    headers = {
        'Content-Type': content_type,
        'Ocp-Apim-Subscription-Key': DCHECKS_AVS_KEY}

    try:
        # Force wait to avoid API rate limit
        time.sleep(AVS_WAIT)

        # Open the image file in binary mode
        files = open(file_path, 'rb')

        # Make a POST request
        print()
        print(f'\n>>>> Contacting MS Computer Vision (sleeping {AVS_WAIT}s): {file_path[-50:]}', end='')
        response = requests.post(url=url, headers=headers, data=files)

        # Check the response. If valid, return the list of blocks
        if response.ok:
            print('\u2713', end='')
            print()
            aresp = json.loads(response.content.decode('utf-8'))
            cvision_response = True
            return aresp["readResult"]["blocks"], cvision_response
        else:
            print()
            return [], cvision_response
    except requests.exceptions.RequestException as e:
        print()
        print(f"*** An error occurred: {e}... Disabling temporarily external recognition...")
        USE_MS_VISION = False
        return [], cvision_response


def linux_to_windows_paths(path_input):
    """ Makes a linux path or a list of paths compatible with Windows """
    if isinstance(path_input, list):
        return [p.replace('/', '\\') for p in path_input]
    else:
        return path_input.replace('/', '\\')


def stop_adb_if(original_state=False, path=ADB_PATH):
    """ Stops adb if original_state is False (i.e if adb was not running) OR if no parameter is provided.
        Parameters:
        - original_state: boolean.
        - path: path to the adb executable"""
    if not original_state:
        command = [path, 'kill-server']
        try:
            subprocess.run(command)
        except Exception as e:
            print(f"An error occurred: {e}")
            print(f'*** Failed to stop ADB by calling "{command}"')
            return
    return


def is_adb_running():
    """Check if ADB server is running by looking for its process."""
    for proc in psutil.process_iter(['name']):
        if proc.info['name'][:3] == 'adb':
            return True
    return False


def attached_sn(device_name=DEVICE_SN, path=ADB_PATH, host=ADB_HOST, port=ADB_LOCAL_PORT):
    """ Try to locate Supernote serial number on a USB port.
        Returns a SN device object and a boolean indicating if
         ADB was running before the connection

         Parameters:
         - device_name    :   the serial number of the supernote, if available
         - path           :   path to the adb executable

         Returns:
         - A tuple consisting of: an adb device object and the state of ADB prior the call
         """

    exclude = ['List', 'of', 'devices', 'attached', 'device']  # List of keywords to exclude from output

    # Check if ADB server is already running
    adb_was_running = is_adb_running()
    if adb_was_running:
        print('    > ADB is already running')
    else:
        print('  > Starting ADB server ...')
        try:
            subprocess.run([path, 'start-server'], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            print("*** Could not start ADB server:", e)
            return None, adb_was_running

    # Identify connected devices and connect to a sn, preferably device_name
    try:
        list_devices_cmd = [path, 'devices']
        result = subprocess.run(list_devices_cmd, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        command_result = result.stdout.split()

        device_serials = [x for x in command_result if x not in exclude and x[:2] == 'SN']
        if len(device_serials) > 0:
            if device_name in device_serials:
                device_sn_to_use = device_name
            else:
                device_sn_to_use = device_serials[0]
                device_sn_to_use_x = f'SNXXXXXXXX{device_sn_to_use[-4:]}'
                print(f'    > Found Supernote {device_sn_to_use_x}')
                if device_sn_to_use_x in NOTEBOOK_SERIES:
                    global NOTEBOOK_DEVICE
                    NOTEBOOK_DEVICE = NOTEBOOK_SERIES[device_sn_to_use_x]
                    print(f'      Type: {NOTEBOOK_DEVICE}')
                else:
                    while True:
                        user_input = input('    Device type needed. Enter "5" for A5x or Manta, "6" for A6x or Nomad:')
                        if user_input in ("5", "6"):
                            NOTEBOOK_DEVICE = f'N{user_input}'
                            user_settings_dict = load_user_settings()
                            dt_settings = user_settings_dict['series']
                            dt_settings[device_sn_to_use_x] = NOTEBOOK_DEVICE
                            save_user_settings(user_settings_dict)
                            print()
                            print(f'    Thank you! {device_sn_to_use} will be recognized as series N{user_input}')
                            print()
                            break
        else:
            print("    > No Supernote device found")
            print("      Please ensure that SN is connected to USB port AND that Sideloading is allowed")
            print("      (In Supernote settings: Security & Privacy -> Sideloading -> ON)")
            return None, adb_was_running
    except subprocess.CalledProcessError as e:
        print("*** Failed to list devices:", e)
        return None, adb_was_running

    # Try to create an adb client device object
    try:
        client = AdbClient(host=host, port=port)
        device = client.device(device_sn_to_use)
    except Exception as e:
        print("*** No Supernote device found on USB:", e)
        return None, adb_was_running

    return device, adb_was_running


def get_file_system_in_userspace(device):
    """ Retrieve FUSE header information for internal
        storage and SD Card, when connecting through USB
        """
    internal_prefix = '/storage/emulated/0/'
    external_prefix = ''
    try:
        # We send a 'disk free human readable' request
        command_sending = "df -h"
        disk_free_request = device.shell(command_sending)
        # FUSE entries contain '/dev/fuse' . We only keep those lines
        lines = disk_free_request.strip().split('\n')

        fuse_entries = [line.split()[-1] for line in lines if 'storage' in line]
        linternal_prefix = [item for item in fuse_entries if 'emulated' in item]
        if len(linternal_prefix) > 0:
            internal_prefix = linternal_prefix[0] + '/0/'
        lexternal_prefix = [item for item in fuse_entries if 'emulated' not in item]
        if len(lexternal_prefix) > 0:
            external_prefix = lexternal_prefix[0] + '/'

    except Exception as e:
        print()
        print(f'*** Error in FUSE: {e}, when sending "{command_sending}" to the shell')
        command_sending = "cat /proc/mounts | grep '/storage/' | grep -v '/storage/emulated' | awk '{print $2}'"
        print(' >> Trying one more time "cat /proc/mounts for external drive ...')
        try:
            internal_prefix = '/storage/emulated/0/'
            external_prefix = device.shell(command_sending).split('\n')[0] + '/'
            print(f'--- I will resort using "{internal_prefix}" (internal) and "{external_prefix}" (external)')
        except Exception as e:
            print()
            print(f'*** failed again: {e}, resulting to defaults')
            external_prefix = ''
    print(f'      internal_prefix: {internal_prefix}')
    print(f'      external_prefix: {external_prefix}')
    return internal_prefix, external_prefix


def copy_files_from_device(device, source_folder, destination_folder, hist_dict,
                           cached_folder=PDF_CACHED, ignore_cache_list=NEVER_USE_CACHE_FILES):
    """ Copies files from a folder on adb client device to a destination folder.
        For pdf files, looks first in a cached folder before proceeding.
        Populates the cached folder for new pdf files
        We are only checking the filename to see if the cache should be used and this is
        not very reliable, thus the ignore_cache_list."""
    try:
        print()

        # Get the list of files in the source folder on the device

        source_folder_ = source_folder.replace(' ', '\\ ')
        filestr = device.shell(f"ls -p {source_folder_}")
        filestr = filestr.replace('\n', '  ').replace('\\ ', '%20')
        filestr_list = re.split(r'\s', filestr)
        files_list = [x for x in filestr_list if x != '' and not x.endswith('/')]
        files_list.sort()

        # Iterate over each file and pull it to the destination folder
        for file_name in files_list:
            base_name = file_name.replace('%20', ' ')
            source_path = os.path.join(source_folder, base_name)
            dest_path = os.path.join(destination_folder, base_name)

            # Fixing a bug where same file name is used with different versions from different source folders
            version_ = 1
            while os.path.exists(dest_path):
                f_name, f_extension = os.path.splitext(dest_path)
                dest_path = f'{f_name}(pysnv{version_}){f_extension}'  # Introducing a temporary pysn version nb
                version_ += 1

            ignore_cache_list_l = [x.lower() for x in ignore_cache_list]
            cache_exception = base_name.lower() in ignore_cache_list_l
            if source_path[-3:].lower() == 'pdf':
                cached_path = os.path.join(cached_folder, base_name)
                if os.path.exists(cached_path) and not cache_exception:
                    shutil.copy(cached_path, dest_path)
                else:
                    device.pull(source_path, dest_path)
                    print(f'  - {base_name}')
                    if not cache_exception:
                        shutil.copy(dest_path, cached_path)
            else:
                device.pull(source_path, dest_path)
                print(f'    {base_name}')
            clean_path = remove_storage_paths(source_path)
            hist_dict[dest_path] = clean_path

    except Exception as e:
        print(f"An error occurred: {e}")
        print(f'*** Failed to copy "{source_folder}"')
    return hist_dict


def delete_files_by_pattern(device, pattern, base_folder, folder=SN_IO_UPLOAD_FOLDER):
    """ Deletes files of a certain pattern on attached usb device """
    try:
        pattern = pattern.replace(' ', '%20')
        # Construct and execute command to delete files matching the pattern
        delete_command = f"find {base_folder}{folder} -name '{pattern}' -exec rm {{}} +"
        result = device.shell(delete_command)
        # Print the result of the delete command
        print(result)
        # Optional: Confirm deletion or list remaining files
        remaining_files = device.shell(f"ls {base_folder}{folder}")
        rfl = remaining_files.split()
        rfl_ = [x.replace('%20', ' ') for x in rfl]
        print("Remaining files in the directory:")
        print(rfl_)

    except Exception as e:
        print(f"An error occurred: {e}")
        print(f'*** Failed to delete file(s) like {pattern} on "{base_folder}{folder}"')


def timestamp_to_pdf_datetime(timestamp):
    # Convert millisecond timestamp to seconds
    dt = datetime.fromtimestamp(timestamp / 1000)

    # Localize datetime to current timezone
    # This requires third-party libraries like pytz or dateutil if you need to handle time zones other than the local system's.
    local_dt = dt.astimezone()

    # Format the timezone offset
    # Offset returned is in seconds. Convert it to 'HH'mm'' format.
    offset_sec = local_dt.utcoffset().total_seconds()
    sign = '+' if offset_sec >= 0 else '-'
    offset_h = int(abs(offset_sec) // 3600)
    offset_m = int((abs(offset_sec) % 3600) // 60)

    # Format according to PDF specification
    pdf_datetime = local_dt.strftime(f"D:%Y%m%d%H%M%S{sign}{offset_h:02d}\'{offset_m:02d}\'")

    return pdf_datetime


def time_now_to_pdf_datetime():
    # Convert millisecond timestamp to seconds
    dt = datetime.now()

    # Localize datetime to current timezone
    # This requires third-party libraries like pytz or dateutil if you need to handle time zones other than the local system's.
    local_dt = dt.astimezone()

    # Format the timezone offset
    # Offset returned is in seconds. Convert it to 'HH'mm'' format.
    offset_sec = local_dt.utcoffset().total_seconds()
    sign = '+' if offset_sec >= 0 else '-'
    offset_h = int(abs(offset_sec) // 3600)
    offset_m = int((abs(offset_sec) % 3600) // 60)

    # Format according to PDF specification
    pdf_datetime = local_dt.strftime(f"D:%Y%m%d%H%M%S{sign}{offset_h:02d}\'{offset_m:02d}\'")

    return pdf_datetime


def hide_console_window():
    """ Windows platform only: Hide the terminal poping up, for tasks launched by
        Windows task scheduler"""
    # Get the handle of the console window
    console_window = ctypes.windll.kernel32.GetConsoleWindow() if platform.system() == 'Windows' else None
    if console_window:
        # Hide the window
        # pass
        ctypes.windll.user32.ShowWindow(console_window, 0)


def get_text_from_image(image_fn, inv_pr_matrix, pix_dict_fn=PIX_DICT_FN, recognize=USE_MS_VISION, device=NOTEBOOK_DEVICE):
    """ If the hash signature of the file isn't found in the pix_dict
        dictionary, submits the image to computer vision at Microsoft for
        text detection """
    pix_text = ''
    hw_ocr_list = []
    hw_ocr_detail_list = []

    if recognize:
        try:
            # Get the file hash signature
            image_fn_sha = file_sha(image_fn)

            # Retrieve its entry in the pix_dict, if available
            pix_dict = read_json(pix_dict_fn)
            if image_fn_sha in pix_dict:
                saved_rec = pix_dict[image_fn_sha]
                pix_text = saved_rec['text']
                hw_ocr_list = saved_rec['map']
                if 'map_single' in saved_rec:
                    hw_ocr_detail_list = saved_rec['map_single']
                    if 'device' in saved_rec:
                        saved_device = saved_rec['device']
                        # Makes a conversion for OCR made with another device type
                        if saved_device != device:

                            if saved_device == 'N5':
                                mult_ratio = MAX_HORIZONTAL_PIXELS_N6/MAX_HORIZONTAL_PIXELS_N5
                            else:
                                mult_ratio = MAX_HORIZONTAL_PIXELS_N5/MAX_HORIZONTAL_PIXELS_N6
                            hw_ocr_detail_list = [[x[0], [y * mult_ratio for y in x[1]], x[2]] for x in hw_ocr_detail_list]

            else:
                apng_ocr, cvision_response = submit_png_to_cvision(image_fn)
                if len(apng_ocr) > 0 or cvision_response:
                    if len(apng_ocr) > 0:
                        apng_ocr_j = apng_ocr[0]
                        for element in apng_ocr_j['lines']:
                            a_text = element['text']
                            pix_text += a_text + ' '
                            a_bound = element['boundingPolygon']

                            x0, y0, x1, y1 = (a_bound[0]['x'], a_bound[0]['y'], a_bound[2]['x'], a_bound[2]['y'])
                            a_rect_s = fitz.Rect((x0, y0, x1, y1))*inv_pr_matrix
                            a_rect = [a_rect_s[i] for i in range(4)]
                            hw_ocr_list.append((a_text, a_rect))

                            a_words = element['words']
                            for word_element in a_words:
                                single_word = word_element['text']
                                p1, p2, p3, p4 = word_element['boundingPolygon']
                                a_confidence = word_element['confidence']
                                sx0 = min(p1['x'], p2['x'], p3['x'], p4['x'])
                                sy0 = min(p1['y'], p2['y'], p3['y'], p4['y'])
                                sx1 = max(p1['x'], p2['x'], p3['x'], p4['x'])
                                sy1 = max(p1['y'], p2['y'], p3['y'], p4['y'])
                                a_rect_s_single = fitz.Rect((sx0, sy0, sx1, sy1))*inv_pr_matrix
                                a_rect_single = [a_rect_s_single[i] for i in range(4)]
                                hw_ocr_detail_list.append((single_word, a_rect_single, a_confidence))

                    pix_text = pix_text.strip()
                    pix_dict[image_fn_sha] = {
                        "text": pix_text,
                        "map": hw_ocr_list,
                        "map_single": hw_ocr_detail_list,
                        "device": device}
                    save_json(pix_dict_fn, pix_dict)
        except Exception as e:

            print()

            print(f'*** Failed to get text from MS vision: {e}')
            return '', [], []
    return pix_text, hw_ocr_list, hw_ocr_detail_list


async def download_file(session, file_url, destination, hist_dict):
    async with session.get(file_url) as response:
        if response.status == 200:
            destination = unquote(destination)
            a_dest_split = destination.split('\\')[-1]
            print(f"     {a_dest_split}")
            data = await response.read()
            version_ = 1

            while os.path.exists(destination):
                f_name, f_extension = os.path.splitext(destination)
                destination = f'{f_name}(pysnv{version_}){f_extension}'  # Introducing a temporary pysn version nb
                version_ += 1
            async with aiofiles.open(destination, 'wb') as f:
                await f.write(data)
            clean_path = remove_storage_paths(file_url).replace('+', ' ')

            hist_dict[unquote(destination)] = clean_path


async def upload_file(file_path, upload_url, filename):

    boundary = '----WebKitFormBoundary7MA4YWxkTrZu0gW'
    async with aiohttp.ClientSession() as session:
        # Read the file data
        async with aiofiles.open(file_path, 'rb') as f:
            file_data = await f.read()

        # Create multipart form body as bytes
        body = (
            f'--{boundary}\r\n'
            f'Content-Disposition: form-data; name="file"; filename="{filename}"\r\n'
            f'Content-Type: {mimetypes.guess_type(file_path)[0]}\r\n\r\n'
        ).encode() + file_data + (
            f'\r\n--{boundary}\r\n'
            f'Content-Disposition: form-data; name="fileinfo"\r\n'
            f'Content-Type: application/json\r\n\r\n'
            f'{{"name": "{filename}"}}\r\n'
            f'--{boundary}--\r\n'
        ).encode()

        headers = {
            'Content-Type': f'multipart/form-data; boundary={boundary}'
        }

        async with session.post(upload_url, data=body, headers=headers) as response:
            if response.status != 200:
                print(f"*** Error: Failed to upload {file_path}, status: {response.status}")


async def lanon_and_microsdname(url):
    """ Checks if the Supernote is reachable through LAN on and returns the microSD card prefix, if any"""

    async with aiohttp.ClientSession() as session:
        html = await fetch(session, url)
        if html is None:  # Error connecting
            return False, ''
        soup = BeautifulSoup(html, 'html.parser')

        # Find the script containing the data
        script_text = ''
        found = False
        for script in soup.find_all("script"):
            if 'const json =' in script.string or 'const json=' in script.string:  # Check if the script contains the JSON variable
                script_text = script.string
                found = True
                break
        if not found:
            print("No script with JSON data found.")
            return False, ''
        # Extract JSON data from the script
        match = re.search(r"('.*?')", script_text, re.DOTALL)
        if match:
            json_text = match.group(1)
            data = json.loads(json_text[1:-1])
        else:
            print("Failed to extract JSON data.")
            return False, ''

        # Find uri that is not in the default list
        default_list = SUPERNOTE_FOLDERS
        disk_uri_list = [x['uri'] for x in data['fileList'] if x['name'] not in default_list]
        if len(disk_uri_list) > 0:
            microsd_card = disk_uri_list[0]
            # print(f"  > Connected to {url}. Found card uri: '{microsd_card}'")
            # print()
            return True, microsd_card
        print(f'  > Connected to {url}')
        print()
        return True, ''


async def parse_and_download_files_deep(url, base_folder, hist_dict, deep_folder):
    """ WIFI parsing and download. Can be recursive, if deep_folder is True"""
    root_uri = remove_storage_paths(url)
    print()
    print(f'>>> deep:{deep_folder} - Parsing: {url}')
    print()

    async with aiohttp.ClientSession() as session:
        html = await fetch(session, url)
        if html is None:  # Error connecting
            return False, ''
        soup = BeautifulSoup(html, 'html.parser')

        # Find the script containing the data
        script_text = ''
        found = False
        for script in soup.find_all("script"):
            if 'const json =' in script.string or 'const json=' in script.string:  # Check if the script contains the JSON variable
                script_text = script.string
                found = True
                break
        if not found:
            print("No script with JSON data found.")
            return hist_dict

        # Extract JSON data from the script
        match = re.search(r"('.*?')", script_text, re.DOTALL)
        if match:
            json_text = match.group(1)
            data = json.loads(json_text[1:-1])
        else:
            print("Failed to extract JSON data.")
            return hist_dict

        # Asynchronously download all files
        tasks = []
        files_to_download = []
        path_error = False
        path_error_list = []

        file_list = data['fileList']
        # print(f'---data: {data["fileList"]}')
        for file in file_list:
            file_uri = file['uri']
            if root_uri in file_uri:
                if not file['isDirectory']:  # Ensure it's a file
                    a_uri = file['uri']
                    file_url = urljoin(url, a_uri)
                    file_nm = file['name']
                    destination = base_folder
                    a_sub_local_path = a_uri.replace('+', ' ').replace('\\REMOVABLE0', '')

                    destination = os.path.normpath(os.path.join(base_folder, a_sub_local_path[1:]))
                    local_dir = unquote(os.path.dirname(destination))

                    os.makedirs(local_dir, exist_ok=True)

                    if file_nm[-3:].lower() == 'pdf':
                        cached_destination = os.path.join(PDF_CACHED, file_nm)
                        if os.path.exists(cached_destination):
                            shutil.copy(cached_destination, destination)
                        else:
                            files_to_download.append((destination, cached_destination))
                            task = asyncio.create_task(download_file(session, file_url, destination, hist_dict))
                            tasks.append(task)
                            hist_dict[unquote(destination)] = remove_storage_paths(file_url)
                    else:
                        task = asyncio.create_task(download_file(session, file_url, destination, hist_dict))
                        tasks.append(task)
                elif deep_folder:  # It's a directory: let's make a recursive call
                    if not is_low_priority():
                        set_low_priority()
                    await parse_and_download_files_deep(urljoin(url, file_uri), base_folder, hist_dict, deep_folder)
            else:
                path_error_list.append(file_uri)
                path_error = True
        if path_error:
            if DEBUG_MODE:
                print()
                print(f'*** Unknown paths: {path_error_list}')
            print(f'*** Does path "{root_uri}" exist on the Supernote?')

        await asyncio.gather(*tasks)
        for destination, cached_destination in files_to_download:
            if os.path.exists(destination):
                shutil.copy(destination, cached_destination)
        return hist_dict


async def parse_and_download_files(url, base_folder, hist_dict):

    async with aiohttp.ClientSession() as session:
        html = await fetch(session, url)
        soup = BeautifulSoup(html, 'html.parser')

        # Find the script containing the data
        script_text = ''
        found = False
        for script in soup.find_all("script"):
            if 'const json =' in script.string or 'const json=' in script.string:  # Check if the script contains the JSON variable
                script_text = script.string
                found = True
                break

        if not found:
            print("No script with JSON data found.")
            return hist_dict

        # Extract JSON data from the script
        match = re.search(r"('.*?')", script_text, re.DOTALL)
        if match:
            json_text = match.group(1)
            data = json.loads(json_text[1:-1])
        else:
            print("Failed to extract JSON data.")
            return hist_dict

        # Asynchronously download all files
        tasks = []
        files_to_download = []
        for file in data['fileList']:
            if not file['isDirectory']:  # Ensure it's a file
                file_url = urljoin(url, file['uri'])
                file_nm = file['name']
                destination = os.path.join(base_folder, file_nm)
                if file_nm[-3:].lower() == 'pdf':
                    cached_destination = os.path.join(PDF_CACHED, file_nm)
                    if os.path.exists(cached_destination):
                        shutil.copy(cached_destination, destination)
                    else:
                        files_to_download.append((destination, cached_destination))
                        task = asyncio.create_task(download_file(session, file_url, destination, hist_dict))
                        tasks.append(task)
                        hist_dict[unquote(destination)] = remove_storage_paths(file_url)
                else:
                    task = asyncio.create_task(download_file(session, file_url, destination, hist_dict))
                    tasks.append(task)

        await asyncio.gather(*tasks)
        for destination, cached_destination in files_to_download:
            if os.path.exists(destination):
                shutil.copy(destination, cached_destination)
        return hist_dict


async def async_upload(file_to_upload, upload_url, filename):
    await upload_file(file_to_upload, upload_url, filename)


def copy_rects(source_img, target_img, rects):
    """ Copies rect areas from source to target"""
    # Process each rectangle
    for x0, y0, x1, y1 in rects:
        # Define the box to copy from source image
        box = (x0, y0, x1, y1)
        # Extract the region
        region = source_img.crop(box)
        # Paste the region into the target image
        target_img.paste(region, box)


def find_fonts_in_rect(rect, page):
    """
    Finds unique font names and sizes in the specified rectangle on a given page.

    Parameters:
    - rect: fitz.Rect, the rectangle within which to search for text.
    - page: Page object from a PyMuPDF document.

    Returns:
    - List of tuples, where each tuple contains (font name, font size).
    """
    # Ensure rect is a fitz.Rect object
    rect = fitz.Rect(rect)
    # Initialize a set to store unique (font, size) tuples
    unique_fonts = set()

    # Extract text blocks from the page
    text_blocks = page.get_text("dict")["blocks"]

    # Iterate through text blocks to find fonts in the specified rectangle
    for block in text_blocks:
        if "lines" in block:  # Ensure the block contains lines of text
            for line in block["lines"]:
                for span in line.get("spans", []):  # Iterate through each span in a line
                    span_rect = fitz.Rect(span["bbox"])  # Get the bounding box of the span
                    if rect.intersects(span_rect):  # Check if the span intersects with the given rect
                        # Collect unique font name and size
                        unique_fonts.add((span["font"], int(span["size"])))

    # Return a list of unique font names and sizes
    return list(unique_fonts)


def page_text_under_rects(directory, page, rect_dict, inv_pr_matrix, device=NOTEBOOK_DEVICE):
    """ Using the pdf_rect coordinates in a dict, extracts text and fonts
        then updates the dict """
    for rect_nb in rect_dict.keys():
        # Retrieve the pdf_rect
        rect_object = rect_dict[rect_nb]
        rect = fitz.Rect(rect_object['pdf_rect'])
        # Extract words within the rectangle
        try:
            text = page.get_textbox(rect).strip().replace("\n", " ")
            if text != '':
                myfonts = find_fonts_in_rect(rect, page)
            else:

                if USE_MS_VISION:
                    crop_fn = os.path.join(directory, f'crop_{page.number}_{rect_nb}.png')
                    text, crop_ocr_list, _ = get_text_from_image(crop_fn, inv_pr_matrix, device=device)
                if text == '':
                    text = f" digest {page.number+1}-{int(rect_nb)+1}"
                myfonts = [["None", 16]]

            rect_object["text"] = text
            rect_object["fonts"] = myfonts

        except Exception as e:
            print()
            print(f'*** page_text_under_rects: {e}')
    return rect_dict


def file_sha(filename):
    """ Computes a sha256 for a file.
    In simple terms: The unique signature of a file contents.
    We use this to check if the contents of a file has changed"""
    with open(filename, 'rb') as f:
        sha256_hash = hashlib.sha256()
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)
    return sha256_hash.hexdigest()


def get_last_modified_date(file_path, datetime_format='%Y-%m-%d %H:%M:%S UTC'):
    """ Return the last mod time of a file_path"""
    timestamp = os.path.getmtime(file_path)
    return datetime.fromtimestamp(timestamp, timezone.utc).strftime(datetime_format)


def list_files(directories_, exclude_folder):
    """ List all files in directories excluding those in the exclude_folder"""
    if isinstance(directories_, list):
        directories = directories_
    else:
        directories = [directories_]
    result = []
    try:
        # Convert exclude_folder to an absolute path for reliable comparison
        if exclude_folder == '':
            exclude_folder_abs = None
        else:
            exclude_folder_abs = os.path.abspath(exclude_folder)

        for directory in directories:

            directory_abs = os.path.abspath(directory)

            for root, dirs, files in os.walk(directory_abs):

                # Check if the current root is within the exclude_folder
                if exclude_folder_abs is not None:
                    if exclude_folder_abs in os.path.abspath(root):
                        continue  # Skip this iteration

                for name in files:
                    result.append(os.path.join(root, name))
        return result
    except Exception as e:
        print(f"An error occurred: {e}")
        return []


def create_workdir(source_subfolder=SN_VAULT):
    """ Creates a temp timestamped sub-folder """
    current_time = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    work_folder = os.path.join(source_subfolder, f'work-{current_time}')
    os.makedirs(work_folder, exist_ok=True)
    return work_folder


def calculate_min_distance(rect1, rect2):
    left1, bottom1, right1, top1 = rect1
    left2, bottom2, right2, top2 = rect2

    h_dist = max(0, max(left1, left2) - min(right1, right2))
    v_dist = max(0, max(bottom1, bottom2) - min(top1, top2))
    result = (h_dist**2 + v_dist**2)**0.5

    return result


def assemble_puzzle(pieces, rects, inv_pr_matrix):
    """
    Assemble puzzle pieces into one image by calculating the necessary width and height from the bounding rectangles.

    :param pieces: List of PIL.Image objects for each piece of the puzzle.
    :param rects: List of tuples (x0, y0, x1, y1) defining the bounding box for each piece.
    :return: PIL.Image object representing the assembled image.
    """
    if not pieces or not rects:
        return None  # Return None if no pieces or rects

    # Calculate the minimum and maximum x and y values
    min_x = min(rect[0] for rect in rects)
    min_y = min(rect[1] for rect in rects)
    max_x = max(rect[2] for rect in rects)
    max_y = max(rect[3] for rect in rects)

    # Calculate the width and height of the output image
    width = max_x - min_x
    height = max_y - min_y

    # Create a new blank image with the computed dimensions
    final_image = Image.new('RGBA', (width, height))

    # Iterate over each piece and its corresponding rectangle
    for piece, rect in zip(pieces, rects):
        x0, y0, x1, y1 = rect
        # Resize the piece to match its target size
        resized_piece = piece.resize((x1 - x0, y1 - y0), Image.Resampling.LANCZOS)
        # Paste the resized piece onto the final image, adjusting position relative to the minimum x and y
        final_image.paste(resized_piece, (x0 - min_x, y0 - min_y), resized_piece)

    # Find the relative coordinates in the reporting
    rel_coordinates = fitz.Rect(0, 0, width, height) * inv_pr_matrix

    return final_image, (rel_coordinates.width, rel_coordinates.height)


def create_note_toc(titles_dict):
    """ Creates a table of content for a note,
        based on the text recognition dictionary in the format
         {'1': [
            {'TITLESEQNO': '0',
            'TITLELEVEL': '1',
            ...
            'TITLERECTSTD': [206, 223, 1140, 381],
            'TITLERANK': 1,            'TITLETEXT': 'My quest to Improve my notes'},
             {...}],
         '2': ...} """
    toc_final = None

    try:
        toc = []
        previous_level = 1
        for page_nb, page_titles_list in titles_dict.items():
            toc_page = []
            for a_title in page_titles_list:
                title_rank = a_title['TITLERANK']
                title_rect = a_title['TITLERECTSTD']
                if 'TITLETEXT' in a_title:
                    title_text = a_title['TITLETEXT']
                else:
                    title_text = f'Header-{title_rank}'
                if title_rank - previous_level > 1:
                    title_rank = previous_level + 1
                toc_page.append([title_rank, title_text, int(page_nb), title_rect[0], title_rect[1]])
                previous_level = title_rank

            sorted_toc_page = sorted(toc_page, key=lambda x: (x[4], x[3]))
            resized_list = [[x[0], x[1], x[2], x[3]] for x in sorted_toc_page]

            toc.extend(resized_list)
        toc.insert(0, [1, 'Headers', 0])
        toc_final = toc

    except Exception as e:
        print()
        print(f'*** Error creating toc: {e}')
    return toc_final


def get_dict_toc(doc):
    """ Retrieves existing TOC from doc as a dictionary"""

    try:
        toc_f = doc.get_toc(simple=False)

        a_bookmark_dict = {
            "main_bookmark": [],
            "Digested": [],
            "Last annotations": [],
            "KEYWORDS": [],
            "STARS": []
        }

        a_bookmark_key = 'main_bookmark'

        for a_toc in toc_f:
            a_dict_caption = a_toc[1]
            if a_dict_caption in a_bookmark_dict:
                a_bookmark_key = a_dict_caption
                a_bookmark_key_list = a_bookmark_dict[a_bookmark_key]
                a_bookmark_key_list.append(a_toc)

        main_bookmark = a_bookmark_dict['main_bookmark']
        main_bookmark_simple = [(x[0], x[1], x[2]) for x in main_bookmark]  # TODO: Investigate why including default dict fails
        a_bookmark_dict['main_bookmark'] = main_bookmark_simple
        print()
        print(f'----a_bookmark_dict: {a_bookmark_dict}')
        return a_bookmark_dict

    except Exception as e:
        print()
        print(f'*** get_dict_toc: {e} - dict: {a_bookmark_dict}')
        return {"main_bookmark": doc.get_toc(simple=True)}


def simplify_toc(toc_list, exceptions=SPECIAL_TOC):
    """ Removes dictionary settings for TOC that are not part of the special TOC"""
    try:
        new_list = []
        for a_bookmark in toc_list:
            if a_bookmark[1] in exceptions:
                new_list.append(a_bookmark)
            else:
                new_list.append([a_bookmark[0], a_bookmark[1], a_bookmark[2]])
        return new_list
    except Exception as e:
        print()
        print(f'*** simplify_toc: {e}')
        return toc_list


def create_toc(doc, toc_info, header):
    """ Appends :toc_info as a table of content of of a :doc with a title named :header """
    last_annot_toc = []
    a_headers_bookmark_dict = {}

    try:

        toc_f = doc.get_toc(simple=False)

        # Try to locate a 'Last annotations' toc header

        a_bookmark_dict = {
            "main_bookmark": [],
            "Digested": [],
            "Last annotations": [],
            "KEYWORDS": [],
            "STARS": []
        }

        a_bookmark_key = 'main_bookmark'

        for a_toc in toc_f:
            a_dict_caption = a_toc[1]
            if a_dict_caption in a_bookmark_dict:
                a_bookmark_key = a_dict_caption
                atoc_ext = a_toc
                a_headers_bookmark_dict[a_bookmark_key] = atoc_ext
            else:
                a_bookmark_key_list = a_bookmark_dict[a_bookmark_key]
                a_bookmark_key_list.append(a_toc)

        last_annot_toc = a_bookmark_dict['Last annotations']

        main_bookmark = a_bookmark_dict['main_bookmark']

        digest_bookmark = a_bookmark_dict['Digested']
        if digest_bookmark != []:
            digest_bookmark.insert(0, a_headers_bookmark_dict["Digested"])
            main_bookmark.extend(digest_bookmark)

        keywords_bookmark = a_bookmark_dict['KEYWORDS']
        if keywords_bookmark != []:
            keywords_bookmark.insert(0, a_headers_bookmark_dict["KEYWORDS"])
            main_bookmark.extend(keywords_bookmark)

        stars_bookmark = a_bookmark_dict['STARS']
        if stars_bookmark != []:
            stars_bookmark.insert(0, a_headers_bookmark_dict["STARS"])
            main_bookmark.extend(stars_bookmark)

        main_bookmark_simple = [(x[0], x[1], x[2]) for x in main_bookmark]  # TODO: Investigate why including default dict fails
        doc.set_toc(main_bookmark_simple, collapse=1)
        doc.saveIncr()

        if toc_info == []:
            if 'Last annotations' in a_headers_bookmark_dict:
                la_header = a_headers_bookmark_dict["Last annotations"]
            else:
                la_header = (1, 'Last annotations', -1, {'color': (1.0, 0.0, 0.0)})
            last_annot_toc.insert(0, la_header)
            return last_annot_toc
        len_toc = len(main_bookmark)

        # Define the table of contents (TOC)
        # Each entry is a list: [level, title, page, top]

        # Find previous headers
        previous_header_items_list = []
        an_index = 0
        finished = False
        header_level = None
        while (an_index < len_toc) and (not finished):
            an_item = main_bookmark[an_index]
            if an_item[1] == header:
                header_level = an_item[0]
                an_index += 1
                if an_index < len_toc:
                    toc_level = main_bookmark[an_index][0]
                    while (toc_level > header_level) and (an_index < len_toc):
                        previous_header_items_list.append(main_bookmark[an_index])
                        an_index += 1
                        if an_index < len_toc:
                            toc_level = main_bookmark[an_index][0]
                    finished = True
                else:
                    finished = True
            an_index += 1

        clean_toc = [x for x in main_bookmark_simple if x not in previous_header_items_list and x[1] != header]

        if not header_level:
            header_level = 1

        clean_toc.append([header_level, header, toc_info[0][2], {"color": (1, 0, 0)}])
        toc_info_l = [[header_level+1, x[0], x[2]] for x in toc_info]

        previous_header_items_list.extend(toc_info_l)
        previous_header_items_list.sort(key=lambda x: x[2])

        # previous_header_items_list.sort(key=lambda x:x[2])
        clean_toc.extend(previous_header_items_list)

        # Update the document's TOC
        # This method also accepts a second parameter to replace existing TOC (default) or append
        clean_toc = simplify_toc(clean_toc)
        doc.set_toc(clean_toc, collapse=1)
        doc.saveIncr()
        return last_annot_toc
    except Exception as e:
        print()
        print(f'*** create toc: {e} - toc_info: {toc_info}')
        return last_annot_toc


def create_digest_pdf(pysn_dict, highlights_dict, current_sha, marked_dict, source_fn, digested, doc_e=None,
                      embed_summary=EMBED_SUMMARY, show_rec_notes=SHOW_REC_NOTES, comment_author=AUTHOR):
    """ Creates a digest summary, using dictionary info in marked_json
     and create the links to the digested pdf """
    try:
        toc_info = []
        # Get xref of the search layer
        oc_search = xref_layer_by_name(digested, 'pysn_search')
        if oc_search is None:
            oc_search = digested.add_ocg("pysn_search", on=True)
        if doc_e:
            oc_search_e = xref_layer_by_name(doc_e, 'pysn_search')
            if oc_search_e is None:
                oc_search_e = doc_e.add_ocg("pysn_search", on=True)
        # Current directory
        current_dir = os.path.dirname(source_fn)
        # output_fn = f'{source_fn[:-4]}-summary.pdf'

        # Create a new pdf, if needed
        if embed_summary:
            summary_doc = digested
            summary_doc_e = doc_e
            offset_summary = digested.page_count
        else:
            summary_doc = fitz.Document()
            basename_fn = os.path.basename(source_fn)[:-4]
            offset_summary = 0

            digested_pdf_path = f'{basename_fn}_.pdf'
            summary_pdf_path = f'{basename_fn}-summary.pdf'
        page_width = PDF_LETTER_WIDTH
        page_heigth = PDF_LETTER_HEIGHT
        page_digest_count = 0
        rec_notes_space_used = 100

        summary_page_count = 0
        vertical_page_position = SUMMARY_REPORT_MARGIN + 20
        fn_report_caption = source_fn[-25:]

        if marked_dict != {}:
            a_marked_page_nb = list(marked_dict.keys())[0]

        # Parse the json for marked pages

        for a_marked_page_nb, marked_items in marked_dict.items():

            if a_marked_page_nb in pysn_dict:
                page_settings_dict = pysn_dict[a_marked_page_nb]
            else:
                pysn_dict[a_marked_page_nb] = {}
                page_settings_dict = pysn_dict[a_marked_page_nb]

            digested_page = digested[int(a_marked_page_nb)]

            digested_page_rect = digested_page.rect

            page_summary_list = []
            # Parse each marked page for the number of digests on that page
            for page_digest_nb, digest_content in marked_items.items():
                if page_digest_nb != 'highlights' and page_digest_nb != 'annots':

                    std_target_rect = digest_content['pdf_rect']
                    target_rect = digest_content['pdf_rect_t']
                    rotation = (target_rect[0] > target_rect[1]) != (std_target_rect[0] > std_target_rect[1])

                    digest_title = f'...{fn_report_caption} p{int(a_marked_page_nb)+1} - digest #{int(page_digest_nb) + 1}:'
                    crop_fn = os.path.join(current_dir, f'crop_{a_marked_page_nb}_{page_digest_nb}.png')  # Get the filename of the crop
                    digest_text = digest_content['text']  # Get the text extracted in the original pdf

                    if rotation:
                        crop_h, crop_w = digest_content['cropped_size']  # Dimensions of the cropped image
                    else:
                        crop_w, crop_h = digest_content['cropped_size']  # Dimensions of the cropped image

                    # Extract information to display and compute height of element
                    # This is an approximation, because some elements, like the height of recognized text, is not known before printing
                    computed_height = SUMMARY_REPORT_MARGIN + 20  # Initial position
                    handwritings_exist = 'hw_image' in digest_content.keys()

                    handwritings_file_exists = False
                    if handwritings_exist:
                        notes_fn = digest_content['hw_image']
                        notes_ocr = digest_content['hw_text']
                        handwritings_file_exists = os.path.exists(notes_fn)
                        if handwritings_file_exists:
                            if rotation:
                                notes_h_o, notes_w_o = digest_content['hw_size']
                            else:
                                notes_w_o, notes_h_o = digest_content['hw_size']
                            computed_height += notes_h_o

                    crop_file_exists = os.path.exists(crop_fn)

                    computed_height += VERTICAL_REPORT_SPACING  # Spacing after Title

                    if crop_file_exists:
                        computed_height += 8  # Position after 'cropped Image' caption
                        computed_height += crop_h  # Position after cropped image
                        computed_height += 1  # Image frame
                        computed_height += VERTICAL_REPORT_SPACING  # Visual spacing

                    print_recognized_text = show_rec_notes and digest_text != '' and digest_text[:7].strip() != 'digest'
                    if print_recognized_text:
                        computed_height += 1
                        computed_height += 8
                        computed_height += rec_notes_space_used
                        computed_height += VERTICAL_REPORT_SPACING

                    computed_height += 8  # 'Notes' caption
                    computed_height += 100  # space for Notes
                    computed_height += VERTICAL_REPORT_SPACING

                    page_width = max(PDF_LETTER_WIDTH, digested_page_rect[2])
                    page_heigth = max(PDF_LETTER_HEIGHT, digested_page_rect[3])

                    page = summary_doc.new_page(width=page_width, height=page_heigth)
                    if doc_e:
                        page_e = summary_doc_e.new_page(width=page_width, height=page_heigth)

                    # TODO: Quick fix to display recognized text. Revisit this
                    if digest_text != '' and digest_text[:7].strip() != 'digest':
                        standard_rect = fitz.Rect([
                            SUMMARY_REPORT_MARGIN, SUMMARY_REPORT_MARGIN,
                            PDF_LETTER_WIDTH - SUMMARY_REPORT_MARGIN, PDF_LETTER_HEIGHT / 2 - SUMMARY_REPORT_MARGIN])

                        page.insert_textbox(standard_rect, digest_text, fontname='cour', fill_opacity=0.01, oc=oc_search)
                        if doc_e:
                            page_e.insert_textbox(standard_rect, digest_text, fontname='cour', fill_opacity=0.01, oc=oc_search)

                    page_summary_list = []
                    page_digest_count = 0
                    summary_page_count += 1
                    vertical_page_position = SUMMARY_REPORT_MARGIN + 20

                    # Store position of the digest's vertical top position
                    beg_block_v = vertical_page_position-5
                    # Insert digest's title and link of the digest
                    rect_title = fitz.Rect(
                        SUMMARY_REPORT_MARGIN, vertical_page_position - 5,
                        page_width - SUMMARY_REPORT_MARGIN,
                        vertical_page_position + 25)
                    page.insert_textbox(rect_title, digest_title, fontsize=13, color=(0, 0, 1))
                    if doc_e:
                        page_e.insert_textbox(rect_title, digest_title, fontsize=13, color=(0, 0, 1))
                    vertical_page_position += VERTICAL_REPORT_SPACING

                    # if embed_summary, create an internal link. If not, create an external link

                    xd, yd = target_rect[0], target_rect[1]
                    xs, ys = rect_title[0], rect_title[1]
                    # link_already_exists = rect_exists(current_sha,a_marked_page_nb,target_rect,'pdf_rect_t')
                    link_already_exists, intersecting_rect_position = rect_exists(page_settings_dict, target_rect)
                    if embed_summary:
                        if not link_already_exists:
                            link_to_digested = {
                                "kind": fitz.LINK_GOTO,
                                "from": rect_title,
                                "page": int(a_marked_page_nb),
                                "to": fitz.Point(xd, yd)}

                            link_to_summary = {
                                "kind": fitz.LINK_GOTO,
                                "from": fitz.Rect(target_rect),
                                "page": summary_page_count-1+offset_summary,
                                "to": fitz.Point(xs, ys)}
                    else:
                        # I think there is a bug in 'insert_link' method of this
                        # PyMupdf version. This is my dirty workaround
                        use_page_nb = int(a_marked_page_nb)

                        # # # if int(a_marked_page_nb) > 1:
                        # # #     use_page_nb = int(a_marked_page_nb)
                        # # # else:
                        # # #     use_page_nb = 0
                        use_summary_page_count = summary_page_count - 1
                        # # # if summary_page_count > 3:
                        # # #     use_summary_page_count = 3 - 2
                        # # # else:
                        # # #     use_summary_page_count = summary_page_count -1
                        link_to_digested = {
                            "kind": fitz.LINK_GOTOR,
                            "from": rect_title,
                            "page": use_page_nb,
                            "file": digested_pdf_path
                            }
                        link_to_summary = {
                            "kind": fitz.LINK_GOTOR,
                            "from": fitz.Rect(target_rect),
                            "page": use_summary_page_count,
                            "file": summary_pdf_path
                            }
                    if not link_already_exists:

                        page.insert_link(link_to_digested)  # Insert link from summary to digested
                        digested[int(a_marked_page_nb)].insert_link(link_to_summary)  # Insert link from digested to summary

                        if doc_e:
                            page_e.insert_link(link_to_digested)  # Insert link from summary to digested
                            doc_e[int(a_marked_page_nb)].insert_link(link_to_summary)  # Insert link from digested to summary

                        if intersecting_rect_position != -1:
                            an_smarker = page_settings_dict['s_marker']
                            an_smarker[intersecting_rect_position][5] = 1
                            an_smarker[intersecting_rect_position].append(link_to_summary['page'])
                            page_settings_dict['s_marker'] = an_smarker

                    # Insertion of cropped picture
                    if crop_file_exists:
                        # Insert caption image
                        page.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Cropped image:", fontsize=8)
                        vertical_page_position += 8
                        # Insert image
                        page.clean_contents()

                        if doc_e:
                            page_e.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Cropped image:", fontsize=8)
                            page_e.clean_contents()

                        rect_insert = fitz.Rect(
                            SUMMARY_REPORT_MARGIN + 5, vertical_page_position, SUMMARY_REPORT_MARGIN + 5 + crop_w, vertical_page_position+crop_h)

                        page.insert_image(rect_insert, filename=crop_fn, height=80)
                        if doc_e:
                            page_e.insert_image(rect_insert, filename=crop_fn, height=80)

                        # Insert image frame
                        rect_image_frame = fitz.Rect(
                            SUMMARY_REPORT_MARGIN + 4, vertical_page_position-1,
                            page_width - SUMMARY_REPORT_MARGIN - 4, vertical_page_position + crop_h + 1)

                        page.draw_rect(rect_image_frame, stroke_opacity=0.05)
                        if doc_e:
                            page_e.draw_rect(rect_image_frame, stroke_opacity=0.05)

                        vertical_page_position += VERTICAL_REPORT_SPACING + crop_h + 1

                        if embed_summary:
                            link_to_digested = {
                                "kind": fitz.LINK_GOTO,
                                "from": rect_image_frame,
                                "page": int(a_marked_page_nb),
                                "to": fitz.Point(xd, yd)}
                        else:
                            # I think there is a bug in 'insert_link' method of this
                            # PyMupdf version. This is my dirty workaround
                            if int(a_marked_page_nb) > 1:
                                use_page_nb = int(a_marked_page_nb)
                            else:
                                use_page_nb = 0
                            if summary_page_count > 3:
                                use_summary_page_count = 3 - 2
                            else:
                                use_summary_page_count = summary_page_count - 1
                            link_to_digested = {
                                "kind": fitz.LINK_GOTOR,
                                "from": rect_image_frame,
                                "page": use_page_nb,
                                "file": digested_pdf_path
                                }

                        page.insert_link(link_to_digested)
                        if doc_e:
                            page_e.insert_link(link_to_digested)

                    # Inserts text recognized under the "selected areas"
                    if print_recognized_text:
                        # Insertion of recognized text
                        page.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Identified text:", fontsize=8)
                        if doc_e:
                            page_e.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Identified text:", fontsize=8)

                        vertical_page_position += 8
                        sizetb = page.insert_textbox(
                            fitz.Rect(
                                SUMMARY_REPORT_MARGIN + 35,
                                vertical_page_position,
                                page_width - SUMMARY_REPORT_MARGIN,
                                vertical_page_position+rec_notes_space_used), digest_text, fontsize=9, color=(0.254, 0.482, 0.549))
                        if doc_e:
                            page_e.insert_textbox(
                                fitz.Rect(
                                    SUMMARY_REPORT_MARGIN + 35, vertical_page_position,
                                    page_width - SUMMARY_REPORT_MARGIN,
                                    vertical_page_position+rec_notes_space_used), digest_text, fontsize=9, color=(0.254, 0.482, 0.549))

                        rec_notes_space_used -= int(sizetb)
                        vertical_page_position += VERTICAL_REPORT_SPACING + rec_notes_space_used
                    else:
                        rec_notes_space_used = 0

                    # Insert handwritings section
                    page.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Notes:", fontsize=8)
                    if doc_e:
                        page_e.insert_text(fitz.Point(SUMMARY_REPORT_MARGIN + 5, vertical_page_position), "Notes:", fontsize=8)

                    vertical_page_position += 8
                    hw_notes_position = vertical_page_position

                    # Adds lines in the digest summary notes to guide handwritings
                    if DIGEST_LINE_HEIGHT > 0:
                        if NOTEBOOK_DEVICE in ['N5']:
                            hw_ratio_size = 1.0
                        else:
                            hw_ratio_size = 1.0

                        adjusted_line_height = DIGEST_LINE_HEIGHT*hw_ratio_size
                        line_y = vertical_page_position
                        x_start = SUMMARY_REPORT_MARGIN + 5
                        x_end = page_width-SUMMARY_REPORT_MARGIN - 5
                        while line_y < page_heigth-SUMMARY_REPORT_MARGIN - 8:
                            p1 = fitz.Point(x_start, line_y)
                            p2 = fitz.Point(x_end, line_y)
                            page.draw_line(p1, p2, color=(0.254, 0.482, 0.549), stroke_opacity=0.4)
                            if doc_e:
                                page_e.draw_line(p1, p2, color=(0.254, 0.482, 0.549), stroke_opacity=0.4)
                            line_y += adjusted_line_height

                    if handwritings_file_exists:
                        notes_w = int(NOTES_SHRINK_RATIO*notes_w_o*hw_ratio_size)
                        notes_h = int(NOTES_SHRINK_RATIO*notes_h_o*hw_ratio_size)
                        page.clean_contents()

                        rect_insert = fitz.Rect(
                            SUMMARY_REPORT_MARGIN + 5, vertical_page_position,
                            SUMMARY_REPORT_MARGIN + 5 + notes_w, vertical_page_position + notes_h)
                        sizetb = page.insert_image(
                            rect_insert, filename=notes_fn, keep_proportion=False)
                        if doc_e:
                            page_e.clean_contents()
                            page_e.insert_image(rect_insert, filename=notes_fn, keep_proportion=False)

                        page.insert_textbox(rect_insert, notes_ocr, fontname='cour', fill_opacity=0.01, oc=oc_search)
                        if doc_e:
                            page_e.insert_textbox(rect_insert, notes_ocr, fontname='cour', fill_opacity=0.01, oc=oc_search_e)

                        vertical_page_position += notes_h

                    vertical_page_position += VERTICAL_REPORT_SPACING + 100

                    # Insert digest's overall frame
                    page.draw_rect(
                        fitz.Rect(
                            SUMMARY_REPORT_MARGIN - 5, beg_block_v - 10, page_width - SUMMARY_REPORT_MARGIN + 5,
                            page_heigth - SUMMARY_REPORT_MARGIN), color=(0.254, 0.482, 0.549), stroke_opacity=0.35)
                    if doc_e:
                        page_e.draw_rect(
                            fitz.Rect(
                                SUMMARY_REPORT_MARGIN-5, beg_block_v-10, page_width-SUMMARY_REPORT_MARGIN+5,
                                page_heigth-SUMMARY_REPORT_MARGIN), color=(0.254, 0.482, 0.549), stroke_opacity=0.35)
                    # Saving position of digest handwritings
                    page_summary_list.append(
                        (
                            SUMMARY_REPORT_MARGIN-5, hw_notes_position, page_width - SUMMARY_REPORT_MARGIN + 5,
                            vertical_page_position))

                    vertical_page_position += VERTICAL_REPORT_SPACING

                    # Generate bookmark for the TOC offset_summary
                    a_bookmark = (f' •  P{summary_page_count} -  digest {int(a_marked_page_nb)+1}-{int(page_digest_nb)+1}', 'Title 1', summary_page_count + offset_summary)
                    toc_info.append(a_bookmark)
                    # Increment nb of digests & pages
                    page_digest_count += 1

                if 'summary_hw_zone' in page_settings_dict:
                    previous_summary_hw_zone = page_settings_dict['summary_hw_zone']
                else:
                    previous_summary_hw_zone = []

                previous_summary_hw_zone.extend(page_summary_list)  # saving for metadata
                previous_summary_hw_zone = set(tuple(item) for item in previous_summary_hw_zone)
                previous_summary_hw_zone = [list(item) for item in previous_summary_hw_zone]
                page_settings_dict['summary_hw_zone'] = previous_summary_hw_zone
                pysn_dict[a_marked_page_nb] = page_settings_dict

        # Now handling the highlights from the SN
        # Get page and corresponding highlight dictionary (ah_page_key is zero start based)
        for ah_page_key, highlights_info in highlights_dict.items():
            # Parse each of the highlights on that page
            for an_item_hl in highlights_info:
                # Get the pdf coordinates of the current highlight
                highlights_rect = an_item_hl['mupdfRectList']
                highlights_rect_list = [[item['x0'], item['y0'], item['x1'], item['y1']] for item in highlights_rect]  # Put the coordinates in a list of rects format
                highlights_rect_list_m = merge_rects(list(highlights_rect_list))  # Merge adjacent rects together
                highlights_rect_merged = merge_rects(list(highlights_rect_list))[0]

                highlights_rect_list_ = [fitz.Rect((x[0], x[1], x[2], x[3])) for x in highlights_rect_list_m]

                annotation_type = an_item_hl['annotationType']
                creation_date = timestamp_to_pdf_datetime(an_item_hl['time'])

                if 'annotationContent' in an_item_hl:
                    highlights_text = an_item_hl['annotationContent']

                    annot_icon = digested[int(ah_page_key)].add_caret_annot(fitz.Point([highlights_rect_merged[0]-20, highlights_rect_merged[1]]))
                    annot_icon.set_opacity(0.6)
                    if doc_e:
                        annot_icon_e = doc_e[int(ah_page_key)].add_caret_annot(fitz.Point([highlights_rect_merged[0]-20, highlights_rect_merged[1]]))
                        annot_icon_e.set_opacity(0.6)
                else:
                    highlights_text = ''

                # The below are highlights stored in the .mark file. We express them out to the pdf only for exporting docs
                if doc_e:
                    annot_e = None
                    # Type 1 highlights for the SN are "regular" highlights
                    if annotation_type == 0 or annotation_type == 2:
                        annot_e = doc_e[int(ah_page_key)].add_highlight_annot(highlights_rect_list_)
                        annot_e.set_opacity(0.3)

                    elif annotation_type == 1:
                        # The SN '1' type is of type underline. Here we prefer not to merge the rects
                        highlights_rect_list_ul = [fitz.Rect((x[0], x[1], x[2], x[3])) for x in highlights_rect_list]
                        annot_e = doc_e[int(ah_page_key)].add_underline_annot(highlights_rect_list_ul)
                        # PyMupdf highlights are ugly in supernote, we dim by adjusting down the opacity
                        annot_e.set_opacity(0.3)
                    hl_dict = {
                        "name": "ForComment",
                        "content": highlights_text,
                        "modDate": creation_date,
                        "title": comment_author}
                    if annot_e:
                        annot_e.set_info(info=hl_dict)

        # Close the summary

        # # if not embed_summary: TODO: This is for splitting the export between pure digest
        # #     create_toc(summary_doc, toc_info, 'Digest summary')
        # #     summary_doc.save(output_fn,garbage=4, deflate=True, clean=True)
        # #     summary_doc.close()
        # #     basename_fn = os.path.basename(pdf_path)[:-4]
        # #     source_digest_fn = f'{pdf_path[:-4]}-summary.pdf'
        # #     dest_digest_fn = os.path.join(EXPORT_FOLDER,f'{basename_fn}-summary.pdf')
        # #     shutil.copy(source_digest_fn, dest_digest_fn)
    except Exception as e:
        print(f'*** create_digest_pdf: {e}')


def find_handwriting_proximity(
    rect_dict, rects_notes, page, current_dir, imgi, inv_pr_matrix,
        proximity_threshold=PROXIMITY_THRESHOLD, device=NOTEBOOK_DEVICE):
    """
    For selection rect in rect_dict, find handwritings rects that are in proximity then
    update the dict with a 'proximity' key/value
    """
    img_rect_used = []
    mark_counter = page.number

    imgi_array = np.array(imgi)
    for rect_nb in rect_dict.keys():
        # Retrieve the pix_rect
        rect_object = rect_dict[rect_nb]
        rect = fitz.Rect(rect_object['raw_rect'])
        index_note = 0
        list_images = []
        list_rects = []
        for handwriting in rects_notes:

            if calculate_min_distance(rect, handwriting) <= proximity_threshold:
                if handwriting not in img_rect_used:
                    img_rect_used.append(handwriting)
                    list_rects.append(handwriting)
                    if 'proximity' in rect_object:
                        rect_object['proximity'].append(handwriting)
                    else:
                        rect_object['proximity'] = [handwriting]
                    cropped_img_filename = os.path.join(current_dir, f'p_note_{mark_counter}_{rect_nb}_{index_note}_.png')
                    cropped_img = imgi.crop(handwriting)
                    data = np.array(cropped_img)
                    # Extract RGB components and Alpha channel
                    r, g, b, a = data.T
                    # Set the alpha value of selection pixels to 0 (fully transparent) and remove transparency of notes colors
                    target_color = HIGHLIGHTS_COLOR[DIGEST_COLOR]
                    notes_color = HIGHLIGHTS_COLOR[NOTES_COLOR]
                    selection_pixels = (r == target_color[0]) & (g == target_color[1]) & (b == target_color[2])
                    notes_pixels = (r == notes_color[0]) & (g == notes_color[1]) & (b == notes_color[2])
                    # Clear the pixels (Set them to be fully transparent)
                    data[..., 3][selection_pixels.T] = 0
                    data[..., 3][notes_pixels.T] = 255
                    img_no_transparency = Image.fromarray(data, 'RGBA')
                    img_no_transparency.save(cropped_img_filename)
                    list_images.append(img_no_transparency)
                    # Also remove the notes from imgi
                    # Convert to NumPy array for fast manipulation

                    x1, y1, x2, y2 = handwriting
                    # Make the specified rectangle area transparent
                    # by setting the alpha channel to 0 in that area
                    imgi_array[y1:y2, x1:x2, 3] = 0
            index_note += 1
        # Now merge the handwritten notes, for each digest
        if list_images != []:
            handwritten_image_fn = os.path.join(current_dir, f'hand_notes_{mark_counter}_{rect_nb}.png')
            handwritten_image, hw_size = assemble_puzzle(list_images, list_rects, inv_pr_matrix)
            handwritten_image.save(handwritten_image_fn)
            rect_object['hw_image'] = handwritten_image_fn
            rect_object['hw_size'] = hw_size
            hw_text, crop_ocr_list, _ = get_text_from_image(handwritten_image_fn, inv_pr_matrix, device=device)
            rect_object['hw_text'] = hw_text

        rect_dict[rect_nb] = rect_object
        lrect = [int(x) for x in rect]
        x1, y1, x2, y2 = lrect
        imgi_array[y1:y2, x1:x2, 3] = 0
    # Convert back to an Image object
    imgi = Image.fromarray(imgi_array)
    return rect_dict, imgi


def merge_rects(rects, proximity=PROXIMITY_RECT_MERGE):
    """ Regroup together rectangles that are intersecting, within
        the distance of a certain proximity"""
    def are_close(rect1, rect2):
        # Expanding rect1's boundaries by 'proximity' pixels
        expanded_rect1 = (
            rect1[0] - proximity, rect1[1] - proximity, rect1[2] + proximity, rect1[3] + proximity
        )
        # Checking if rect2 intersects with expanded rect1
        return not (
            expanded_rect1[2] < rect2[0] or  # rect1 is left of rect2
            expanded_rect1[3] < rect2[1] or  # rect1 is above rect2
            expanded_rect1[0] > rect2[2] or  # rect1 is right of rect2
            expanded_rect1[1] > rect2[3]     # rect1 is below rect2
        )

    merged = True
    while merged:
        merged = False
        new_rects = []
        while rects:
            current = rects.pop()
            merged_any = False
            for idx, other in enumerate(rects):
                if are_close(current, other):
                    # Merging the rects by taking the min x,y and max x,y
                    merged_rect = (
                        min(current[0], other[0]),
                        min(current[1], other[1]),
                        max(current[2], other[2]),
                        max(current[3], other[3])
                    )
                    rects[idx] = merged_rect
                    merged_any = True
                    merged = True
                    break
            if not merged_any:
                new_rects.append(current)
        rects = new_rects

    return list(rects)


def find_rects_with_color(pysn_dict, mark_counter, total_marked_nb, doc, page,  pdf_path, converter, page_bitmap, mark_file_binaries, annotated_is_pdf=True, doc_e=None, page_e=None,
                          selection_rgb=HIGHLIGHTS_COLOR[DIGEST_COLOR],
                          highlight_rgb=HIGHLIGHTS_COLOR[HIGHLIGHT_COLOR],
                          erase_rgb=HIGHLIGHTS_COLOR[ERASE_COLOR], transparency_level=TRANSPARENCY_LVL, other_transparency=TRANSPARENCY_OTHER, palette=None,
                          series=NOTEBOOK_DEVICE):
    """ This is the core function, called for each marked page.

    It:
        -   Identifies the highlight colors rects and merges them into selection rects
            The important thing to remember is that selection rects come with different coordinates,
            depending what reference is being used: a picture from the device or the pdf
        -   Crops images of the oginal pdf
        -   Extracts text under the selection rects in the original pdf (if applicable)
        -   Identifies handrwritten text inside or near the selection rects

        mark_counter: The zero based page counter of the marked file
        total_marked_nb: The total number of marked pages
        page: the page of the pdf filename that is marked
        pdf_path: the current pdf filename
        converter: the converter used to export pictures from .mark files

        selection_rgb: the specific color used to identify selections

        """
    max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(series)
    pages_marked_for_deletion = []
    page_rect = page.rect
    current_dir = os.path.dirname(pdf_path)
    # Define filename of current page image of the marked notes, including background
    image_path = pdf_path+'_p' + str(mark_counter).zfill(total_marked_nb) + '.png'
    # Extract corresponding marked image from .mark file

    img = converter.convert(mark_counter, vo)

    # Get xref of the search layer

    oc_search = xref_layer_by_name(doc, 'pysn_search')

    if annotated_is_pdf:
        if oc_search is None:
            oc_search = doc.add_ocg("pysn_search", on=True)

        if doc_e:
            oc_search_e = xref_layer_by_name(doc_e, 'pysn_search')
            if oc_search_e is None:
                oc_search_e = doc_e.add_ocg("pysn_search", on=True)

    # Define filename of current page image of the marked notes, NOT including background
    #  (the 'i' at the end is for invisible background)
    image_path_i = pdf_path+'_ip' + str(mark_counter).zfill(total_marked_nb) + '.png'

    imgi = converter.convert(mark_counter, vi)

    p_nb_k = str(page.number)

    # Check if the user wants to remove a previously created 'digest'
    if p_nb_k in pysn_dict:
        # Get the page settings
        p_nb_k_settings = pysn_dict[p_nb_k]
        # Parse the settings for that page
        for a_marker in p_nb_k_settings['s_marker']:
            # Get location (rect) of the icon signaling a 'digest'
            highlight_icon_rect = a_marker[0]
            highlight_icon_rect_pdf = a_marker[1]
            red_rectangle_rect = a_marker[2]

            # Determine if the icon was 'marked' to be erased by the user
            if check_color_threshold(imgi, highlight_icon_rect, HIGHLIGHTS_COLOR[ERASE_COLOR]):
                # First remove the caret icon
                page_annots = page.annots(types=[fitz.PDF_ANNOT_CARET])
                for annot in page_annots:
                    if annot.rect.intersects(highlight_icon_rect_pdf):
                        page.delete_annot(annot)
                # Then remove the red rectangle
                page_annots = page.annots(types=[fitz.PDF_ANNOT_SQUARE])
                for annot in page_annots:
                    if annot.rect.intersects(red_rectangle_rect):
                        page.delete_annot(annot)
                # Then remove the associated link
                page_links = page.get_links()
                internal_links = [x for x in page_links if x['kind'] == 1]
                for a_link in internal_links:
                    a_link_rect = a_link['from']
                    # if a_link_rect.intersects(red_rectangle_rect):

                    if intersection_area(a_link_rect, red_rectangle_rect, threshold=0.8):
                        linked_page_summary = a_link['page']
                        page.delete_link(a_link)
                        pages_marked_for_deletion.append((page.number, linked_page_summary))

    if DEBUG_MODE or USE_MS_VISION:
        img.save(image_path)
        imgi.save(image_path_i)

    # Get the pixmap of the current page
    matrix_pix = fitz.Matrix(RESOLUTION_FACTOR, RESOLUTION_FACTOR)
    pagepix = page.get_pixmap(alpha=True, matrix=matrix_pix)
    if page_e:
        pagepix_e = page_e.get_pixmap(alpha=True, matrix=matrix_pix)

    matrx = page.rect.torect(pagepix.irect)
    pr_matrix = page.rotation_matrix * matrx
    inv_pr_matrix = ~pr_matrix

    if USE_MS_VISION:
        page_hw_text, page_hw_map, _ = get_text_from_image(image_path, inv_pr_matrix, recognize=USE_MS_VISION, device=series)
        a_text = page_hw_text.strip()
        if a_text != '':
            try:
                ov_text, ov_rect = concatenate_text_and_get_rect(page_hw_map)

                # TODO: the code was like this: page.insert_textbox(fitz.Rect(ov_rect), a_text,fontname='cour', fill_opacity=0.8, oc=oc_search)
                # But the position is wrong. Passibly because ratio needs to be taken into account
                # coordinates page_hw_map were computed using inversion matrix
                # For now, just swithcing to standard rect because we need only page reference, not exact position
                standard_rect = fitz.Rect(
                    [SUMMARY_REPORT_MARGIN, SUMMARY_REPORT_MARGIN + PDF_LETTER_HEIGHT / 2,
                     PDF_LETTER_WIDTH - SUMMARY_REPORT_MARGIN, PDF_LETTER_HEIGHT-SUMMARY_REPORT_MARGIN])

                page.insert_textbox(standard_rect, a_text, fontname='cour', fill_opacity=0.01, oc=oc_search)

                if doc_e:
                    page_e.insert_textbox(standard_rect, a_text, fontname='cour', fill_opacity=0.01, oc=oc_search_e)
            except Exception as e:
                print()
                print(page_hw_map)
                ov_text, ov_rect = concatenate_text_and_get_rect(page_hw_map)
                print(f'------ov_text: {ov_text}')
                print(f'------ov_rect: {ov_rect}')
                print(f'**** Error OCR: {e} -- Rect:({standard_rect})----arec_ocr_txt:{a_text}')

    # Store pixmap content as an Image object
    pdfpixmap = Image.open(BytesIO(pagepix.tobytes(output='png')))

    # Store the page marked image with background in a numpy array
    pixels = np.array(img)

    # Open the no-background image and convert it to RGBA if it's not already
    if imgi.mode != 'RGBA':
        imgi = imgi.convert('RGBA')
    # Store its pixels in a numpy array
    pixelsi = np.array(imgi)

    # Convert RGB values of to numpy arrays for easier comparison
    selection_rgb = np.array(selection_rgb)
    highlight_rgb = np.array(highlight_rgb)
    erase_rgb = np.array(erase_rgb)

    # Find connected components of the selection color
    selection_mask = (pixels == selection_rgb).all(axis=-1)
    labeled_array, num_features = ndimage.label(selection_mask)
    rects = ndimage.find_objects(labeled_array)

    # Find connected components of the erase color
    # This will get colors to erase ON THE PDF (as opposed to the mark)
    selection_mask_erase = (pixels == erase_rgb).all(axis=-1)
    labeled_array_erase, num_features = ndimage.label(selection_mask_erase)
    rects_erase = ndimage.find_objects(labeled_array_erase)
    # Convert rects to Python native types for JSON compatibility
    native_erasing_rects = [(slice_x.start, slice_y.start, slice_x.stop - 1, slice_y.stop - 1) for slice_y, slice_x in rects_erase]
    # Sort and merge erasing_rects
    pdf_rects_to_erase = merge_rects(native_erasing_rects)

    # Apply transparency based on color conditions using NumPy
    condition = (pixelsi[:, :, :3] == highlight_rgb).all(axis=-1) | (pixelsi[:, :, :3] == erase_rgb).all(axis=-1)
    pixelsi[condition, 3] = (pixelsi[condition, 3] * (1 - other_transparency)).astype(int)

    # Apply additional transparency to pixels within the identified rectangles
    for slice_y, slice_x in rects:
        pixelsi[slice_y, slice_x, 3] = (pixelsi[slice_y, slice_x, 3] * (1 - transparency_level)).astype(int)

    # Erase the slices of white (erasing color)
    for slice_y, slice_x in rects_erase:
        pixelsi[slice_y, slice_x, 3] = 0

    # Save the modified image
    imgi = Image.fromarray(pixelsi)
    # imgi.save(image_path_i)

    # Convert rects to Python native types for JSON compatibility
    native_rects = [(slice_x.start, slice_y.start, slice_x.stop - 1, slice_y.stop - 1) for slice_y, slice_x in rects]

    # Sort and merge rects
    rects = merge_rects(native_rects)
    sorted_rects = sorted(rects, key=lambda rect: (rect[1], rect[0]))

    # Compare sorted_rects with previous ones stored in the metadata
    # This was a concept I had before figuring out how to edit the trail container strokes,
    # and avoid re-creating existing links (since there was no way to remove the selection strokes)
    # Leaving it because it does prevent the user to recreate an existing link
    # The dictionary of pysn 'objects' is kept in the trail metadata of the pdf

    # Store the page_key
    page_key = str(page.number)

    if page_key in pysn_dict:
        # There are pysn objects (settings) for that pdf page, retrieve them
        page_settings = pysn_dict[page_key]
        if 'selection' in page_settings:
            # There are 'selection' items (links were previously created), retrieve them
            previous_selection_list = page_settings['selection']

            # Now compare current selection with the previous
            common_items, sorted_rects_specific, previous_selection_specific, union_list = compare_lists(sorted_rects, previous_selection_list)
            # now save the selection
            page_settings['selection'] = union_list
            # Remove common items for further processing (we already have digest summary zone for these)
            sorted_rects = list(sorted_rects_specific)
        else:
            page_settings['selection'] = sorted_rects
    else:
        pysn_dict[page_key] = {"selection": sorted_rects}
        page_settings = pysn_dict[page_key]

    # Keep original rects before snapping
    un_snapped_rects = list(sorted_rects)

    # Storing a mark picture without selection for digestion and deletion
    imgi_to_alter = erase_pixels(imgi, color_to_erase=HIGHLIGHTS_COLOR[DIGEST_COLOR])
    imgi_to_alter = erase_pixels(imgi_to_alter, color_to_erase=HIGHLIGHTS_COLOR[ERASE_COLOR])
    # RLE encode the modified picture
    imgi_to_alter = imgi_to_alter.convert('L')
    replacement_bytes, encoded = rle_encode_img(imgi_to_alter)

    if encoded:
        # Replace the ephemeral static image in the .mark file
        # ('ephemeral', because strokes stored in trailcontainer will overwrite it in future rendering of the page)
        replaced, mark_file_binaries = replace_hex_bytes_in_file_block(mark_file_binaries, replacement_bytes, page_bitmap)
        if not replaced and DEBUG_MODE:
            print()
            print(f'*** could not alter static image for page {page.number}')

    # The img variable stores marks with a background
    # We paste these bits of backgrounds over the "erased" areas in imgi,
    # our real ultimate picture with notes and 'clear' background
    # TODO: Rethink the logic here
    copy_rects(img, imgi, pdf_rects_to_erase)

    # **** Now expand each rect to fit the text in the original pdf and store them in a list
    expanded_rects_list = []
    cropped_rects = []

    x_ratio = pdfpixmap.width/imgi.width
    y_ratio = pdfpixmap.height/imgi.height

    # Initialize the rect counter
    rectno = 0

    # Parse the list of rects and 'snap' them to a box of content in the pdf page

    for a_sorted_rec in sorted_rects:

        # Convert the marked image reference to the pdf page coordinates by 'resizing them'
        # * Do not change this: operations at pixel level
        resized_rect_p = (int(a_sorted_rec[0]*x_ratio), int(a_sorted_rec[1]*y_ratio), int(a_sorted_rec[2]*x_ratio), int(a_sorted_rec[3]*y_ratio))

        # Snapping
        an_expanded_rect, pdfpixmap = snap_to_text(pdfpixmap, resized_rect_p)

        # Save the cropped image as a png
        if an_expanded_rect != []:
            if abs(int(an_expanded_rect[0])-int(an_expanded_rect[2])) > 1 and abs(int(an_expanded_rect[1])-int(an_expanded_rect[3])) > 1:
                im_rect = pdfpixmap.crop(an_expanded_rect)
                rect_file_name = os.path.join(current_dir, f'crop_{page.number}_{rectno}.png')
                im_rect.save(rect_file_name)

                # Find the relative coordinates in the reporting
                rel_coordinates = fitz.Rect(an_expanded_rect)*inv_pr_matrix
                cropped_rect_size = (rel_coordinates.width, rel_coordinates.height)
            else:

                im_rect = None
                cropped_rect_size = ()

        # Conversion from Numpy.int64 to regular int for serialization and storage to json purpose
        a_e_r_l = [int(x) for x in an_expanded_rect]
        expanded_rects_list.append(a_e_r_l)
        cropped_rects.append(cropped_rect_size)
        rectno += 1

    if cropped_rects and expanded_rects_list:

        # Pair each element of `cropped_rects` with its corresponding element in `expanded_rects_list`
        paired_rects = list(zip(expanded_rects_list, cropped_rects))

        # Sort the paired list based on the sorting criteria for `expanded_rects_list`
        # This is the sorting of the rects based on thier natural order top left to right down
        # obsolete because need to keep cropped width correspondance: sorted_rects = sorted(expanded_rects_list, key=lambda rect: (rect[1], rect[0]))
        sorted_paired_rects = sorted(paired_rects, key=lambda x: (x[0][1], x[0][0]))

        # Unpack the sorted pairs back into two lists
        sorted_expanded_rects_list, sorted_cropped_rects = zip(*sorted_paired_rects)

        # We need the results as lists (since `zip` returns tuples)
        sorted_rects = list(sorted_expanded_rects_list)
        sorted_cropped_rects = list(sorted_cropped_rects)
    else:
        # Handle the empty list case
        sorted_rects, sorted_cropped_rects = [], []

    # At this point, let's load the rects in a dict

    # Initialize and populate a dict with the pix and corresponding pdf rects reference
    rect_dict = {}
    for rect_index in range(len(sorted_rects)):
        x = sorted_rects[rect_index]
        un_snapped_x = un_snapped_rects[rect_index]  # Unsnapped may still be useful
        theorical_pdf_rect = fitz.Rect(x)*inv_pr_matrix
        pdf_rect_t = [theorical_pdf_rect[0], theorical_pdf_rect[1], theorical_pdf_rect[2], theorical_pdf_rect[3]]
        rect_dict[str(rect_index)] = {
            "raw_rect": un_snapped_x,
            "pix_rect": x,
            "pdf_rect": [
                x[0]*page_rect[2]/pagepix.w+EXPAND_SHRINK_PXL-3, x[1]*page_rect[3]/pagepix.h+EXPAND_SHRINK_PXL-3,
                x[2]*page_rect[2]/pagepix.w-EXPAND_SHRINK_PXL+3, x[3]*page_rect[3]/pagepix.h-EXPAND_SHRINK_PXL+3],
            "pdf_rect_t": pdf_rect_t,
            "cropped_size": sorted_cropped_rects[rect_index]}

    # Now find texts and fonts  under the rectangles in the original pdf (if not a scanned doc without ocr)
    # The function updates the dict with the identified text and fonts
    rect_dict = page_text_under_rects(current_dir, page, rect_dict, inv_pr_matrix, device=series)

    # Let's now find all the rects with handwritten notes, based on their color (black by default)

    rects_notes = find_notes_image(img)

    # Now compare these rect notes with each of the selection rects. Keep only those close to the selection rect
    # The function saves notes inside and close to the selection rect as separate png files
    # It then removes those specifics notes from the mark image and returns a "clean" mark picture
    rect_dict, imgi = find_handwriting_proximity(
        rect_dict, rects_notes, page, current_dir, imgi, inv_pr_matrix,
        proximity_threshold=PROXIMITY_THRESHOLD, device=series)

    # Now we merge the imgi picture with the pdf picture
    # For SUPER_HACK_MODE, we need to remove handwritings
    # (because the user keeps editing them through the .mark file)

    # Automatically save an image for export
    if page_e:
        imgi_e = imgi
        if palette is not None:
            pcolors = [palette.black, palette.darkgray, palette.gray, palette.white]
            hex_pcolors = [decimal_to_rgba_hex(x) for x in pcolors]
            a_color_palette = [f'{x}ff'.upper() for x in COLOR_PALETTE]

            for name, hex_color, a_color in zip(COLOR_PALETTE_NAMES, hex_pcolors, a_color_palette):
                # Check if hex_color is different from a_color
                if hex_color != a_color and name not in [DIGEST_COLOR, ERASE_COLOR]:  # TODO: redo with highlight color
                    # replace color
                    imgi_e = replace_color(imgi_e, a_color[:-2], hex_color[:-2])

    if SUPER_HACK_MODE:
        imgi_array = np.array(imgi)
        notes_color = HIGHLIGHTS_COLOR[NOTES_COLOR]
        highlight_color = HIGHLIGHTS_COLOR[HIGHLIGHT_COLOR]  # TODO: redo code below. Quick fix using highlight color to allow more than one color change

        r, g, b, a = imgi_array.T
        notes_pixels = ((r == notes_color[0]) & (g == notes_color[1]) & (b == notes_color[2])) | (
            (r == highlight_color[0]) & (g == highlight_color[1]) & (b == highlight_color[2]))
        imgi_array[..., 3][notes_pixels.T] = 0  # Setting transparency to zero removes the handwritten notes
        imgi = Image.fromarray(imgi_array, 'RGBA')

    imgi_resized = imgi.resize((pagepix.w, pagepix.h), Image.Resampling.LANCZOS)

    # result = Image.alpha_composite(pdfpixmap, imgi_resized.convert("RGBA"))
    result = imgi_resized.convert("RGBA")

    if page_e:
        imgi_resized_e = imgi_e.resize((pagepix.w, pagepix.h), Image.Resampling.LANCZOS)
        result_e = imgi_resized_e.convert("RGBA")
        destination_rect_e = page_e.rect

    # Store the page rect before possible rotation
    destination_rect = page.rect

    if pagepix.w > pagepix.h:
        result = result.rotate(-90, expand=True)

        # Change destination rect
        destination_rect = fitz.Rect((0, 0, destination_rect.y1, destination_rect.x1))

        if page_e:
            result_e = result_e.rotate(-90, expand=True)
            destination_rect_e = fitz.Rect((0, 0, destination_rect_e.y1, destination_rect_e.x1))

    bytes_io = io.BytesIO()

    result.save(bytes_io, format='PNG')

    if page_e:
        bytes_io_e = io.BytesIO()
        result_e.save(bytes_io_e, format='PNG')
        page_e.clean_contents()
        page_e.insert_image(rect=destination_rect_e, stream=bytes_io_e)
        pagepix_e.set_rect(fitz.Rect(0, 0, 50, 50), (255, 255, 0, 1))

    page.clean_contents()
    page.insert_image(rect=destination_rect, stream=bytes_io)

    pagepix.set_rect(fitz.Rect(0, 0, 50, 50), (255, 255, 0, 1))
    annots_list = []
    # List of carets holders
    s_marker = []

    # Adding a red rectangle on pdf to signal digest (and link)
    apmap = page.get_pixmap(alpha=True, matrix=matrix_pix)

    ratio_matrix = fitz.Matrix(a=max_horizontal_pixels/apmap.w, d=max_vertical_pixels/apmap.h)

    an_index = 0
    for a_rect_ in expanded_rects_list:
        a_rect_f = fitz.Rect(a_rect_)
        arectp = a_rect_f*inv_pr_matrix

        ahnotr = page.add_rect_annot(arectp)
        annots_list.append(ahnotr.info['id'])
        ahnotr.set_info(
            {"modDate": time_now_to_pdf_datetime(), "title": AUTHOR})
        ahnotr.update(border_color=(1, 0, 0), opacity=0.15, fill_color=(1, 0, 0))

        ahnot_frame = page.add_rect_annot(arectp)
        ahnot_frame.set_border(border={"width": 1, "dashes": [], "style": "S"})
        ahnot_frame.update(border_color=(1, 0, 0), opacity=0.55)

        r_ = [a_rect_f[0]-50, a_rect_f[1]-15+a_rect_f.height/2, a_rect_f[0], a_rect_f[1]+a_rect_f.height/2+15]
        r = fitz.Rect(r_)*inv_pr_matrix
        a_point_c = [r[0], r[1]]
        # Add a caret to signal notes/link
        ahnot = page.add_caret_annot(fitz.Point(a_point_c))

        # Storing insertion point of the caret pix coordinates, caret pdf coordinates, rectangle pdf coordinates, index of link on page
        s_marker.append([tuple(ahnot.rect*pr_matrix*ratio_matrix),
                         tuple(ahnot.rect), tuple(ahnotr.rect), an_index, ahnotr.info['id'], 0])
        an_index += 1
        ahnot.set_opacity(0.6)
        annots_list.append(ahnot.info['id'])

        if page_e:
            ahnotr_e = page_e.add_rect_annot(arectp)
            ahnotr_e.set_info(
                {"modDate": time_now_to_pdf_datetime(), "title": AUTHOR})
            ahnotr_e.update(border_color=(1, 0, 0), opacity=0.15, fill_color=(1, 0, 0))
            ahnot_frame_e = page_e.add_rect_annot(arectp)
            ahnot_frame_e.set_border(border={"width": 1, "dashes": [], "style": "S"})
            ahnot_frame_e.update(border_color=(1, 0, 0), opacity=0.55)
            # Add a caret to signal notes/link
            ahnot_e = page_e.add_caret_annot(fitz.Point(a_point_c))
            ahnot_e.set_opacity(0.6)

    rect_dict['annots'] = annots_list

    # Updates pdf pysn metadata to reflect s_marker
    if 's_marker' in page_settings:
        previous_s_marker = page_settings['s_marker']
    else:
        previous_s_marker = []
    previous_s_marker.extend(s_marker)

    page_settings['s_marker'] = previous_s_marker
    return rect_dict, mark_file_binaries, pages_marked_for_deletion


def find_notes_image(img, selection_rgb=HIGHLIGHTS_COLOR[NOTES_COLOR]):

    """
    Same principle as above, this time identifying the rects of notes

    """
    pixels = img.load()
    width, height = img.size
    visited = set()
    rects = []

    def dfs_iterative(start_x, start_y):
        stack = [(start_x, start_y)]
        xmin, ymin, xmax, ymax = float('inf'), float('inf'), -float('inf'), -float('inf')
        while stack:
            x, y = stack.pop()
            if (x, y) in visited or not (0 <= x < width and 0 <= y < height):
                continue
            visited.add((x, y))
            if pixels[x, y] != selection_rgb:
                continue
            xmin, ymin = min(xmin, x), min(ymin, y)
            xmax, ymax = max(xmax, x), max(ymax, y)
            for dx, dy in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
                nx, ny = x + dx, y + dy
                if (nx, ny) not in visited:
                    stack.append((nx, ny))
        return xmin, ymin, xmax, ymax

    for x in range(width):
        for y in range(height):
            if (x, y) not in visited and pixels[x, y] == selection_rgb:
                rect = dfs_iterative(x, y)
                if rect[2] - rect[0] >= 0 and rect[3] - rect[1] >= 0:
                    rects.append(rect)

    rects = merge_rects(rects, proximity=PROXIMITY_NOTES)
    rect_ser = [list(x) for x in rects]
    return rect_ser


def detect_modified_files(notes_list):
    """ Returns the list of files in the watched folder that are
        different from the last properties stored in the registry """
    modified = []

    # Read registry value
    registry = read_json(os.path.join(SN_VAULT, SN_REGISTRY_FN))

    files_in_registry_list = [item[0] for item in registry.items() if item[1]['type'] == 'file']
    removed_list = [x for x in files_in_registry_list if x not in notes_list]
    # Store the current datetime
    current_datetime = datetime.now(timezone.utc).strftime('%Y-%m-%d %H:%M:%S UTC')
    # For each filename, compute its sha and compare it to the one in the registry

    for a_fn in notes_list:
        a_fn_modification = get_last_modified_date(a_fn)
        # modification
        modification = False
        # Compute the sha
        a_sha = file_sha(a_fn)

        # if sha is in registry, just update last check
        # otherwise enter it
        if a_sha in registry:
            a_file_json = registry[a_sha]
            if a_fn not in a_file_json['filenames']:
                a_file_json['filenames'].append(a_fn)
                modification = True
                if DEBUG_MODE:
                    print(f">>> Warning, duplicate files: {a_file_json['filenames']}")

        else:
            a_file_json = {'type': 'sha', 'filenames': [a_fn]}
            registry[a_sha] = a_file_json
            modification = True

        if a_fn in registry:
            a_fn_n = registry[a_fn]
            a_fn_n['last_check'] = current_datetime
            a_fn_n['modified'] = a_fn_modification
            if a_fn_n['sha'] != a_sha:
                a_fn_n['sha'] = a_sha
                modification = True
        else:
            registry[a_fn] = {'type': 'file', 'sha': a_sha, 'modified': a_fn_modification, 'last_check': current_datetime}
            modification = True

        a_file_json['last_check'] = current_datetime

        if modification:
            a_file_json['modified'] = a_fn_modification

            modified.append(a_fn)

    for missing_file in removed_list:
        sha_for_missing = registry[missing_file]['sha']
        if sha_for_missing in registry:
            sha_entry = registry[sha_for_missing]
            filenames = sha_entry['filenames']
            try:
                filenames.remove(missing_file)
            except Exception as e:
                if DEBUG_MODE:
                    print()
                    print(f'***Error Removing:{e} -- Missing file:{missing_file}')
                else:
                    pass
            try:

                del registry[missing_file]
            except Exception as e:
                if DEBUG_MODE:
                    print()
                    print(f'***Error Deleting:{e} -- Missing file:{missing_file}')
                else:
                    pass
    return modified, removed_list, registry


def marked_pdf_pages(a_mark_json):
    """ Returns 3 lists from the mark metadata:
        - The annotated page numbers
        - The corresponding positions of the static bitmap file
        - The corresponding positions of the stroke containers
        - The dictionnary of the page addresses in the mark file
    TODO: Needs to be more robust: this is fragile construct that assumes that all 3 list are sorted
    by the page number """

    if a_mark_json is not None:
        if '__footer__' in a_mark_json:
            page_address_dict = a_mark_json['__footer__']
            marked_pdf_pages_list = [int(x[4:]) for x in page_address_dict if x[:4] == 'PAGE']

        if '__pages__' in a_mark_json:
            pages_list = a_mark_json['__pages__']

            marked_pdf_pages_paths_list = [int(a_page['TOTALPATH']) for a_page in pages_list if 'TOTALPATH' in a_page.keys()]
            marked_pdf_pages_bitmap_list = [int(
                a_page['__layers__'][0]['LAYERBITMAP']) for a_page in pages_list if len(a_page['__layers__']) > 0]
    return marked_pdf_pages_list, marked_pdf_pages_bitmap_list, marked_pdf_pages_paths_list, page_address_dict


def titles_and_text_from_notes(note_meta_json, binary_data):
    """ Retrieves useful information about titles and recognized text from notes"""
    pages_dict = {}
    result = False
    if note_meta_json is not None:
        if '__pages__' in note_meta_json:
            pages_list = note_meta_json['__pages__']

            for page_index in range(len(pages_list)):
                a_page = pages_list[page_index]
                if 'RECOGNTEXT' in a_page:
                    a_page_str_rec = a_page['RECOGNTEXT']
                    if a_page_str_rec != "0":
                        a_page_rec_pos = int(a_page_str_rec)
                        completed, size_rec = read_endian_int_at_position(binary_data, a_page_rec_pos)
                        if completed:
                            try:
                                begin_read = a_page_rec_pos + 4
                                end_read = begin_read + size_rec
                                byte_string = bytes(binary_data[begin_read:end_read])
                                encoded_json = base64.b64decode(byte_string).decode('utf-8')
                                rec_text = json.loads(encoded_json)
                                pages_dict[str(page_index)] = rec_text
                            except Exception as e:
                                print()
                                print(f'*** Error retrieving rec text: {e} -- page:{page_index}')
                                print(encoded_json)
                                exit(0)

    result = True
    return result, pages_dict


def stars_from_notes(note_meta_json):
    """ Retrieves stars info from notes"""
    pages_dict = {}
    sorted_pages_dict = {}
    result = False
    try:
        if note_meta_json is not None:
            if '__pages__' in note_meta_json:
                pages_list = note_meta_json['__pages__']

                for page_index in range(len(pages_list)):
                    a_page = pages_list[page_index]
                    if 'FIVESTAR' in a_page:
                        a_page_stars_ = a_page['FIVESTAR']
                        if type(a_page_stars_) is list:
                            a_page_stars = a_page_stars_
                        else:
                            a_page_stars = [a_page_stars_]
                        a_page_nb_stars = len(a_page_stars)
                        # Not sure, but the format appears to be  a series of 5 (y,x) points.
                        # The y coordinates don't seem to need any adjustment, so we will infer the rect containing the star
                        # I can't figure out what the numbers xo,yo... mean. There could be a bug, so not a reliable coordinates
                        # DO NOT TRUST CONTAINERS FOR NOW
                        stars_list = []
                        for a_star in a_page_stars:
                            aslc = a_star.split(',')[:-1]

                            aslc = [int(x) for x in aslc]
                            max_y = max([aslc[0], aslc[2], aslc[4], aslc[6], aslc[8]])
                            container_length = max_y - aslc[0]
                            x0 = round(aslc[1]/22.0 - container_length/2.0/8.45)
                            x1 = x0 + round(container_length/8.45)
                            container_rect = [x0, round(aslc[0]/8.45), x1, round(max_y/8.45)]
                            stars_list.append(container_rect)
                        pages_dict[str(page_index)] = {'nb': a_page_nb_stars, 'containers': stars_list}

        # Sort dictionary based on the 'nb' key in the JSON values
        sorted_pages_dict = dict(sorted(pages_dict.items(), key=lambda item: -1*item[1]['nb']))
        result = True
    except Exception as e:
        print()
        print(f'*** stars_from_notes: {e}')

    return result, sorted_pages_dict


def examine_trans_matrix(m):
    """ Check basic properties of an image transformation matrix.
        TODO: See later if any use. Unused for now

    Supported matrices *must* have either
    - m.a == m.d == 0
    or:
    - m.b == m.c == 0
    """
    m = fitz.Matrix(m)  # ensure this is a matrix
    msg = ""
    error = False
    if m.b == m.c == 0:  # means 0/180 rotations or flippings
        if m.a * m.d > 0:  # same sign -> no flippings
            if m.a < 0:  # so both, a and d are negative!
                msg = (4, "rot 180")
            else:
                msg = (0, "nothing")
        else:  # we have a flip
            if m.a < 0:  # horizontal flip
                msg = (1, "left-right")
            else:
                msg = (2, "up-down")
    elif m.a == m.d == 0:  # means 90/270 rotations
        if m.b * m.c < 0:
            if m.b > 0:
                msg = (3, "rot 90")
            else:
                msg = (5, "rot 270")
        else:
            if m.b > 0:
                msg = (6, "up-down, rot 90")
            else:
                msg = (7, "rot 90, up-down")
    else:
        error = True

    if error:
        raise ValueError("unsupported matrix")
    return msg


def snap_to_text(image_pic, original_rect, delta=DELTA_PXL_SEARCH, threshold=None):
    """ Snaps an original rect to the closest text in a pict"""
    # Load the image and convert to a NumPy array
    RGB_base = Image.new('RGB', image_pic.size, (255, 255, 255))  # White background
    RGBA_base = RGB_base.convert('RGBA')

    # If the original image has an alpha channel, use it as a mask to blend the images
    if 'A' in image_pic.getbands():  # Check if the image has an alpha channel
        # The alpha channel is used as a mask to composite the original image over the base
        RGB_image = Image.alpha_composite(RGBA_base, image_pic.convert('RGBA'))
        RGB_image = RGB_image.convert('RGB')
    else:
        # If no alpha channel, just convert directly to RGB
        RGB_image = image_pic.convert('RGB')

    image = np.array(RGB_image)

    # Extract the sub-image based on the original rectangle
    x1, y1, x2, y2 = original_rect
    sub_image = image[y1:y2, x1:x2]
    original_height = y2-y1

    # Get the predominant background color in the rectangle
    colors, counts = np.unique(sub_image.reshape(-1, 3), axis=0, return_counts=True)
    background_color = colors[counts.argmax()]

    # If threshold is None, calculate an optimal threshold

    if threshold is None:

        # Calculate distances from the background color to all other colors
        distances = np.linalg.norm(colors.astype(float) - background_color.astype(float), axis=1)

        if distances.shape[0] > 1:
            # Filter out the background color distance (which is zero)
            non_background_distances = distances[distances > 0]
            # Calculate mean and standard deviation of these distances
            mean_distance = np.mean(non_background_distances)
            std_deviation = np.std(non_background_distances)
            try:
                maximum = np.max(non_background_distances)
            except Exception as e:
                maximum = 255
                if DEBUG_MODE:
                    print(f'*** snap_to_text: {e}')
            # Adjust threshold based on standard deviation
            if std_deviation > mean_distance/2:
                threshold = min(mean_distance + 50*(mean_distance / std_deviation), maximum - 50)
            else:
                threshold = mean_distance
        else:
            threshold = 255

    # Create a mask for text pixels in the entire image
    color_distances = np.linalg.norm(image.astype(float) - background_color.astype(float), axis=2)
    text_mask = color_distances > threshold

    # Find the bounds of text within the original rectangle
    text_indices = np.argwhere(text_mask[y1:y2, x1:x2])
    if text_indices.size > 0:
        text_min_y, text_min_x = text_indices.min(axis=0)
        text_max_y, text_max_x = text_indices.max(axis=0)
        # Adjust coordinates relative to the full image
        text_min_x += x1
        text_max_x += x1
        text_min_y += y1
        text_max_y += y1
    else:
        # No text within the rectangle, return the original
        return original_rect, image_pic

    # Expand to include all text pixels while respecting image boundaries
    expanded_x1 = max(text_min_x - delta, 0)
    expanded_y1 = max(text_min_y - delta, 0)
    expanded_x2 = min(text_max_x + delta, image.shape[1])
    expanded_y2 = min(text_max_y + delta, image.shape[0])

    # Ensure the expanded rectangle does not intersect with text pixels outside the original bounds
    def is_clear_of_text(x1, y1, x2, y2):
        # Check within the expanded bounds
        region = text_mask[y1:y2, x1:x2]
        return not np.any(region)
    result_rect_before_shrink = (expanded_x1, expanded_y1, expanded_x2, expanded_y2)
    # Adjust if necessary to avoid text pixels while keeping within the expanded bounds
    while not is_clear_of_text(expanded_x1-1, expanded_y1, expanded_x1, expanded_y2) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_x1 > 1):
        expanded_x1 -= 1  # EXPAND_SHRINK_PXL
    while not is_clear_of_text(expanded_x1, expanded_y1, expanded_x2, expanded_y1+1) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_y1 < image.shape[0]):
        expanded_y1 += 1
    while not is_clear_of_text(expanded_x2, expanded_y1, expanded_x2+1, expanded_y2) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_x2 < image.shape[1]):
        expanded_x2 += 1  # EXPAND_SHRINK_PXL
    while not is_clear_of_text(expanded_x1, expanded_y2-1, expanded_x2, expanded_y2) and (expanded_x1 < expanded_x2 and expanded_y1 < expanded_y2) and (expanded_y2 > 1):
        expanded_y2 -= 1
    # draw = ImageDraw.Draw(image_pic)
    expansion_contraction = abs((expanded_y2-expanded_y1)/original_height-1)
    if (expanded_y2 > expanded_y1 + 15) and (expansion_contraction < 0.5):
        result_rect = (expanded_x1, expanded_y1, expanded_x2, expanded_y2)
    else:
        result_rect = result_rect_before_shrink
    # draw.rectangle(result_rect, outline="red", width=1)

    return result_rect, image_pic


def inventory_fonts(mark_dict):
    font_inventory = set()
    if mark_dict is not None:
        for page, content in mark_dict.items():
            for key, box_content in content.items():
                if key != 'highlights' and key != 'annots':
                    if 'fonts' in box_content:
                        for font in box_content['fonts']:
                            font_inventory.add((font[0], font[1]))

    return font_inventory


def determine_thickness(font_name):
    """
    Simple heuristic to determine font thickness based on common naming conventions.
    This function returns a numerical representation of the thickness, where higher
    numbers indicate a thicker font.
    """
    if "Bold" in font_name:
        return 2  # Assume 'Bold' is thicker than normal and other variants
    elif "Regular" in font_name:
        return 1  # Assume 'Regular' is the standard thickness
    else:
        return 0  # Default case for fonts without a clear thickness indicator


def create_reverse_title_hierarchy(fonts):
    """
    Create a dictionary that allows looking up title type based on font name and size.
    The title levels are determined first by font size, then by font thickness, but the
    returned dictionary maps directly from font specifications to a title level string.
    """
    # Sort the fonts first by size (descending) and then by thickness (descending)
    sorted_fonts = sorted(fonts, key=lambda font: (-font[1], -determine_thickness(font[0])))

    # Create the reverse hierarchy
    reverse_hierarchy = {}
    for level, font in enumerate(sorted_fonts, start=1):
        key = (font[0], font[1])  # Create a tuple of font name and size
        reverse_hierarchy[key] = f"Title {level}"

    return reverse_hierarchy


def extract_text_with_titles(mark_dict, reverse_hierarchy):
    """
    Extracts text along with its corresponding highest hierarchy title from JSON data,
    including the page number as part of the tuple.

    :param data: The JSON data as a dictionary.
    :param reverse_hierarchy: A dictionary mapping font name and size to title levels.
    :return: A list of tuples, each containing text, its corresponding highest title, and the page number.
    """
    text_with_titles_pages = []
    for page, content in mark_dict.items():
        for box_nb, box_content in content.items():
            if box_nb != 'highlights' and box_nb != 'annots':
                text = f" •  P{int(page)+1} - {box_content['text']}"
                # Determine the highest title level for each text block
                highest_title = "Title 1"  # Default value
                highest_level = float('inf')  # Initial comparison value
                if 'fonts' in box_content:
                    for font in box_content['fonts']:
                        font_name_size = (font[0], font[1])
                        title_level = reverse_hierarchy.get(font_name_size)
                        if title_level:
                            # Extract the numeric level from the title string
                            current_level = int(title_level.split(' ')[-1])
                            if current_level < highest_level:
                                highest_level = current_level
                                highest_title = title_level
                    text_with_titles_pages.append((text, highest_title, int(page)+1))

    return text_with_titles_pages


def check_color_threshold(image, rect, target_color, threshold=10, margin=5):
    """
    We assume here pix coordinates
    Checks if the number of pixels of a given color within a specified rectangle area of an image
    is above the threshold percentage.

    :param image: PIL Image object
    :param rect: Tuple of (left, upper, right, lower) specifying the rectangle area
    :param target_color: Tuple of (R, G, B) specifying the target color
    :param threshold: absolute pixels nb threshold as an integer
    :return: True if the number of target color pixels is above the threshold, False otherwise
    """
    # Crop the image to the specified rectangle + margin TODO: Check why caret no exactly in place
    x0, y0, x1, y1 = rect
    a_rect = [x0-margin, y0-margin, x1+margin, y1+margin]

    cropped_image = image.crop(a_rect)

    # Convert cropped image to numpy array
    np_image = np.array(cropped_image)

    # If the image has an alpha channel, ignore it for the comparison
    if np_image.shape[-1] == 4:
        np_image = np_image[:, :, :3]

    # Create a mask for pixels that match the target color
    mask = np.all(np_image == target_color, axis=-1)

    # Count the number of matching pixels
    matching_pixels = np.sum(mask)

    # Check if the matching percentage is above the threshold
    return matching_pixels > threshold


def load_user_settings(path=os.path.join(SN_VAULT, USER_SETTINGS_FN)):
    """ Reads user settings and set the value of the global variable accordingly
        TODO: Warn users not to accept someone's else user_settings without checking
         that it is safe, because malignant code could be hidden in it """
    try:
        user_settings_dict = {}
        if os.path.exists(path):
            user_settings_dict = read_json(path)
            # Parse the keys
            for key, value in user_settings_dict.items():
                if key in allowed_globals and isinstance(value, (str, list, dict, int, float, bool, type(None))):
                    eval(f"{key} = {repr(value)}")

    except Exception as e:
        print()
        print(f'Cannot load load_user_settings: {e}')
        return {}
    return user_settings_dict


def save_user_settings(user_settings_dict):
    """ Saves user settings in a json"""
    try:
        user_settings_fn = os.path.join(SN_VAULT, USER_SETTINGS_FN)
        save_json(user_settings_fn, user_settings_dict)
    except Exception as e:
        print()
        print(f'*** Error in save_user_settings: {e}')


# The core of the program starts here. As of May 27, this is only to process pdf annotation,
# so really we process .mark and pdf files and ignore the rest.
# TODO: reshape as a __main__ core with some settings available from the command line

def main():

    global SN_IO_UPLOAD_FOLDER
    global NOTEBOOK_SERIES
    global NOTEBOOK_DEVICE
    links_reg_dict = {}
    extra_copy_folders_dict_final = {}
    ascii_ps_list = None
    asciiset_fn = ''
    file_series = NOTEBOOK_DEVICE
    # Initializing transfer_mode (indicates method of file transfer)
    transfer_mode = None
    changed_icon = False  # Indicator flag for visually showing that PySN is running (Windows, Linux)

    try:

        # Now load the user_settings
        user_settings = load_user_settings()
        max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(user_settings['device'])
        print()
        # Argument parser setup
        epilog = ""
        parser = argparse.ArgumentParser(epilog=epilog, description="PySN command line interpreter (CLI) for updating global variables.\
                                         These settings are stored in SN_vault\\user_settings.json. \
                                         If you are setting multiple values (like for example multiple input folders), separate the values with a space.\
                                         The current setting value for each parameter is showing between [].")

        for long_param, details in settings_desc_dict.items():  # Go through the list of global variables exposed to CLI

            description = details['description']
            param_type = details['type']

            a_variable_name = param_to_global_var[long_param]  # Retrieve the variable name from the dictionary
            a_variable_value = eval(a_variable_name)  # Retrieve the current variable value (the hard-coded value)

            if long_param in user_settings:
                a_variable_value = user_settings[long_param]  # Overwrite with user's saved values

            if param_type == bool:  # Add a '-no-' prefix option to set boolean values
                parser.add_argument(f'--{long_param}', action='store_true', default=a_variable_value, help=f'{description}. [%(default)s]')
                parser.add_argument(f'--no-{long_param}', action='store_true', default=None,  help='')

            elif long_param == 'input':

                parser.add_argument(
                    f'--{long_param}', nargs='+',  type=param_type, default=a_variable_value, metavar=f'{a_variable_name}',
                    help=f'{description}.  [{" ".join(a_variable_value)}]')

            else:
                parser.add_argument(f'--{long_param}', type=param_type, default=a_variable_value, metavar=f'{a_variable_name}', help=f'{description}. [%(default)s]')

        parser.add_argument('--reset', action='store_true', help='Reset to hard-coded values: the values that are affected to digest.py global variables (in UPPERCASE when using the help)')
        # Add a reset option (back to hard-coded values)

        # Parse command line arguments
        args = parser.parse_args()

        # Convert args to a dictionary for easier handling
        args_dict = vars(args)

        if args_dict['reset']:
            # The user has decided to revert settings to hard-coded values
            new_user_settings = {}
        else:
            # Now browse the args_back, handling boolean settings, if needed
            args_dict.pop('reset')
            new_user_settings = dict(args_dict)
            for a_key, a_value in args_dict.items():
                if type(a_value) is bool:
                    a_key_list = a_key.split('_')
                    if 'no' == a_key_list[0]:
                        base_key = '_'.join(a_key_list[1:])
                        new_user_settings[base_key] = False
                        new_user_settings.pop(a_key)
                elif a_value is None:
                    new_user_settings.pop(a_key)

            # Now altering global variables
            for a_key, a_value in new_user_settings.items():
                a_global_var_name = param_to_global_var[a_key]

                if a_global_var_name in allowed_globals:
                    if type(a_value) in [str, float, dict]:
                        if a_value != "":
                            globals()[a_global_var_name] = a_value
                    else:
                        globals()[a_global_var_name] = a_value

        save_user_settings(new_user_settings)  # Saving the last set of settings in 'user_settings.json'

        # apix_fn = "D:\\eink\\bk\\myhw1.png"
        # vect_points = image2vectors(apix_fn, adaptative_thresholding=True)
        # print(vect_points.keys())
        # exit(1)

        print('   ----------------------------')
        print(f"    Version: {PYSN_VERSION}")
        print('   ----------------------------')
        print()
        print('USB requirement: In Supernote settings, turn on Sideloading')
        print('                 go to Security & Privacy -> Sideloading -> ON')
        print()
        print(f">>> WATCHING: {user_settings['input']}")
        print()
        try:

            if os.name == 'nt':
                # Windows only: SHORTCUT_PATH stores the path of the PySN shortcut
                #               The script tries to inverse its icon while running (see finally section at the end when it restores it)
                #               This is just a visual effect and the shortcut path doesn't have to be necessarily linked to digest.pay
                if os.path.exists(SHORTCUT_PATH):
                    try:
                        import winshell
                        with winshell.shortcut(SHORTCUT_PATH) as link:
                            link.icon_location = (os.path.join(PYSN_DIRECTORY, "sni.ico"), 0)
                        changed_icon = True
                    except Exception as e:
                        print()
                        print(f'--{os.path.join(PYSN_DIRECTORY, "sni.ico")}--')  # sni is the inversed SN icon, used for showing that the
                        print(f'****Error: {e}')

            elif sys.platform == 'darwin':
                if os.path.exists(SHORTCUT_PATH):
                    set_bundle_icon('sni.icns', SHORTCUT_PATH)
                    changed_icon = True

            elif sys.platform.startswith('linux'):
                if os.path.exists(SHORTCUT_PATH):
                    shell_script_path = os.path.join(PYSN_DIRECTORY, 'run_pysn_script.sh')
                    icon_path = os.path.join(PYSN_DIRECTORY, 'sni.png')
                    with open(SHORTCUT_PATH, 'w') as desktop_entry:
                        desktop_entry.write(
                            f"""[Desktop Entry]
Version=1.0
Type=Application
Name=Run Python Script
Exec={shell_script_path}
Icon={icon_path}
Terminal=true
""")
                        os.chmod(SHORTCUT_PATH, 0o755)
                        changed_icon = True
        except Exception as e:
            print()
            print(f'*** Error trying to set shortcut permissions and icon: {e}')

        # Initialize task burden variable
        estimation_task = {'.note': 0, '.mark': 0}

        hist_dict = {}
        external_prefix = internal_prefix = root_url = None
        # PySN will attempt to connect using the following preference:
        #   1. USB
        #   2. WI-FI
        #   3. Local folders of a mirrored synchronization folder (for me OneDrive)
        #
        # Once a tranfer mode is found, the script copies the files to a local directory
        # It then checks if the copied files have been modified, looking at their
        # respective hash signatures

        # Initialize the list of folders to watch
        remote_watched_folders = []

        # Splitting between deep and shallow folders. Users indicate a deep folder with a '*'
        shallow_folders = [x for x in SN_IO_WATCHED_FOLDERS if '*' not in x]
        deep_folders = [x[:-1] for x in SN_IO_WATCHED_FOLDERS if x[-1] == '*']
        # afull_list is built, combining both shallow and deep, for estimation burden purposes (usb only, assessing low prioritize)
        afull_list = list(shallow_folders)
        afull_list.extend(list(deep_folders))

        # 1. Attempting to find Supernote on USB port
        print('>>> Trying to locate Supernote on USB ...')
        try:
            if platform.system() == 'Windows':
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb.exe')
            else:
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb')
            settings_device = NOTEBOOK_DEVICE
            usb_supernote, was_adb_running = attached_sn(device_name=DEVICE_SN, path=ADB_PATH, host=ADB_HOST, port=ADB_LOCAL_PORT)
            # In case device type detected, reload the maxes
            if settings_device != NOTEBOOK_DEVICE:
                print
                print(f' *** WARNING ***: Device type settings conflict! We will be using those for: {NOTEBOOK_DEVICE}')
                max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(NOTEBOOK_DEVICE)
                print()
            # was_adb_running is a flag to know if the script needs to kill that program when it is done with it
        except Exception as e:
            print()
            print(f'*** Cannot find SN on USB: {e}')
            usb_supernote = None

        if usb_supernote:  # If a Supernote is tethered on a USB port

            # Get the 2 types of prefix
            internal_prefix, external_prefix = get_file_system_in_userspace(usb_supernote)

            # Estimating task burden
            if not is_low_priority():
                estimation_task = count_files(usb_supernote, afull_list, internal_prefix, external_prefix)
                # Define conditions to switch to low priority run
                if estimation_task['.note'] > PRIORITY_NOTES_THRESHOLD:
                    set_low_priority()

            if deep_folders != []:
                # Build the list of deep folders (all sub folders to the given root folder)
                remote_watched_folders = get_folders_containing_files_usb(usb_supernote, deep_folders, internal_prefix, external_prefix)

            # Now handle the list of shallow folders; append them to 'remote_watched_folders'
            for a_folder_name in shallow_folders:
                a_folder_name_list = a_folder_name.split('/')
                if a_folder_name_list[0].lower() == 'sdcard':
                    # The folder is deemed to be external if it starts with sdcard
                    # The assumption is that a path always ends with a '/'. However, user can forget the last '/'
                    # Testing for this use case
                    if a_folder_name_list[-1] == '':
                        full_folder_name = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:-1]) + '/')
                    else:
                        full_folder_name = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:]) + '/')
                else:
                    full_folder_name = os.path.join(internal_prefix, '/'.join(a_folder_name_list)[:-1] + '/')
                remote_watched_folders.append(full_folder_name)

            # We now store the proper prefix for the upload to come. TODO: Change this later, difficult to read code
            a_folder_name_list = SN_IO_UPLOAD_FOLDER.split('/')
            if a_folder_name_list[0].lower() == 'sdcard':
                upload_base_path = external_prefix
                SN_IO_UPLOAD_FOLDER = '/'.join(a_folder_name_list[1:-1])
            else:
                upload_base_path = internal_prefix

            # Build the paths to the machine local mirror
            local_watched_folder = os.path.join(SN_VAULT, SN_IO_FOLDER)
            # Ensures the 'local_watched_folder' is cleaned up at each session
            try:
                if os.path.exists(local_watched_folder):
                    shutil.rmtree(local_watched_folder)
                os.makedirs(local_watched_folder)

            except Exception as e:
                print(e)
                pass

            # Remove possible redundancies in 'remote_watched_folders'
            remote_watched_folders = list(set(remote_watched_folders))
            remote_watched_folders.sort()

            # Now parse 'remote_watched_folders' and download using usb, recording in hist_dict
            for a_folder in remote_watched_folders:
                source_folder = remove_storage_paths(a_folder)
                dest_folder = os.path.normpath(os.path.join(local_watched_folder, source_folder))
                if not os.path.exists(dest_folder):
                    os.makedirs(dest_folder)

                print()
                print(f' >> Parsing: {source_folder}')
                copy_files_from_device(usb_supernote, a_folder, dest_folder, hist_dict)

            export_folder = ''
            transfer_mode = 'usb'
            if is_low_priority():
                set_normal_priority()

        # 2. No SN found on USB, trying to reach it on the LAN (if enabled)
        elif USE_DIRECT_LAN_TRANSFER:
            print()
            lan_is_on = False
            print('>>> Trying to locate Supernote on local network ...')
            on_counter = 0
            lan_to_use = []
            while True:
                # Retrieve the list of IP addresses from the settings
                list_ips = [x[2:] for x in NOTEBOOK_SERIES if x[:2] == 'IP']
                found_on_lan = False
                list_ips_on = []
                

                # If that list includes some IP addresses, check which one is on
                if len(list_ips) > 0:
                    for an_ip in list_ips:
                        root_url = f'http://{an_ip}:{SN_IO_PORT}'
                        lan_is_on, _ = asyncio.run(lanon_and_microsdname(root_url))
                        found_on_lan = found_on_lan or lan_is_on
                        on_counter += 1
                        if lan_is_on:
                            
                            
                            lan_to_use.append(on_counter)
                            verb_msg = "Device is Ready"
                        else:
                            verb_msg = '"Browse & Access" activation is required!'
                        list_ips_on.append((an_ip, lan_is_on, verb_msg))

                len_list_ips_on = len(list_ips_on)

                print()
                print('    > Please type a device list *')
                valid_selection = []
                for i in range(len_list_ips_on):
                    valid_selection.append(str(i+1))
                    x = list_ips_on[i]
                    device_setting = NOTEBOOK_SERIES[f'IP{x[0]}']
                    if device_setting == 'N5':
                        device_setting = 'Manta'
                    print(f'      [{i+1}]:  IP:{x[0]:<15}   {device_setting:<10} {x[2]}')
                print('      [+]:  Add a new IP address')
                print('      [0]:  Skip LAN connection through wifi')
                print()

                if len(lan_to_use) == 1:
                    user_input = str(lan_to_use[0])
                else:
                    user_input = input('     Option [#]: ')

                if user_input in valid_selection:
                    ip_selected = list_ips_on[int(user_input)-1]
                    root_url = f'http://{ip_selected[0]}:{SN_IO_PORT}'
                    lan_is_on, external_prefix = asyncio.run(lanon_and_microsdname(root_url))
                    if lan_is_on:
                        print(f"  > Connected to {root_url}. Found card uri: '{external_prefix}'")
                        NOTEBOOK_DEVICE = NOTEBOOK_SERIES[f'IP{ip_selected[0]}']
                        break
                elif user_input == '0':
                    print(' > Skipped LAN. Proceeding with cloud, if available')
                    print()
                    break

                elif user_input == '+':

                    print()
                    print(' > Adding a new IP address')

                    new_ip = input(' IP address (Do not include port 8089): ')
                    new_ip_key = f'IP{new_ip}'
                    while True:
                        device_type = input(' Device type needed. Enter "5" for A5x or Manta, "6" for A6x or Nomad:')

                        if device_type in ("5", "6"):
                            NOTEBOOK_DEVICE = f'N{device_type}'
                            user_settings_dict = load_user_settings()
                            NOTEBOOK_SERIES = user_settings_dict['series']
                            NOTEBOOK_SERIES[new_ip_key] = NOTEBOOK_DEVICE
                            save_user_settings(user_settings_dict)
                            print()
                            print(f'Thank you! {new_ip} will be recognized as IP address for series {NOTEBOOK_DEVICE}')
                            print()
                            break
                    root_url = f'http://{new_ip}:{SN_IO_PORT}'
                    lan_is_on, external_prefix = asyncio.run(lanon_and_microsdname(root_url))
                    if lan_is_on:
                        print(f"  > Connected to {root_url}. Found card uri: '{external_prefix}'")
                        break
                    else:
                        print()
                else:
                    print(' Invalid selection')

            # exit(1)

            # root_url = f'http://{SN_IO_IP_ADDRESS}:{SN_IO_PORT}'
            # # Check if LAN connection is on and retrieve microSD card prefix, if any
            # lan_is_on, external_prefix = asyncio.run(lanon_and_microsdname(root_url))

            if lan_is_on:
                # print(f"  > Connected to {root_url}. Found card uri: '{external_prefix}'")
                # print()
                # device_sn_to_use_x = f'IP{SN_IO_IP_ADDRESS}'
                # if device_sn_to_use_x in NOTEBOOK_SERIES:

                #     NOTEBOOK_DEVICE = NOTEBOOK_SERIES[device_sn_to_use_x]
                #     print(f'  >> DEVICE: {NOTEBOOK_DEVICE} <<')
                # else:
                #     while True:
                #         user_input = input('    Device type needed. Enter "5" for A5x or Manta, "6" for A6x or Nomad:')
                #         if user_input in ("5", "6"):
                #             NOTEBOOK_DEVICE = f'N{user_input}'
                #             user_settings_dict = load_user_settings()
                #             dt_settings = user_settings_dict['series']
                #             dt_settings[device_sn_to_use_x] = NOTEBOOK_DEVICE
                #             save_user_settings(user_settings_dict)
                #             print()
                #             print(f'    Thank you! {SN_IO_IP_ADDRESS} will be recognized as IP address for series N{user_input}')
                #             print()
                #             break

                # Prefixing the upload folder for SDcard, iff applicable
                if SN_IO_UPLOAD_FOLDER.split('/')[0].lower() == 'sdcard':
                    # Store where to upload the modified pair of .pdf and .mark files
                    url_upload = f'{root_url}{external_prefix}/{SN_IO_UPLOAD_FOLDER}'
                    url_upload = re.sub(re.escape('sdcard/'), '', url_upload, flags=re.IGNORECASE)
                else:
                    url_upload = f'{root_url}/{SN_IO_UPLOAD_FOLDER}'

                # Store where to upload the folder of the pdf obtained by
                # merging the original pdf and the annotation file (.mark file)
                # Prefixing the export folder for SDcard, if applicable
                if EXPORT_FOLDER.split('/')[0].lower() == 'sdcard':
                    url_export = f'{root_url}{external_prefix}/{EXPORT_FOLDER}'
                    url_export = re.sub(re.escape('sdcard/'), '', url_export, flags=re.IGNORECASE)
                else:
                    url_export = f'{root_url}/{EXPORT_FOLDER}'

                # Build the paths to the machine local mirror
                local_watched_folder = os.path.join(SN_VAULT, SN_IO_FOLDER)
                # Ensures the 'local_watched_folder' is cleaned up at each session
                try:
                    print()
                    if os.path.exists(local_watched_folder):
                        shutil.rmtree(local_watched_folder)
                    os.makedirs(local_watched_folder)

                except Exception as e:
                    print(e)
                    pass

                try:
                    # For WIFI, the root url path on the SN webserver is 'Supernote'
                    # Subolders: Document, EXPORT, ... etc. AND the microSD card (if installed)
                    # Stores the list of folders you want to transfer from

                    # Now populate the local mirror by downloading from the SN webserver
                    # We take a different approach for determining the prioritization here
                    # because it is costly to make an inventory (we can't make one in a single command like in usb)
                    # and will switch to lower priority whenever deep folder is involved

                    for remote_folder in SN_IO_WATCHED_FOLDERS:
                        # First detect if this is a deep folder
                        if remote_folder[-1] == '*':
                            deep_folder = True
                            remote_folder = remote_folder[:-1]  # Truncate the folder by removing the '*'
                        else:
                            deep_folder = False

                        # Adding card prefix, if applicable
                        if remote_folder.split('/')[0].lower() == 'sdcard':
                            # The folder is deemed to be external if it starts with SDcard
                            url_download = f'{root_url}{external_prefix}/{remote_folder}'
                            url_download = re.sub(re.escape('sdcard/'), '', url_download, flags=re.IGNORECASE)
                        else:
                            url_download = f'{root_url}/{remote_folder}'
                            url_download = url_download.replace(' ', '+')
                        # Downloading folder asynchronously
                        # local_watched_folder is here as a root folder. The exact file download folder will be assigned inside of the download function
                        hist_dict = asyncio.run(parse_and_download_files_deep(url_download, local_watched_folder, hist_dict, deep_folder))

                    export_folder = ''
                    transfer_mode = 'webserver'
                except Exception as e:
                    print(e)
                    print(f'*** Cannot connect to SN webserver: IP: {SN_IO_IP_ADDRESS} - Port: {SN_IO_PORT}')
                    print(f'hist_dict: {hist_dict}')
                    exit(1)
                finally:
                    if is_low_priority():
                        set_normal_priority()

        # 3. Lastly try is to use folders on your system that are synchronized (or not) with the cloud
        # Note: As of 2024-05-27, the SDcard doesn't synch with the cloud
        if transfer_mode is None:

            # build the list of remote folders
            remote_watched_folders = [os.path.join(REMOTE_BASE_PATH, rel_path) for rel_path in SN_IO_WATCHED_FOLDERS]
            output_folder = os.path.join(REMOTE_BASE_PATH, SN_IO_UPLOAD_FOLDER)
            export_folder = os.path.join(REMOTE_BASE_PATH, EXPORT_FOLDER)

            # Adjust settings for Windows
            if platform.system() == 'Windows':
                remote_watched_folders = linux_to_windows_paths(remote_watched_folders)
                remote_upload_folder = linux_to_windows_paths(output_folder)
                remote_export_folder = linux_to_windows_paths(export_folder)

            transfer_mode = 'folder'

            remote_watched_folders_ = []
            for rfx in remote_watched_folders:
                if rfx[-1] == '*':
                    remote_watched_folders_.append(rfx[:-1])
                else:
                    remote_watched_folders_.append(rfx)

            full_remote_watched_folders = find_folders_with_extension_cloud(remote_watched_folders_)

            # Build the paths to the machine local mirror
            local_watched_folder = os.path.join(SN_VAULT, SN_IO_FOLDER)
            # Ensures the 'local_watched_folder' is cleaned up at each session
            try:
                if os.path.exists(local_watched_folder):
                    shutil.rmtree(local_watched_folder)
                os.makedirs(local_watched_folder)
            except Exception as e:
                print(e)
                pass        # Read all files sitting in the watched folder, excluding those in the 'export_folder'

            for a_folder_to_process in full_remote_watched_folders:
                source_ = a_folder_to_process.replace(REMOTE_BASE_PATH, '')
                dest_folder = os.path.normpath(os.path.join(local_watched_folder, source_))
                copy_files_without_subfolders(a_folder_to_process, dest_folder)

        current_notes_list = list_files(local_watched_folder, export_folder)

        # We only process files that have been modified since last run
        # We call detect_modified_files, which compares existing filenames with
        # sha signatures of previously processed files stored in a double dictionary (registry.json)

        changed_files, files_removed, registry = detect_modified_files(current_notes_list)

        note_or_mark_list = ['pdf', 'mark', 'note', 'doc', 'docx']
        non_notes_nor_mark = [x for x in changed_files if x.split('.')[-1].lower() not in note_or_mark_list]

        for afile_name in non_notes_nor_mark:
            afile_name_ = unquote(afile_name)
            bckp_destination = afile_name_.replace(local_watched_folder, LOCAL_BKP_NOTES)
            copy_file_with_directories(afile_name_, bckp_destination)

        for a_changed_file in changed_files:
            a_changed_file_ = unquote(a_changed_file)
            if (a_changed_file_ in hist_dict) and (a_changed_file_ not in non_notes_nor_mark):
                copy_sn_file(a_changed_file_, unquote(hist_dict[a_changed_file_]))

        print()
        print(f'>>> {len(changed_files)} changed files')
        print(f'>>> {len(files_removed)} removed files')
        print()

        for a_removed in files_removed:
            rel_fn = os.path.normpath(a_removed.replace(local_watched_folder, LOCAL_BKP_NOTES))
            if os.path.exists(rel_fn):
                print(f'  > Archived: {archive_with_structure(rel_fn, remove=True)}')

        nb_files_to_process = len(changed_files)

        if nb_files_to_process > 0:

            if nb_files_to_process > PRIORITY_NOTES_THRESHOLD + PRIORITY_DIGEST_THRESHOLD:
                set_low_priority()

            # We store all temp data in a time stamped folder
            temp_work_directory = create_workdir()

            # Parsing changed files
            dimmed_dict = {"color": (DIMMED_MENU, DIMMED_MENU, DIMMED_MENU)}
            extra_copy_folders_dict = {}

            for changed_file in changed_files:

                inline_css = False  # Initialize ks (Keyword Setting) for inline css
                vectorize_pdf = False  # Initialize ks (Keyword Setting) for vectorize pdf
                recognize_shapes = False  # Initialize ks (Keyword Setting) for shapes recognition
                extra_copy_folders = []  # Initialize ks (Keyword Setting) for extra-copies
                notes_kw_words = []
                notes_links_jsons = []
                notes_kw_jsons = []
                notes_link_dict = {}
                notes_dict = {}

                notes_kw_toc = []
                notes_kw_toc_settings = []
                stars_toc = []
                keys_set = set()
                a_palette = None

                basename, extension = os.path.splitext(os.path.basename(changed_file))

                # Digest only: For each file, we create a subfolder of the same name
                changed_file_dir = os.path.join(temp_work_directory, basename)

                if extension.lower() == '.note':
                    print()
                    print(f' > Processing Note: {changed_file}')

                    destination_fn = changed_file

                    pdf_notebook = sn.load_notebook(destination_fn)  # load the notebook

                    # Read the metadata
                    a_metadata = json.loads(extract_metadata(destination_fn, destination_fn + '.json'))
                    if '__header__' in a_metadata:
                        a_header = a_metadata['__header__']
                        current_fileid = a_header['FILE_ID']
                        notes_link_dict['fileid'] = current_fileid
                        if 'APPLY_EQUIPMENT' in a_header:
                            file_series = a_header['APPLY_EQUIPMENT']
                            try:
                                max_horizontal_pixels, max_vertical_pixels, adb_screen_max_x, adb_screen_max_y = series_bounds(file_series)
                            except Exception as e:
                                print(f'**- series_bounds: {e}')
                                print(f'--destination_fn: {destination_fn}')
                            if file_series in ['N5']:
                                if NOTEBOOK_DEVICE not in ['N5']:
                                    print(f'**- {NOTEBOOK_DEVICE} device handling a N5 file')

                            else:
                                if NOTEBOOK_DEVICE in ['N5']:
                                    print(f'**- {NOTEBOOK_DEVICE} device handling a Non Manta file')

                    if '__footer__' in a_metadata:
                        note_footer = a_metadata['__footer__']
                        note_general_titles_list = [int(x[6:10]) for x in note_footer.keys() if 'TITLE_' in x]
                        # Build list of source pages of links
                        note_general_links_list = [int(x[6:10]) for x in note_footer.keys() if 'LINKO_' in x]

                        if '__titles__' in note_footer:
                            note_titles_list = note_footer['__titles__']
                            for index_title in range(len(note_general_titles_list)):
                                a_page_nb = note_general_titles_list[index_title]
                                a_note_title_details = note_titles_list[index_title]
                                a_note_title_details_rect = a_note_title_details['TITLERECT']
                                title_rect_list = [int(x) for x in a_note_title_details_rect.split(',')]
                                title_rect_xy = [
                                    title_rect_list[0], title_rect_list[1],
                                    title_rect_list[0]+title_rect_list[2],
                                    title_rect_list[1] + title_rect_list[3]]
                                a_note_title_details["TITLERECTSTD"] = title_rect_xy
                                a_note_title_details_style = a_note_title_details['TITLESTYLE']
                                a_note_title_details["TITLERANK"] = 1 + TITLE_STYLES.index(
                                    a_note_title_details_style) if a_note_title_details_style in TITLE_STYLES else -1
                                a_page_key = str(a_page_nb)
                                if a_page_key in notes_dict:
                                    notes_dict[a_page_key].append(a_note_title_details)
                                else:
                                    notes_dict[a_page_key] = [a_note_title_details]

                        if '__keywords__' in note_footer:
                            try:
                                note_kw_list = note_footer['__keywords__']
                                for a_note_kw_details in note_kw_list:
                                    a_page_key = a_note_kw_details['KEYWORDPAGE']
                                    a_note_kw_details_rect = a_note_kw_details['KEYWORDRECT']
                                    kw_rect_list = [int(x) for x in a_note_kw_details_rect.split(',')]
                                    kw_rect_xy = [
                                        kw_rect_list[0], kw_rect_list[1],
                                        kw_rect_list[0]+kw_rect_list[2],
                                        kw_rect_list[1] + kw_rect_list[3]]

                                    kw_keyword = a_note_kw_details['KEYWORD']

                                    notes_kw_words.append(kw_keyword)

                                    keys_set.add(kw_keyword)
                                    notes_kw_jsons.append(
                                        {
                                            "page": a_page_key,
                                            "src_rect": kw_rect_xy,
                                            "keyword": kw_keyword

                                        })
                            except Exception as e:
                                print()
                                print(f'*** Note keyword reading: {e}')
                                print(' Please note that the Supernote takes some time in the background to process keywords and this error may just be temporary')

                            # Now build a keywords toc
                            notes_link_dict["keywords"] = notes_kw_jsons
                            if DEBUG_MODE:
                                save_json(changed_file.replace('.note', '_keys.json'), notes_kw_jsons)
                            notes_kw_jsons_sorted = sorted(notes_kw_jsons, key=lambda x: (x['keyword'], int(x['page'])))
                            previous_kw = ''
                            previous_cat = ''
                            previous_setting = ''

                            for a_skw in notes_kw_jsons_sorted:
                                a_kw = a_skw['keyword']
                                a_pg = a_skw['page']
                                # Processing keyword-settings
                                if a_kw[:3] == 'ks-':
                                    try:
                                        a_kw_list = a_kw.split('-')
                                        boolean_setting = len(a_kw_list) == 2
                                        ks_category = a_kw_list[1]
                                        if boolean_setting:
                                            ks_setting = 'ON'
                                        else:
                                            ks_setting = a_kw_list[2]
                                        if ks_category == previous_cat:
                                            if ks_setting == previous_setting:
                                                notes_kw_toc_settings.append((5, f'  page {a_pg}', int(a_pg)))
                                            else:
                                                notes_kw_toc_settings.append((4, ks_setting, int(a_pg), dimmed_dict))
                                                notes_kw_toc_settings.append((5, f'  page {a_pg}', int(a_pg)))
                                        else:
                                            notes_kw_toc_settings.append((3, ks_category, int(a_pg), dimmed_dict))
                                            notes_kw_toc_settings.append((4, ks_setting, int(a_pg), dimmed_dict))
                                            notes_kw_toc_settings.append((5, f'  page {a_pg}', int(a_pg)))
                                        previous_setting = ks_setting
                                        previous_cat = ks_category
                                    except Exception as e:
                                        print(f'*** Keyword setting: {e}')

                                elif a_kw == previous_kw:
                                    notes_kw_toc.append((3, f'  page {a_pg}', int(a_pg)))
                                else:
                                    notes_kw_toc.append((2, a_kw, int(a_pg)))
                                    notes_kw_toc.append((3, f'  page {a_pg}', int(a_pg)))
                                previous_kw = a_kw
                            notes_kw_toc_settings.insert(0, (2, 'SETTINGS', -1, dimmed_dict))
                            notes_kw_toc.extend(notes_kw_toc_settings)
                            notes_kw_toc.insert(0, (1, 'KEYWORDS', -1, {"color": (1, 0, 0)}))

                            # Now build a Stars toc
                            retrieved_stars, stars_dict = stars_from_notes(a_metadata)

                            previous_stars = 0
                            for a_star_page, a_star_json in stars_dict.items():
                                a_star_nb = a_star_json['nb']
                                a_star_page_int = int(a_star_page) + 1
                                if a_star_nb == previous_stars:
                                    stars_toc.append((3, f'  page {a_star_page_int}', a_star_page_int))
                                else:
                                    if a_star_nb == 1:
                                        # star_title = '1 star'
                                        star_title = '\u2606'
                                    else:
                                        # star_title = f'{a_star_nb} stars'
                                        star_title = '\u2606'*a_star_nb
                                    stars_toc.append((2, star_title, a_star_page_int))
                                    stars_toc.append((3, f'  page {a_star_page_int}', a_star_page_int))
                                previous_stars = a_star_nb
                            stars_toc.insert(0, (1, 'STARS', -1, {"color": (1, 0, 0)}))

                        # Now check if the notebook has external links. If it does, populate the notes_link_dict
                        # We're doing so to recreate links to external files, when the fie becomes available
                        try:
                            if '__links__' in note_footer:
                                note_links_list = note_footer['__links__']
                                for index_link in range(len(note_general_links_list)):
                                    a_note_link_details = note_links_list[index_link]
                                    a_note_link_details_type = a_note_link_details['LINKTYPE']

                                    source_page_nb = note_general_links_list[index_link]
                                    source_page_nb_key = str(source_page_nb)
                                    link_timestamp = a_note_link_details['LINKTIMESTAMP']
                                    source_page_rect = a_note_link_details['LINKRECT']
                                    link_rect_list = [int(x) for x in source_page_rect.split(',')]
                                    link_rect_xy = [link_rect_list[0], link_rect_list[1],
                                                    link_rect_list[0]+link_rect_list[2],
                                                    link_rect_list[1] + link_rect_list[3]]
                                    target_fileid = a_note_link_details['LINKFILEID']
                                    target_page = a_note_link_details['OBJPAGE']
                                    target_filename = base64.b64decode(
                                        a_note_link_details['LINKFILE']).decode('utf-8')
                                    notes_links_jsons.append(
                                        {
                                            "page": source_page_nb_key,
                                            "src_rect": link_rect_xy,
                                            "tgt_fileid": target_fileid,
                                            "tgt_filename": target_filename,
                                            "tgt_page": target_page,
                                            "timestamp": link_timestamp,
                                            "link_type": a_note_link_details_type
                                        })
                                notes_link_dict["links"] = notes_links_jsons

                                if DEBUG_MODE:
                                    save_json(changed_file.replace('.note', '_links.json'), notes_links_jsons)
                        except Exception as e:
                            print()
                            print(f'*** Error: {e}')

                    recognize_shapes = 'ks-shapes' in notes_kw_words

                    if recognize_shapes:

                        print('   - Shapes recognition ...')

                        shapes_detected = False

                        shape_fn = None

                        shape_color = HIGHLIGHTS_COLOR[SHAPES_COLOR][0]

                        pen_strokes_dict, _ = get_pen_strokes_dict(changed_file)

                        # A dictionary to hold all shapes found in the notebook. One entry per page.
                        pages_ps = {}

                        # Go through each pair of apage and its corresponding total path information
                        if pen_strokes_dict is not None:
                            for apage, avalue in pen_strokes_dict.items():

                                # Retrieve the list of all pen stroke objects on page "apage"
                                a_list_strokes = avalue['strokes']

                                # Build the list of all individual pen strokes paths on the current page
                                a_list_paths = [(x['color'], x['vector_points']) for x in a_list_strokes]

                                # Index to keep track of the individual pen stroke object since we submit to
                                # classification only a list of paths
                                index_stroke = 0

                                # For each path, check if it is a shape. The classify_shape returns either
                                # None or a dictionary {"shape":<"line" or "trinagle", etc.>, "points": <perfect minimal points>}
                                for a_path in a_list_paths:

                                    # Filter out shapes that are not in the shape color detection
                                    if a_path[0] == shape_color:
                                        shape = classify_shape(
                                            a_path[1], max_horizontal_pixels, max_vertical_pixels,
                                            adb_screen_max_x, adb_screen_max_y)
                                    else:
                                        shape = None

                                    # If a shape was detected in the current path
                                    if shape:

                                        shapes_detected = True

                                        # Retrieve the address, the size in bytes of the pen stroke with identified shapes
                                        pen_stroke_object = a_list_strokes[index_stroke]
                                        pen_stroke_address = pen_stroke_object['address']
                                        pen_stroke_bytesize = pen_stroke_object['size']
                                        pen_stroke_vectorsize = pen_stroke_object['vector_size']

                                        # Retrieve the shape type and point from the detected shape
                                        shape_shape = shape['shape']
                                        shape_points = shape['points']

                                        # If we already have shapes for that page, pull the page dictionary of shapes
                                        if apage in pages_ps:
                                            page_shapes_dict = pages_ps[apage]

                                            # If the shape type is already in the page dictionary of shapes, append it
                                            if shape_shape in page_shapes_dict:
                                                page_shape_list = page_shapes_dict[shape_shape]
                                                page_shape_list.append(
                                                    {
                                                        "address": pen_stroke_address,
                                                        "bytes_nb": pen_stroke_bytesize,
                                                        "points_nb": pen_stroke_vectorsize,
                                                        "points": shape_points})

                                            # Otherwise create an entry for that type of shape for that page
                                            else:
                                                page_shapes_dict[shape_shape] = [
                                                    {
                                                        "address": pen_stroke_address,
                                                        "bytes_nb": pen_stroke_bytesize,
                                                        "points_nb": pen_stroke_vectorsize,
                                                        "points": shape_points}]

                                        else:
                                            pages_ps[apage] = {
                                                shape['shape']: [
                                                    {
                                                        "address": pen_stroke_address,
                                                        "bytes_nb": pen_stroke_bytesize,
                                                        "points_nb": pen_stroke_vectorsize,
                                                        "points": shape_points}]}
                                    index_stroke += 1

                        # If there is at least one shape detected in the file
                        if shapes_detected:
                            print(f'     > Detected shapes in {len(pages_ps.keys())} pages')
                            try:
                                # Read the binary file
                                with open(changed_file, 'rb') as a_note_file:
                                    note_file_binary = a_note_file.read()

                                for a_page, a_page_shapes in pages_ps.items():

                                    # Generates the ideal vector points
                                    process_shapes(a_page_shapes)

                                    for shape_type, pen_strokes in a_page_shapes.items():
                                        a_nfb, completed = replace_note_strokes(note_file_binary, pen_strokes)
                                        if completed:
                                            note_file_binary = a_nfb

                                if DEBUG_MODE:
                                    save_json(changed_file+'_perfect.json', pages_ps)

                                mod_fn = changed_file + '_mod.note'
                                with open(mod_fn, 'wb') as file:
                                    file.write(note_file_binary)

                                pen_strokes_dict, _ = get_pen_strokes_dict(mod_fn)

                                shape_fn = fit_ephemeral_images(mod_fn, pen_strokes_dict)

                                upload_gen_file(
                                    shape_fn, changed_file, hist_dict, internal_prefix,
                                    external_prefix, transfer_mode, device=usb_supernote, root_url=root_url)

                                shape_sn_folder = remove_storage_paths(shape_fn)
                                hist_dict[shape_fn] = shape_sn_folder

                            except Exception as e:
                                print(f'**- Shape detection: {e}')

                    if transfer_mode == 'folder':
                        source_sn_folder = remove_storage_paths(changed_file)
                        hist_dict[destination_fn] = source_sn_folder
                    else:
                        source_sn_folder = hist_dict[changed_file]

                    if AUTO_CONVERT_NOTES:
                        print()
                        print(f'>>> CONVERTING: {basename}')

                        try:     # Exporting
                            # Double reading of the binary but will do for now
                            with open(destination_fn, 'rb') as a_note_file:
                                note_file_binaries = a_note_file.read()
                            retrieved_rec, rec_dict = titles_and_text_from_notes(a_metadata, note_file_binaries)

                            # retrieved_stars, stars_dict = stars_from_notes(a_metadata)
                            # print(f'----stars_dict: {stars_dict}')

                            list_dicts = ['main']
                            ks_dictionary_list = [x[14:] for x in notes_kw_words if x[:14] == 'ks-dictionary-']
                            list_dicts.extend(list(ks_dictionary_list))
                            # For backward compatibility (existing notebooks with keywords but no explicit keyword-settings),
                            # we make an exception and include all keywords so that load_dictionary can just check if a json exists
                            if LOAD_NON_KS_DICTIONARIES:
                                list_dicts.extend(list(keys_set))
                            correction_dict = load_dictionaries(PYSN_DIRECTORY, list_dicts)

                            # Build a dictionary per page of recognized text
                            dict_rect = {}
                            if retrieved_rec:
                                # save_json(os.path.join(changed_file_dir, basename + '_rec.json'),rec_dict)
                                if DEBUG_MODE:
                                    save_json(changed_file.replace('.note', '_rec.json'), rec_dict)
                                for a_key, a_value in rec_dict.items():
                                    a_key_str = str(int(a_key)+1)
                                    if 'elements' in a_value:
                                        elements_list = a_value['elements']
                                        for an_element in elements_list:
                                            if 'words' in an_element:
                                                words_list = an_element['words']
                                                for a_word in words_list:
                                                    if 'bounding-box' in a_word:
                                                        b_b = a_word['bounding-box']
                                                        word_rect = [
                                                            b_b['x'], b_b['y'],
                                                            b_b['x'] + b_b['width'],
                                                            b_b['y'] + b_b['height']]
                                                        word_rect_std = [x*MYSCRIPT_RATIO for x in word_rect]
                                                        word_text = a_word['label']

                                                        if a_key_str in dict_rect:
                                                            dict_rect[a_key_str].append((word_rect_std, word_text))
                                                        else:
                                                            dict_rect[a_key_str] = [(word_rect_std, word_text)]

                                # Get the list of pages (either pages with titles or pages with reconized text)
                                pages_with_text = [x for x in dict_rect.keys()]
                                pages_with_titles = [x for x in notes_dict.keys()]
                                pages_with_text.extend(pages_with_titles)

                                all_relevant_pages_ = list(set(pages_with_text))
                                all_relevant_pages = sorted(all_relevant_pages_, key=lambda x: int(x))

                                # Finding Titles recognized text
                                md_raw = {}
                                # Now parsing all relevant pages
                                for a_note_page in all_relevant_pages:

                                    if a_note_page in dict_rect:                     # Get the list of all words in the current page
                                        rect_text_list_ = dict_rect[a_note_page]
                                        rect_text_list = natural_reading_order(rect_text_list_)
                                    else:
                                        rect_text_list = []

                                    if a_note_page in notes_dict:
                                        # Get the list of all titles in the current page
                                        a_note_page_titles = notes_dict[a_note_page]
                                    else:
                                        a_note_page_titles = []

                                    for a_title in a_note_page_titles:                      # Now parses each title on the page
                                        a_rect = a_title['TITLERECTSTD']                    # Get the title rect
                                        # Build the list of words intersection with a title

                                        title_words_sorted = [x for x in rect_text_list if intersection_area(x[0], a_rect, threshold=0.1)]

                                        # Get the non-titles before the first title and embed them in '%'
                                        if len(title_words_sorted) > 0:
                                            try:
                                                position_x = rect_text_list.index(title_words_sorted[0])
                                                non_titles_to_move = rect_text_list[:position_x]
                                                for xx in non_titles_to_move:
                                                    if a_note_page in md_raw:
                                                        md_raw[a_note_page].append((xx[0], f'%{xx[1]}%', 0))
                                                    else:
                                                        md_raw[a_note_page] = [(xx[0], f'%{xx[1]}%', 0)]
                                            except Exception as e:
                                                print()
                                                print(f'**** Error in parting titles text: {e}')

                                        # Build the list of remaining words
                                        remaining_words = [x for x in rect_text_list if x not in title_words_sorted and x not in non_titles_to_move]
                                        rect_text_list = list(remaining_words)
                                        # Concatenate the list of wors for the current title
                                        title_words_sorted_word_only = [x[1] for x in title_words_sorted]
                                        title_words_concatenated = (' ').join(title_words_sorted_word_only)
                                        a_title['TITLETEXT'] = title_words_concatenated
                                        # Add the title in md_raw
                                        if a_note_page in md_raw:
                                            md_raw[a_note_page].append((a_rect, title_words_concatenated, a_title['TITLERANK']))
                                        else:
                                            md_raw[a_note_page] = [(a_rect, title_words_concatenated, a_title['TITLERANK'])]

                                    # We are done with the titles on the current page, let's add the remaining words to md_raw
                                    # That's a bug right here: we are assuming that titles come first
                                    for item_left in rect_text_list:
                                        if a_note_page in md_raw:
                                            md_raw[a_note_page].append((item_left[0], f'%{item_left[1]}%', 0))
                                        else:
                                            md_raw[a_note_page] = [(item_left[0], f'%{item_left[1]}%', 0)]

                                # Create md_dict from md_raw by sorting
                                if DEBUG_MODE:
                                    save_json(changed_file.replace('.note', '_mdraw.json'), md_raw)

                                md_dict = {}
                                for a_page_md_raw, a_list_md_raw in md_raw.items():
                                    sentences = a_list_md_raw
                                    a_text = '\n'
                                    current_conc = []
                                    for el_md in sentences:
                                        a_rank = el_md[2]
                                        if a_rank != 0:
                                            if len(current_conc) > 0:
                                                a_text += '\n'+' '.join(current_conc) + '\n'

                                            for idx_hashtag in range(a_rank):
                                                a_text += '#'
                                            a_text += ' ' + el_md[1] + '\n'
                                            current_conc = []
                                        else:
                                            current_conc.append(el_md[1])
                                    if len(current_conc) > 0:
                                        a_text += ' '.join(current_conc)

                                    md_dict[a_page_md_raw] = a_text

                                if DEBUG_MODE:
                                    save_json(changed_file.replace('.note', '_mdict.json'), md_dict)

                                    save_json(changed_file.replace('.note', '_titles.json'), notes_dict)

                                md_output = generate_md(md_dict)

                                if correction_dict is not None:
                                    for dict_c_key, dict_c_value in correction_dict.items():
                                        md_output = re.sub(re.escape(dict_c_key), dict_c_value, md_output, flags=re.IGNORECASE)

                                with open(changed_file.replace('.note', '.md'), 'w', encoding='utf-8') as file:
                                    file.write(md_output)

                                # Save the HTML content to a file if boolean ks-embedded style is present
                                inline_css = 'ks-embedded style' in notes_kw_words
                                file_path = changed_file.replace('.note', '.html')
                                css_source_path = os.path.join(PYSN_DIRECTORY, 'styles.css')
                                css_dest_path = os.path.join(LOCAL_PDF_NOTES, 'styles.css')
                                if not os.path.exists(css_dest_path):
                                    shutil.copy(css_source_path, css_dest_path)
                                html_output = generate_html(
                                    md_output, css_dest_path, file_path, inline=inline_css)

                                # Convert to pdf using vector if boolean ks-vector is present
                                vectorize_pdf = 'ks-vector' in notes_kw_words

                                with open(file_path, 'w', encoding='utf-8') as file:
                                    file.write(html_output)
                            # Retrieving ks-color- settings
                            a_palette = None
                            processed = []
                            ks_color_json_list = [f'{x[3:]}.json' for x in notes_kw_words if x[:9] == 'ks-color-']
                            for ks_color_json in ks_color_json_list:
                                try:
                                    a_dict = read_json(os.path.join(PYSN_DIRECTORY, ks_color_json))
                                    for a_key, a_value in a_dict.items():
                                        if a_key in COLOR_PALETTE_NAMES:
                                            idx_color = COLOR_PALETTE_NAMES.index(a_key)
                                            COLOR_PALETTE[idx_color] = a_value
                                            if a_key in processed:
                                                print(f'*** Warning: overriding color {a_key} with {ks_color_json} settings')
                                            else:
                                                processed.append(a_key)
                                    colors = parse_color(','.join(COLOR_PALETTE))
                                    a_palette = sn.color.ColorPalette(sn.color.MODE_RGB, colors)
                                except Exception as e:
                                    print(f'*** Color settings: {e}')

                            # Retrieving ks-copy- settings
                            extra_copy_folders = []
                            ks_copy_json_list = [f'{x[3:]}.json' for x in notes_kw_words if x[:8] == 'ks-copy-']
                            for ks_copy_json in ks_copy_json_list:
                                try:
                                    a_dict = read_json(os.path.join(PYSN_DIRECTORY, ks_copy_json))
                                    for a_key, a_value in a_dict.items():
                                        if a_key != 'pysn-usage':
                                            if a_value not in extra_copy_folders:
                                                extra_copy_folders.append(a_value)
                                except Exception as e:
                                    print(f'*** Extra copy settings: {e}')
                            extra_copy_folders_dict[changed_file] = extra_copy_folders

                            # Retrieving ks-strokes- settings
                            ks_strokes_json_list = [f'{x[3:]}.json' for x in notes_kw_words if x[:11] == 'ks-strokes-']
                            if len(ks_strokes_json_list) > 0:
                                print('>> Applying ks-settings for pen strokes')

                            for ks_strokes_json in ks_strokes_json_list:
                                size_conditions = []
                                size_conditions_not = []
                                color_conditions = []
                                color_conditions_not = []
                                pages_conditions = []
                                pages_conditions_not = []
                                size_output = None
                                color_output = None
                                destination_output = None
                                try:
                                    a_dict = read_json(os.path.join(PYSN_DIRECTORY, ks_strokes_json))

                                    if 'conditions' in a_dict:
                                        dict_cond = a_dict['conditions']
                                        if 'size' in dict_cond:
                                            size_conditions_str = dict_cond['size']
                                            size_conditions = [NEEDLE_POINT_SIZES[str(abs(x))] for x in size_conditions_str if x > 0]
                                            size_conditions_not = [NEEDLE_POINT_SIZES[str(abs(x))] for x in size_conditions_str if x < 0]

                                        if 'color' in dict_cond:
                                            color_conditions_str = dict_cond['color']
                                            color_conditions = [HIGHLIGHTS_COLOR[x][0] for x in color_conditions_str if x[0] != '-']
                                            color_conditions_not = [HIGHLIGHTS_COLOR[x[1:]][0] for x in color_conditions_str if x[0] == '-']

                                        if 'pages' in dict_cond:
                                            pages_conditions_r = dict_cond['pages']
                                            pages_conditions = [x for x in pages_conditions_r if x > 0]
                                            pages_conditions_not = [abs(x) for x in pages_conditions_r if x < 0]

                                    if 'output' in a_dict:
                                        dict_output = a_dict['output']
                                        if 'size' in dict_output:
                                            size_output_d = NEEDLE_POINT_SIZES[str(dict_output['size'])]
                                            size_output = size_output_d.to_bytes(2, byteorder='little')

                                        if 'color' in dict_output:
                                            color_output_d = HIGHLIGHTS_COLOR[dict_output['color']][0]
                                            color_output = color_output_d.to_bytes(1, byteorder='little')

                                    if 'destination' in a_dict:
                                        destination_output = a_dict['destination']

                                    if destination_output:
                                        os.makedirs(destination_output, exist_ok=True)
                                        new_note_fn = os.path.join(destination_output, os.path.basename(changed_file))
                                        shutil.copy2(changed_file, new_note_fn)

                                        # Read .note file binary
                                        with open(new_note_fn, 'rb') as anote_file:
                                            note_file_binary = anote_file.read()

                                        changed_data = False

                                        notebook_json_pages = a_metadata['__pages__']
                                        ps_pge_nb = 1
                                        for a_page_json in notebook_json_pages:

                                            if (ps_pge_nb in pages_conditions) or (pages_conditions == []) and (ps_pge_nb not in pages_conditions_not):

                                                a_totalpath = int(a_page_json['TOTALPATH'])

                                                position_reading = a_totalpath

                                                completed, total_size = read_endian_int_at_position(note_file_binary, position_reading, num_bytes=4, endian='little')
                                                position_reading += 4

                                                completed, nb_strokes = read_endian_int_at_position(note_file_binary, position_reading, num_bytes=4, endian='little')
                                                position_reading += 4

                                                for a_stroke_index in range(0, nb_strokes):

                                                    completed, stroke_size = read_endian_int_at_position(note_file_binary, position_reading, num_bytes=4, endian='little')
                                                    position_reading += 4
                                                    stroke_data = note_file_binary[position_reading: position_reading+stroke_size]
                                                    completed, color_nb = read_endian_int_at_position(stroke_data, 4, num_bytes=1)

                                                    if (color_nb in color_conditions) or (color_conditions == []) and (color_nb not in color_conditions_not):

                                                        completed, stroke_tip_size = read_endian_int_at_position(stroke_data, 8, num_bytes=2)

                                                        if (stroke_tip_size in size_conditions) or (size_conditions == []) and (stroke_tip_size not in size_conditions_not):
                                                            stroke_data_n1 = stroke_data[:4] + color_output + stroke_data[5:]
                                                            stroke_data_n2 = stroke_data_n1[:8] + size_output + stroke_data_n1[10:]
                                                            note_file_binary = note_file_binary[:position_reading] + stroke_data_n2 + note_file_binary[position_reading+stroke_size:]
                                                            changed_data = True

                                                    position_reading += stroke_size

                                            ps_pge_nb += 1

                                        if changed_data:
                                            # Write .note file binary
                                            with open(new_note_fn, 'wb') as file:
                                                file.write(note_file_binary)

                                            if transfer_mode == 'folder':
                                                source_sn_folder = remove_storage_paths(changed_file)
                                                hist_dict[destination_fn] = source_sn_folder
                                            else:
                                                source_sn_folder = hist_dict[changed_file]
                                            source_sn_folder_list = source_sn_folder.split('/')

                                            if transfer_mode == 'usb':  # For transfers via USB

                                                if source_sn_folder_list[0].lower() == 'sdcard':
                                                    upload_base_path = external_prefix
                                                    conversion_note_parent_folder = '/'.join(source_sn_folder_list[1:-1])
                                                else:
                                                    conversion_note_parent_folder = internal_prefix
                                                upload_output_text_converted = os.path.join(conversion_note_parent_folder, source_sn_folder) + '.note'
                                                if os.path.exists(new_note_fn):
                                                    usb_supernote.push(new_note_fn, upload_output_text_converted)

                                            elif transfer_mode == 'webserver':  # For transfers via WIFI
                                                try:
                                                    source_sn_folder_path = ('/').join(source_sn_folder.split('/')[:-1])
                                                    url_export = f'{root_url}/{source_sn_folder_path}'
                                                    rood_folder = source_sn_folder_list[0]
                                                    if rood_folder not in SUPERNOTE_FOLDERS:
                                                        url_export = f'{root_url}{external_prefix}'+('/').join(source_sn_folder_list[1:-1])
                                                    asyncio.run(async_upload(new_note_fn, url_export, f'{basename}.note'))
                                                except Exception as e:
                                                    print(e)
                                                    print(f"  *** Could not upload {f'{basename}.note'}. Is the SN access - Browse mode on? ***")
                                            elif transfer_mode == 'folder':  # For transfers via cloud sync
                                                try:
                                                    dest_note_fn = os.path.join(REMOTE_BASE_PATH, source_sn_folder) + '.note'
                                                    shutil.copy(new_note_fn, dest_note_fn)
                                                except Exception as e:
                                                    print(e)
                                                    print(f"  *** Could not save file {dest_note_fn}. If you have it open, please close and try again.  ***")
                                        else:
                                            print('   > No changes were made')

                                except Exception as e:
                                    print(f'*** Pen strokes settings: {e}')

                            pdf_converter = PdfConverter(pdf_notebook, palette=a_palette)  # Instantiating the note converter

                            data_n = pdf_converter.convert(-1, VECTORIZE_HANDWRITINGS or vectorize_pdf, enable_link=True)  # minus value means converting all pages

                            full_pdf_name = changed_file.replace('.note', '.pdf')

                            full_pdf_name_ = changed_file.replace('.note', '_.pdf')
                            with open(full_pdf_name_, 'wb') as fpdf:
                                fpdf.write(data_n)

                            note_toc = create_note_toc(notes_dict)  # Builds a pdf table of contents

                            note_pdf_doc = fitz.Document(full_pdf_name_)      # Instantiates a pdf from full_pdf_name
                            # Now writing into the extended metadata

                            arefxx, e_metadata = extended_metadata(note_pdf_doc)
                            notes_kw_words_without_settings = [x for x in notes_kw_words if x[:3] != 'ks-']

                            notes_link_dict['source'] = source_sn_folder
                            save_to_extended_metadata(note_pdf_doc, arefxx, 'pysn', notes_link_dict)
                            save_to_extended_metadata(note_pdf_doc, arefxx, 'Author', AUTHOR)
                            save_to_extended_metadata(note_pdf_doc, arefxx, 'Title', source_sn_folder)
                            save_to_extended_metadata(note_pdf_doc, arefxx, 'Producer', PYSN_VERSION)
                            save_to_extended_metadata(note_pdf_doc, arefxx, 'Creator', 'https://gitlab.com/mmujynya/pysn-digest')
                            save_to_extended_metadata(note_pdf_doc, arefxx, 'Keywords', ' | '.join(list(set(notes_kw_words_without_settings))))
                            note_pdf_doc.saveIncr()

                            if note_toc:  # If a TOC was built, attach it to the pdf
                                note_toc.extend(notes_kw_toc)

                            else:
                                note_toc = notes_kw_toc
                            if stars_toc != []:
                                note_toc.extend(stars_toc)
                            if note_toc != []:
                                note_toc = simplify_toc(note_toc)
                                note_pdf_doc.set_toc(note_toc, collapse=1)
                                note_pdf_doc.saveIncr()

                            note_pdf_page = note_pdf_doc[-1]
                            note_pagepix = note_pdf_page.get_pixmap()
                            pdf_ratio_matrix = fitz.Matrix(a=note_pagepix.w/max_horizontal_pixels, d=note_pagepix.h/max_vertical_pixels)
                            note_pdf_matrx = note_pdf_page.rect.torect(note_pagepix.irect)
                            note_pr_matrix = note_pdf_page.rotation_matrix * note_pdf_matrx
                            inv_note_pr_matrix = ~note_pr_matrix

                            if dict_rect != {}:  # IF there is text recognition
                                oc_search = note_pdf_doc.add_ocg("pysn_search", on=True)  # Create an ocg search layer

                                for pdf_page_nb_str, pdf_page_rec_texts in dict_rect.items():

                                    pdf_page_nb_zb = int(pdf_page_nb_str) - 1
                                    note_pdf_page = note_pdf_doc[pdf_page_nb_zb]
                                    note_pagepix = note_pdf_page.get_pixmap()
                                    pdf_ratio_matrix = fitz.Matrix(a=note_pagepix.w/max_horizontal_pixels, d=note_pagepix.h/max_vertical_pixels)

                                    note_pdf_matrx = note_pdf_page.rect.torect(note_pagepix.irect)
                                    note_pr_matrix = note_pdf_page.rotation_matrix * note_pdf_matrx
                                    inv_note_pr_matrix = ~note_pr_matrix

                                    for a_rec_text in pdf_page_rec_texts:

                                        pix_rect = fitz.Rect(a_rec_text[0][:4])*pdf_ratio_matrix

                                        a_text = a_rec_text[1]
                                        if a_text.strip() != '':
                                            pdf_rect = pix_rect*inv_note_pr_matrix
                                            if correction_dict is not None:
                                                for dict_c_key, dict_c_value in correction_dict.items():
                                                    a_text = re.sub(re.escape(dict_c_key), dict_c_value, a_text, flags=re.IGNORECASE)
                                            apoint = fitz.Point((pdf_rect[0], pdf_rect[1]+round((pdf_rect[3] - pdf_rect[1])/2)))
                                            note_pdf_page.insert_text(
                                                apoint, a_text, fontname='cour', fontsize=REC_TEXT_FONT_SIZE, fill_opacity=0.01, oc=oc_search)
                                note_pdf_doc.saveIncr()

                            # Instantiating the mark converter
                            converter = ImageConverter(pdf_notebook, mark=False)

                            # Browse the titles dict
                            note_pdf_page_rect = note_pdf_page.rect

                            a_page_v_position = 80
                            a_page_h_position = 80
                            a_point = fitz.Point([a_page_h_position, a_page_v_position])*pdf_ratio_matrix*inv_note_pr_matrix

                            len_note_pdf_doc = len(note_pdf_doc)
                            if len(notes_dict.keys()) > 0:
                                page = note_pdf_doc.new_page(width=note_pdf_page_rect.width, height=note_pdf_page_rect.height)
                                page_idx = 1
                                link_to_toc_beg = {
                                    "kind": fitz.LINK_GOTO,
                                    "from": fitz.Rect([20, 20, 40, 40]),
                                    "page": len_note_pdf_doc,
                                    "to": a_point}
                                for a_page_nb in range(1, len_note_pdf_doc+1):
                                    a_page_nb_str = str(a_page_nb)

                                    # for a_page_nb_str, a_page_titles_list in notes_dict.items():
                                    # Get the note image at that page
                                    if a_page_nb_str in notes_dict:
                                        a_page_titles_list = notes_dict[a_page_nb_str]
                                        title_page_nb = int(a_page_nb_str)-1
                                        page_bitmap = converter.convert(title_page_nb, vo)
                                        # page_bitmap.save(os.path.join(changed_file_dir, basename + f'_{a_page_nb_str}.png'))   #For debug only
                                        for a_title in a_page_titles_list:  # For each title at in the notebook
                                            a_title_rect = a_title['TITLERECTSTD']
                                            a_title_rect_pdf = fitz.Rect(a_title_rect)*pdf_ratio_matrix*inv_note_pr_matrix
                                            a_title_indent = a_title['TITLERANK']*50
                                            a_title_bitmap = page_bitmap.crop(a_title_rect).convert('RGBA')
                                            dest_rect = fitz.Rect(
                                                [a_page_h_position+a_title_indent, a_page_v_position, a_page_h_position+a_title_indent+a_title_bitmap.width,
                                                 a_page_v_position+a_title_bitmap.height])*pdf_ratio_matrix*inv_note_pr_matrix
                                            bytes_io = io.BytesIO()
                                            a_title_bitmap.save(bytes_io, format='PNG')
                                            if a_page_v_position + a_title_rect[3]-a_title_rect[1] + 40 > max_vertical_pixels:
                                                page = note_pdf_doc.new_page(width=note_pdf_page_rect.width, height=note_pdf_page_rect.height)
                                                a_page_v_position = 20
                                                dest_rect = fitz.Rect(
                                                    [a_page_h_position, a_page_v_position, a_page_h_position+a_title_bitmap.width,
                                                     a_page_v_position+a_title_bitmap.height])*pdf_ratio_matrix*inv_note_pr_matrix
                                                page_idx += 1

                                            page.insert_image(dest_rect, stream=bytes_io)
                                            note_pdf_doc.saveIncr()
                                            link_to_toc = {
                                                "kind": fitz.LINK_GOTO,
                                                "from": a_title_rect_pdf,
                                                "page": len_note_pdf_doc + page_idx-1,
                                                "to": fitz.Point(dest_rect[0], dest_rect[1])}
                                            link_to_title = {
                                                "kind": fitz.LINK_GOTO,
                                                "from": dest_rect,
                                                "page": title_page_nb,
                                                "to": fitz.Point(a_title_rect_pdf[0], a_title_rect_pdf[1])}
                                            page.insert_link(link_to_title)
                                            note_pdf_doc[title_page_nb].insert_link(link_to_toc)
                                            a_page_v_position += a_title_rect[3]-a_title_rect[1] + 40

                                    current_page = note_pdf_doc[a_page_nb-1]
                                    current_page.add_caret_annot(fitz.Point(20, 20))
                                    current_page.insert_link(link_to_toc_beg)

                            note_pdf_doc.save(full_pdf_name)
                            note_pdf_doc.close()

                            # Now exporting to the SN/Folders
                            if UPLOAD_CONVERTED_NOTES:
                                if transfer_mode == 'usb':  # For transfers via USB
                                    try:
                                        a_folder_name_list = EXPORT_FOLDER.split('/')
                                        if a_folder_name_list[0].lower() == 'sdcard':
                                            upload_base_path_c = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:]))
                                        else:
                                            upload_base_path_c = os.path.join(internal_prefix, '/'.join(a_folder_name_list))
                                        destination_path_c = os.path.join(upload_base_path_c, f'{basename}_c.pdf').replace('\\', '/')
                                        # Pushing the converted note to pdf file to the device
                                        usb_supernote.push(full_pdf_name, destination_path_c)
                                    except Exception as e:
                                        print(e)
                                        print(f"  *** Could not push {f'{basename}_c.pdf'} to the Supernote on USB ***")
                                elif transfer_mode == 'webserver':  # For transfers via WIFI
                                    try:
                                        asyncio.run(async_upload(full_pdf_name, url_export, f'{basename}_c.pdf'))
                                    except Exception as e:
                                        print(e)
                                        print(f"  *** Could not upload {f'{basename}_c.pdf'}. Is the SN access - Browse mode on? ***")
                                elif transfer_mode == 'folder':
                                    try:
                                        dest_pdf_merged_fn = os.path.join(remote_export_folder, f'{basename}_c.pdf')
                                        shutil.copy(full_pdf_name, dest_pdf_merged_fn)  # TODO: if no debug mode, just save to the export folder
                                    except Exception as e:
                                        print(e)
                                        print(f'  *** Could not save file: {dest_pdf_merged_fn}. If you have it open, please close and try again. ***')

                        except Exception as e:
                            print()
                            print(f'*** Error exporting converted note: {e}')
                            print(f'    Filename: {destination_fn}')

                # For .mark files, download the file and the corresponding pdf
                elif extension.lower() == '.mark':
                    marks_link_dict = {}
                    marks_kw_jsons = []
                    marks_kw_words = []
                    marks_kw_toc_settings = []
                    marks_kw_toc = []
                    keys_set = set()
                    a_palette = None
                    digestive_requirement_path = DIGESTIBLE_FILTER.strip().lower()
                    digestible_file = True
                    if digestive_requirement_path != '':
                        digestible_file = digestive_requirement_path in os.path.dirname(changed_file).lower()

                    if digestible_file:

                        print()
                        print(f'>> Digesting {os.path.basename(changed_file)} ...')

                        try:

                            changed_file_dir, annotated_fn_ext = os.path.splitext(changed_file_dir)

                            if not os.path.exists(changed_file_dir):
                                os.makedirs(changed_file_dir)

                            destination_fn = os.path.join(changed_file_dir, basename + '.mark')

                            annotated_fn = destination_fn[:-5]

                            # epub_def_settings = extract_epub_display_settings(changed_file[:-5])
                            # print(f'----epub_def_settings:{epub_def_settings}')

                            _, annotated_fn_ext = os.path.splitext(annotated_fn)

                            annotated_is_pdf = annotated_fn_ext.lower() == '.pdf'

                            pdf_path = annotated_fn  # For now, assuming pdf_path can also be epub. TODO: replace this variable name with a general name

                            basename_pdf = os.path.basename(pdf_path)
                            reg_fn_bkp_h = os.path.normpath(os.path.join(os.path.join(LOCAL_PDF_NOTES, EXPORT_FOLDER), basename_pdf))[:-4] + '_h.md'
                            output_text_converted = f'{reg_fn_bkp_h}.note'
                            shutil.copy(changed_file, destination_fn)

                            if not annotated_is_pdf:
                                pass
                                # # # epub_fn = changed_file[:-5]
                                # # # pdf_from_epub_fn = f'{epub_fn}.pdf'
                                # # # print(f'-----pdf_from_epub_fn:{pdf_from_epub_fn}')
                                # # # styles_ = extract_default_styles(epub_fn)
                                # # # print(f'-----styles_:{styles_}')
                                # # # epub = fitz.open(changed_file[:-5])
                                # # # trial_nb = 0
                                # # # target_page_nb = 354
                                # # # found_nb_page = False
                                # # # fontsize_ = 11
                                # # # multiplier_ = 1
                                # # # increasing_ = True
                                # # # last_status = increasing_
                                # # # while (trial_nb < 50) and (not found_nb_page):
                                # # #     epub.layout(fitz.Rect([0, 0, 612, 792]), fontsize=fontsize_)
                                # # #     pdfbytes = epub.convert_to_pdf()
                                # # #     pdf_from_epub = fitz.Document(stream=pdfbytes)
                                # # #     len_ = len(pdf_from_epub)
                                # # #     print(f'---- page nb: {len_}  -  font size: {fontsize_}')
                                # # #     found_nb_page = len_ == target_page_nb
                                # # #     increasing_ = len_ < target_page_nb
                                # # #     if last_status != increasing_:
                                # # #         last_status = increasing_
                                # # #         multiplier_ *= -0.5
                                # # #     fontsize_ += 1*multiplier_
                                # # #     trial_nb += 1
                                # # # pdf_from_epub_fn = f'{changed_file[:-10]}.pdf'
                                # # # pdf_from_epub.save(pdf_from_epub_fn)
                                # # # pdf_path = f'{annotated_fn[:-5]}.pdf'
                                # # # shutil.copy(pdf_from_epub_fn, pdf_path)
                            else:
                                # Copying the associated pdf
                                shutil.copy(changed_file[:-5], pdf_path)

                            # Read the metadata
                            a_metadata = json.loads(extract_metadata(changed_file, destination_fn + '.json'))
                            if '__footer__' in a_metadata:
                                mark_footer = a_metadata['__footer__']
                                if '__keywords__' in mark_footer:
                                    try:
                                        mark_kw_list = mark_footer['__keywords__']
                                        for a_mark_kw_details in mark_kw_list:
                                            a_page_key = a_mark_kw_details['KEYWORDPAGE']
                                            a_mark_kw_details_rect = a_mark_kw_details['KEYWORDRECT']
                                            kw_rect_list = [int(x) for x in a_mark_kw_details_rect.split(',')]
                                            kw_rect_xy = [kw_rect_list[0], kw_rect_list[1],
                                                          kw_rect_list[0]+kw_rect_list[2],
                                                          kw_rect_list[1] + kw_rect_list[3]]

                                            kw_keyword = a_mark_kw_details['KEYWORD']

                                            marks_kw_words.append(kw_keyword)
                                            # if kw_keyword[:4].lower() != 'nss_':
                                            keys_set.add(kw_keyword)
                                            marks_kw_jsons.append(
                                                {
                                                    "page": a_page_key,
                                                    "src_rect": kw_rect_xy,
                                                    "keyword": kw_keyword

                                                })
                                    except Exception as e:
                                        print()
                                        print(f'*** Error: {e}')
                                        print(' Please note that the Supernote takes some time in the background to process keywords and this error may just be temporary')

                                    # Now build a keywords toc
                                    marks_link_dict["keywords"] = marks_kw_jsons
                                    if DEBUG_MODE:
                                        save_json(changed_file.replace('.mark', '_keys.json'), marks_kw_jsons)
                                    marks_kw_jsons_sorted = sorted(marks_kw_jsons, key=lambda x: (x['keyword'], int(x['page'])))
                                    previous_kw = ''
                                    previous_cat = ''
                                    previous_setting = ''
                                    for a_skw in marks_kw_jsons_sorted:
                                        a_kw = a_skw['keyword']
                                        a_pg = a_skw['page']
                                        # Processing keyword-settings
                                        if a_kw[:3] == 'ks-':
                                            try:
                                                a_kw_list = a_kw.split('-')
                                                boolean_setting = len(a_kw_list) == 2
                                                ks_category = a_kw_list[1]
                                                if boolean_setting:
                                                    ks_setting = 'ON'
                                                else:
                                                    ks_setting = a_kw_list[2]
                                                if ks_category == previous_cat:
                                                    if ks_setting == previous_setting:
                                                        marks_kw_toc_settings.append((5, f'  page {a_pg}', int(a_pg)))
                                                    else:
                                                        marks_kw_toc_settings.append((4, ks_setting, int(a_pg), dimmed_dict))
                                                        marks_kw_toc_settings.append((5, f'  page {a_pg}', int(a_pg)))
                                                else:
                                                    marks_kw_toc_settings.append((3, ks_category, int(a_pg), dimmed_dict))
                                                    marks_kw_toc_settings.append((4, ks_setting, int(a_pg), dimmed_dict))
                                                    marks_kw_toc_settings.append((5, f'  page {a_pg}', int(a_pg)))
                                                previous_setting = ks_setting
                                                previous_cat = ks_category
                                            except Exception as e:
                                                print(f'*** Keyword setting: {e}')

                                        elif a_kw == previous_kw:
                                            marks_kw_toc.append((3, f'  page {a_pg}', int(a_pg)))
                                        else:
                                            marks_kw_toc.append((2, a_kw, int(a_pg)))
                                            marks_kw_toc.append((3, f'  page {a_pg}', int(a_pg)))
                                        previous_kw = a_kw
                                    marks_kw_toc_settings.insert(0, (2, 'SETTINGS', -1, dimmed_dict))
                                    marks_kw_toc.extend(marks_kw_toc_settings)
                                    marks_kw_toc.insert(0, (1, 'KEYWORDS', -1, {"color": (1, 0, 0)}))

                                    # Retrieving ks-color- settings
                                    a_palette = None
                                    processed = []
                                    a_color_palette = list(COLOR_PALETTE)
                                    ks_color_json_list = [f'{x[3:]}.json' for x in marks_kw_words if x[:9] == 'ks-color-']
                                    for ks_color_json in ks_color_json_list:
                                        try:
                                            a_dict = read_json(os.path.join(PYSN_DIRECTORY, ks_color_json))
                                            for a_key, a_value in a_dict.items():
                                                if a_key in COLOR_PALETTE_NAMES:
                                                    idx_color = COLOR_PALETTE_NAMES.index(a_key)
                                                    a_color_palette[idx_color] = a_value
                                                    if a_key in processed:
                                                        print(f'*** Warning: overriding color {a_key} with {ks_color_json} settings')
                                                    else:
                                                        processed.append(a_key)
                                            colors = parse_color(','.join(a_color_palette))
                                            a_palette = sn.color.ColorPalette(sn.color.MODE_RGB, colors)
                                        except Exception as e:
                                            print(f'*** Color settings: {e}')

                            # Read and store .mark file binaries
                            with open(destination_fn, 'rb') as amark_file:
                                mark_file_binaries = amark_file.read()

                            # Read and store current annotated pdf highlights.
                            # IMPORTANT:    SN does not store that list as a given marked page attribute, therefore this dictionary
                            #               This is likely because highlights are fundamentally different in the sense that they are not
                            #               strokes per se, but rather pdf attributes.
                            #               As a consequence: when exporting a merged pdf + mark files, remember to process highlights
                            #               that were not part of an marked page in the pdf (we do so in create_digest_pdf)

                            highlights_dict = {}
                            highlighted_pages = {}
                            if '__header__' in a_metadata:
                                a_header = a_metadata['__header__']
                                if 'HIGHLIGHTDETAILS' in a_header:
                                    highlights_dict = a_header['HIGHLIGHTDETAILS']
                                if 'APPLY_EQUIPMENT' in a_header:
                                    file_series = a_header['APPLY_EQUIPMENT']
                                    if file_series in ['N5']:
                                        if NOTEBOOK_DEVICE not in ['N5']:
                                            print(f'**- {NOTEBOOK_DEVICE} device handling a N5 file')
                                    else:
                                        if NOTEBOOK_DEVICE in ['N5']:
                                            print(f'**- {NOTEBOOK_DEVICE} device handling a non Manta file')

                            # Read metadata to extract list of marked pages, their paths and address in the binary file
                            list_pdf_pages, list_bitmap_pages, marked_pdf_pages_paths_list, page_address_dict = marked_pdf_pages(a_metadata)

                            # Loading the mark file
                            marked_notebook = sn.load_notebook(destination_fn)

                            # Instantiating the mark converter
                            converter = ImageConverter(marked_notebook, mark=True)

                            # Determining how many leading zero we will use for naming the temporary files
                            max_digits = len(str(len(list_pdf_pages)))

                            # Initialize the current marked counter (zero based), for the current file
                            mark_counter = 0

                            marked_pages = {}

                            # Stores sha signature of the source pdf
                            current_sha = file_sha(pdf_path)

                            # Creates a pdf document object, using the source pdf file. This object will contain our outputpdf
                            doc = fitz.open(pdf_path)

                            if EXTRACT_HIGHLIGHTS:

                                for hl_key, hl_page_info_list in highlights_dict.items():

                                    # Extracting the highlights, if available and desired
                                    page_h = doc[int(hl_key)]
                                    page_words = []
                                    for a_mupdf_highlight in hl_page_info_list:
                                        if 'mupdfRectList' in a_mupdf_highlight:
                                            mupdf_rect_list = a_mupdf_highlight['mupdfRectList']
                                            for a_mupdf_rect in mupdf_rect_list:
                                                a_clip = fitz.Rect(
                                                    [a_mupdf_rect['x0'], a_mupdf_rect['y0']+HIGHLIGHT_OFFSET,
                                                        a_mupdf_rect['x1'], a_mupdf_rect['y1']-HIGHLIGHT_OFFSET])
                                                mupdf_words = page_h.get_text('words', clip=a_clip, sort=True)
                                                page_words.extend(mupdf_words)
                                    sorted_page_words = sorted(page_words, key=lambda x: (x[1]+x[3], x[0]))

                                    paragraph_list = []
                                    single_par_list = []
                                    if len(sorted_page_words) > 0:
                                        start_y = sorted_page_words[0][1]
                                    else:
                                        start_y = 0

                                    for pymu_element in sorted_page_words:
                                        if pymu_element[1] - start_y > HIGHLIGHT_SEPARATOR:
                                            paragraph_list.append(' '.join(single_par_list))
                                            start_y = pymu_element[1]
                                            single_par_list = []
                                        single_par_list.append(pymu_element[4])
                                    if len(single_par_list) > 0:
                                        paragraph_list.append(' '.join(single_par_list))

                                    if paragraph_list != []:
                                        highlighted_pages[hl_key] = paragraph_list

                                # If there are highlights, export them to exported notes
                                if highlighted_pages != {}:
                                    # Sort the dictionary
                                    highlighted_pages_sorted = {key: highlighted_pages[key] for key in sorted(highlighted_pages, key=int)}
                                    a_h_dir = os.path.dirname(reg_fn_bkp_h)
                                    # Create the directory if it doesn't exist
                                    os.makedirs(a_h_dir, exist_ok=True)
                                    a_h_contents = ''
                                    a_h_contents_for_t2n = ''
                                    for a_page_h, a_value_h in highlighted_pages_sorted.items():
                                        a_h_contents += f'\n\n---\n\n## Page {int(a_page_h)+1}\n\n'
                                        # a_h_contents_for_t2n += f' ^p ^p $Page {int(a_page_h)+1} ^p ^p '
                                        a_h_contents_for_t2n += f'\n\n# $Page {int(a_page_h)+1}\n\n\n'
                                        a_h_contents_for_t2n += '\n\n'.join(a_value_h)
                                        a_h_contents += '\n\n'.join(a_value_h)
                                    with open(reg_fn_bkp_h, 'w', encoding='utf-8') as file:
                                        file.write(a_h_contents)
                                    output_text_converted = f'{reg_fn_bkp_h}.note'

                                    if ascii_ps_list is None:

                                        if NOTEBOOK_DEVICE in ['N5']:
                                            asciiset_fn = DEFAULT_FONTS2
                                        else:
                                            asciiset_fn = DEFAULT_FONTS
                                        # PDF

                                        ascii_ps_list, _ = get_pen_stokes_list_from_table(
                                            asciiset_fn=asciiset_fn, font_name=FONT_NAME)

                                    sn_pdf_fn = os.path.normpath(
                                        changed_file.split(os.path.normpath(os.path.join(local_watched_folder)))[1]).replace(os.sep, '/')[:-5]

                                    text_to_note(
                                        a_h_contents_for_t2n, output_text_converted, ascii_ps_list,
                                        scratio=SCALE_RATIO_TEXT2NOTE, series=NOTEBOOK_DEVICE, sn_pdf_fn=sn_pdf_fn)

                            if AUTO_EXPORT:
                                # If AUTO_EXPORT is set to True, we also instantiate another pdf document, with the same source, but here doc_e
                                # one will be a full merge of our original .pdf and .mark annotation.
                                # TODO: This is a poor programming logic introduced later on the fly, instead of rethinking the
                                #       the refactoring. It clutters the code and badly needs optimization.
                                doc_e = fitz.open(pdf_path)

                            else:
                                # In most what follows we will often check if doc_e exits to know if we have to output
                                # such export pdf (i.e. the conditional statement 'if doc_e:' is equivalent to 'if AUTO_EXPORT:')
                                doc_e = None

                            # To make the processed files non-device specific, PySN stores information about
                            # the rect location of the created shortcuts and the corresponding linked pages
                            # See 'pysn' key and value in the extended metadata dictionary.
                            #
                            # TODO: As apparently even Adobe Pro doesn't see the info, this could potentially
                            #       become a privacy issue if you mod the script and store something else.
                            #       Users and developers should be made aware of this.

                            # Retrieve the extended metadata of the pdf
                            arefxx, e_metadata = extended_metadata(doc)

                            if 'pysn' in e_metadata:
                                pysn_dict = json.loads(e_metadata['pysn'])
                            else:
                                pysn_dict = {}

                            # Initialize the list of pages to delete (when the user has 'marked for deletion' the caret on the left side
                            # of a digested link)
                            pages_to_delete = []

                            # OKAY: This is the main loop where we are parsing all the all the marked pages

                            for a_page in list_pdf_pages:

                                if a_page > len(doc):  # TODO: Not sure if we should keep this. Was initially here when we did not have the logic in place to 'mute' a marked page in the binaries
                                    continue

                                page_bitmap = list_bitmap_pages[mark_counter]

                                print(f"    - page {a_page}")

                                page = doc[a_page-1]

                                # Retrieve highlights of the current marked page
                                hl_key = str(a_page-1)
                                if hl_key in highlights_dict:
                                    hl_page_info = highlights_dict[hl_key]
                                else:
                                    hl_page_info = []

                                # Get picture of a_page of the original pdf
                                if AUTO_EXPORT:
                                    page_e = doc_e[a_page-1]
                                else:
                                    page_e = None

                                # This is the core of the logic and it is ugly programming.
                                # Look into the details of 'find_rects_with_color', but basically what we do is interpreting the 'selecting' and 'erasing' strokes of the user
                                pdic_rect, mark_file_binaries, pages_marked_for_deletion = find_rects_with_color(
                                    pysn_dict, mark_counter, max_digits, doc, page, pdf_path, converter, page_bitmap, mark_file_binaries, annotated_is_pdf=annotated_is_pdf, doc_e=doc_e, page_e=page_e,
                                    selection_rgb=HIGHLIGHTS_COLOR[DIGEST_COLOR], palette=a_palette)

                                pages_to_delete.extend(pages_marked_for_deletion)
                                pdic_rect['highlights'] = hl_page_info

                                marked_pages[str(a_page-1)] = pdic_rect

                                mark_counter += 1

                            # The idea behind the next 2 lines was to see if we could create a hierachy of Table Of Contents (TOC), based on the styles
                            # TODO: Investigate later if this is worth it
                            inventory_fonts_set = inventory_fonts(marked_pages)
                            reverse_hierarchy = create_reverse_title_hierarchy(inventory_fonts_set)

                            toc_info = extract_text_with_titles(marked_pages, reverse_hierarchy)

                            # Create the 'Digested' table of contents ... the bookmarks of the digest location of the pdf (not the summary digest where the notes are taken)
                            last_annot_toc = create_toc(doc, toc_info, 'Digested')

                            if doc_e:
                                last_annot_toc_e = create_toc(doc_e, toc_info, 'Digested')

                            create_digest_pdf(pysn_dict, highlights_dict, current_sha, marked_pages, pdf_path, doc, doc_e=doc_e, embed_summary=True, comment_author=AUTHOR)

                            basename_fn = os.path.basename(pdf_path)[:-4]
                            source_pdf_merged_fn = pdf_path[:-4]+'_.pdf'

                            sorted_annots_toc(doc, last_annot_toc)

                            if doc_e:
                                source_pdf_merged_fn_e = pdf_path[:-4]+'_e.pdf'
                                sorted_annots_toc(doc_e, last_annot_toc_e)

                            # Deleting pages that were marked for deletion
                            page_set_to_delete = tuple(set(pages_to_delete))

                            if len(page_set_to_delete) > 0:
                                # Deleting pdf pages
                                pages_delete_list = [x[1] for x in page_set_to_delete]
                                doc.delete_pages(pages_delete_list)
                                if doc_e:
                                    doc_e.delete_pages(pages_delete_list)

                                # Now let's look into adjusting binaries and pysn dictionary
                                # parsing the list of pages to delete (actually a list of tuples (source and dest))
                                for a_page_to_delete in page_set_to_delete:
                                    page_source = a_page_to_delete[0]
                                    page_dest = a_page_to_delete[1]
                                    a_page_key = f'PAGE{page_dest+1}'
                                    # 'Deleting' (muting) corresponding deleted pages in the .mark binaries
                                    if a_page_key in page_address_dict:
                                        page_address = int(page_address_dict[a_page_key])
                                        mark_file_binaries = mute_page(mark_file_binaries, page_address)

                                    # Updating the pysn_dict for the current page
                                    page_settings_key = str(page_source)
                                    if page_settings_key in pysn_dict:
                                        page_settings = pysn_dict[page_settings_key]
                                        element_to_pop = None
                                        if 's_marker' in page_settings:
                                            page_settings_marks = page_settings['s_marker']
                                            page_settings_marks_len = len(page_settings_marks)
                                            index_list = []
                                            for i in range(page_settings_marks_len):
                                                if len(page_settings_marks[i]) == 7:
                                                    if page_settings_marks[i][6] == page_dest:
                                                        index_list.append(i)

                                        if index_list:
                                            element_to_pop = index_list[0]
                                            if page_settings['selection']:
                                                page_settings['selection'].pop(element_to_pop)
                                            if page_settings['s_marker']:
                                                page_settings['s_marker'].pop(element_to_pop)
                                            if page_settings['summary_hw_zone']:
                                                page_settings['summary_hw_zone'].pop(element_to_pop)

                                            pysn_dict[page_settings_key] = page_settings

                            # Save pysn dict
                            save_to_extended_metadata(doc, arefxx, 'pysn', pysn_dict)
                            save_to_extended_metadata(doc, arefxx, 'Author', AUTHOR)
                            save_to_extended_metadata(doc, arefxx, 'Producer', PYSN_VERSION)
                            save_to_extended_metadata(doc, arefxx, 'Creator', 'https://gitlab.com/mmujynya/pysn-digest')
                            # Saving pdf document
                            doc.save(source_pdf_merged_fn, garbage=4, deflate=True, clean=True)
                            doc.close()

                            if doc_e:
                                save_to_extended_metadata(doc_e, arefxx, 'pysn', pysn_dict)
                                save_to_extended_metadata(doc_e, arefxx, 'Author', AUTHOR)
                                save_to_extended_metadata(doc_e, arefxx, 'Producer', PYSN_VERSION)
                                save_to_extended_metadata(doc_e, arefxx, 'Creator', 'https://gitlab.com/mmujynya/pysn-digest')
                                toc_doc = doc_e.get_toc(simple=False)

                                toc_doc.extend(marks_kw_toc)
                                toc_doc = simplify_toc(toc_doc)
                                doc_e.set_toc(toc_doc)
                                doc_e.save(source_pdf_merged_fn_e, garbage=4, deflate=True, clean=True)
                                doc_e.close()

                            # Removing selection and erasing color TODO: defaults assume also 202 (marker light gray) and 254 (marker white)
                            completed, mark_file_binaries = mute_stroke_color(mark_file_binaries, marked_pdf_pages_paths_list)

                            # Saving .mark binaries
                            with open(destination_fn, 'wb') as file:
                                file.write(mark_file_binaries)

                            # Saving sha signature of newly created pdf files in the registry
                            basename = os.path.basename(source_pdf_merged_fn)
                            reg_fn = os.path.normpath(os.path.join(os.path.join(local_watched_folder, SN_IO_UPLOAD_FOLDER), basename))
                            reg_fn_bkp = reg_fn.replace(local_watched_folder, LOCAL_BKP_NOTES)

                            pdf_sha = file_sha(source_pdf_merged_fn)
                            if pdf_sha not in registry:
                                file_mod = get_last_modified_date(source_pdf_merged_fn)
                                # key values for pdf_sha (once uploaded to SN)
                                k_fn_local_pdf_sha = pdf_sha
                                v_fn_local_pdf_sha = {
                                    "type": "sha",
                                    "filenames": [reg_fn],
                                    "last_check": file_mod,
                                    "modified": file_mod}
                                k_pdf_sha = reg_fn
                                v_pdf_sha = {
                                    "type": "file",
                                    "sha": pdf_sha,
                                    "last_check": file_mod,
                                    "modified": file_mod}

                            # Cache the pdf
                            shutil.copy(source_pdf_merged_fn, os.path.join(PDF_CACHED, f'{basename_fn}_.pdf'))

                            # Saving sha signature of new .mark binary
                            mark_sha = file_sha(destination_fn)
                            k_mark_sha = reg_fn+'.mark'

                            if mark_sha not in registry:
                                file_mod = get_last_modified_date(destination_fn)
                                # key values for registry mark_sha (once uploaded to SN)
                                k_fn_local_mark_sha = mark_sha
                                v_fn_local_mark_sha = {
                                    "type": "sha",
                                    "filenames": [reg_fn+'.mark'],
                                    "last_check": file_mod,
                                    "modified": file_mod}

                                v_mark_sha = {
                                    "type": "file",
                                    "sha": mark_sha,
                                    "last_check": file_mod,
                                    "modified": file_mod}

                            # Check if need to save full pdf export file in the registry
                            if AUTO_EXPORT:
                                # Saving sha signature of newly created pdf files in the registry
                                pdf_sha_e = file_sha(source_pdf_merged_fn_e)
                                file_mod = get_last_modified_date(source_pdf_merged_fn_e)
                                upload_base_path_e = os.path.normpath(os.path.join(os.path.join(SN_VAULT, SN_IO_FOLDER), EXPORT_FOLDER))
                                destination_path_e = os.path.normpath(os.path.join(upload_base_path_e, f'{basename_fn}_e.pdf'))

                                if pdf_sha_e not in registry:

                                    # key values for pdf_sha_e (once uploaded to SN)
                                    k_fn_local_pdf_sha_e = pdf_sha_e
                                    v_fn_local_pdf_sha_e = {
                                        "type": "sha",
                                        "filenames": [destination_path_e],
                                        "last_check": file_mod,
                                        "modified": file_mod}
                                    k_pdf_sha_e = destination_path_e
                                    v_pdf_sha_e = {
                                        "type": "file",
                                        "sha": pdf_sha_e,
                                        "last_check": file_mod,
                                        "modified": file_mod}

                                shutil.copy(source_pdf_merged_fn_e, os.path.join(PDF_CACHED, f'{basename_fn}_e.pdf'))

                                # Export the merged digest to the directory of exported_notes
                                reg_fn_bkp_e = os.path.normpath(os.path.join(os.path.join(LOCAL_BKP_NOTES, EXPORT_FOLDER), basename))[:-4] + 'e.pdf'
                                copy_file_with_directories(source_pdf_merged_fn_e, reg_fn_bkp_e)

                                # We need to remove reg_fn_bkp_e from hist_dict to avoid that old versions in the Supernote overwrite the new digested file
                                reg_fn_bkp_e2 = reg_fn_bkp_e.replace(LOCAL_BKP_NOTES, local_watched_folder)
                                if reg_fn_bkp_e2 in hist_dict:
                                    del hist_dict[reg_fn_bkp_e2]

                                for a_folder in extra_copy_folders:
                                    copy_file_with_directories(source_pdf_merged_fn_e, os.path.join(a_folder, f'{basename_fn}_e.pdf'))

                            # Now exporting to the SN/Folders

                            if transfer_mode == 'usb':  # For transfers via USB

                                try:
                                    destination_path = os.path.join(os.path.join(upload_base_path, SN_IO_UPLOAD_FOLDER), f'{basename_fn}_.pdf').replace('\\', '/')
                                    # Pushing the pdf file to the device
                                    usb_supernote.push(source_pdf_merged_fn, destination_path)

                                    # Performing "backup" of pushed file
                                    copy_file_with_directories(source_pdf_merged_fn, reg_fn_bkp)

                                    # Saving in registry the pdf sha
                                    if pdf_sha not in registry:

                                        registry[k_fn_local_pdf_sha] = v_fn_local_pdf_sha
                                        registry[k_pdf_sha] = v_pdf_sha

                                    if AUTO_EXPORT:
                                        a_folder_name_list = EXPORT_FOLDER.split('/')
                                        if a_folder_name_list[0].lower() == 'sdcard':
                                            upload_base_path_e = os.path.join(external_prefix, '/'.join(a_folder_name_list[1:]))
                                        else:
                                            upload_base_path_e = os.path.join(internal_prefix, '/'.join(a_folder_name_list))

                                        destination_path_e = os.path.join(upload_base_path_e, f'{basename_fn}_e.pdf').replace('\\', '/')
                                        # Pushing the full pdf export file to the device

                                        usb_supernote.push(source_pdf_merged_fn_e, destination_path_e)
                                        # Performing "backup" of pushed file

                                        # Push highlights exports
                                        if highlighted_pages != {}:
                                            if os.path.exists(reg_fn_bkp_h):
                                                reg_fn_bkp_h_txt = os.path.join(upload_base_path_e, f'{basename_fn}_h.txt').replace('\\', '/')
                                                usb_supernote.push(reg_fn_bkp_h, reg_fn_bkp_h_txt)
                                            if os.path.exists(output_text_converted):
                                                reg_fn_bkp_h_txt_conv = os.path.join(upload_base_path_e, f'{basename_fn}_h.md.note').replace('\\', '/')
                                                usb_supernote.push(output_text_converted, reg_fn_bkp_h_txt_conv)

                                        if k_pdf_sha_e not in registry:
                                            registry[k_fn_local_pdf_sha_e] = v_fn_local_pdf_sha_e
                                            registry[k_pdf_sha_e] = v_pdf_sha_e

                                    # In superhack mode, push the .mark file
                                    if SUPER_HACK_MODE:
                                        print()
                                        mark_source_fn = f'{source_pdf_merged_fn[:-5]}.pdf.mark'
                                        mark_dest_fn = f'{destination_path}.mark'
                                        usb_supernote.push(mark_source_fn, mark_dest_fn)
                                        copy_file_with_directories(mark_source_fn, reg_fn_bkp+'.mark')

                                        # Saving in registry the mark sha
                                        if mark_sha not in registry:
                                            registry[k_fn_local_mark_sha] = v_fn_local_mark_sha
                                            registry[k_mark_sha] = v_mark_sha

                                except Exception as e:
                                    print(e)
                                    print(f"  *** Could not push {f'{basename_fn}_.pdf'} to the Supernote on USB ***")

                            elif transfer_mode == 'webserver':  # For transfers via WIFI
                                try:
                                    asyncio.run(async_upload(source_pdf_merged_fn, url_upload, f'{basename_fn}_.pdf'))
                                    asyncio.run(async_upload(f'{source_pdf_merged_fn[:-5]}.pdf.mark', url_upload, f'{basename_fn}_.pdf.mark'))
                                    # We can't easily do a backup of pushed files like in the USB connection because no file can be ovewritten
                                    # using 'browse & access': the SN auto-increments a postfix. If we want to implement this, it would
                                    # require anticipating the name of the uploaded file. not too difficult, but for the time being, we'll let the
                                    # backup catchup at subsequent script execution.
                                    if AUTO_EXPORT:
                                        asyncio.run(async_upload(source_pdf_merged_fn_e, url_export, f'{basename_fn}_e.pdf'))
                                        # upload highlights exports
                                        if highlighted_pages != {}:
                                            if os.path.exists(reg_fn_bkp_h):
                                                asyncio.run(async_upload(reg_fn_bkp_h, url_export, f'{basename_fn}_h.txt'))
                                            if os.path.exists(output_text_converted):
                                                asyncio.run(async_upload(output_text_converted, url_export, f'{basename_fn}_h.md.note'))

                                except Exception as e:
                                    print(e)
                                    print(f"  *** Could not upload {f'{basename_fn}<_e>.pdf'}. Is the SN access - Browse mode on? ***")
                                    exit(1)

                            elif transfer_mode == 'folder':

                                try:
                                    dest_pdf_merged_fn = os.path.join(remote_upload_folder, f'{basename_fn}_.pdf')
                                    shutil.copy(source_pdf_merged_fn, dest_pdf_merged_fn)  # TODO: if no debug mode, just save to the export folder
                                    shutil.copy(source_pdf_merged_fn[:-5]+'.pdf.mark', dest_pdf_merged_fn+'.mark')  # TODO: if no debug mode, just save to the export folder
                                    if pdf_sha not in registry:
                                        registry[k_fn_local_pdf_sha] = v_fn_local_pdf_sha
                                        registry[k_pdf_sha] = v_pdf_sha
                                    # Saving in registry the mark sha
                                    if mark_sha not in registry:
                                        registry[k_fn_local_mark_sha] = v_fn_local_mark_sha
                                        registry[k_mark_sha] = v_mark_sha
                                    if AUTO_EXPORT:
                                        upload_base_path_e = os.path.normpath(os.path.join(LOCAL_PDF_NOTES, EXPORT_FOLDER))
                                        destination_path_e = os.path.normpath(os.path.join(upload_base_path_e, f'{basename_fn}_e.pdf'))

                                        copy_file_with_directories(source_pdf_merged_fn[:-4]+'e.pdf', destination_path_e)

                                        destination_path_e_remote = os.path.normpath(os.path.join(os.path.join(REMOTE_BASE_PATH, EXPORT_FOLDER), f'{basename_fn}_e.pdf'))
                                        copy_file_with_directories(source_pdf_merged_fn[:-4]+'e.pdf', destination_path_e_remote)
                                        # upload highlights exports
                                        if highlighted_pages != {}:
                                            if os.path.exists(reg_fn_bkp_h):
                                                reg_fn_bkp_h_txt = destination_path_e_remote[:-5] + 'h.txt'
                                                copy_file_with_directories(reg_fn_bkp_h, reg_fn_bkp_h_txt)
                                            if os.path.exists(output_text_converted):
                                                output_text_converted_note = destination_path_e_remote[:-5] + 'h.md.note'
                                                copy_file_with_directories(output_text_converted, output_text_converted_note)

                                        if pdf_sha_e not in registry:
                                            registry[k_fn_local_pdf_sha_e] = v_fn_local_pdf_sha_e
                                            registry[k_pdf_sha_e] = v_pdf_sha_e
                                except Exception as e:
                                    print()
                                    print(f'*** Error: {e}')
                                    print(f'    Could not save file: {dest_pdf_merged_fn}. If you have it open, please close and try again.')
                        except Exception as e:
                            print()
                            print(f'*** Error in Main: {e}')
                        print()

                # If the file is eiter Word, TRext or Markdown
                elif extension.lower() in ['.docx', '.txt', '.md']:

                    upload_output_text_converted = ''

                    # Determine if the filepath meets conversion to notebook requirement (text2notes_file)
                    # This is filtering OUT (if the filter is empty, ALL files will be converted)
                    text2notes_requirement_path = TO_PEN_STROKES_FILTER.strip().lower()
                    text2notes_file = True
                    if text2notes_requirement_path != '':
                        text2notes_file = text2notes_requirement_path in os.path.dirname(changed_file).lower()

                    # If conversion criteria is met
                    if text2notes_file:

                        print()
                        print(f'>> Converting doc to notebook {os.path.basename(changed_file)} ...')
                        try:
                            # initialize text2notes_text, the content to be converted
                            text2notes_text = ''

                            # If docx document, convert first to markdown then copy content into text2notes_text
                            if extension.lower() == '.docx':
                                text2notes_text = docx_to_markdown(changed_file)
                                mkd_file_fn = changed_file+'_.md'
                                with open(mkd_file_fn, 'w', encoding='utf-8') as mkd_file:
                                    mkd_file.write(text2notes_text)

                            # If text or makdown, read file content into text2notes_text
                            elif extension.lower() in ['.md', '.txt']:
                                with open(changed_file, 'r', encoding='utf-8') as text_file:
                                    text2notes_text = text_file.read()

                            if text2notes_text != '':

                                # Create name of output notebook file: output_text_converted
                                basename_doc = os.path.basename(changed_file)
                                output_text_converted = os.path.normpath(os.path.join(os.path.join(LOCAL_PDF_NOTES, EXPORT_FOLDER), basename_doc)) + '.note'

                                # if no ascii table set has been loaded, load it once
                                # The ascii table notebook is distinct for Manta
                                if ascii_ps_list is None:
                                    if NOTEBOOK_DEVICE in ['N5']:
                                        asciiset_fn = DEFAULT_FONTS2
                                    else:
                                        asciiset_fn = DEFAULT_FONTS

                                    # One time load of the fonts table
                                    ascii_ps_list, _ = get_pen_stokes_list_from_table(
                                        asciiset_fn=asciiset_fn, font_name=FONT_NAME)

                                os.makedirs(os.path.dirname(output_text_converted), exist_ok=True)  # Create the directory if it doesn't exist
                                text_to_note(
                                    text2notes_text, output_text_converted, ascii_ps_list,
                                    scratio=SCALE_RATIO_TEXT2NOTE, series=NOTEBOOK_DEVICE)

                                if transfer_mode == 'folder':
                                    source_sn_folder = remove_storage_paths(changed_file)
                                    hist_dict[output_text_converted] = source_sn_folder
                                else:
                                    source_sn_folder = hist_dict[changed_file]

                                source_sn_folder_list = source_sn_folder.split('/')

                                if transfer_mode == 'usb':  # For transfers via USB
                                    if source_sn_folder_list[0].lower() == 'sdcard':
                                        upload_base_path = external_prefix
                                        conversion_note_parent_folder = '/'.join(source_sn_folder_list[1:-1])
                                    else:
                                        conversion_note_parent_folder = internal_prefix
                                    upload_output_text_converted = os.path.join(conversion_note_parent_folder, source_sn_folder) + '.note'
                                    if os.path.exists(output_text_converted):

                                        usb_supernote.push(output_text_converted, upload_output_text_converted)
                                elif transfer_mode == 'webserver':  # For transfers via WIFI
                                    try:
                                        source_sn_folder_path = ('/').join(source_sn_folder.split('/')[:-1])
                                        url_export = f'{root_url}/{source_sn_folder_path}'
                                        rood_folder = source_sn_folder_list[0]
                                        if rood_folder not in SUPERNOTE_FOLDERS:
                                            url_export = f'{root_url}{external_prefix}'+('/').join(source_sn_folder_list[1:-1])
                                        asyncio.run(async_upload(output_text_converted, url_export, f'{basename}.note'))
                                    except Exception as e:
                                        print(e)
                                        print(f"  *** Could not upload {f'{basename}.note'}. Is the SN access - Browse mode on? ***")
                                elif transfer_mode == 'folder':  # For transfers via cloud sync
                                    try:
                                        dest_note_fn = os.path.join(REMOTE_BASE_PATH, source_sn_folder) + '.note'
                                        shutil.copy(output_text_converted, dest_note_fn)
                                    except Exception as e:
                                        print(e)
                                        print(f"  *** Could not save file {dest_note_fn}. If you have it open, please close and try again.  ***")

                        except Exception as e:
                            print(f'**** Conversion of doc: {e}')
                            exit(1)

                elif extension.lower() in ['.pdf']:

                    import_requirement_path = TO_NOTES_FILTER.strip().lower()

                    import_file = True
                    if import_requirement_path != '':
                        import_file = import_requirement_path in os.path.dirname(changed_file).lower()
                    if import_file:

                        print()
                        print(f'>>> IMPORTING: {os.path.basename(changed_file)} ...')
                        try:
                            basename_doc = os.path.basename(changed_file)
                            output_file_converted = os.path.normpath(os.path.join(os.path.join(LOCAL_PDF_NOTES, EXPORT_FOLDER), basename_doc)) + '.note'

                            os.makedirs(os.path.dirname(output_file_converted), exist_ok=True)  # Create the directory if it doesn't exist
                            print(f'>>>>> series: {NOTEBOOK_DEVICE}')
                            import_pics(changed_file, output_file_converted, series=NOTEBOOK_DEVICE)

                            if transfer_mode == 'folder':
                                source_sn_folder = remove_storage_paths(changed_file)
                                hist_dict[output_file_converted] = source_sn_folder
                            else:
                                source_sn_folder = hist_dict[changed_file]

                            source_sn_folder_list = source_sn_folder.split('/')

                            if transfer_mode == 'usb':  # For transfers via USB
                                if source_sn_folder_list[0].lower() == 'sdcard':
                                    upload_base_path = external_prefix
                                    conversion_note_parent_folder = '/'.join(source_sn_folder_list[1:-1])
                                else:
                                    conversion_note_parent_folder = internal_prefix
                                upload_output_file_converted = os.path.join(conversion_note_parent_folder, source_sn_folder) + '.note'
                                if os.path.exists(output_file_converted):
                                    usb_supernote.push(output_file_converted, upload_output_file_converted)
                            elif transfer_mode == 'webserver':  # For transfers via WIFI
                                try:
                                    source_sn_folder_path = ('/').join(source_sn_folder.split('/')[:-1])
                                    url_export = f'{root_url}/{source_sn_folder_path}'
                                    rood_folder = source_sn_folder_list[0]
                                    if rood_folder not in SUPERNOTE_FOLDERS:
                                        url_export = f'{root_url}{external_prefix}'+('/').join(source_sn_folder_list[1:-1])
                                    asyncio.run(async_upload(output_file_converted, url_export, f'{basename}.note'))
                                except Exception as e:
                                    print(e)
                                    print(f"  *** Could not upload {f'{basename}.note'}. Is the SN access - Browse mode on? ***")
                            elif transfer_mode == 'folder':  # For transfers via cloud sync
                                try:
                                    dest_note_fn = os.path.join(REMOTE_BASE_PATH, source_sn_folder) + '.note'
                                    shutil.copy(output_file_converted, dest_note_fn)
                                except Exception as e:
                                    print(e)
                                    print(f"  *** Could not save file {dest_note_fn}. If you have it open, please close and try again.  ***")

                        except Exception as e:
                            print(f'**- Import of pdf: {e}')
                if DEBUG_MODE:
                    input("Press Enter to continue (debug mode is ON)...")
            # Replicating SN File Structure locally
            print()
            print(f'>>> Replicating SN file structure ({len(hist_dict.items())} files)...')
            print()

            try:
                for a_file_fname, a_dest in hist_dict.items():
                    bname, an_ext = os.path.splitext(a_file_fname)
                    if os.path.exists(a_file_fname):
                        split_path = os.path.split(a_dest)
                        a_note_basename = os.path.basename(a_file_fname)

                        new_dir_note = unquote(os.path.normpath(os.path.join(LOCAL_BKP_NOTES, os.sep.join(split_path[:-1]))))
                        new_dir_pdf = unquote(os.path.normpath(os.path.join(LOCAL_PDF_NOTES, os.sep.join(split_path[:-1]))))

                        if not os.path.exists(new_dir_note):
                            os.makedirs(new_dir_note)
                            # print(f'-----new_dir_note:{new_dir_note}')

                        if not os.path.exists(new_dir_pdf):
                            os.makedirs(new_dir_pdf)

                        # moving .note files and converted to pdf, md, html files
                        if an_ext.lower() == '.note':

                            s_tree_note = a_file_fname
                            d_tree_note = os.path.join(new_dir_note, a_note_basename)
                            d_tree_note = re.sub(r'\(pysnv([^.]*)\)', '', d_tree_note)

                            if os.path.exists(s_tree_note):
                                shutil.move(s_tree_note, d_tree_note)
                            pdf_basename = os.path.basename(a_file_fname)[:-5] + '.pdf'
                            s_tree_pdf = s_tree_note.replace('.note', '.pdf')
                            d_tree_pdf = os.path.join(new_dir_pdf, pdf_basename)
                            d_tree_pdf = re.sub(r'\(pysnv([^.]*)\)', '', d_tree_pdf)

                            if os.path.exists(s_tree_pdf):

                                if a_file_fname in extra_copy_folders_dict:
                                    extra_copy_folders = extra_copy_folders_dict[a_file_fname]
                                    extra_copy_folders_dict_final[d_tree_pdf] = extra_copy_folders

                                shutil.move(s_tree_pdf, d_tree_pdf)

                            s_tree_md = s_tree_pdf.replace('.pdf', '.md')
                            d_tree_md = d_tree_pdf.replace('.pdf', '.md')
                            if EXPORT_MD and os.path.exists(s_tree_md):
                                shutil.move(s_tree_md, d_tree_md)

                            s_tree_html = s_tree_md.replace('.md', '.html')
                            d_tree_html = d_tree_pdf.replace('.pdf', '.html')
                            if EXPORT_HTML and os.path.exists(s_tree_html):
                                shutil.move(s_tree_html, d_tree_html)

                        else:
                            if os.path.exists(a_file_fname):
                                if an_ext.lower() == '.pdf':
                                    dest_pdf = os.path.normpath(os.path.join(new_dir_pdf, os.path.basename(a_file_fname)))
                                    shutil.copy(a_file_fname, unquote(dest_pdf))
                                dest_fn = os.path.normpath(os.path.join(new_dir_note, os.path.basename(a_file_fname)))
                                shutil.move(a_file_fname, unquote(dest_fn))

            except Exception as e:
                print()
                print(f'*** Error: {e}')
                print('     Replication of SN file structure aborted')

            # Remove all temporary files
            if not DEBUG_MODE:
                try:
                    shutil.rmtree(temp_work_directory)
                except Exception as e:
                    print()
                    print(f'*** Error: {e}')
                    print(f'Cannot remove folder: {temp_work_directory}')

        # Save the registry. We save it only at the end so that any failure to complete will allow reprocessing the files
        save_json(os.path.join(SN_VAULT, SN_REGISTRY_FN), registry)
        usettings_fn = os.path.join(SN_VAULT, USER_SETTINGS_FN)
        usettings = read_json(usettings_fn)
        usettings['lowpriority'] = False
        save_json(usettings_fn, usettings)
        list_pdfs = find_pdf_or_else(LOCAL_PDF_NOTES)

        # Initialize complementary download list
        needed_files = {}

        # Browse the list of all pdf in the exported folder
        internal_prefix = '/storage/emulated/0/'
        external_prefix = ''
        nb_pdfs_to_link = len(list_pdfs)
        if nb_pdfs_to_link > 0:
            print()
            links_reg_dict = {}
            print(f'>>> Setting links for {nb_pdfs_to_link} pdf(s) ...')

        for a_pdf in list_pdfs:
            try:
                a_doc = fitz.open(a_pdf)  # Open a pdf

                arefxx, e_metadata = extended_metadata(a_doc)   # Read its extended metadata
                added_links = False
                if 'pysn' in e_metadata:
                    pysn_dict = json.loads(e_metadata['pysn'])  # Retrieve the 'pysn' dictionary

                    if 'links' in pysn_dict:
                        p_links = pysn_dict['links']  # Retrieve the list of links
                        for a_link in p_links:  # Parse the list

                            if 'tgt_filename' in a_link:
                                tgt_filename = a_link['tgt_filename']  # Get the target filename
                                link_type = a_link['link_type']
                                tgt_page = max(0, int(a_link['tgt_page'])-1)
                                page = int(a_link['page'])-1  # Retrieve the source (not the target) page
                                a_page = a_doc[page]

                                if link_type in ["0", "1", "2", "3", "4"]:

                                    l_pagepix = a_page.get_pixmap()
                                    l_matrix = fitz.Matrix(a=l_pagepix.w/max_horizontal_pixels, d=l_pagepix.h/max_vertical_pixels)
                                    l_matrx = a_page.rect.torect(l_pagepix.irect)
                                    l_pr_matrix = a_page.rotation_matrix * l_matrx
                                    inv_l_pr_matrix = ~l_pr_matrix
                                    src_rect = fitz.Rect(a_link['src_rect'])*l_matrix*inv_l_pr_matrix  # Retrieve coordinates of the source link

                                    tgt_full_name = os.path.join(
                                        LOCAL_PDF_NOTES, convert_android_path(tgt_filename)).replace('.note', '.pdf')  # Assumed full path of the target filename as a pdf
                                    if os.name == 'nt':
                                        tgt_full_name = tgt_full_name.replace('\\', '\\\\')

                                    if os.path.exists(tgt_full_name):

                                        link_to_file = {
                                            "kind": fitz.LINK_GOTOR,
                                            "from": src_rect,
                                            "page": tgt_page,
                                            "file": tgt_full_name}

                                        a_page.clean_contents()
                                        a_page.insert_link(link_to_file)
                                        added_links = True

                                        # Now bulding the dictionaries of links pointing to a note
                                        n_tgt_full_name = os.path.normpath(tgt_full_name)
                                        n_a_pdf = os.path.normpath(a_pdf)
                                        n_link = (tgt_page, n_a_pdf, page)
                                        if n_tgt_full_name in links_reg_dict:
                                            a_list_pointers = links_reg_dict[n_tgt_full_name]
                                            if n_link not in a_list_pointers:
                                                symetrical_link = (n_link[2], n_link[1], n_link[0])
                                                if symetrical_link in a_list_pointers:
                                                    a_list_pointers = [x for x in a_list_pointers if x != symetrical_link]
                                                    if a_list_pointers == []:
                                                        del links_reg_dict[n_tgt_full_name]
                                                    else:
                                                        links_reg_dict[n_tgt_full_name] = a_list_pointers
                                                        # links_reg_dict[n_tgt_full_name] = build_dict_backlinks(a_list_pointers)
                                                else:
                                                    a_list_pointers.append(n_link)
                                        else:
                                            links_reg_dict[n_tgt_full_name] = [n_link]
                                            # links_reg_dict[n_tgt_full_name] = build_dict_backlinks([n_link])

                                    elif not ('http://' in tgt_filename or 'https://' in tgt_filename):
                                        # The target pdf version is not found locally. Add the corresponding note to the list of files to download and convert
                                        if a_pdf in needed_files:
                                            if (page+1, tgt_filename) not in needed_files[a_pdf]:
                                                needed_files[a_pdf].append((page + 1, tgt_filename))
                                            else:
                                                needed_files[a_pdf] = [(page + 1, tgt_filename)]

                if added_links:
                    # This is an ugly workaround to save while saveIncr was required. TODO: Research and fix
                    try:
                        t_name = a_pdf+'.r'
                        a_doc.save(t_name)
                        a_doc.close()
                        os.remove(a_pdf)
                        os.rename(t_name, a_pdf)

                    except Exception as e:
                        print(e)
                        print(f'*** Cannot save file: {a_pdf}. If you had it open, please close and launch the script again')

            except Exception as e:
                print()
                print(f'*** Setting links: {e}')
                print(f'    Filename: {a_pdf}')
            finally:
                if not a_doc.is_closed:
                    a_doc.close()

        nb_pdfs_to_link_back = len(links_reg_dict)

        if nb_pdfs_to_link_back > 0:
            print()
            print(f'>>> Setting back links for {nb_pdfs_to_link_back} pdf(s) ...')
        lrd_bottom = max_vertical_pixels - 60
        back_links_area = [20, lrd_bottom, max_horizontal_pixels, lrd_bottom + 30]
        for lrd_k, lrd_v in links_reg_dict.items():
            lrd_doc = fitz.open(lrd_k)

            nb_links = len(lrd_v)
            lrd_item_width = max_horizontal_pixels/nb_links
            lrd_v_dict = build_dict_backlinks(lrd_v)
            for lrd_v_dict_key, lrd_v_dict_value in lrd_v_dict.items():
                page_source = lrd_doc[lrd_v_dict_key]

                l_pagepix = page_source.get_pixmap()
                l_matrix = fitz.Matrix(a=l_pagepix.w/max_horizontal_pixels, d=l_pagepix.h/max_vertical_pixels)
                l_matrx = page_source.rect.torect(l_pagepix.irect)
                l_pr_matrix = page_source.rotation_matrix * l_matrx
                inv_l_pr_matrix = ~l_pr_matrix

                page_back_links_area = fitz.Rect(back_links_area)*l_matrix*inv_l_pr_matrix

                existing_links = page_source.get_links()
                for a_link in existing_links:
                    if 'file' in a_link.keys():
                        if a_link['from'].intersects(page_back_links_area):
                            page_source.delete_link(a_link)
                annots = page_source.annots(types=[fitz.PDF_ANNOT_FREE_TEXT])
                for an_annot in annots:
                    if an_annot.rect.intersects(page_back_links_area):
                        page_source.delete_annot(an_annot)

                a_link_idx = 0
                for lrd_v_dict_value_item in lrd_v_dict_value:
                    lrd_item_file = lrd_v_dict_value_item[0]
                    lrd_item_file_short_ = os.path.basename(lrd_item_file)
                    lrd_item_file_short, _ = os.path.splitext(lrd_item_file_short_)
                    lrd_item_page_dest = lrd_v_dict_value_item[1]

                    lrd_item_x0 = a_link_idx*lrd_item_width+20
                    lrd_item_x1 = (a_link_idx+1) * lrd_item_width
                    lrd_item_y0 = lrd_bottom
                    lrd_item_y1 = lrd_bottom + 30

                    lrd_item_rect = fitz.Rect((lrd_item_x0, lrd_item_y0, lrd_item_x1, lrd_item_y1))*l_matrix*inv_l_pr_matrix  # Retrieve coordinates of the source link

                    # page_source.insert_textbox(lrd_item_rect, lrd_item_file_short, fontsize=8, color=(0, 0.0, 1.0))
                    page_source.add_freetext_annot(lrd_item_rect, lrd_item_file_short, fontsize=BACK_LINK_FONT_SIZE, text_color=(0, 0.0, 1.0))
                    if os.name == 'nt':
                        lrd_item_file = lrd_item_file.replace('\\', '\\\\')
                    link_to_file = {
                        "kind": fitz.LINK_GOTOR,
                        "from": lrd_item_rect,
                        "page": lrd_item_page_dest,
                        "file": lrd_item_file}
                    page_source.clean_contents()
                    page_source.insert_link(link_to_file)

                    a_link_idx += 1
            t_name = lrd_k+'.p'
            lrd_doc.save(t_name)
            lrd_doc.close()
            os.remove(lrd_k)
            os.rename(t_name, lrd_k)

        missing_links_fn = os.path.join(SN_VAULT, 'missing_links.json')
        missing_links_nb = len(needed_files.items())
        save_json(missing_links_fn, needed_files)
        if missing_links_nb > 0:
            print('')
            print(f'*** Warning: PySN was unable to set links for {missing_links_nb} file(s) ***')
            print()
            for incomplete_pdf, missing_pages in needed_files.items():
                print(f'     {incomplete_pdf}')
                for a_link in missing_pages:
                    print(f'     - page {a_link[0]}: {a_link[1]}')
                print()

    finally:
        # Kill ADB if it was instantiated by PySN
        if transfer_mode == 'usb':
            if platform.system() == 'Windows':
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb.exe')
            else:
                ADB_PATH = os.path.join(ADB_FOLDER, 'adb')
            stop_adb_if(original_state=was_adb_running, path=ADB_PATH)

        if is_low_priority():
            set_normal_priority()

        for a_filename, a_list_of_folders in extra_copy_folders_dict_final.items():
            for a_folder in a_list_of_folders:
                basename = os.path.basename(a_filename)
                dest_extra_copy = os.path.join(a_folder, basename)
                copy_file_with_directories(a_filename, dest_extra_copy)
                md_file = f'{a_filename[:-4]}.md'
                html_file = f'{a_filename[:-4]}.html'
                if EXPORT_MD and os.path.exists(md_file):
                    copy_file_with_directories(md_file, f'{dest_extra_copy[:-4]}.md')
                if EXPORT_HTML and os.path.exists(html_file):
                    copy_file_with_directories(html_file, f'{dest_extra_copy[:-4]}.html')

        # Restoring the normal icon (for Windows)
        if changed_icon:
            try:
                if os.name == 'nt':
                    with winshell.shortcut(SHORTCUT_PATH) as link:
                        link.icon_location = (os.path.join(PYSN_DIRECTORY, "sn.ico"), 0)

                elif sys.platform == 'darwin':
                    if os.path.exists(SHORTCUT_PATH):
                        set_bundle_icon('sn.icns', SHORTCUT_PATH)

                elif sys.platform.startswith('linux'):
                    if os.path.exists(SHORTCUT_PATH):
                        shell_script_path = os.path.join(PYSN_DIRECTORY, 'run_pysn_script.sh')
                        icon_path = os.path.join(PYSN_DIRECTORY, 'sn.png')
                        with open(SHORTCUT_PATH, 'w') as desktop_entry:
                            desktop_entry.write(f"""[Desktop Entry]
Version=1.0
Type=Application
Name=Run Python Script
Exec={shell_script_path}
Icon={icon_path}
Terminal=true
""")
                        os.chmod(SHORTCUT_PATH, 0o755)
            except Exception as e:
                print()
                print(f'*** Error trying to restore normal shortcut icon and permissions: {e}')


if __name__ == "__main__":
    main()
