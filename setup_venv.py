import os
import sys
import subprocess
import zipfile
import shutil
import plistlib

INSTALL_DIR = os.path.dirname(os.path.abspath(__file__))


def install_package(package_name):
    try:
        subprocess.check_call([sys.executable, "-m", "pip", "install", package_name])
    except subprocess.CalledProcessError as e:
        print(f"Failed to install {package_name}:", e)
        sys.exit(1)


def create_virtualenv(venv_name):
    try:
        subprocess.check_call([sys.executable, "-m", "venv", venv_name])
    except subprocess.CalledProcessError as e:
        print("Failed to create virtual environment:", e)
        sys.exit(1)


def install_requirements(venv_name):
    if os.name == 'nt':
        pip_executable = os.path.join(venv_name, 'Scripts', 'pip.exe')
    else:
        pip_executable = os.path.join(venv_name, 'bin', 'pip')

    try:
        if os.name == 'nt':
            subprocess.check_call([pip_executable, "install", "-r", "windows_pysn_req.txt"])
        elif sys.platform == 'darwin':
            subprocess.check_call([pip_executable, "install", "-r", "macos_pysn_req.txt"])
        subprocess.check_call([pip_executable, "install", "-r", "requirements_pysn.txt"])

    except subprocess.CalledProcessError as e:
        print("Failed to install requirements:", e)
        sys.exit(1)


def get_activate_command(venv_name):
    if os.name == 'nt':
        return f"{venv_name}\\Scripts\\activate (For Bash: source {venv_name}/Scripts/activate)"
    else:
        return f"source {venv_name}/bin/activate"


def create_shortcut(script_path, venv_name, icon_path):
    script_dir = os.path.dirname(script_path)
    if os.name == 'nt':
        try:
            from win32com.client import Dispatch
        except ImportError as e:
            print(f"Failed to import required modules: {e}")
            print("Current Python executable:", sys.executable)
            print("Current PATH:", os.environ.get('PATH'))
            print("Trying to install modules again...")
            try:
                subprocess.check_call([sys.executable, "-m", "pip", "install", "pywin32==306", "winshell==0.6"])
                from win32com.client import Dispatch
            except Exception as install_error:
                print(f"Failed to install modules: {install_error}")
                sys.exit(1)

        try:
            shortcut_path = os.path.join(script_dir, "PySN.lnk")
            target = os.path.abspath(os.path.join(venv_name, "Scripts", "python.exe"))
            arguments = script_path
            working_dir = script_dir
            icon = icon_path

            shell = Dispatch('WScript.Shell')
            shortcut = shell.CreateShortCut(shortcut_path)
            shortcut.Targetpath = target
            shortcut.Arguments = f'"{arguments}"'
            shortcut.WorkingDirectory = working_dir
            shortcut.IconLocation = icon
            shortcut.save()

            print(f"Shortcut created at {shortcut_path}")

        except Exception as e:
            print(f"Failed to create shortcut: {e}")
            print("Python version:", sys.version)

    elif sys.platform == 'darwin':
        try:
            # Create an application bundle
            app_name = "PySN.app"
            app_path = os.path.join(script_dir, app_name)
            os.makedirs(os.path.join(app_path, "Contents", "MacOS"), exist_ok=True)
            os.makedirs(os.path.join(app_path, "Contents", "Resources"), exist_ok=True)

            # Create the AppleScript
            applescript_path = os.path.join(app_path, "Contents", "MacOS", "PySN")
            with open(applescript_path, 'w') as f:
                f.write(f'''#!/usr/bin/osascript
tell application "Terminal"
    activate
    do script "cd {script_dir}; chmod +x {os.path.join(script_dir, 'platform-tools/adb')}; source {os.path.abspath(venv_name)}/bin/activate; python3 {os.path.abspath(script_path)}; exit"
end tell
''')
            # Set the AppleScript as executable
            os.chmod(applescript_path, 0o755)

            # Create Info.plist
            info_plist = {
                'CFBundleExecutable': 'PySN',
                'CFBundleIconFile': 'icon.icns',
                'CFBundleIdentifier': 'com.example.PySN',
                'CFBundleName': 'PySN',
                'CFBundlePackageType': 'APPL',
                'CFBundleVersion': '1.0',
                'CFBundleShortVersionString': '1.0',
                'NSHighResolutionCapable': True,
            }
            plist_path = os.path.join(app_path, "Contents", "Info.plist")
            with open(plist_path, 'wb') as f:
                plistlib.dump(info_plist, f)

            # Copy the icon
            if icon_path:
                icon_dest = os.path.join(app_path, "Contents", "Resources", "icon.icns")
                if icon_path.endswith('.icns'):
                    subprocess.run(["cp", icon_path, icon_dest], check=True)
                else:
                    subprocess.run(["sips", "-s", "format", "icns", icon_path, "--out", icon_dest], check=True)

            # Set proper permissions for the entire bundle
            for root, dirs, files in os.walk(app_path):
                for dir in dirs:
                    os.chmod(os.path.join(root, dir), 0o755)
                for file in files:
                    os.chmod(os.path.join(root, file), 0o755)

            # Set execute permission for the .app bundle itself
            os.chmod(app_path, 0o755)

            print(f"Application bundle created at {app_path}")
            print("Permissions set: rwxr-xr-x (755) for the .app bundle and all contents")

        except Exception as e:
            print(f"Failed to create application bundle: {e}")

    elif sys.platform.startswith('linux'):
        # Define paths for the shell script and desktop entry
        shell_script_path = os.path.join(script_dir, 'run_pysn_script.sh')
        desktop_entry_path = os.path.join(script_dir, 'PySN.desktop')

        # Create the shell script
        with open(shell_script_path, 'w') as shell_script:
            shell_script.write(
                f"""#!/bin/bash
# Activate the virtual environment
source "{venv_name}/bin/activate"
# Run the Python script
python3 "{script_path}"
# Deactivate the virtual environment
deactivate
                """)

        # Make the shell script executable
        os.chmod(shell_script_path, 0o755)

        # Create the desktop entry file
        with open(desktop_entry_path, 'w') as desktop_entry:
            desktop_entry.write(
                f"""[Desktop Entry]
Version=1.0
Type=Application
Name=Run Python Script
Exec={shell_script_path}
""")
            if icon_path:
                desktop_entry.write(f"Icon={icon_path}\n")
            else:
                desktop_entry.write("Icon=utilities-terminal\n")
            desktop_entry.write("Terminal=true\n")

        # Make the desktop entry file executable (optional)
        os.chmod(desktop_entry_path, 0o755)
        print()
        print(f'Shortcut created successfully!\nShell Script: {shell_script_path}\nDesktop Entry: {desktop_entry_path}')


def setup_platform_tools():
    platform_tools_dir = os.path.join(INSTALL_DIR, 'platform-tools')
    if os.path.exists(platform_tools_dir):
        shutil.rmtree(platform_tools_dir)

    if os.name == 'nt':
        adb_source = os.path.join(INSTALL_DIR, 'platform-tools-latest-windows.zip')
    elif sys.platform == 'darwin':
        adb_source = os.path.join(INSTALL_DIR, 'platform-tools-latest-darwin.zip')
    elif sys.platform.startswith('linux'):
        adb_source = os.path.join(INSTALL_DIR, 'platform-tools-latest-linux.zip')
    else:
        print("Unsupported platform.")
        sys.exit(1)

    if not os.path.exists(adb_source):
        print(f"{adb_source} not found.")
        sys.exit(1)

    try:
        with zipfile.ZipFile(adb_source, 'r') as zip_ref:
            zip_ref.extractall(INSTALL_DIR)
        try:
            os.remove(os.path.join(INSTALL_DIR, 'platform-tools-latest-windows.zip'))
            os.remove(os.path.join(INSTALL_DIR, 'platform-tools-latest-darwin.zip'))
            os.remove(os.path.join(INSTALL_DIR, 'platform-tools-latest-linux.zip'))
            print(f"Platform tools set up at {platform_tools_dir} and {adb_source} removed.")
        except Exception as e:
            print()
            print(f'*** setup_platform_tools: {e}')
            print('Could not remove zip files... Were they already deleted?...Bypassing this step')
        if os.name != 'nt':
            os.chmod(f'{INSTALL_DIR}/platform-tools/adb', 0o755)
    except Exception as e:
        print(f"Failed to set up platform tools: {e}")
        sys.exit(1)


def main():
    venv_name = os.path.join(INSTALL_DIR, "pysnvenv")

    if not os.path.exists('requirements_pysn.txt'):
        print("requirements_pysn.txt not found in the current directory.")
        sys.exit(1)

    if not os.path.exists(venv_name):
        print("Virtual environment not found. Creating new one...")
        create_virtualenv(venv_name)
        install_requirements(venv_name)
        activate_command = get_activate_command(venv_name)
        print()
        print(f">>> Virtual environment setup complete. To activate it, run:\n{activate_command}")
    else:
        print()
        print(f">>> Virtual environment '{venv_name}' already exists. Skipping creation.")
        activate_command = get_activate_command(venv_name)
        print(f">>> To activate the existing virtual environment, run:\n{activate_command}")

    setup_platform_tools()

    # Create shortcut to digest.py
    script_path = os.path.abspath('digest.py')
    icon_path_windows = os.path.abspath('sn.ico')  # For Windows
    icon_path_macos = os.path.abspath('sn.icns')   # For macOS
    icon_path_linux = os.path.abspath('sn.png')   # For Linux

    # Rename install vault folder containing squeletton user settings
    os.rename("SN_vaulti", "SN_vault")

    # Create Windows shortcut
    if os.name == 'nt':
        create_shortcut(script_path, venv_name, icon_path_windows)
    # Create macOS command file
    elif sys.platform == 'darwin':
        create_shortcut(script_path, venv_name, icon_path_macos)
    # Create Linux shortcut
    elif sys.platform.startswith('linux'):
        create_shortcut(script_path, venv_name, icon_path_linux)


if __name__ == "__main__":
    main()
